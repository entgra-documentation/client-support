---
bookCollapseSection: true
weight: 1
---

# Setting up the database


Entgra IoTS provides scripts for installing and configuring MySQL, H2, Oracle, MSSQL and PostgreSQL. Each of these databases supports **stored procedures**, which allow the business logic to be embedded inside the database as an API, providing a powerful mechanism to interact with a relational database. Because these procedures are stored in a pre-compiled format within the database itself, execution speed is very fast. Client programs can be restricted to access a database via stored procedures only, thereby enforcing fine-grained security, preservation of data integrity, and improved productivity.

The default database of the user manager is the H2 database that comes with Entgra IoTS. You can configure it to point to databases by other vendors such as MySQL, Oracle, PostgreSQL, and Microsoft SQL Servers, using the scripts provided by WSO2 for installing and configuring relational databases. The scripts in the `<PRODUCT_HOME>/dbscripts` folder are available for all WSO2 products. They store data related to WSO2 Carbon, on top of which all WSO2 products are built. 





The embedded H2 database is suitable for development. However, for most enterprise testing and production environments, we recommend an industry-standard RDBMS such as MySQL etc.





For more information on working with the databases check out the following subsections:

# Setting up the physical database

The `<IoTS_HOME>/dbscripts` folder contains all the database script files related to configuring the respective databases:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Database</th>
      <th>File path</th>
    </tr>
    <tr>
      <td>API Manager</td>
      <td><code>&lt;IoTS_HOME&gt;/dbscripts/apimgt</code></td>
    </tr>
    <tr>
      <td>Carbon Device Management Framework (CDMF)</td>
      <td><code>&lt;IoTS_HOME&gt;/dbscripts/cdm</code></td>
    </tr>
    <tr>
      <td>Message Broker</td>
      <td><code>&lt;IoTS_HOME&gt;/wso2/broker/dbscripts/mb-store</code></td>
    </tr>
    <tr>
      <td>Data Analytics Server</td>
      <td><code>&lt;IoTS_HOME&gt;/dbscripts/metrics</code></td>
    </tr>
  </tbody>
</table>




## Folder structure



*   `<IOTS_HOME>/dbscripts` folder.
*   The `<IOTS_HOME>/dbscripts/cdm` folder contains the database script files related to the Connected Device Management Framework (CDMF).

    ![image](352820968.png)

    *   The `<IOTS_HOME>/dbscripts/cdm/plugins` folder contains the database scripts for platform specific plugins. 
     By default, you only have scripts for the android and windows device types.

        ![image](352820984.png)Example:  Database scripts for the Android plugin are located in 
        the `<IOTS_HOME>/dbscripts/cdm/plugins/android` directory.  
        ![image](352820990.png)

# Setup Embeded H2

The following sections describe how to set up an embedded H2 database.

## Install H2

Download and install the H2 database engine on your computer.
For instructions on installing DB2 Express-C, see [H2 installation guide.](http://www.h2database.com/html/quickstart.html)

## Setting up drivers

Entgra IoTS currently ships H2 database engine version h2-1.2.140.* and its related H2 database driver. If you want to use a different H2 database driver, take the following steps:

1.  Delete the following H2 database-related JAR file, which is shipped with WSO2 products:  
    `<IoTS_HOME>/wso2/components/plugins/h2_1.3.175.wso2v1.jar`
2.  Find the JAR file of the new H2 database driver (`<H2_HOME>/bin/h2-*.jar`, where `<H2_HOME>` is the H2 installation directory) and copy it to your Entgra product's `<IoTS_HOME>/lib/` directory.



# Setting up MySQL



The following sections describe how to set up a MySQL database:

## Setting up the database and users

1.  Download and install MySQL on your computer. 
  For instructions on installing MySQL on MAC OS, go to [Homebrew](http://brew.sh/).

    `sudo apt-get install mysql-server mysql-client`

2.  Start the MySQL service. If you are using Linux, use the following command:

    `sudo /etc/init.d/mysql start`

3.  Log in to the MySQL client as the root user (or any other user with database creation privileges).

    `mysql -u root -p`

4.  Enter the password when prompted.

   In most systems, there is no default root password. Press the Enter key without typing anything if you have not changed the default root password.

5.  In the MySQL command prompt, create the database using the following command:

    `create database <DATABASE_NAME>; `

    For example: 

    `create database regdb;`

 About using MySQL in different operating systems

    For users of Microsoft Windows, when creating the database in MySQL, it is important to specify the character set as latin1\. Failure to do this may result in an error (error code: 1709) when starting your cluster. This error occurs in certain versions of MySQL (5.6.x) and is related to the UTF-8 encoding. MySQL originally used the latin1 character set by default, which stored characters in a 2-byte sequence. However, in recent versions, MySQL defaults to UTF-8 to be friendlier to international users. Hence, you must use latin1 as the character set as indicated below in the database creation commands to avoid this problem. Note that this may result in issues with non-latin characters (like Hebrew, Japanese, etc.). The database creation command should be as follows:

    `mysql> create database <DATABASE_NAME> character set latin1;`

    For users of other operating systems, the standard database creation commands will suffice. For these operating systems, the database creation command should be as follows:.

    `mysql> create database <DATABASE_NAME>;`

6.  Give authorization of the database to the `regadmin` user as follows:

    `GRANT ALL ON regdb.* TO regadmin@localhost IDENTIFIED BY "regadmin";`

7.  Once you have finalized the permissions, reload all the privileges by executing the following command:

    `FLUSH PRIVILEGES;`

8.  Log out from the MySQL prompt by executing the following command:

    `quit;`

## Setting up the drivers

Download the MySQL Java connector [JAR file](http://dev.mysql.com/downloads/connector/j/5.1.html), and copy it to the <`IoTS_HOME>/lib` directory.


# Setting up Oracle

The following sections describe how to replace the default H2 databases with Oracle:

## Setting up the database and users

Follow the steps below to set up an Oracle database.

1.  Create a new database manually or by using the Oracle database configuration assistant (dbca).

2.  Make the necessary changes in the Oracle `tnsnames.ora` and `listner.ora` files in order to define addresses of the databases for establishing connections to the newly created database.

3.  After configuring the `.ora` files, start the Oracle instance using the following command:

    `$ sudo /etc/init.d/oracle-xe restart`

4.  Connect to Oracle using SQL*Plus as SYSDBA as follows:

    `$ ./$<ORACLE_HOME>/config/scripts/sqlplus.sh sysadm/password as SYSDBA`

5.  Connect to the instance with the username and password using the following command:

    `$ connect`

6.  As SYSDBA, create a database user and grant privileges to the user as shown below:

    
    Create user <USER_NAME> identified by password account unlock;
    grant connect to <USER_NAME>;
    grant create session, create table, create sequence, create trigger to <USER_NAME>;
    alter user <USER_NAME> quota <SPACE_QUOTA_SIZE_IN_MEGABYTES> on '<TABLE_SPACE_NAME>';
    commit;
    

7.  Exit from the SQL*Plus session by executing the `quit` command.

## Setting up the JDBC driver

1.  Copy the Oracle JDBC libraries (for example, <`ORACLE_HOME/jdbc/lib/ojdbc14.jar)` to the <`IoTS_HOME>/lib` directory.
2.  Remove the old database driver from the `<IoTS_HOME>/dropins` directory.





If you get a `**timezone region not found**` error when using the `ojdbc6.jar` with WSO2 servers, set the Java property as follows: `export JAVA_OPTS="-Duser.timezone='+05:30'"`

The value of this property should be the GMT difference of the country. If it is necessary to set this property permanently, define it inside the `wso2server.sh` as a new `JAVA_OPT` property.

# Changing Databases

Entgra products are shipped with an H2 database, which serves as the default Carbon database. You can change this default database to one of the standard databases listed below.


## Changing the embed H2


The following sections describe how to replace the default H2 database with Embedded H2: 


Before you begin



*   Set up the database as explained in [Setting up Embedded H2]({{< param doclink >}}set-up-the-server/set-up-the-database/#setup-embeded-h2).
*   Download the H2 database driver and copy it to Entgra IoT Server by following the steps under [Setting up drivers]({{< param doclink >}}set-up-the-server/set-up-the-database/#setting-up-drivers).





### Setting up configuration files

##### Configuration properties

Following are the datasource configuration properties:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><strong>url</strong></td>
      <td>The URL of the database.</td>
    </tr>
    <tr>
      <td><strong>userName</strong>&nbsp;</td>
      <td>The name of the database user.</td>
    </tr>
    <tr>
      <td><strong>password</strong></td>
      <td>The password of the database user.</td>
    </tr>
    <tr>
      <td><strong>driverClassName</strong></td>
      <td>The class name of the database driver.</td>
    </tr>
    <tr>
      <td><strong>maxActive</strong></td>
      <td>The maximum number of active connections that can be allocated from this pool at the same time, or enter a negative value for no limit.</td>
    </tr>
    <tr>
      <td><strong>maxWait</strong></td>
      <td>The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception. You can enter zero or a negative value to wait indefinitely.</td>
    </tr>
    <tr>
      <td><strong>minIdle</strong></td>
      <td>The minimum number of active connections that can remain idle in the pool, without extra ones being created, or enter zero to create none.</td>
    </tr>
    <tr>
      <td><strong>testOnBorrow</strong></td>
      <td>The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and another attempt will be made to borrow another.&nbsp;</td>
    </tr>
    <tr>
      <td><strong>validationQuery</strong></td>
      <td>The SQL query that will be used to validate connections from this pool before returning them to the caller.</td>
    </tr>
    <tr>
      <td><strong>validationInterval</strong></td>
      <td>
        <p>The indication to avoid excess validation, and only run validation at the most, at this frequency (time in milliseconds). If a connection is due for&nbsp;validation,&nbsp;but has been validated previously within this interval, it will not be validated again.&nbsp;</p>
      </td>
    </tr>
  </tbody>
</table>


For more information on other parameters that can be defined in the datasource `xml` file, see [Tomcat JDBC Connection Pool](http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html#Tomcat_JDBC_Enhanced_Attributes).





##### Configuration files

Follow the steps given below to set up the configuration files:

1.  The default datasource configuration is defined in the `master-datasources.xml` file, located in the <`IoTS_HOME>/conf/datasources/` directory. The database configurations in `registry.xml` and `user-mgt.xml` refer to the `WSO2_CARBON_DB` datasource.

    Edit the `WSO2_CARBON_DB`, `WSO2_MB_STORE_DB` and `WSO2AM_DB` datasources in the `master-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

    **WSO2_Carbon_DB Datasource**

    
    <datasource>
       <name>WSO2_CARBON_DB</name>
       <description>The datasource used for registry and user manager</description>
       <jndiConfig>
          <name>jdbc/WSO2CarbonDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/WSO2CARBON_DB;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <minIdle>5</minIdle>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    

    **WSO2_MB_STORE_DB Datasource**

    
    <datasource>
       <name>WSO2_MB_STORE_DB</name>
       <description>The datasource used for message broker database</description>
       <jndiConfig>
          <name>jdbc/WSO2MBStoreDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/WSO2MB_DB;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>false</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
    

    **WSO2AM_DB Datasource**

    
    <datasource>
       <name>WSO2AM_DB</name>
       <description>The datasource used for API Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2AM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/WSO2AM_DB;DB_CLOSE_ON_EXIT=FALSE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    

2.  Configure the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `analytics-datasources.xml`, located in the  file `<IoTS_HOME>/analytics/repository/conf/datasources `directory.

    Edit the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `analytics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

    **WSO2_ANALYTICS_EVENT_STORE_DB Datasource**

    
    <datasource>
       <name>WSO2_ANALYTICS_EVENT_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/ANALYTICS_EVENT_STORE;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
    

    **WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB Datasource**

    
    <datasource>
       <name>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/ANALYTICS_PROCESSED_DATA_STORE;AUTO_SERVER=TRUE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
    

3.  Configure the the `WSO2_METRICS_DB` datasource in the `metrics-datasources.xml` file, located in the  file `<IoTS_HOME>/conf/datasources `directory. 


    Edit the `WSO2_METRICS_DB` datasources in the `metrics-datasources.xml` file by replacing the `url`, `username`, `password`, and `driverClassName` settings with your custom values and also the other values accordingly.


 **WSO2_METRICS_DB Datasource**

    
    <datasource>
       <name>WSO2_METRICS_DB</name>
       <description>The default datasource used for WSO2 Carbon Metrics</description>
       <jndiConfig>
          <name>jdbc/WSO2MetricsDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/WSO2METRICS_DB;DB_CLOSE_ON_EXIT=FALSE;AUTO_SERVER=TRUE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
    

4.  Configure the the `DM_DS` datasource in the `cdm-datasources.xml` file located in the `<IoTS_HOME>/conf/datasources `directory.  

    Edit the `DM_DS` datasources in the `cdm-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.


    **DM_DS Datasource**

    
    <datasource>
       <name>DM_DS</name>
       <description>The datasource used for CDM</description>
       <jndiConfig>
          <name>jdbc/DM_DS</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:h2:repository/database/WSO2DM_DB;DB_CLOSE_ON_EXIT=FALSE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.h2.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    

5.  Configure all the device plugin datasources by editing the `<PLUGIN_NAME>-datasources.xml` file of the respective plugin by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

    **Example: Android plugin datasource**

    
    <datasource>
        <name>Android_DB</name>
        <description>The datasource used as the Android Device Management database</description>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <definition type="RDBMS">
            <configuration>
                <url>jdbc:h2:repository/database/WSO2MobileAndroid_DB;DB_CLOSE_ON_EXIT=FALSE
                </url>
                <username>wso2carbon</username>
                <password>wso2carbon</password>
                <driverClassName>org.h2.Driver</driverClassName>
                <maxActive>50</maxActive>
                <maxWait>60000</maxWait>
                <testOnBorrow>true</testOnBorrow>
                <validationQuery>SELECT 1</validationQuery>
                <validationInterval>30000</validationInterval>
            </configuration>
        </definition>
    </datasource>
    

### Creating database tables

Create the database tables either [manually by running a script](about:blank#ChangingtoEmbeddedH2-Usingthescript) or [automatically by using a startup parameter](about:blank#ChangingtoEmbeddedH2-Usingastartupparameter) as described below.

#### Using the script

You can create the database tables manually by executing the H2 script in the shell or web console:

<table>
  <tbody>
    <tr>
      <th>Database</th>
      <th>DB Script name and Location</th>
    </tr>
    <tr>
      <td>
        <p><code>WSO2_CARBON_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IoTS_HOME&gt;/dbscripts//h2.sql</code></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><code>WSO2AM_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IoTS_HOME&gt;/dbscripts//apimgt/h2.sql</code></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><code>WSO2_MB_STORE_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IoTS_HOME&gt;/wso2/broker/dbscripts/mb-store/h2.sql</code></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><code>WSO2_ANALYTICS_EVENT_STORE_DB</code></p>
      </td>
      <td>
        <p><br></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><code>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</code></p>
      </td>
      <td>
        <p><br></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><code>WSO2_METRICS_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IoTS_HOME&gt;/dbscripts//metrics/h2.sql</code></p>
      </td>
    </tr>
    <tr>
      <td><code>DM_DS</code></td>
      <td><code>&lt;IoTS_HOME&gt;/dbscripts//cdm/h2.sql</code></td>
    </tr>
    <tr>
      <td>
        <p>Device plugin databases</p>
        <p>Example: <code>Arduino_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IoTS_HOME&gt;/dbscripts//cdm/plugins/&lt;PLUGIN_NAME&gt;/h2.sql</code></p>
        <p>Example:</p>
        <p><code>&lt;IoTS_HOME&gt;/dbscripts//cdm/plugins/android/h2.sql</code></p>
      </td>
    </tr>
  </tbody>
</table>

Follow the steps below to run the H2 script in web console: 


The following steps need to be repeated to create database tables for the various databases.


1.  Navigate to the folder with the respective dbscripts.
2.  Run the `./h2.sh` command to start the web console.
3.  Copy the script text from the SQL file.
4.  Paste it into the console.
5.  Click **Run**.

After setting up all the database tables, start the server by executing one of the following commands:

*   For Linux:

    `sh wso2server.sh`

*   For Windows:

    `wso2server.bat`

#### Using a startup parameter

You can create database tables automatically when starting the product for the first time by using the `-Dsetup` parameter as follows:

*   For Linux:

    `sh wso2server.sh -Dsetup`

*   For Windows:

    `wso2server.bat -Dsetup`

## Changing to MySQL


By default, Entgra IoT Server uses the embedded H2 database as the database for storing user management and registry data. Given below are the steps you need to follow in order to use a MySQL database for this purpose.



Before you begin



*   Set up the database as explained in [Setting up MySQL]({{< param doclink >}}set-up-the-server/set-up-the-database/#setting-up-mysql).
*   Download the MySQL Java connector [JAR file](http://dev.mysql.com/downloads/connector/j/5.1.html), and copy it to the <`IoTS_HOME>/lib` directory.


### Setting up configuration files

The default datasource configurations are defined in the files listed below.

*   `**master-datasources.xml**` 

    This file contains the following default datasource configurations to configure IoTS with the Carbon database and the WSO2 API Manager database.

    Edit the datasources in the <`IOTS_HOME>/conf/datasources/` `master-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values.

  
   <datasource>
       <name>WSO2_CARBON_DB</name>
       <description>The datasource used for registry and user manager</description>
       <jndiConfig>
          <name>jdbc/WSO2CarbonDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2CARBON_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <minIdle>5</minIdle>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    <datasource>
       <name>WSO2AM_DB</name>
       <description>The datasource used for API Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2AM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2AM_DB?zeroDateTimeBehavior=convertToNull</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
     <datasource>
       <name>WSO2_MB_STORE_DB</name>
       <description>The datasource used for message broker database</description>
       <jndiConfig>
          <name>jdbc/WSO2MBStoreDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2MB_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>false</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
   <datasource>
       <name>WSO2APPM_DB</name>
       <description>The datasource used for App Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2APPM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2APPM_DB;DB_CLOSE_ON_EXIT=FALSE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
   <datasource>
       <name>JAGH2</name>
       <description>The datasource used for by the Jaggery Storage Manager</description>
       <jndiConfig>
          <name>jdbc/ES_Storage</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/ES_STORAGE;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
          </configuration>
       </definition>
    </datasource>
   <datasource>
       <name>WSO2_SOCIAL_DB</name>
       <description>The datasource used for Store social database</description>
       <jndiConfig>
          <name>jdbc/WSO2_SOCIAL_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2_SOCIAL_DB;DB_CLOSE_ON_EXIT=FALSE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>

  

*   **`metrics-datasources.xml`**

    

    This file contains the datasource required to enable the JVM metrics.

    Edit the `WSO2_METRICS_DB` datasource in the <`IOTS_HOME>/conf/datasources/` `metrics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
       <name>WSO2_METRICS_DB</name>
       <description>The default datasource used for WSO2 Carbon Metrics</description>
       <jndiConfig>
          <name>jdbc/WSO2MetricsDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2METRICS_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>


*   **`cdm-datasources.XML`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Edit the `DM_DS` datasource in the <`IOTS_HOME>/conf/datasources/` `cdm-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

  <datasource>
       <name>DM_DS</name>
       <description>The datasource used for CDM</description>
       <jndiConfig>
          <name>jdbc/DM_DS</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/WSO2DM_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>


*   **`<PLUGIN_NAME>-datasources.xml`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Example:  
    Edit the `Arduino_DB` datasource in the `arduino-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
        <name>Android_DB</name>
        <description>The datasource used as the Android Device Management database</description>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <definition type="RDBMS">
            <configuration>
                <url>jdbc:mysql://localhost:3306/WSO2MobileAndroid_DB;DB_CLOSE_ON_EXIT=FALSE
                </url>
                <username>wso2carbon</username>
                <password>wso2carbon</password>
                <driverClassName>com.mysql.jdbc.Driver</driverClassName>
                <maxActive>50</maxActive>
                <maxWait>60000</maxWait>
                <testOnBorrow>true</testOnBorrow>
                <validationQuery>SELECT 1</validationQuery>
                <validationInterval>30000</validationInterval>
            </configuration>
        </definition>
    </datasource>

  

*   `**analytics-datasources.xml**`

    

    This file contains the following default datasources used for summarization and to persist stream data. The database tables are created dynamically when running the spark script along with the required tables.  
    Example: Arduino publishes the temperature data that gets stored in this database.  

    Edit the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `<IOTS_HOME>/wso2/analytics/conf/datasources/analytics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassNamesettings` with your custom values and also the other values accordingly.


 <datasource>
       <name>WSO2_ANALYTICS_EVENT_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/ANALYTICS_EVENT_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
   <datasource>
       <name>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:mysql://localhost:3306/ANALYTICS_PROCESSED_DATA_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.mysql.jdbc.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>   

The datasource configuration options are as follows:

*   **url** - The URL of the database.
*   **username**  - The name of the database user.

*   **password** - The password of the database user.
*   **driverClassName**  - The class name of the database driver.
*   **maxActive** - The maximum number of active connections that can be allocated from this pool at the same time, or enter a negative value for no limit.
*   **maxWait** - The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception. You can enter zero or a negative value to wait indefinitely.
*   **minIdle** - The minimum number of active connections that can remain idle in the pool without extra ones being created, or enter zero to create none.
*   **testOnBorrow** -  The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and another attempt will be made to borrow another. 
*   **validationQuery** - The SQL query that will be used to validate connections from this pool before returning them to the caller.
*   **validationInterval** -  The indication to avoid excess validation, and only run validation at the most, at this frequency (time in milliseconds). If a connection is due for validation but has been validated previously within this interval, it will not be validated again.   

    For more information on other parameters that can be defined in the `master-datasources.xml` file, see [Tomcat JDBC Connection Pool](http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html#Tomcat_JDBC_Enhanced_Attributes).
### Creating database tables

Create the following database tables either manually or automatically.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Database</th>
      <th>DB Script name and Location</th>
    </tr>
    <tr>
      <td><code>WSO2_CARBON_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/mysql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2AM_DB</code></td>
      <td>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/apimgt/mysql.sql</code></p>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/identity/mysql.sql</code></p>
      </td>
    </tr>
    <tr>
      <td><code>WSO2_MB_STORE_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/wso2/broker/dbscripts/mb-store/mysql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2_METRICS_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/metrics/mysql.sql</code></td>
    </tr>
    <tr>
      <td><code>DM_DS</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/mysql.sql</code></td>
    </tr>
    <tr>
      <td>
        <p><code>&lt;DEVICE_PLUGIN&gt;_DB</code></p>
        <p>Example:<code> Arduino_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/&lt;DEVICE_PLUGIN&gt;/mysq</code>l.sql</p>
        <p>Example: <code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/android/mysql.sql</code></p>
      </td>
    </tr>
  </tbody>
</table>

##### Using the script

You can create database tables manually by executing the following script:

1.  Run the MySQL script individually for the latter mentioned databases that are provided with the product using the below command (outside the MySQL prompt):

    `mysql -u <USERNAME> -p -D<DATABASE-NAME> '<PATH-TO-MYSQL-DB-SCRIPT>';`

    For example:

    `mysql -u username -p -Dregdb '<IOTS_HOME>/dbscripts/mysql.sql';`

    

    

    Enter the password for each command when prompted.

    

    

2.  Start Entgra IoT Server as follows:  

    *   For Linux:

        `./iot-server.sh`

    *   For Windows:

        `iot-server.bat`

##### Using a startup parameter

You can create database tables automatically when starting the product for the first time by running the following command:





If you are using MySQL 5.7 you need to rename the `mysql5.7.sql` file to `mysql.sql` in the following directories:

*   `<IOTS_HOME>/dbscripts`
*   `<IOTS_HOME>/dbscripts/identity`
*   `<IOTS_HOME>/dbscripts/apimgt`





*   For Linux:

    `./iot-server.sh -Dsetup`

*   For Windows:

    `iot-server.bat -Dsetup`


## Changing to Oracle


By default, Entgra IoTS uses the embedded H2 database as the database for storing user management and registry data. Given below are the steps you need to follow in order to use an Oracle database for this purpose:



Before you begin



*   Set up the database as explained in [Setting up Oracle]({{< param doclink >}}set-up-the-server/set-up-the-database/#setting-up-oracle).
*   Copy the Oracle JDBC libraries to Entgra IoT Server by following the steps under [Setting up the JDBC driver]({{< param doclink >}}set-up-the-server/set-up-the-database/#setting-up-the-jdbc-driver).





### Setting up datasource configurations

A datasource is used to establish the connection to a database. By default datasources are used to connect to the default  H2 database. After setting up the Oracle database to replace the default H2 database, either [change the default configurations of the `WSO2_CARBON_DB` datasource](about:blank#ChangingtoOracle-ChangingthedefaultWSO2_CARBON_DBdatasource)  or [configure a new datasource]({{< param doclink >}}product-administration-guide/set-up-the-server/set-up-the-database/#changing-to-oracle) to point it to the new database as explained below.

#### Changing the default WSO2_CARBON_DB datasource

Follow the instructions given below to change the type of the default `WSO2_CARBON_DB` datasource. The default datasource configurations are defined in the files listed below. 

*   `**master-datasources.xml**`

    

    This file contains the following default datasource configurations to configure IoTS with the Carbon database and the WSO2 API Manager database.

    Edit the `WSO2_CARBON_DB`, `WSO2_MB_STORE_DB` and `WSO2AM_DB` datasources in the `<IoTS_HOME>/conf/datasources/master-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.
 <datasource>
       <name>WSO2_CARBON_DB</name>
       <description>The datasource used for registry and user manager</description>
       <jndiConfig>
          <name>jdbc/WSO2CarbonDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <minIdle>5</minIdle>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
  <datasource>
       <name>WSO2_MB_STORE_DB</name>
       <description>The datasource used for message broker database</description>
       <jndiConfig>
          <name>jdbc/WSO2MBStoreDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>false</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
 <datasource>
       <name>WSO2AM_DB</name>
       <description>The datasource used for API Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2AM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    

*   **`analytics-datasources.xml`**

    

    This file contains the following default datasources used for summarization and to persist stream data. The database tables are created dynamically when running the spark script along with the required tables.  
    Example: Arduino publishes the temperature data that gets stored in this database.

    Edit the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `<IoTS_HOME>/analytics/repository/conf/datasources/analytics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.
 <datasource>
       <name>WSO2_ANALYTICS_EVENT_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
  <datasource>
       <name>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>

    

*   **`metrics-datasources.xml`**

    

    This file contains the datasource required to enable the JVM metrics.

    Edit the `WSO2_METRICS_DB` datasource in the `<IoTS_HOME>/conf/datasources/metrics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
       <name>WSO2_METRICS_DB</name>
       <description>The default datasource used for WSO2 Carbon Metrics</description>
       <jndiConfig>
          <name>jdbc/WSO2MetricsDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>

*   **`cdm-datasources.xml`**

    

    This file contains the following default datasource configurations to configure Entgra IoTS with the Connected Device Management Framework and for device management.

    Edit the datasources in the `<IoTS_HOME>/conf/datasources/cdm-datasources.xml`  file by replacing the `url`, `username`,  `password`  and `driverClassName` settings with your custom values and also the other values accordingly.
  <datasource>
       <name>DM_DS</name>
       <description>The datasource used for CDM</description>
       <jndiConfig>
          <name>jdbc/DM_DS</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
 

*   **`<PLUGIN_NAME>-datasource.xml`**

    

    Each device type will have its own datasource so that all the data required for the device type can be stored in it. Therefore, when [writing a new device plugin]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/) make sure to create your own datasource too.


    For example, let’s take Android plugin.
 <datasource>
        <name>Android_DB</name>
        <description>The datasource used as the Android Device Management database</description>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <definition type="RDBMS">
            <configuration>
                <url>jdbc:oracle:thin:@localhost:1521/orcl</url>
                <username>wso2carbon</username>
                <password>wso2carbon</password>
                <driverClassName>oracle.jdbc.driver.OracleDriver</driverClassName>
                <maxActive>50</maxActive>
                <maxWait>60000</maxWait>
                <testOnBorrow>true</testOnBorrow>
                <validationQuery>SELECT 1</validationQuery>
                <validationInterval>30000</validationInterval>
            </configuration>
        </definition>
    </datasource>

The elements in the above configuration are described below:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Element</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><strong>url</strong></td>
      <td>The URL of the database. The default port for a DB2 instance is 50000.</td>
    </tr>
    <tr>
      <td><strong>username</strong> and <strong>password</strong></td>
      <td>The name and password of the database user</td>
    </tr>
    <tr>
      <td><strong>driverClassName</strong></td>
      <td>The class name of the database driver</td>
    </tr>
    <tr>
      <td><strong>maxActive</strong></td>
      <td>The maximum number of active connections that can be allocated &nbsp;at the same time&nbsp;from this pool. Enter any negative value to denote an unlimited number of active connections.</td>
    </tr>
    <tr>
      <td><strong>maxWait</strong></td>
      <td>The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception. You can enter zero or a negative value to wait indefinitely.</td>
    </tr>
    <tr>
      <td><strong>minIdle</strong></td>
      <td>The minimum number of active connections that can remain idle in the pool without extra ones being created, or enter zero to create none.</td>
    </tr>
    <tr>
      <td>
        <p><strong>testOnBorrow</strong></p>
      </td>
      <td>The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and another attempt will be made to borrow another.</td>
    </tr>
    <tr>
      <td><strong>validationQuery</strong></td>
      <td>The SQL query that will be used to validate connections from this pool before returning them to the caller.</td>
    </tr>
    <tr>
      <td><strong>validationInterval</strong></td>
      <td>The indication to avoid excess validation, and only run validation at the most, at this frequency (time in milliseconds). If a connection is due for validation but has been validated previously within this interval, it will not be validated again.</td>
    </tr>
  </tbody>
</table>





*   The default port for Oracle is 1521.
*   For more information on other parameters that can be defined in the < `IoTS_HOME>/conf/datasources/``master-datasources.xml` file, see  [Tomcat JDBC Connection Pool](http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html#Tomcat_JDBC_Enhanced_Attributes) .





#### Configuring new datasources to manage registry or user management data

Follow the steps given below to configure the new datasources to point to the new databases you create to manage registry and/or user management data separately:

1.  Add a new datasource with similar configurations as the `WSO2_CARBON_DB` datasource above to the <`IoTS_HOME>/conf/datasources/` `master-datasources.xml` file. Change its elements with your custom values. For instructions, see [Setting up datasource configurations.](about:blank#ChangingtoOracle-Settingupdatasourceconfigurations)
2.  If you are setting up a separate database to store registry-related data, update the following configurations in the <`IoTS_HOME>/conf/``registry.xml `file.

    
    <dbConfig name="wso2registry">
    	<dataSource>jdbc/MY_DATASOURCE_NAME</dataSource>
    </dbConfig>
    

3.  If you are setting up a separate database to store user management data, update the following configurations in the <`IoTS_HOME>/conf/``user-mgt.xml` file.

    
    <Configuration>
    	<Property name="dataSource">jdbc/MY_DATASOURCE_NAME</Property>
    </Configuration>
    

### Creating the database tables

To create the database tables, connect to the database that you created earlier and run the following scripts in SQL*Plus:

1.  To create tables in the registry and user manager database (`WSO2CARBON_DB`), use the below script:

    `SQL> @$<IoTS_HOME>/dbscripts//oracle.sql`

2.  Restart the server.

    

    

    You can create database tables automatically **when starting Entgra IoTS for the first time** by using the `-Dsetup` parameter as follows:

    *   For Windows: `<IoTS_HOME>/core/bin/wso2server.bat -Dsetup`

    *   For Linux: `<IoTS_HOME>/core/bin/wso2server.sh -Dsetup`

    

    
## Changing to PostgreSQL



By default, Entgra IoT Server uses the embedded H2 database as the database for storing user management and registry data. Given below are the steps you need to follow in order to use a PostgreSQL database for this purpose.



Before you begin



*   Set up the database as explained in [Setting up PostgreSQL](/doc/en/lb2/Setting-up-PostgreSQL.html).
*   Download the [PostgreSQL JDBC4 driver](http://jdbc.postgresql.org/download.html) and copy it to your WSO2 product's <`IOTS_HOME>/lib` directory. 





### Setting up configuration files

The default datasource configurations are defined in the files listed below.

*   `**master-datasources.xml**`

    

    This file contains the following default datasource configurations to configure IOTS with the Carbon database and the WSO2 API Manager database.

    Edit the `WSO2_CARBON_DB` `and` `WSO2AM_DB` datasources in the <`IOTS_HOME>/conf/datasources/` `master-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values.
 <datasource>
       <name>WSO2_CARBON_DB</name>
       <description>The datasource used for registry and user manager</description>
       <jndiConfig>
          <name>jdbc/WSO2CarbonDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/WSO2CARBON_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <minIdle>5</minIdle>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
    		 <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
 <datasource>
       <name>WSO2AM_DB</name>
       <description>The datasource used for API Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2AM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/WSO2AM_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
    		 <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
 <datasource>
       <name>WSO2_MB_STORE_DB</name>
       <description>The datasource used for message broker database</description>
       <jndiConfig>
          <name>jdbc/WSO2MBStoreDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/WSO2MB_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>false</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>
  

*   **`metrics-datasources.xml`**

    

    This file contains the datasource required to enable the JVM metrics.

    Edit the `WSO2_METRICS_DB` datasource in the <`IOTS_HOME>/conf/datasources/` `metrics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.
 <datasource>
       <name>WSO2_METRICS_DB</name>
       <description>The default datasource used for WSO2 Carbon Metrics</description>
       <jndiConfig>
          <name>jdbc/WSO2MetricsDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/WSO2METRICS_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource> 

*   **`cdm-datasources.XML`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Edit the `DM_DS` datasource in the <`IOTS_HOME>/conf/datasources/` `cdm-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
       <name>DM_DS</name>
       <description>The datasource used for CDM</description>
       <jndiConfig>
          <name>jdbc/DM_DS</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/WSO2DM_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
    


*   **`<PLUGIN_NAME>-datasources.xml`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Example: Edit the `Arduino_DB` datasource in the `arduino-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
        <name>Android_DB</name>
        <description>The datasource used as the Android Device Management database</description>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <definition type="RDBMS">
            <configuration>
                <url>jdbc:postgresql://localhost:5433/WSO2MobileAndroid_DB;DB_CLOSE_ON_EXIT=FALSE
                </url>
                <username>wso2carbon</username>
                <password>wso2carbon</password>
                <driverClassName>org.postgresql.Driver</driverClassName>
                <maxActive>50</maxActive>
                <maxWait>60000</maxWait>
                <testOnBorrow>true</testOnBorrow>
                <validationQuery>SELECT 1</validationQuery>
                <validationInterval>30000</validationInterval>
            </configuration>
        </definition>
    </datasource>


*   `**analytics-datasources.xml**` 

    

    This file contains the following default datasources used for summarization and to persist stream data. The database tables are created dynamically when running the spark script along with the required tables.  
    Example: Arduino publishes the temperature data that gets stored in this database.

    Edit the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `<IOTS_HOME>/wso2/analytics/conf/datasources/analytics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassNamesettings` with your custom values and also the other values accordingly.
  <datasource>
       <name>WSO2_ANALYTICS_EVENT_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/ANALYTICS_EVENT_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
 <datasource>
       <name>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:postgresql://localhost:5433/ANALYTICS_PROCESSED_DATA_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>org.postgresql.Driver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
    

The datasource configuration options are as follows:

*   **url** - The URL of the database.
*   **username**  - The name of the database user.

*   **password** - The password of the database user.
*   **driverClassName**  - The class name of the database driver.
*   **maxActive** - The maximum number of active connections that can be allocated from this pool at the same time, or enter a negative value for no limit.
*   **maxWait** - The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception. You can enter zero or a negative value to wait indefinitely.
*   **minIdle** - The minimum number of active connections that can remain idle in the pool without extra ones being created, or enter zero to create none.
*   **testOnBorrow** -  The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and another attempt will be made to borrow another. 
*   **validationQuery** - The SQL query that will be used to validate connections from this pool before returning them to the caller.
*   **validationInterval** -  The indication to avoid excess validation, and only run validation at the most, at this frequency (time in milliseconds). If a connection is due for validation but has been validated previously within this interval, it will not be validated again. 

    

    

    For more information on other parameters that can be defined in the `master-datasources.xml` file, see [Tomcat JDBC Connection Pool](http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html#Tomcat_JDBC_Enhanced_Attributes).

    

    

### Creating database tables

Create the following database tables either manually or automatically.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Database</th>
      <th>DB Script name and Location</th>
    </tr>
    <tr>
      <td><code>WSO2_CARBON_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/postgresql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2AM_DB</code></td>
      <td>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/apimgt/postgresql.sql</code></p>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/identity/postgresql.sql</code></p>
      </td>
    </tr>
    <tr>
      <td><code>WSO2_MB_STORE_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/wso2/broker/dbscripts/mb-store/postgresql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2_METRICS_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/metrics/Postgresql.sql</code></td>
    </tr>
    <tr>
      <td><code>DM_DS</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/postgresql.sql</code></td>
    </tr>
    <tr>
      <td>
        <p><code>&lt;DEVICE_PLUGIN&gt;_DB</code></p>
        <p>Example:<code> Arduino_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/&lt;DEVICE_PLUGIN&gt;/postgresql.sql</code></p>
        <p>Example: <code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/android/postgresql.sql</code></p>
      </td>
    </tr>
  </tbody>
</table>

##### Using the script

You can create database tables manually by executing the following script:

1.  Run the PostgreSQL script individually for the latter mentioned databases that are provided with the product using the below command (outside the PostgreSQL prompt):

    `psql -U <USERNAME> -d <DATABASE-NAME> -a -f '<PATH-TO-PostgreSQL-DB-SCRIPT>'`

    Example:

    `psql -U username -d WSO2_CARBON_DB -a -f <IOTS_HOME>/dbscripts/postgresql.sql`

    

    

    Enter the password for each command when prompted.

    

    

2.  Start the Entgra IoT Server as follows:  

    *   For Linux:

        `./iot-server.sh`

    *   For Windows:

        `iot-server.bat`

##### Using a startup parameter

You can create database tables automatically when starting the product for the first time by running the following command:

*   For Linux:

    `./iot-server.sh -Dsetup`

*   For Windows:

    `iot-server.bat -Dsetup`



## Changing to Microsoft SQL



By default, Entgra IoT Server uses the embedded H2 database as the database for storing user management and registry data. Given below are the steps you need to follow in order to use a Microsoft SQL (MSSQL) database for this purpose.



Before you begin



*   Set up the database as explained in [Setting up Microsoft SQL](/doc/en/lb2/Setting-up-Microsoft-SQL.html).
*   Download the sqljdbc4 Microsoft SQL JDBC driver and copy it to Entgra IoT Server by following the steps under [Setting up the JDBC driver]({{< param doclink >}}set-up-the-server/set-up-the-database/#setting-up-the-jdbc-driver).





### Setting up configuration files

The default datasource configurations are defined in the files listed below.

*   `**master-datasources.xml**` 

    

    This file contains the following default datasource configurations to configure IOTS with the Carbon database and the WSO2 API Manager database.

    Edit the `WSO2_CARBON_DB` `and` `WSO2AM_DB` datasources in the <`IOTS_HOME>/conf/datasources/` `master-datasources.xml` file by replacing the `url`, `username`, `password`, and `driverClassName` settings with your custom values.

  <datasource>
       <name>WSO2_CARBON_DB</name>
       <description>The datasource used for registry and user manager</description>
       <jndiConfig>
          <name>jdbc/WSO2CarbonDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=WSO2CARBON_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <minIdle>5</minIdle>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
     <datasource>
       <name>WSO2AM_DB</name>
       <description>The datasource used for API Manager database</description>
       <jndiConfig>
          <name>jdbc/WSO2AM_DB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=WSO2AM_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>
<datasource>
       <name>WSO2_MB_STORE_DB</name>
       <description>The datasource used for message broker database</description>
       <jndiConfig>
          <name>jdbc/WSO2MBStoreDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=WSO2MB_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>false</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>


*   **`metrics-datasources.xml`**

    

    This file contains the datasource required to enable the JVM metrics.

    Edit the `WSO2_METRICS_DB` datasource in the <`IOTS_HOME>/conf/datasources/` `metrics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.
<datasource>
       <name>WSO2_METRICS_DB</name>
       <description>The default datasource used for WSO2 Carbon Metrics</description>
       <jndiConfig>
          <name>jdbc/WSO2MetricsDB</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=/WSO2METRICS_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
             <defaultAutoCommit>true</defaultAutoCommit>
          </configuration>
       </definition>
    </datasource>

*   **`cdm-datasources.XML`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Edit the `DM_DS` datasource in the <`IOTS_HOME>/conf/datasources/` `cdm-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.

 <datasource>
       <name>DM_DS</name>
       <description>The datasource used for CDM</description>
       <jndiConfig>
          <name>jdbc/DM_DS</name>
       </jndiConfig>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=WSO2DM_DB</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <testOnBorrow>true</testOnBorrow>
             <validationQuery>SELECT 1</validationQuery>
             <validationInterval>30000</validationInterval>
          </configuration>
       </definition>
    </datasource>

*   **`<PLUGIN_NAME>-datasources.xml`**

    

    This file contains the following default datasource configurations to configure Entgra IoT Server with the Connected Device Management Framework and for device management.

    Example: Edit the `Arduino_DB` datasource in the `arduino-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassName` settings with your custom values and also the other values accordingly.
  <datasource>
        <name>Android_DB</name>
        <description>The datasource used as the Android Device Management database</description>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <definition type="RDBMS">
            <configuration>
                <url>jdbc:sqlserver://<IP>:1433;databaseName=WSO2MobileAndroid_DB;DB_CLOSE_ON_EXIT=FALSE
                </url>
                <username>wso2carbon</username>
                <password>wso2carbon</password>
                <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
                <maxActive>50</maxActive>
                <maxWait>60000</maxWait>
                <testOnBorrow>true</testOnBorrow>
                <validationQuery>SELECT 1</validationQuery>
                <validationInterval>30000</validationInterval>
            </configuration>
        </definition>
    </datasource>


*   `**analytics-datasources.xml**`

    

    This file contains the following default datasources used for summarization and to persist stream data. The database tables are created dynamically when running the spark script along with the required tables.  
    Example: Arduino publishes the temperature data that gets stored in this database.  

    Edit the `WSO2_ANALYTICS_EVENT_STORE_DB` and `WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB` datasources in the `<IOTS_HOME>/wso2/analytics/conf/datasources/analytics-datasources.xml` file by replacing the `url`, `username`, `password` and `driverClassNamesettings` with your custom values and also the other values accordingly.

 <datasource>
       <name>WSO2_ANALYTICS_EVENT_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=ANALYTICS_EVENT_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
 <datasource>
       <name>WSO2_ANALYTICS_PROCESSED_DATA_STORE_DB</name>
       <description>The datasource used for analytics record store</description>
       <definition type="RDBMS">
          <configuration>
             <url>jdbc:sqlserver://<IP>:1433;databaseName=ANALYTICS_PROCESSED_DATA_STORE</url>
             <username>wso2carbon</username>
             <password>wso2carbon</password>
             <driverClassName>com.microsoft.sqlserver.jdbc.SQLServerDriver</driverClassName>
             <maxActive>50</maxActive>
             <maxWait>60000</maxWait>
             <validationQuery>SELECT 1</validationQuery>
             <defaultAutoCommit>false</defaultAutoCommit>
             <initialSize>0</initialSize>
             <testWhileIdle>true</testWhileIdle>
             <minEvictableIdleTimeMillis>4000</minEvictableIdleTimeMillis>
          </configuration>
       </definition>
    </datasource>
 

The datasource configuration options are as follows:

*   **url** - The URL of the database.
*   **username**  - The name of the database user.

*   **password** - The password of the database user.
*   **driverClassName**  - The class name of the database driver.
*   **maxActive** - The maximum number of active connections that can be allocated from this pool at the same time, or enter a negative value for no limit.
*   **maxWait** - The maximum number of milliseconds that the pool will wait (when there are no available connections) for a connection to be returned before throwing an exception. You can enter zero or a negative value to wait indefinitely.
*   **minIdle** - The minimum number of active connections that can remain idle in the pool without extra ones being created, or enter zero to create none.
*   **testOnBorrow** -  The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, it will be dropped from the pool, and another attempt will be made to borrow another. 
*   **validationQuery** - The SQL query that will be used to validate connections from this pool before returning them to the caller.
*   **validationInterval** -  The indication to avoid excess validation, and only run validation at the most, at this frequency (time in milliseconds). If a connection is due for validation but has been validated previously within this interval, it will not be validated again. 
  

    For more information on other parameters that can be defined in the `master-datasources.xml` file, see [Tomcat JDBC Connection Pool](http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html#Tomcat_JDBC_Enhanced_Attributes).
  

### Creating database tables

Create the following database tables either manually or automatically.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Database</th>
      <th>DB Script name and Location</th>
    </tr>
    <tr>
      <td><code>WSO2_CARBON_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/mssql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2AM_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/apimgt/mssql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2_MB_STORE_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/wso2/broker/dbscripts/mb-store/mssql.sql</code></td>
    </tr>
    <tr>
      <td><code>WSO2_METRICS_DB</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/metrics/mssql.sql</code></td>
    </tr>
    <tr>
      <td><code>DM_DS</code></td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/myssql.sql</code></td>
    </tr>
    <tr>
      <td>
        <p><code>&lt;DEVICE_PLUGIN&gt;_DB</code></p>
        <p>Example:<code> Arduino_DB</code></p>
      </td>
      <td>
        <p><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/&lt;DEVICE_PLUGIN&gt;/mssq</code>l.sql</p>
        <p>Example: <code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/android/mssql.sql</code></p>
      </td>
    </tr>
  </tbody>
</table>

##### Using the script

You can create database tables manually by executing the following script:

1.  Run the MySQL script individually for the latter mentioned databases that are provided with the product using the below command (outside the MySQL prompt):

    `mysql -u <USERNAME> -p -D<DATABASE-NAME> '<PATH-TO-MSSQL-DB-SCRIPT>';`

    For example:

    `mysql -u username -p -Dregdb '<IOTS_HOME>/dbscripts/mssql.sql';`

    

    

    Enter the password for each command when prompted.

    

    

2.  Start Entgra IoT Server as follows:  

    *   For Linux:

        `./iot-server.sh`

    *   For Windows:

        `iot-server.bat`

##### Using a startup parameter

You can create database tables automatically when starting the product for the first time by running the following command:

*   For Linux:

    `./iot-server.sh -Dsetup`

*   For Windows:

    `iot-server.bat -Dsetup`
