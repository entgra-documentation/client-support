# Understanding the Device Management Framework

The Entgra Connected Device Management Framework (CDMF) is a framework that provides extensions to plug device type implementations. This framework can primarily be used to manage device type plugins. Entgra CDMF has several exposed APIs and supports operation, policy, and configuration management. It also has inbuilt support to execute operations and fetch the status of operations. Furthermore, this framework also provides the capability of extending its existing functionality by implementing interfaces.

## Creating a device type

Before creating a device type, let’s dive into a few basic concepts of a device type in the Entgra Connected Device Management Framework (CDMF). Each device type in Entgra CDMF consists of 3 main sections as shown below:

*   **[Device plugin]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-the-device-types)**
    Device plugin is a device type extension from CDMF.

*   **[Device APIs]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-apis)**
    Device APIs is a REST API that is secured with WSO2 API Manager. 

*   **[UI Extensions](/doc/en/lb2/Writing-UI-Extensions.html)**  
    UI Extension can be used to override existing default user interfaces. This extension follows the Unified UI Framework.
