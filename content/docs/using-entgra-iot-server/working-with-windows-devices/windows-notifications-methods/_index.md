# Windows Notification Methods
Entgra IoT Sever (Entgra IoTS) only uses the Local notification method for devices that use the Windows mobile OS.

## Local

The local notification method only works with the client side polling mechanism. Therefore the device will connect as scheduled and defined in the bootstrap message.The Entgra IoTS retry interval for client renewal, and the regular client polling schedule for device management needs to be configured via the device management session.

The wake-up command triggers periodically based on the notifier frequency. When the wake-up command is triggered the MDM client communicates with the Entgra IoTS server and receives the list of pending operations that need to be executed on a respective device.





The time period for the wake-up command to automatically trigger the Entgra IoTS client needs to be specified as the notifier frequency, and it needs to be specified in minutes when configuring the Windows tenant-based settings (the default notifier frequency is 8 minutes).





![image](352824677.png)

## Setting the notification type

### Local

If you wish to enable the Local notification method, you need to select Local as the notification type and set the notifier frequency to a value greater than zero.
