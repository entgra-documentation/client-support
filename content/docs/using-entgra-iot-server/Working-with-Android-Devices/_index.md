---
bookCollapseSection: true
weight: 7
---
# Working with Android Devices

The following sections describe the configurations that the system administrator needs to carry out in order to complete the Android device registration on Entgra IoT Server.

## Android Platform Configurations


Multiple tenants can use Entgra IoTS; while, maintaining tenant-based isolation. The Android configurations enable the tenants to customize the Android settings based on their own requirements.

Follow the steps given below to configure the Android platform:

1.  [Sign in to the Entgra device management console]({{< param generaldoclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    ![image](352822951.png)
2.  Click **CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS** and select Android Configurations.
3.  Enter the following:
    *   **Notifier Type** - The notifier type determines the way in which the notification will take place. The available options are as follows:
        *   **Local Polling** - The device will contact the Entgra IoT server periodically. 
        *   **Firebase Cloud Messaging (FCM)** - FCM will send a notification to the device when there are pending operations available for a specific device. If FCM has been selected as the notifier type, [configure Entgra IoTS with Firebase Cloud Messaging (FCM)]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#configuring-android-with-firebase-cloud-messaging).

            

            

            For more information on the notifier type and the notifier frequency, see [Android Notification Methods]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#android-notification-methods).

            

            

    *   If **Local Polling** was selected as the notifier type, provide the **Notifier Frequency** (in seconds) - This is the interval after which the wake-up command will be automatically triggered by the Android Agent.

        

        Note

        

        From Android 4.4 (KitKat) onwards, the  OS **does not allow** applications to trigger wake-up commands when the trigger period is set to less than a minute. Therefore, make sure to set the notifier frequency to 60 seconds or more.  

        

        

        ![image](352822956.png)

    *   If **Firebase Cloud Messaging (FCM)** was selected as the notifier type, enter the [Firebase Cloud Token](http://localhost:1313/v3.7.0/docs/using-entgra-iot-server/Working-with-Android-Devices/#configuring-android-with-firebase-cloud-messaging) you generated as the **API Key**.
        ![image](352822945.png)
    *   **End User License Agreement (EULA)** - Provide the license agreement that a user must adhere to when enrolling an Android device with Entgra IoTS.
4.  Click **SAVE**.





If the Administrator changes the notifier type (from **FCM** to **Local Polling** or from **Local Polling** to **FCM**) the existing devices should re-register with Entgra IoTS for the change on the notifier type to be applied to the devices.

In the situation where the notifier type changes from **Local Polling** to **FCM**, even if the existing devices are not re-registered they are able to contact the server via local notifications. But in situations where the notifier type is changed from **FCM** to **Local Polling,** you must re-register the existing devices as they will not be able to contact the server after the change.


## Android Notification Methods

Entgra IoT Server uses the following notification methods for devices that use the Android mobile OS:

*   [FCM]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#fcm)
*   [Local]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#local)

#### FCM

The Firebase Cloud Messaging (FCM) notification method uses the FCM server to wake-up the Android device. If the notification type is set to FCM (the Android Agent within the Android device will trigger a wake-up command), when there are pending operations that need to be enforced on a device, Entgra IoT server will send a push notification to the FCM server and the FCM server will in-turn send the message to the respective Android Agent that is on the Android device. When the Android Agent receives the push notification, it will communicate with Entgra IoT server and receive the list of pending operations that need to be enforced on the Android device.

![](352822974.png)

#### Local

The local notification method will not use Firebase Cloud Messaging (FCM), but it will use its own local push notification service. The Android Agent within the Android device will trigger a wake-up command every one minute. The wake-up command will be triggered periodically based on the notifier frequency, which determines the time period after which the wake-up command will be automatically triggered by the Android Agent. The notifier frequency needs to be specified in milliseconds (ms) when configuring Android tenant-based settings. The default notifier frequency is 60000ms, which translates to one minute.



Note



From Android 4.4 (KitKat) onwards the  OS does not allow applications to trigger wake-up commands when the trigger period is set to less than a minute. Therefore, make sure to set the notifier frequency to 60000ms or more.  





When the wake-up command is triggered the Android Agent will communicate with Entgra IoT server and receive the list of pending operations that need to be executed on the respective device.

![]({{site.baseurl}}/assets/images/352822985.jpg)

#### Setting the notification type

The following variations can be used when setting the notification type:

*   [FCM]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#fcm-1)
*   [Local]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#local-1)

##### FCM

If you wish to enable the FCM notification method, you need to select FCM as the notification type, and set the FCM API key and sender ID.

##### Local

If you wish to enable the Local notification method, you need to select Local as the notification type and set the notifier frequency to a value greater or equal to 60000ms.





For more information on setting the Android configurations, see the [Android Platform Configurations]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#android-platform-configurations).


## Configuring Android with Firebase Cloud Messaging

Follow the instructions given below to configure Entgra IoT Server Firebase Cloud Messaging (FCM):



Prerequisites



*   Clone the `cdmf-agent-android` GIT repository. This is be referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout this document.

    `git clone https://github.com/wso2/cdmf-agent-android/tree/release-3.1.27`

    

    

    Make sure that the Android agent release version matches the IoT Server release version. [Click here for more information]({{< param doclink >}}entgra-iot-server-agent-compatibility.html).


    

    

*   Download and install Android Studio. For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).





1.  Go to the [Firebase API Console](https://console.firebase.google.com/).
2.  Click **CREATE NEW PROJECT**.  
    ![image](352823040.png)
3.  Provide a preferred name for the project (example: WSO2-FCM), select the **country/region**, and click **CREATE PROJECT**.

    ![image](352823028.png)  
    The new project is created in few seconds, and you are navigated to the overview page of the project.

4.  Click **Add Firebase to your Android App** on the overview page of the project.  
    ![image](352823022.png)

5.  Provide the package name of the Entgra Android agent, that is `org.wso2.iot.agent` and click **REGISTER APP**.  
    ![image](352823034.png)

6.  Click **Download google-service.json** to download the configurations.

    ![image](352823057.png)

7.  Replace the `<ANDROID_AGENT_SOURCE_CODE>/client/client/google-services.json` file with the **`google-services.json` **file you just downloaded.
8.  Click **CONTINUE > FINISH** to finish the process and create the application.   
    You are navigated to the overview of the created application.  
    ![image](352823063.png)
9.  Scroll up the page and go to the **CLOUD MESSAGING** tab. Note down the Firebase Cloud messaging token. You need to provide this token as the server key when [configuring the Android platform](about:blank#).  
    ![image](352823051.png)

10.  Open `<ANDROID_AGENT_SOURCE_CODE>` via Android Studio, clean the project, and [build the project]({{< param doclink >}}product-administration/customizing-entgra-iot-server.html#creating-a-new-apk).

    

    

    The Entgra Android agent can't be built via the usual Android developer SDK, as it requires access to [developer restricted APIs](https://docs.wso2.com/display/EMM220/Integrating+the+Android+System+Service+Application#IntegratingtheAndroidSystemServiceApplication-Operationssupportedviathesystemserviceapplication). Therefore, you need to replace the existing `android.jar` file that is in the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs.   

    For more information on how to build the project, see [creating a new APK file]({{< param doclink >}}product-administration/customizing-entgra-iot-server.html#creating-a-new-apk).

    

    

11.  Rename the created `.apk` file to `android-agent.apk`.
12.  Copy the renamed file and replace the existing `android-agent.apk` file that is in the `<IoT_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.type-view/public/assets` directory.
13.  Open the `<IoT_HOME>/repository/deployment/server/devicetypes/android.xml` file and uncomment the configuration given below to enable communication via FCM.

    <PushNotificationProviderConfig type="FCM" isScheduled="false">
    </PushNotificationProviderConfig>

    

    

    The `isScheduled` element used to enable the scheduler task that sends push notifications. The task sends the push notifications in batches. So this reduces the sudden request burst when many devices try to access the server after receiving the push notification.

    

    

14.  Restart the Entgra IoT Server in order to apply the changes.


## Configuring Android for HTTPS Communication


You are not required to perform any additional steps to configure the Android server as it is preconfigured in Entgra IoTS. This section provides information on how you can make the Android agent production ready.



Prerequisites



*   Download [Android Studio 1.2.3 or later](https://developer.android.com/sdk/index.html).
*   Entgra IoTS supports devices on Android version 4.2.x to 5.0 (Android Jelly Beans to Lollipop).
*   If you are in a **production environment**, make sure to have the following ports open:
    *   The ports to open are 5228, 5229 and 5230\. Firebase Cloud Messaging (FCM) typically only uses 5228, but it sometimes uses 5229 and 5230.  
        FCM does not provide specific IPs, so it is recommended to allow the firewall to accept outgoing connections to all IP addresses contained in the IP blocks listed in Google's ASN of 15169. 
    *   10397 - Thrift client and server ports
    *   8280, 8243 - NIO/PT transport ports





Follow the instructions given below to configure and build the Android client application:





Support for the Android Developer Tools (ADT) in Eclipse is ending. Therefore, you should migrate the app development projects to Android Studio. For more information on transitioning to Android Studio, see [Migrating to Android Studio](http://developer.android.com/sdk/installing/migrate.html).





1.  Get a GIT clone of the Android Agent application from [GitHub](https://github.com/wso2/cdmf-agent-android).

2.  Update the Android Software Development Kit with the following:

    *   SDK Versions 23 and 16. 

    *   Build Tools Version 22.0.1.

    *   Install or update Android Support Library.

    *   Install or update Google Play Services.

    *   Install Google USB Driver, if you are on a Windows platform.

3.  By default, the Android agent communicates with the server through HTTP. For production, this
 needs to be changed to https, by configuring the following parameters in the Java class named `Constants.java,` which is in the `org.wso2.emm.agent.utils` package: `SERVER_PORT` and `SERVER_PROTOCOL`.

    

    For more information see below:
    
    The Android agent must have the CA certificate inside the application when configuring the Secure Sockets Layer (SSL). The CA certificate is stored in a BKS (`bouncycastle`) file. Follow the steps given below to create and generate a BKS file: 

    #### Prerequisites 

    *   OpenSSL version 3.0.0.

        

        

        For more information, see how to [download and install OpenSSL](https://www.openssl.org/source/).

        

        

    *   Set up the required environment variables when running on Windows.

        

        

        

    *   The `bcprov-jdk16-1.46.jar` file.

        

        

        [Download the `bcprov-jdk16-1.46.jar` ](http://repo1.maven.org/maven2/org/bouncycastle/bcprov-jdk16/1.46/bcprov-jdk16-1.46.jar)file from the maven repository.

        

        

    *   Clone the  `cdmf-agent-android`  GIT repository. This will be referred to as `<ANDROID_AGENT>`.

        `git clone --branch v{ANDROID_AGENT_VERSION} https://github.com/wso2/cdmf-agent-android.git`

        

        

        

        This section provides information on the API and device agent versions that are compatible with each release of the [Entgra IoT Server](http://entgra.io/). 

        <table>
          <colgroup>
            <col>
            <col>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>IoT Server version</th>
              <th>API version</th>
              <th>Android agent version</th>
              <th>iOS agent version</th>
            </tr>
            <tr>
              <td>3.0.0</td>
              <td>1.0.0</td>
              <td><a href="https://gitlab.com/entgra/android-agent/tree/v3.4.0" class="external-link" rel="nofollow">3.4.0</a></td>
              <td><a href="https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.0" class="external-link" rel="nofollow">2.0.0</a></td>
            </tr>
            <tr>
              <td><br></td>
              <td><br></td>
              <td><br></td>
              <td><br></td>
            </tr>
          </tbody>
        </table>

        

        

    #### Step 1: Creating a BKS file

    

    

    If you **[configured IoTS for iOS]({{< param doclink >}}key-concepts.html)**, or if you have **[changed the IP of Entgra IoT Server using the `change-ip` script]({{< param doclink >}}product-administration/configuring-ip-or-hostname)**, you can **skip this step** and **move to [Step 2](/doc/en/lb2/Generating-a-BKS-File-for-Android.html)** by using the already generated and imported Certificate Authority (CA), Registration Authority (RA), and SSL certificate files.


    

    

    1.  Navigate to the `openssl.cnf` file of the OpenSSL installation. 

    2.  Make a copy of the `openssl.cnf` file, move it to another location, and configure the file to generate version 3 certificates as shown below:


        [ v3_req ] 
        Extensions to add to a certificate request 
        basicConstraints=CA:TRUE 
        keyUsage = Digital Signature, Key Encipherment 

        [ v3_ca ] 
        Extensions for a typical CA 
        PKIX recommendation. 
        subjectKeyIdentifier=hash 
        authorityKeyIdentifier=keyid:always,issuer 
        This is what PKIX recommends but some broken software chokes on critical 
        extensions. 
        basicConstraints = critical,CA:true 
        So we do this instead. 
        basicConstraints = CA:true 
        Key usage: this is typical for a CA certificate. However since it will 
        prevent it being used as an test self-signed certificate it is best 
        left out by default. 
        keyUsage = Digital Signature, Certificate Sign, CRL Sign
        

    3.  In the location where you modified and saved the `openssl.cnf` file, run the following commands to generate a self-signed Certificate Authority (CA) certificate (version 3) and convert the certificate to the `.pem` format:

        1.  `openssl genrsa -out <CA PRIVATE KEY> 4096`  
            For example: `openssl genrsa -out ca_private.key 4096`
        2.  `openssl req -new -key <CA PRIVATE KEY> -out <CA CSR>`  
            For example: `openssl req -new -key ca_private.key -out ca.csr`
        3.  `openssl x509 -req -days <DAYS> -in <CA CSR> -signkey <CA PRIVATE KEY> -out <CA CRT> -extensions v3_ca -extfile <PATH-TO-MODIFIED-openssl.cnf-FILE> `  
            For example: `openssl x509 -req -days 365 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile ./openssl.cnf`
        4.  `openssl rsa -in <CA PRIVATE KEY> -text > <CA PRIVATE PEM>`  
            For example:  `openssl rsa -in ca_private.key -text > ca_private.pem`
        5.  `openssl x509 -in <CA CRT> -out <CA CERT PEM>`  
            For example: `openssl x509 -in ca.crt -out ca_cert.pem`
    4.  In the same location, run the following commands to generate a Registration Authority (RA) certificate (version 3), sign it with the CA, and convert the certificate to the `.pem` format. 

        1.  `openssl genrsa -out <RA PRIVATE KEY> 4096`   
            For example:  `openssl genrsa -out ra_private.key 4096`

        2.  `openssl req -new -key <RA PRIVATE KEY> -out <RA CSR>`   
            For example: `openssl req -new -key ra_private.key -out ra.csr`
        3.  `openssl x509 -req -days <DAYS> -in <RA CSR> -CA <CA CRT> -CAkey <CA PRIVATE KEY> -set_serial <SERIAL NO> -out <RA CRT> -extensions v3_req -extfile <PATH-TO-MODIFIED- openssl.cnf-FILE > `  
            For example: `openssl x509 -req -days 365 -in ra.csr -CA ca.crt -CAkey ca_private.key -set_serial 02 -out ra.crt -extensions v3_req -extfile ./openssl.cnf`
        4.  `openssl rsa -in <CA PRIVATE KEY> -text > <RA PRIVATE PEM>`   
            For example: `openssl rsa -in ra_private.key -text > ra_private.pem`
        5.  `openssl x509 -in <RA CRT> -out <RA CERT PEM>`   
            For example: `openssl x509 -in ra.crt -out ra_cert.pem`
    5.  Generate the SSL certificate (version 3) based on your domain/IP address:

        

        

        You must add your IP address/domain as the Common Name. Otherwise, provisioning will fail. 

        

        

        1.  Generate an RSA key.  
            `openssl genrsa -out <RSA_key>.key 4096`   
            For example:  
            `openssl genrsa -out ia.key 4096`
        2.  Generate a CSR file.  
            `openssl req -new -key <RSA_key>.key -out <CSR>.csr`   
            For example:  
            `openssl req -new -key ia.key -out ia.csr`   
            Enter your server IP address/domain name (e.g., 192.168.1.157) as the Common Name else provisioning will fail.
        3.  Generate the SSL certificate.  
            `openssl x509 -req -days 730 -in <CSR>.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial <serial number> -out ia.crt`   
            For example:    
            `openssl x509 -req -days 730 -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt`
    6.  Export the SSL, CA, and RA files as PKCS12 files with an alias.

        1.  Export the SSL file as a PKCS12 file with "`wso2carbo`n" as the alias.   
            `openssl pkcs12 -export -out <KEYSTORE>.p12 -inkey <RSA_key>.key -in ia.crt -CAfile ca_cert.pem -name "<alias>"`   
            For example:  
            `openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name "wso2carbon"`

        2.  Export the CA file as a PKCS12 file with an alias.  
            `openssl pkcs12 -export -out <CA>.p12 -inkey <CA private key>.pem -in <CA Cert>.pem -name "<alias>" `  
            For example:   
            `openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -name "cacert"`   
            In the above example, `cacert` has been used as the CA alias. 
        3.  Export the RA file as a PKCS12 file with an alias.  
            `openssl pkcs12 -export -out <RA>.p12 -inkey <RA private key>.pem -in <RA Cert>.pem -chain -CAfile <CA cert>.pem -name "<alias>"`   
            For example:   
            `openssl pkcs12 -export -out ra.p12 -inkey ra_private.pem -in ra_cert.pem -chain -CAfile ca_cert.pem -name "racert"`   
            In the above example, `racert` has been used as the RA alias. 
    7.  Copy the three P12 files to the `<IOTS_HOME>/core/repository/resources/security` directory.
    8.  Import the generated P12 files as follows:  

        1.  Import the generated `<KEYSTORE>.p12` file into the `wso2carbon.jks` and `client-truststore.jks` in the `<IoT_HOME>/core/repository/resources/security` directory.  
            `keytool -importkeystore -srckeystore <KEYSTORE>.p12 -srcstoretype PKCS12 -destkeystore <wso2carbon.jks/client-truststore.jks>`

            

            

            When prompted, enter the key store password and key store key password as `wso2carbon`.

            

            

            For example:   
            `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore wso2carbon.jks `  
            `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore client-truststore.jks`

        2.  Import the generated `<CA>.p12` and `<RA>.p12` files into the `wso2certs.jks` file, which is in the `<IoT_HOME>/core/repository/resources/security` directory.  
            `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`   

            For example:  
            `keytool -importkeystore -srckeystore ca.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`   
            Enter the keystore password as `wso2carbon` and keystore key password as `cacert`.

            `keytool -importkeystore -srckeystore ra.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`   
            Enter the keystore password as  `wso2carbon` and keystore key password as `racert`.

            

            Troubleshooting

            

            ##### Why does the following error occur: `"` keytool error: [`java.io`](http://java.io/).IOException: Invalid keystore format"?

            If you enter the wrong private key password when importing the `<CA>.p12` or `<RA>.p12` files, the `wso2certs.jks` file will get corrupted and the above error message will appear.

            In such a situation, delete the `wso2certs.jks` file and execute the following command to import the generated `<CA>.p12` and `<RA>.p12` files into the `wso2certs.jks` file again.  
            `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`  

            When the above command is executed, IoTS will automatically create a new `wso2certs.jks` file with the imported file.

            

            

    #### Step 2: Generating a BKS file

    

    

    Follow all the steps given under step 1 before generating the BKS file to avoid errors.

    

    

    1.  Generate the BKS file:

        

        

        Make sure to generate the BKS file into the same folder that has the [`bcprov-jdk16-1.46.jar`](http://repo1.maven.org/maven2/org/bouncycastle/bcprov-jdk16/1.46/bcprov-jdk16-1.46.jar) file before running the command given below. Else, you get the error given below:

        keytool error: java.lang.ClassNotFoundException: org.bouncycastle.jce.provider.BouncyCastleProvider
        java.lang.ClassNotFoundException: org.bouncycastle.jce.provider.BouncyCastleProvider
        	at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
        	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
        	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
        	at sun.security.tools.keytool.Main.doCommands(Main.java:673)
        	at sun.security.tools.keytool.Main.run(Main.java:343)
        	at sun.security.tools.keytool.Main.main(Main.java:336)

        

        

        If you are using an SSL certificate by a trusted authority such as GoDaddy, the `cert.crt` defined in the command should be the interim certificate.  

        Example:

        `keytool -noprompt -import -v -trustcacerts -alias godaddy -file cert.crt -keystore truststore.bks -storetype BKS -providerclass org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath bcprov-jdk16-1.46.jar -storepass 'wso2carbon'`

    2.  Optionally, view the list of certificates in the BKS form using the following command:

        `keytool -list -v -keystore "truststore.bks" -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "bcprov-jdk16-1.46.jar" -storetype BKS -storepass "wso2carbon"`

    3.  Copy the generated `truststore.bks` file to the `<ANDROID_AGENT>/client/iDPProxy/src/main/res/raw` directory and replace the original file. 
    4.  Navigate to the `<ANDROID_AGENT>/client/client/src/main/java/org/wso2/iot/agent/utils/Constants.java` file, and configure the following:
        *   Provide the `HTTPS_HOST_IP` as the value for the `API_SERVER_PORT` parameter.  
            Example:` 9443`.
        *   Change the `SERVER_PROTOCOL` to `https://`.
    5.  Configure the following files to get SSL to work on the Android agent:

        

        

        Configure the build release you want to compile to get the customized agent. For example, you can build the release, debug, staging or standalone build releases to meet your requirement.

        

        

        1.  Configure the `SERVER_PROTOCOL` property to `https` in the `client/iDPProxy/build.gradle` file.

            `buildConfigField "String", "SERVER_PROTOCOL", "\"https://\""`

        2.  Configure the `DEFAULT_HOST` property with the server URL in the `client/client/build.gradle` file.  
            Example:

            `buildConfigField "String", "DEFAULT_HOST", "\"https://10.10.10.192:8243\""`

            

            

            After this configuration, the Android agent skips the following server URL entering 
            screen during enrollment. 
            ![image](352823115.png)

            

            

    6.  Navigate to the `<ANDROID_AGENT>/client/iDPProxy/src/main/java/org/wso2/iot/agent/proxy/utils/Constants.java` file, and provide the BKS file password as the value for the `TRUSTSTORE_PASSWORD` parameter.

    

    

    

4.  The Android agent can't be built via the usual android developer SDK, as it requires access to [developer restricted APIs](/doc/en/lb2/Integrating-the-Android-System-Service-Application.html). Therefore, you need to replace the existing `android.jar` file that is in the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs.   
    You can get the new `jar` file using one of the following methods:

    *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

    *   Use a pre-built jar file from a third party developer. You can find it here: [https://github.com/anggrayudi/android-hidden-api](https://github.com/anggrayudi/android-hidden-api).
5.  Import the project on Android Studio, clean and build the project.

6.  Run the project on a device.

    

    

    The .`apk` file can be found in the `<IoT_HOME>/core/repository/deployment/server/jaggeryapps/android-web-agent/app/pages/mdm.page.enrollments.android.download-agent/public/asset` directory.

## Generating a BKS File for Android
 
The Android agent must have the CA certificate inside the application when configuring the Secure Sockets Layer (SSL).Â The CA certificate is stored in a BKS (`bouncycastle`) file.Â Follow the steps given below to create and generate a BKS file:Â 

#### PrerequisitesÂ 

*   OpenSSL version 3.0.0.

    

    

    For more information, see how toÂ [download and install OpenSSL](https://www.openssl.org/source/).

    

    

*   Set up the required environment variables when running on Windows.

    

    

    

*   TheÂ `bcprov-jdk16-1.46.jar`Â file.

    

    

    [Download theÂ `bcprov-jdk16-1.46.jar`Â ](http://repo1.maven.org/maven2/org/bouncycastle/bcprov-jdk16/1.46/bcprov-jdk16-1.46.jar)file from the maven repository.

    

    

*   Clone theÂ Â `cdmf-agent-android`Â Â GIT repository. This will be referred toÂ asÂ `<ANDROID_AGENT>`.

    `git clone --branch v{ANDROID_AGENT_VERSION} https://github.com/wso2/cdmf-agent-android.git`

    

    

    

    This section provides information on the API and device agent versions that are compatible with each release of the [Entgra IoT Server](http://entgra.io/).Â 

    <table>
      <colgroup>
        <col>
        <col>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>IoT Server version</th>
          <th>API version</th>
          <th>Android agent version</th>
          <th>iOS agent version</th>
        </tr>
        <tr>
          <td>3.0.0</td>
          <td>1.0.0</td>
          <td><a href="https://gitlab.com/entgra/android-agent/tree/v3.4.0" class="external-link" rel="nofollow">3.4.0</a></td>
          <td><a href="https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.0" class="external-link" rel="nofollow">2.0.0</a></td>
        </tr>
        <tr>
          <td><br></td>
          <td><br></td>
          <td><br></td>
          <td><br></td>
        </tr>
      </tbody>
    </table>

    

    

#### Step 1: Creating a BKS file





If you **[configured IoTS for iOS](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352823877/iOS+Configurations)**, or if you have **[changed the IP of Entgra IoT Server using the `change-ip` script]({{< param doclink >}}product-administration/configuring-ip-or-hostname)**, you can **skip this step** and **move to [Step 2](/doc/en/lb2/Generating-a-BKS-File-for-Android.html)** by using the already generated and imported Certificate Authority (CA), Registration Authority (RA), and SSL certificate files.





1.  Navigate toÂ theÂ `openssl.cnf`Â fileÂ of the OpenSSL installation.Â 

2.  Make a copy of theÂ `openssl.cnf`Â file, move it to another location, and configure the file to generate version 3 certificates as shown below:

    [ v3_req ] 
    Extensions to add to a certificate request 
    basicConstraints=CA:TRUE 
    keyUsage = Digital Signature, Key Encipherment 

    [ v3_ca ] 
    Extensions for a typical CA 
    PKIX recommendation. 
    subjectKeyIdentifier=hash 
    authorityKeyIdentifier=keyid:always,issuer 
    This is what PKIX recommends but some broken software chokes on critical 
    extensions. 
    basicConstraints = critical,CA:true 
    So we do this instead. 
    basicConstraints = CA:true 
    Key usage: this is typical for a CA certificate. However since it will 
    prevent it being used as an test self-signed certificate it is best 
    left out by default. 
    keyUsage = Digital Signature, Certificate Sign, CRL Sign

3.  In the location where you modified and saved theÂ `openssl.cnf`Â file, run the following commands to generate a self-signed Certificate Authority (CA) certificate (version 3) and convert the certificate to theÂ `.pem`Â format:

    1.  `openssl genrsa -out <CA PRIVATE KEY> 4096`  
        For example:Â `openssl genrsa -out ca_private.key 4096`
    2.  `openssl req -new -key <CA PRIVATE KEY> -out <CA CSR>`  
        For example:Â `openssl req -new -key ca_private.key -out ca.csr`
    3.  `openssl x509 -req -days <DAYS> -in <CA CSR> -signkey <CA PRIVATE KEY> -out <CA CRT> -extensions v3_caÂ -extfile <PATH-TO-MODIFIED-openssl.cnf-FILE>Â `  
        For example:Â `openssl x509 -req -days 365 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile ./openssl.cnf`
    4.  `openssl rsa -in <CA PRIVATE KEY> -text > <CA PRIVATE PEM>`  
        For example:Â Â `openssl rsa -in ca_private.key -text > ca_private.pem`
    5.  `openssl x509 -in <CA CRT> -out <CA CERT PEM>`  
        For example:Â `openssl x509 -in ca.crt -out ca_cert.pem`
4.  In the same location, run the following commands to generate a Registration Authority (RA) certificate (version 3), sign it with the CA, and convert the certificate toÂ theÂ `.pem`Â format.Â 

    1.  `openssl genrsa -out <RA PRIVATE KEY> 4096`Â   
        For example:Â Â `openssl genrsa -out ra_private.key 4096`

    2.  `openssl req -new -key <RA PRIVATE KEY> -out <RA CSR>`Â   
        For example:Â `openssl req -new -key ra_private.key -out ra.csr`
    3.  `openssl x509 -req -days <DAYS> -in <RA CSR> -CA <CA CRT> -CAkey <CA PRIVATE KEY> -set_serial <SERIAL NO> -out <RA CRT> -extensions v3_req -extfile <PATH-TO-MODIFIED- openssl.cnf-FILE >Â `  
        For example:Â `openssl x509 -req -days 365 -in ra.csr -CA ca.crt -CAkey ca_private.key -set_serial 02 -out ra.crt -extensions v3_req -extfile ./openssl.cnf`
    4.  `openssl rsa -in <CA PRIVATE KEY> -text > <RA PRIVATE PEM>`Â   
        For example:Â `openssl rsa -in ra_private.key -text > ra_private.pem`
    5.  `openssl x509 -in <RA CRT> -out <RA CERT PEM>`Â   
        For example:Â `openssl x509 -in ra.crt -out ra_cert.pem`
5.  Generate the SSL certificate (version 3)Â based on your domain/IP address:

    

    

    You must add your IP address/domain as the Common Name. Otherwise, provisioning will fail.Â 

    

    

    1.  Generate an RSA key.  
        `openssl genrsa -out <RSA_key>.key 4096`Â   
        For example:  
        `openssl genrsa -out ia.key 4096`
    2.  Generate a CSR file.  
        `openssl req -new -key <RSA_key>.key -out <CSR>.csr`Â   
        For example:  
        `openssl req -new -key ia.key -out ia.csr`Â   
        EnterÂ your server IP address/domain name (e.g., 192.168.1.157)Â as the Common Name elseÂ provisioning will fail.
    3.  Generate the SSL certificate.  
        `openssl x509 -req -days 730 -in <CSR>.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial <serial number> -out ia.crt`Â   
        For example:Â Â   
        `openssl x509 -req -days 730 -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt`
6.  Export the SSL, CA, and RA files as PKCS12 files with an alias.

    1.  Export the SSL file as a PKCS12 file with "`wso2carbo`n" as theÂ alias.Â   
        `openssl pkcs12 -export -out <KEYSTORE>.p12 -inkey <RSA_key>.key -in ia.crt -CAfile ca_cert.pem -name "<alias>"`Â   
        For example:  
        `openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name "wso2carbon"`

    2.  Export the CA file as a PKCS12 file with an alias.  
        `openssl pkcs12 -export -out <CA>.p12 -inkey <CA private key>.pem -in <CA Cert>.pem -name "<alias>"Â `  
        For example:Â   
        `openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -name "cacert"`Â   
        In the above example,Â `cacert`Â has been used as the CA alias.Â 
    3.  Export the RA file as a PKCS12 file with an alias.  
        `openssl pkcs12 -export -out <RA>.p12 -inkey <RA private key>.pem -in <RA Cert>.pem -chain -CAfile <CA cert>.pem -name "<alias>"`Â   
        For example:Â   
        `openssl pkcs12 -export -out ra.p12 -inkey ra_private.pem -in ra_cert.pem -chain -CAfile ca_cert.pem -name "racert"`Â   
        In the above example,Â `racert`Â has been used as the RA alias.Â 
7.  Copy the three P12 files toÂ theÂ `<IOTS_HOME>/core/repository/resources/security`Â directory.
8.  Import the generated P12 files as follows:  

    1.  Import theÂ generatedÂ `<KEYSTORE>.p12`Â fileÂ intoÂ theÂ `wso2carbon.jks`Â andÂ `client-truststore.jks`Â inÂ theÂ `<IoT_HOME>/core/repository/resources/security`Â directory.  
        `keytool -importkeystore -srckeystore <KEYSTORE>.p12 -srcstoretype PKCS12 -destkeystore <wso2carbon.jks/client-truststore.jks>`

        

        

        When prompted, enter the key store password and key store key password as `wso2carbon`.

        

        

        For example:Â   
        `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore wso2carbon.jksÂ `  
        `keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore client-truststore.jks`

    2.  Import theÂ generatedÂ `<CA>.p12`Â andÂ `<RA>.p12`Â filesÂ intoÂ theÂ `wso2certs.jks`Â file, which is inÂ theÂ `<IoT_HOME>/core/repository/resources/security`Â directory.  
        `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`Â   

        For example:  
        `keytool -importkeystore -srckeystore ca.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`Â   
        Enter theÂ keystoreÂ passwordÂ asÂ `wso2carbon`Â andÂ keystoreÂ key passwordÂ asÂ `cacert`.

        `keytool -importkeystore -srckeystore ra.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`Â   
        Enter theÂ keystoreÂ passwordÂ asÂ Â `wso2carbon`Â andÂ keystoreÂ key passwordÂ asÂ `racert`.

        

        Troubleshooting

        

        ##### Why does the following error occur: `"` keytool error: [`java.io`](http://java.io/).IOException: Invalid keystore format"?

        If you enter the wrong private key password when importing the `<CA>.p12`Â orÂ `<RA>.p12`Â files, the `wso2certs.jks` file will get corrupted and the above error message will appear.

        In such a situation, delete the `wso2certs.jks` file and execute the following command to import the generatedÂ `<CA>.p12`Â andÂ `<RA>.p12`Â files into theÂ `wso2certs.jks` file again.  
        `keytool -importkeystore -srckeystore <CA/RA>.p12 -srcstoretype PKCS12 -destkeystore wso2certs.jks`  

        When the above command is executed, IoTS will automatically create a new `wso2certs.jks` file with the imported file.

        

        

#### Step 2: Generating a BKS file





Follow all the steps given under step 1 before generating the BKS file to avoid errors.





1.  Generate the BKS file:

    

    

    Make sure to generate the BKS file into the same folder that has the [`bcprov-jdk16-1.46.jar`](http://repo1.maven.org/maven2/org/bouncycastle/bcprov-jdk16/1.46/bcprov-jdk16-1.46.jar)Â file before running the command given below. Else, you get the error given below:

    keytool error: java.lang.ClassNotFoundException: org.bouncycastle.jce.provider.BouncyCastleProvider
    java.lang.ClassNotFoundException: org.bouncycastle.jce.provider.BouncyCastleProvider
    	at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
    	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
    	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
    	at sun.security.tools.keytool.Main.doCommands(Main.java:673)
    	at sun.security.tools.keytool.Main.run(Main.java:343)
    	at sun.security.tools.keytool.Main.main(Main.java:336)

    

    

    If you are using an SSL certificate by a trusted authority such as GoDaddy,Â theÂ `cert.crt`Â definedÂ in the command should be the interim certificate. Â 

    Example:

    `keytool -noprompt -import -v -trustcacerts -alias godaddy -file cert.crt -keystore truststore.bks -storetype BKS -providerclass org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath bcprov-jdk16-1.46.jar -storepass 'wso2carbon'`

2.  Optionally, view the list of certificates in the BKS form using the following command:

    `keytool -list -v -keystore "truststore.bks" -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "bcprov-jdk16-1.46.jar" -storetype BKS -storepass "wso2carbon"`

3.  Copy theÂ generatedÂ `truststore.bks`Â fileÂ toÂ theÂ `<ANDROID_AGENT>/client/iDPProxy/src/main/res/raw`Â directoryÂ and replace the original file.Â 
4.  Navigate to theÂ `<ANDROID_AGENT>/client/client/src/main/java/org/wso2/iot/agent/utils/Constants.java`Â file, and configure the following:
    *   ProvideÂ theÂ `HTTPS_HOST_IP`Â asÂ the value forÂ theÂ `API_SERVER_PORT`Â parameter.  
        Example:`Â 9443`.
    *   ChangeÂ theÂ `SERVER_PROTOCOL`Â toÂ `https://`.
5.  Configure the following files to get SSL to work on the Android agent:

    

    

    Configure the build release you want to compile to get the customized agent. For example, you can build the release, debug, staging or standalone build releases to meet your requirement.

    

    

    1.  ConfigureÂ theÂ `SERVER_PROTOCOL`Â propertyÂ toÂ `https`Â inÂ theÂ `client/iDPProxy/build.gradle`Â file.

        `buildConfigField "String", "SERVER_PROTOCOL", "\"https://\""`

    2.  ConfigureÂ theÂ `DEFAULT_HOST`Â propertyÂ with the server URL inÂ theÂ `client/client/build.gradle`Â file.  
        Example:

        `buildConfigField "String", "DEFAULT_HOST", "\"https://10.10.10.192:8243\""`

        

        

        After this configuration, the Android agentÂ skips the following server URL entering 
        screen during enrollment.Â ![image](352823115.png)

        

        

6.  Navigate to theÂ `<ANDROID_AGE

## Auto Enrolling an Android Device


You can enroll your device automatically with Entgra IoT Server via mutual SSL. For a better understanding of how auto-enrollment is achieved via mutual SSL in Entgra IoT Server, see [Mutual SSL Authentication]({{< param doclink >}}product-administration/certificate-management.html).






If you are running Entgra IoT Server using an IP and not localhost, make sure to [Configure Entgra IoT Server with the IP]({{< param doclink >}}product-administration/configuring-ip-or-hostname.html).






### Configure Entgra IoT Server for mutual SSL authentication



Before you begin



Download and install OpenSSL. For more information, see the guide on how to [download and install OpenSSL](https://www.openssl.org/source/).





Follow the steps given below to configure Entgra IoT Server for mutual SSL authentication:

1.  Configure the `web.xml` file that is in the `<IOTS_HOME>/repository/deployment/server/webapps/api#device-mgt#android#v1.0/WEB-INF` directory by adding the content given below.

    

    

    *   If you have not started Entgra IoT Server previously, you need to extract the content that is in the `<IoT_HOME>/core/repository/deployment/server/webapps/api#device-mgt#android#v1.0.war` file.
    *   If you have started Entgra IoT Server previously it means that the `api#device-mgt#android#v1.0` folder is already available in the `<IoT_HOME>/repository/deployment/server/webapps` directory.

    

    

    <!--For Mutual SSL enabling, please uncomment login-config and security-constraint tags-->
    <login-config>
       <auth-method>CLIENT-CERT</auth-method>
    </login-config>


    [ v3_req ] 
    Extensions to add to a certificate request 
    basicConstraints=CA:TRUE 

    [ v3_ca ] 
    Extensions for a typical CA 
    PKIX recommendation. 
    subjectKeyIdentifier=hash 
    authorityKeyIdentifier=keyid:always,issuer 
    This is what PKIX recommends but some broken software chokes on critical 
    extensions. 
    basicConstraints = critical,CA:true 
    So we do this instead. 
    basicConstraints = CA:true 
    Key usage: this is typical for a CA certificate. However since it will 
    prevent it being used as an test self-signed certificate it is best 
    left out by default. 
    keyUsage = digitalSignature, keyCertSign, cRLSign

3.  Generate a self-signed Certificate Authority (CA) certificate (version 3) and convert the certificate to the `.pem` format:  

    

    

    If you have already configured iOS with the self-sign certificates, you can skip steps 3, 4 and 6 by and use the same certificates.

    

    

    1.  Generate the private key.

        `openssl genrsa -out <CA PRIVATE KEY> 4096`

        Example:

        `openssl genrsa -out ca_private.key 4096`

    2.  Generate a certificate signing request (CSR).

        

        

        Provide the server IP/hostname as the common name when prompted.

        

        

        `openssl req -new -key <CA PRIVATE KEY> -out <CA CSR> `

        Example:

        `openssl req -new -key ca_private.key -out ca.csr`

    3.  Self-sign the CSR by signing it with the private key.

        `openssl x509 -req -days <DAYS> -in <CA CSR> -signkey <CA PRIVATE KEY> -out <CA CRT> -extensions v3_ca -extfile <PATH-TO-MODIFIED-openssl.cnf-FILE> `

        Example:

        `openssl x509 -req -days 365 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile ./openssl.cnf`

    4.  Convert the private key to the `.pem` format.

        `openssl rsa -in <CA PRIVATE KEY> -text > <CA PRIVATE PEM> `

        Example:

        `openssl rsa -in ca_private.key -text > ca_private.pem`

    5.  Convert the CA certificate to the `.pem` format.

        `openssl x509 -in <CA CRT> -out <CA CERT PEM>`

        Example:

        `openssl x509 -in ca.crt -out ca_cert.pem`

4.  Generate the SSL certificate (version 3) based on your domain/IP address:  

    1.  Generate a private key.

        `openssl genrsa -out <RSA_key>.key 4096 `

        Example:

        `openssl genrsa -out ia.key 4096`

    2.  Generate a certificate signing request (CSR).

        

        

        Provide the server IP/hostname as the common name when prompted.

        

        

        `openssl req -new -key <RSA_key>.key -out <CSR>.csr`

        Example:

        `openssl req -new -key ia.key -out ia.csr`

    3.  Sign the CSR with the CA private key to generate the SSL certificate.

        `openssl x509 -req -days 730 -in <CSR>.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial <serial number> -out ia.crt`

        Example:

        `openssl x509 -req -days 730 -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt`

5.  Generate the client-side SSL certificate (version 3):
    1.  Generate a private key.

        `openssl genrsa -out <CLIENT_key>.key 4096 `

        Example:

        `openssl genrsa -out client.key 4096`

    2.  Generate a certificate signing request (CSR). When generating the client CSR, make sure to give the serial number of the device that is being enrolled as the common name.

        `openssl req -new -key <CLIENT_key>.key -out <CSR>.csr`

        Example:

        `openssl req -new -key client.key -out client.csr`

    3.  Sign the CSR file with the CA private key to generate the client certificate.

        `openssl x509 -req -days 730 -in <CSR>.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial <serial number> -out ia.crt`

        Example:

        `openssl x509 -req -days 730 -in client.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 12438035315552875930 -out client.crt`

    4.  Convert the client certificate to the `.pem` format for future use.

        `openssl x509 -in <CLIENT CRT> -out <CLIENT CERT PEM>`

        Example:

        `openssl x509 -in client.crt -out client.pem`

6.  Copy the server side SSL certificate to the `wso2carbon.jks` file that is in the `<IoT_HOME>/core/repository/resources/security` directory using the command given below:

    openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name "wso2carbon"
    keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore wso2carbon.jks


    openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -name "cacert"
    keytool -importkeystore -srckeystore ca.p12 -srcstoretype PKCS12 -destkeystore client-truststore.jks

    

    Why is this step required?

    

    In the SSL handshake process, the server will send a set of trusted CA details to the client and request the client to send a certificate that matches the certificate signed by the server's trusted certificates.

    

    

8.  Export the `wso2carbon` certificate that is in the `<IoT_HOME>/core/repository/resources/security` directory to the `.pem` format suing the command given below.

    `keytool -exportcert -alias wso2carbon -keystore wso2carbon.jks -rfc -file cert_pem.pem -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt`

9.  Copying the command given below the content between `BEGIN CERTIFICATE` and `END CERTIFICATE` tags of the `cert_pem`.`pem` file you just generated and paste it within the `<Certificate>` tag of the `<IOTS_HOME>/conf/identity/identity-providers/iot_default.xml` file.

10.  Enable Mutual SSL in the Entgra IoT Server API Gateway by configuring the `<IOTS_HOME>/conf/axis2/axis2.xml` file. Make sure to configure the property that is under the HTTPS transport receiver as shown below.

    `<parameter name="SSLVerifyClient">optional</parameter>`

11.  Restart the server and upload the [generated certificate in step 5.d above](about:blank#AutoEnrollinganAndroidDevice-certificate), to the certificate section of the [device management console]({{< param doclink >}}product-administration/certificate-management.html) or add the certificate to the database via the [REST API](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822283/Device+Management+REST+APIs).


12.  Create a new BKS file having the name `truststore` and add the CA certificate.

    

    

    Ensure that you have the `bcprov-jdk16-1.46.jar` file in the same folder where you will be generating the BKS file before you run this command.

    

    

    `keytool -noprompt -import -v -trustcacerts -alias `openssl x509 -inform PEM -subject_hash -noout -in ca_cert.pem` -file ca_cert.pem -keystore truststore.bks -storetype BKS -providerclass org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath bcprov-jdk16-1.46.jar -storepass 'wso2carbon'`

    If you are using an SSL certificate by a trusted authority such as GoDaddy, the `cert.crt` defined in the command should be the interim certificate.    
    Example:

    `keytool -noprompt -import -v -trustcacerts -alias godaddy -file cert.crt -keystore truststore.bks -storetype BKS -providerclass org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath bcprov-jdk16-1.46.jar -storepass 'wso2carbon'`

13.  Create a new BKS file with the name `keystore` and add the client certificate generated bellow.  
    Example:

    `openssl pkcs12 -export -out client.p12 -inkey client.key -in client.crt -CAfile ca_cert.pem -name "wso2carbon"`

    

    

    This client.p12 must be added to the `keystore.bks` file. For example as bellow,

    `keytool -noprompt -importkeystore -v -srckeystore client.p12 -srcstoretype pkcs12 -alias wso2carbon -keystore keystore.bks -storetype BKS -providerclass org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath bcprov-jdk16-1.46.jar -storepass 'wso2carbon`'

    

    

14.  Copy the generated BKS files to the `<ANDROID_AGENT_SOURCE_HOME>/cleint/IDPProxy/src/res/raw` folder.

    

    

    You can download the Android agent source code from **[here](https://github.com/wso2/cdmf-agent-android/releases/tag/v2.0.0)**.

    

    

15.  Navigate to the `Constants.java` file, which is under the `org.wso2.iot.agent.proxy.utils` package of the `<ANDROID_AGENT_SOURCE_HOME>/cleint/IDPProxy/src/java` directory, and configure the following fields.
    *   Assign `MUTUAL_SSL_AUTHENTICATOR` as the value for` AUTHENTICATOR_IN_USE`.

        `public static final String AUTHENTICATOR_IN_USE = MUTUAL_SSL_AUTHENTICATOR;`

    *   Assign `MUTUAL_HTTP_CLIENT` as the value for `HTTP_CLIENT_IN_USE`.

        `public static final String HTTP_CLIENT_IN_USE = MUTUAL_HTTP_CLIENT;`

    *   Assign `https` as the value for `SERVER_PROTOCOL`.

        `public static final String SERVER_PROTOCOL = "https://";`

    *   Optionally, if the passwords given when creating the BKS files was different from the default password used, which is `wso2carbon`, configure the `TRUSTSTORE_PASSWORD` and `KEYSTORE_PASSWORD` fields with the password that was used and if the certificates are stored in the device storage, configure `TRUSTSTORE_LOCATION`,`KEYSTORE_LOCATION` fields with the full path of the storage location.

        public static final String TRUSTSTORE_PASSWORD = "<PASSWORD>";
        public static final String KEYSTORE_PASSWORD = "<PASSWORD>";
        public static final String TRUSTSTORE_LOCATION = "<FULL_PATH_OF_THE_LOCATION>";
        public static final String KEYSTORE_LOCATION = "<FULL_PATH_OF_THE_LOCATION>";

16.  Navigate to the `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main/java/org/wso2/iot/agent/utils/Constants.java` file of the Android agent and configure the following fields.
    *   When enrolling an Android device with Entgra IoT Server, you have the ability to define if the device belongs to the COPE/BYOD device type. This is defined by the `OWNERSHIP_BYOD` and `OWNERSHIP_COPE` fields.  
        Default configuration:

        public static final String OWNERSHIP_BYOD = "BYOD";
        public static final String OWNERSHIP_COPE = "COPE";

        When auto-enrolling an Android device, you **must** define the default device type of the registered device by assigning the respective value for the `DEFAULT_OWNERSHIP` field.

        Example: Define that the registered device belongs to the COPE device type by default.

        `public static final String DEFAULT_OWNERSHIP = OWNERSHIP_COPE;`

    *   Optionally, if you wish to perform the enrollment in the background (without any interaction with the user) set the `AUTO_ENROLLMENT_BACKGROUND_SERVICE_ENABLED` field to `true`.

        

        

        This feature is applicable only for COPE devices where the organization has devices that are manufactured specifically for them (OEM scenarios). Further, the Android agent MUST be pre-set by the OS as a [Device Administrator](https://developer.android.com/guide/topics/admin/device-admin.html). Once this is enabled, the Android agent will automatically trigger this service when the device is connected to the network for the first time.

        

        

        `public static final boolean AUTO_ENROLLMENT_BACKGROUND_SERVICE_ENABLED = true;`

    *   Optionally, if you wish to skip initial step of the device registration where you need to enter the server address, you can pre-configure the server address details under the `DEFAULT_HOST` field.

        

        

        

        ![image](352823140.png)

        

        

        

        

        Make sure to provide the HTTPS details.

        

        

        `public static final String DEFAULT_HOST = "https://<SERVER_IP>:<PORT>";`

        Example:

        `public static final String DEFAULT_HOST = "https://10.10.10.201:8243";`

    *   Optionally, configure the following fields to suit your requirement.
        *   `SKIP_LICENSE` - Configure this field so that the end user license agreement is not shown to the user.
        *   `HIDE_LOGIN_UI` -  Configure this fields to hide the login page, which is important for auto enrollment.
        *   `HIDE_UNREGISTER_BUTTON` - Configure this field to hide the unregister button so that the user can not disenroll the device.
        *   `SKIP_WORK_PROFILE_CREATION` - Configure this field, by assigning the value as true to skip work-profile creation.

            

            

            For more information on the work profile that separates the personal and workdata on Android devices, see [Data Containerization for Android Device](/doc/en/lb2/Data-Containerization-for-Android-Device.html).

            

            

        *   `HIDE_ERROR_DIALOG` - Configure this field to hide errors that pop up after the device is enrolled.  
            Example: The user will not be notified of server configuration issues after the device is enrolled. 

        public static final boolean SKIP_LICENSE = true;
        public static final boolean HIDE_LOGIN_UI = true;
        public static final boolean HIDE_UNREGISTER_BUTTON = true;
        public static final boolean SKIP_WORK_PROFILE_CREATION = true;
        public static final boolean HIDE_ERROR_DIALOG = true;


    

    

    Before installing the agent make sure to check that the device serial number matches the embedded certificate's common name.

    

    

### What's next

Once the above configurations are complete, you are able to register the device with Entgra IoT Server by opening the installed application on the device. Try out the steps given below:

1.  Tap the WSO2 agent application and open it.

2.  Tap **ACTIVATE** to enable the device administrator on your device. A confirmation message will appear after enabling the device admin.  
    ![image](352823135.png)

Now the Android device is successfully registered with Entgra IoT Server.

## Integrating the Android System Service Application


Some enterprises use devices that are customized for their requirement. For example having a custom android device that functions as a POS. In such situations, organizations prefer to have and maintain custom firmwares or get device vendors to build a custom device to suite their requirement. For example, apps or devices having the capability to sign their POS app with the vendor firmware signing key and install it on devices as a system app.

Entgra IoT Server provides a separate service application that can be signed by a firmware signing key and installed on the devices as a system application alongside the Android Agent application. This enables you to have better control over the devices registered with Entgra IoT Server. Since this is a system app, it provides system level capabilities, such as device firmware upgrade, reboot and enforcing security policies, and much more.

![image](352823176.png)

For more information on managing the system service Android application see the following subsections:

### Securing Communication

When the system service app is installed on a device that is registered with Entgra IoT Server, the Android agent communicates with it to trigger system level operations from Entgra IoT Server. The communication between the system service application and the Android agent is secured by two layers of protection as listed below:

*   Via the signature - The system will grant permission only if the requesting application is signed with the same certificate as the application that is declared in the permission.

    

    

    For more information on securing the communication, see [<permissions> on the Android Developer documents](http://developer.android.com/intl/zh-cn/guide/topics/manifest/permission-element.html).

    

    

*   Check the package name of the intent who makes the call to verify that it’s a request from the Android agent.

### Integrating the system service application

Follow the steps given below to integrate the system service Android application:

1.  Build the system service application.
    1.  [Download the source code](https://github.com/wso2/cdmf-agent-android.git).
    2.  The system service app can not be built via the usual Android developer Software Development Kit (SDK), as it requires access to [349634571](about:blank#). Therefore, you need to replace the existing `android.jar` file that is under the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs. You can get the new `jar` file using one of the following options:   

        *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

        *   Use a pre-built jar file from a third party developer. You can find it [here](https://github.com/anggrayudi/android-hidden-api).

            

            

            Make sure to use the jar file that matches the `compileSdkVersion` of the Entgra Android agent. The current `compileSdkVersion` is 25.

            

            

    3.  Open the system service application source code via Android Studio and clean build it as a usual Android application.
2.  Sign the application via the device firmware signing key. If you don’t have access to the firmware signing key, you have to get the system application signed via your device vendor. 

    

    

    For more information of singing the system service, see [Signing Your Applications](http://developer.android.com/intl/zh-cn/tools/publishing/app-signing.html).

    

    

3.  Install the system service application by following any of the methods given below:

    *   If you have your own firmware, the system service application is available out of the box with your firmware distribution.

        1.  Copy the signed system service APK file to the `/system/priv-apps` directory of the device. 

        2.  When the device boots or restarts for the first time, it automatically installs the application as a system application.

    *   Install the system service application externally via an Android Debug Bridge (adb) command.

        

        

        For more information on how this takes place on Entgra IoTS, see [Configuring the service application](/doc/en/lb2/Device-Ownership-Application.html).

        

        

4.  Enable the system service invocations through the Entgra  Android Agent application. 

    

    

    

    

    

    1.  Clone the `cdmf-agent-android` GIT repository. This is referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout this document.

        `https://github.com/wso2/cdmf-agent-android.git -b <ENTER_THE_VERSION>`

        Check the [Entgra IoT Server and Agent Compatibility]({{< param doclink >}}entgra-iot-server-agent-compatibility.html) and find out what branch of this repo you need to clone.


    2.  Open the client folder that is in the `<ANDROID_AGENT_SOURCE_CODE>` via Android Studio.

    

    

    

    

    

    

    Make sure to sign the Android agent using the same device firmware signing key that was used to sign the System Service Application, else you run into security exceptions.

    

    

1.  Navigate to the `Constants.java` class, which is in the `org.wso2.iot.agent.utils` package and configure the `SYSTEM_APP_ENABLED` field as follows:

    ` public static final boolean SYSTEM_APP_ENABLED = true;`

2.  Rebuild the Android agent application.

6.  Install the Android agent you just built to your mobile device.  
    You need to copy the APK to you device and install it. For more information on installing the Android agent, see [Registering an Android device]({{< param doclink >}}quick-start-guide/mobile-device-app-management/android-device.html#AndroidDevice-install). Follow the steps from step 6 onwards.


### Operations supported via the system service application

The following operations are supported via the system service application:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th><strong>Device Reboot</strong></th>
      <td>Restart or reboot your Android device.&nbsp;</td>
    </tr>
    <tr>
      <th><strong>Firmware upgrade</strong></th>
      <td>Upgrade the firmware of Android devices.&nbsp;</td>
    </tr>
    <tr>
      <th><strong>Enforcing user restrictions</strong></th>
      <td>Restrict different functions on the user's device using this REST API. When adding a policy you will have the option of saving the user restriction policy or saving and publishing the user restriction policy.</td>
    </tr>
    <tr>
      <th><strong>Silent app installation, removal and update</strong></th>
      <td>Application installation, removal and update will be performed without the user's confirmation when the Android system service application is available on an Android device.<br>This operation is only available for enterprise applications (apps that were created by your organization) and is not available for public applications (publicly available apps, such as free apps available online).&nbsp;</td>
    </tr>
  </tbody>
</table>

## Data Containerization for Android Device


Entgra IoT Server enables you to register mobile devices via the BYOD or COPE device enrollment scenario. Data containerization allows you to have a separation between data. Therefore, if you are registering your device via the BYOD scenario you are able to have a clear separation between your personal data and the enterprise data. To understand the underlying concept clearly, take a look at the example given below.

Example:

MobX uses Entgra IoT Server to manage and monitor the employees mobile devices and applications. Alex joins as the new Engineering team manager and needs to register the personal mobile device with Entgra IoT Server, but is concerned because Alex doesn't want to expose the personal data on the device to the Organization. On the other hand, MobX is concerned about not letting the other applications installed in Alex's device to access the confidential enterprise data. For example, Alex has installed an application for enterprise docs on the device. This application has access to all the enterprise docs and the personal docs as they are all stored in the same location. Therefore, it is important to clearly separate the enterprise and personal data in a BYOD device enrollment scenario. Follow the steps given below to enable data containerization on your device.













Follow the instructions given below to set up the Android work profile:

1.  Tap **INSTALL** to start installing the Android agent.

    ![image](352816582.png)

2.  Tap **OPEN**, once the Entgra Android Agent is successfully installed.  

3.  Tap **SETUP WORK-PROFILE** to proceed with registering the Android device via the Work-Profile.  
    ![image](352816680.png)

4.  Tap **SET UP**.

    ![image](352816708.png)

    

    

    If your device was not encrypted previously, you will be prompted to encrypt the device.

    

    

5.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

    

    

    If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

    

    

    ![image](352816766.png)

6.  Enter the server address based on your environment, in the text box provided and tap **START REGISTRATION**. A confirmation message appears.

    *   Developer Environment - Enter the server IP as your server address.  
        Example: `10.10.10.123:8280`
    *   Deployment Environment - Enter the domain as your server address.

        

        

        The Android Agent app's default port is 8280\. If you are using any other port, the server address should state the new port in the following format: `www.abc.com:<PORT>`, e.g., if the port is 8289 the server IP is as follows: `www.abc.com:8289`.

        

        

    ![image](352816614.png)

7.  Enter your details and tap **SIGN IN**.  

    *   **Organization -** Enter the organization name only if the server is hosted with multi-tenant support or enter the default `carbon.super`, which is the default organization name on a non-multi-tenant environment.
    *   **Username** - Enter the Entgra IoTS username.
    *   **Password** - Enter the Entgra IoTS password.
    
    ![image](352816593.png)  
    Read the policy agreement, and tap **AGREE** to accept the agreement.  
8.  Tap **ALLOW** to allow the Entgra Android Agent to access photos, media, and files, make and manage phone calls, and access the device location respectively.

    ![image](352816714.png)

9.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the do Not Disturb setting enabled will affect the ring, and mute operations. This settings is only shown for Android Nougat and above.

    1.  Tap **OK.**  
        ![image](352816760.png)

    2.  Enable Entgra Device Management for the Do Not Disturb setting.  
        ![image](352816772.png)
    3.  Click **ALLOW**.  
        ![image](352816744.png)
10.  Set a PIN code of your choice with a minimum of 4 digits. A confirmation message appears.

    

    

    You will be prompted to provide a PIN code only if your device is a [BYOD]({{< param doclink >}}key-concepts/#device-ownership) device. The PIN code will be used to secure your personal data. Thereby, Entgra IoT Server will not be able to carry out critical operations on your personal data without using this PIN. 

    Example: A device management admin cannot wipe your device or remove data from the device without the PIN code. You have to provide the PIN code to get your device wiped or you can log into the device management console and wipe your device by entering the PIN code. 

    

    

    ![image](352816696.png)

11.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server.  
    ![image](352816702.png)

Once the registration process is complete, navigate to the launcher of your device. Notice the duplication of application icons. The applications with red icons are the ones used by Entgra IoT Server.

![image](/352816732.png)





To deactivate Android Work Profile:

1.  Navigate to **Settings > Accounts** on your device.
2.  Click **Remove work profile**.
3.  Tap **DELETE** and proceed with the deactivation.  
    ![image](352816738.png)  
    Once the deactivation is complete, navigate to the launcher of the device. Notice the disappearance of the applications with red icons.  
    ![image](352816720.png)















The following subsections will provide details on how data containerization is achieved via the managed-profile feature.

### Setting up the work profile

Data containerization for Android devices was implemented using the Managed Profile feature that is available on the Android devices that support the Android Lollipop OS or upwards. Let's take a look at the how data containerization works on Entgra IoT Server.

*   When you download and install the Android Agent on your Android mobile device, the agent will check if the device supports the managed profile feature. 
*   If the device supports the managed profile feature, the agent will prompt the user to set up the work profile before the installation.

    

    

    Having the Android Lollipop OS version or above will not enable you to set up the work profile. The setup might fail because of the OS customizations that would have been done on some of the devices by the manufacturers.

    

    

    ![image](352823197.png)

*   Once the profile is set up, the Android agent is automatically copied into the new work profile. Therefore, Entgra IoT Server will prompt you to uninstall the agent you downloaded previously as it was installed in the devices personal profile.
*   After setting up the work profile you need to follow the default steps to register an Android device with Entgra IoT Server.  
    Once the registration process is completed, navigate to the launcher of the device and you will be able to see the applications that are used by the worker profile and the personal profile. The applications having the red icon are used by the Entgra IoT Server work profile.  
    ![image](352823202.png)

    

    

    *   Using this approach, you don't have to switch between the personal profile and work profile as all the applications used by each profile is shown in the same launcher. 
    *   Based on the underlying architecture, the profiles have their own storage locations that can not be accessed by each other.

    

    

### Applying Android device operations

After registering your device with Entgra IoT Server you can apply operations on a device. 





[Click here for more information on applying operations on a device]({{< param doclink >}}tutorials/android/android.html#Android-Tryitout).






For more information on the operations that can and can not be applied once data containerization is enabled, see below:

*   The Android agent is the profile owner of the newly created work profile and only has control over it. Therefore, now the agent is unable to perform operations that affect the entire device, such as changing the device PIN and wiping data of the entire device.

*   If your Organization has imposed a policy to restrict the usage of the camera, you will not be able to use the camera application that is installed in the work profile. You will only be allowed to used the camera application that is installed in your personal profile.

*   The enterprise wipe operation will delete the enterprise-related data along with the work profile on your device while keeping the personal data intact.

## Device Ownership Application


A device owner is an application that runs as a device administrator on your Android device. But it has more control over the device than a device administrator as it get's access to a set of unique APIs. This new concept was introduced for Android 5.0 and above, and this type of enrollment is most suitable for Corporate Owned, Personally Enabled (COPE) device enrolments.

Wondering when to use the device ownership application? You can define the Entgra Android agent as the Device Ownership application for devices that are enrolled with Entgra IoT Server and don't have the [system service application](/doc/en/lb2/Integrating-the-Android-System-Service-Application.html) installed. This way you can impose restrictions on the device that you weren't able to impose when the device in the device administrator state. For more information on the restrictions that can be imposed by the device ownership application, see [Available Android Mobile Device Management Policies]({{< param doclink >}}available-mobile-device-management-policies.html).






Please note that the [restriction functions]({{< param doclink >}}available-mobile-device-management-policies.html) of the device ownership application do not work for Samsung devices at the moment.






Let's get started!

### Device ownership application functionality

Let's take a look at how an application having the device ownership settings enabled, function.

*   Once the device ownership is assigned to an application, it gets access to a set of Android APIs, such as adding a user restriction policy on Android devices. These APIs are only accessible by the device ownership application.
*   At a given time a device can only have one application with the device ownership settings. It prevents another device ownership application from overriding the policies that have already been enforced on the device. 
*   If you wish to remove the device ownership from the application, you need to factory reset the device. For example, in a situation where the device needs to be given to a different user, you need to factory reset the device to remove the application.

### Configuring the Entgra Android agent



Before you begin



Make sure to have the Android agent installed on your device. For more information, see [enrolling an Android device]({{< param doclink >}}tutorials/android/android.html).






Let's take a look at how Entgra IoT Server configures the device ownership settings on the agent.

Assign the device ownership to the Entgra Android agent using one of the following methods.





The device ownership settings only work for SDK API levels 21 and above.





*   A command issued through the Android Debug Bridge (adb).

    

    1.  [Make sure to have your Android device enrolled with Entgra IoT Server]({{< param doclink >}}tutorials/android/android.html). 


    2.  If you have set up a Google account for your device, make sure to remove it before proceeding to the next step.

    3.  Connect the device to your PC and run the command given below:

        `adb shell dpm set-device-owner io.entgra.iot.agent/org.wso2.iot.agent.services.AgentDeviceAdminReceiver`

        

        

        The above command is not supported by SDK API levels below 21 because it does not support `Device Policy Manager` `(dpm)` that is used in the command.

        

        

        

        

        

        The Android system creates an XML file in the device's `<DEVICE>/data/system` directory once you run the command to assign the device ownership settings in the agent application as shown in step1.  
        The name of the XML will be `device_owner` and it contains the package name of the service application having the device ownership.

        Example:

        **device_owner.xml**

        `<device-owner package="org.wso2.iot.agent" name="Your app name" />`

        

        

        A non-system application cannot modify the XML file that is in the device's `<DEVICE>/data/system`  directory unless it's rooted. Therefore Android guarantees that only the configured application gets the privileged device ownership status.

        

        

        

        

    

*   Integrating Android for work. 

    

    

    For more information on how this is done, see [the content under Google account method in the Android Developer Guide.](https://developers.google.com/android/work/prov-devices#google_account_method)

    

    



The device ownership settings can also be assigned through a special Near Field Communication (NFC) message. Since this is a new feature, not all Android devices will have it. Therefore, this feature will not be supported by IoT Server 3.2.0.



### Integrating the service application with Entgra IoT Server

To successfully integrate the service application with Entgra IoT Server you need to have the Entgra Android agent installed as well. Take a look at the diagram given below to clearly understand the role of the Android agent in this scenario.

![image](352823219.png)

## Upgrading Firmware of an Android Device





This feature is applicable only for COPE devices where the organization has devices that are manufactured specifically for them (OEM scenarios) and you need to have a battery level of 50% or higher to upgrade the firmware of an Android Device.





Many organizations manufacture devices to suit their business requirement and to improve the efficiency and effectiveness of work carried out by their employees. For example, if it's an organization related to media, they require mobile devices that have a good voice and video recording capabilities so that employees can use it in their day to day work. These devices may include a firmware and you are able to upgrade the firmware to a newer version over-the-air (OTA) with Entgra IoT Server. For more information on OTA firmware upgrading, see [OTA updates](https://source.android.com/devices/tech/ota/).

### Configuring Entgra IoT Server to handle firmware upgrades

**Prerequisite**



Enable the Android system service application. For more information, see [Integrating the Android System Service Application](/doc/en/lb2/Integrating-the-Android-System-Service-Application.html#IntegratingtheAndroidSystemServiceApplication-Integratingthesystemserviceapplication).



Follow the configuration steps given below to upgrade the firmware of your device.

1.  Configure the `build.prop` file ([click here for a sample build.prop file](attachments/352823254/352823266.prop)) by updating the release version. The version you provide has to be greater than the previous version of the firmware, for the upgrade to take place.

    `ro.build.version.release=<VERSION>`

2.  Host the files mentioned below in your OTA server, under the subdirectory with your device name.
    *   `build.prop` file.
    *   `<DEVICE_NAME>.ota.zip` file, which is the firmware upgrade package file.

        

        

        The value defined for the [`android.os.Build.PRODUCT`](https://developer.android.com/reference/android/os/Build.html#PRODUCT) parameter needs to given as the `<DEVICE_NAME>`. This information is available on most Android devices or you can get it by running a sample application that will print the `android.os.Build.PRODUCT` parameter value in the log.

        

        

        Example: If the device name is `c1ktt`, the hosted file URLs will be of the following format:
    *   `http://<HOST>:<PORT>/<SUB_DIRECTORY>/<DEVICE_NAME>.ota.zip` Example: `http://10.10.10.227:8000/c1ktt/c1ktt.ota.zip`
3.  Configure the following fields in the `org.wso2.emm.system.service.utils.Constants` class.

    public static final String DEFAULT_OTA_SERVER_ADDRESS = "<OTA_SERVER_ADDRESS>";
    public static final String DEFAULT_OTA_SERVER_PROTOCOL = "http";
    public static final int DEFAULT_OTA_SERVER_PORT = <PORT>;
    public static final String DEFAULT_OTA_SERVER_SUB_DIRECTORY = "<SUB_DIRECTORY>"; 
    public static final int REQUIRED_BATTERY_LEVEL_TO_FIRMWARE_UPGRADE = <REQUIRED_BATTERY_LEVEL>;

    

    

    To safely complete the upgrade, the device needs to have a battery level of 50% or more. If it's below 50% the upgrade will not continue until the device is charged.

    

    

    Example:

    public static final String DEFAULT_OTA_SERVER_ADDRESS = "10.10.10.227";
    public static final String DEFAULT_OTA_SERVER_PROTOCOL = "http";
    public static final int DEFAULT_OTA_SERVER_PORT = 8000;
    public static final String DEFAULT_OTA_SERVER_SUB_DIRECTORY = "c1kt"; 
    public static final int REQUIRED_BATTERY_LEVEL_TO_FIRMWARE_UPGRADE = 50;

4.  Configure the following fields in the `org.wso2.emm.agent.utils.Constants` class.

    `public static final int FIRMWARE_UPGRADE_RETRY_COUNT = <NUMBER_OF_RETRIES_ON_FAILURE>;`

    Example:

    `public static final int FIRMWARE_UPGRADE_RETRY_COUNT = 5;`

### Scheduling firmware upgrades

Android firmware upgrades can be scheduled via the device management console or using the REST API.

Follow the steps given below to schedule the firmware upgrade via the device management console.

1.  Navigate to the device page of the device you wish to schedule the firmware upgrade. 

    

    

    For more information, [try out the Android device operations]({{< param doclink >}}tutorials/android/android.html#Android-Tryitout).


    

    

2.  Click **Upgrade Firmware**.  
    ![image](352823288.png)
3.  Scheduling the firmware upgrade.
    *   Scheduling the firmware upgrade for a future date.
        1.  Enter the date and time.
        2.  If you want to set a different server URL than the URL given under Android system service constants, enter the server URL in one of the following formats.   
            Example:  

            *   [`http://abc.com`](http://abc.com/)
            *   [`http://abc.com/ota`](http://abc.com/ota)![image](352823277.png)
        3.  Click **YES** to schedule the operation.
    *   Scheduling the firmware upgrade instantly.
        1.  Select **Instant Upgrade**. 
        2.  If you want to set a different server URL than the URL given under the Android system service constants, enter the server URL in one of the following formats.   
            Example:  

            *   [`http://abc.com`](http://abc.com/)
            *   [`http://abc.com/ota`](http://abc.com/ota)![image](352823271.png)
        3.  Click **YES** to schedule the operation instantly.

        ## Reporting Critical Events via Alerts


When managing devices the administrator must be notified of events occurring on the user's devices that reduce the effectiveness and efficiency of the tasks performed by a device, so that he/she can take immediate action and correct it. For example, if a cooperate application is utilizing a high CPU or memory percentage, you as the admin can stop that application from running or uninstall and re-install the application again on the user's device.

In Entgra IoT Server all such events are captured and published to the WSO2 Data Analytics Server (WSO2 DAS). In this section, let's take a look at how Entgra IoT Server creates alerts to report critical issues.

#### Step 1: Enabling the existing event listeners.

1.  Open the `Constants.java` file that is in the `org.wso2.emm.agent.utils` package via a preferred IDE.

    

    

    For more information on the default configuration, see the [`Constants.java` file](https://github.com/wso2/cdmf-agent-android/blob/release-2.0.0/client/client/src/main/java/org/wso2/emm/agent/utils/Constants.java).

    

    

2.  Assign `true` as the value for the `ENABLE_EVENT_LISTENING` property that is in the `EventListners` class.

    

    Why is this step needed?

    

     The `ENABLE_EVENT_LISTENING` property is a global configuration and it needs to be enabled in order to enable event listening.

    

    

3.  Enable the preferred listeners.
    *   `APPLICATION_STATE_LISTENER`
    *   `RUNTIME_STATE_LISTENER`

#### Step 2: Writing a new event listener.

In this section, let's look at how a new event can be captured from the Android agent and published to WSO2 DAS.

**Prerequisite**



 Download and install Android Studio. For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).



1.  Open the Android agent source code in Android studio.  
    The event related logic is included in the `org.wso2.emm.agent.events` package. It has the following folder structure.  
    ![image](352823313.png)
2.  Implement the `AlertEventListener` interface in the `org.wso2.emm.agent.events.listeners` package.

    

    

    

    

    

    The following methods are used:

    *   `startListening()` - This method is used to start listening to an event broadcasted by the OS. In the case of a polling based check, this method can be called by the onReceive method of the AlarmReceiver.
    *   `stopListening()` - Unregister the event receiver or stop an alarm.
    *   `publishEvent(String payload)` - Handle the data published to IoT Server.

    

    

    

    

    Example:

    **AlertEventListener interface**

    package org.wso2.emm.agent.events.listeners;

    /**
     * This is used to define any new events that needs to be captured and sent to server.
     */
    public interface AlertEventListener {

        /**
         * This can be used to start listening to a specific broadcast receiver.
         * Another usage would be, when there is an event that doesn't do a broadcast. For example
         * Application exceeding 75% of CPU is not broadcasted by default from the Android OS. Only way
         * to catch it is by constantly polling a specific API and check for the status. In such a
         * situation, AlarmManager can call startListening on it onReceiver method to do the polling on
         * an API.
         */
        void startListening();

        /**
         * If in case, listening to a specific receiver need to be done here. This can be a place to,
         * stop an AlarmManager.
         */
        void stopListening();

        /**
         * This is where publishing data to EMM/DAS would happen. This can ideally be called from
         * an onReceive method of a BroadcastReceiver, or from startListening method to inform the
         * results of a polling.
         *
         * @param payload JSON string payload to be published.
         * @param type type of the alert being published.
         */
        void publishEvent(String payload, String type);

    }

3.  Create a new class in the[ `org.wso2.emm.agent.events.listeners` package](https://github.com/wso2/product-emm/tree/master/modules/mobile-agents/android/client/client/src/main/java/org/wso2/emm/agent/events/listeners), extend it from `BroadcastReceiver`, and implement the `AlertEventListener` interface.

    Example: Create a class named `ApplicationStateListener`.

    `public class ApplicationStateListener extends BroadcastReceiver implements AlertEventListener {}`

4.  Capturing events. There are two ways to capture the events as listed below:  

    1.  **Listen to events broadcasted by the Android OS.**

        

        

        

        

        This section provides information on how to listen and capture events that are broadcasted by the Android OS.

        **Prerequsite**

        

        Prior knowledge on the Android Broadcast Receivers will help you understand the concept clearly, as Entgra IoT Server uses it to listen to the events. For more information, see [BroadcastReceiver](http://developer.android.com/intl/zh-cn/reference/android/content/BroadcastReceiver.html).

        

        Follow the steps given below to configure Entgra IoT Server, to listen to the broadcasted events:

        1.  Register the receiver in the `startListening()` method of the `AlertEventListener` interface, in the class you created in the `org.wso2.emm.agent.events.listeners` package.

            Example: 

            @Override
            public void startListening() {
              EventRegistry.context.registerReceiver(this, intentFilter);
            }

        2.  When an event is received the `onReceive` method that is in the `org.wso2.emm.agent.events.listeners` package will be notified. At that point you need to extract the data from the intent and populate the bean with the data.

            

            

            The Bean classes are located in the `org.wso2.emm.agent.events.beans` package.

            

            

        3.  Convert the data populated by the bean to a JSON string using the `CommonUtils.toJSON(beanInstance)`.  
            Example:

            ApplicationStatus applicationState = new ApplicationStatus();
            applicationState.setState(status);
            applicationState.setPackageName(packageName);
            String appState = CommonUtils.toJSON(applicationState);

        4.  Publish the data to Entgra IoT Server by calling the `publishEvent` method.  For this you need to configure the `publish` method so that it calls the `HttpDataPublisher`.  
            Example:

            @Override
            public void publishEvent(String payload, String type) {
             EventPayload eventPayload = new EventPayload();
             eventPayload.setPayload(payload);
             eventPayload.setType(type);
             HttpDataPublisher httpDataPublisher = new HttpDataPublisher();
             httpDataPublisher.publish(eventPayload);
            }

        5.  Optionally, if you wish to publish data to a different server other than Entgra IoT Server or use a different protocol, you can do so by writing a new data publisher.  
            Implement the `DataPublisher` interface that is in the package `org.wso2.emm.agent.events.publisher`, to write the new data publisher.

            

            

            

            /**
             * Listening to application state changes such as an app getting installed, uninstalled,
             * upgraded and data cleared.
             */
            public class ApplicationStateListener extends BroadcastReceiver implements AlertEventListener {
                private static final String TAG = ApplicationStateListener.class.getName();

                @Override
                public void startListening() {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
                    intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
                    intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
                    intentFilter.addAction(Intent.ACTION_PACKAGE_DATA_CLEARED);
                    intentFilter.addDataScheme("package");
                    EventRegistry.context.registerReceiver(this, intentFilter);
                }

                @Override
                public void stopListening() {
                    if (EventRegistry.context != null) {
                        EventRegistry.context.unregisterReceiver(this);
                    }
                }

                    @Override
                    public void publishEvent(String payload, String type) {
                        EventPayload eventPayload = new EventPayload();
                        eventPayload.setPayload(payload);
                        eventPayload.setType(type);
                        HttpDataPublisher httpDataPublisher = new HttpDataPublisher();
                        httpDataPublisher.publish(eventPayload);
                    }

                @Override
                public void onReceive(Context context, final Intent intent) {
                    String status = null;
                    ApplicationStatus applicationState;
                    switch (intent.getAction()) {
                        case Intent.ACTION_PACKAGE_ADDED:
                            status = "added";
                            break;
                        case Intent.ACTION_PACKAGE_REMOVED:
                            status = "removed";
                            break;
                        case Intent.ACTION_PACKAGE_REPLACED:
                            status = "upgraded";
                            break;
                        case Intent.ACTION_PACKAGE_DATA_CLEARED:
                            status = "dataCleared";
                            break;
                        default:
                            Log.i(TAG, "Invalid intent received");
                    }
                    if (status != null) {
                        String packageName = intent.getData().getEncodedSchemeSpecificPart();
                        applicationState = new ApplicationStatus();
                        applicationState.setState(status);
                        applicationState.setPackageName(packageName);
                        try {
                            String appState = CommonUtils.toJSON(applicationState);
                            publishEvent(appState, Constants.EventListners.APPLICATION_STATE);
                            if (Constants.DEBUG_MODE_ENABLED) {
                                Log.d(TAG, appState);
                            }
                        } catch (AndroidAgentException e) {
                            Log.e(TAG, "Could not convert to JSON");
                        }
                    }
                }
            }

            

            

        6.  Open the `AndroidManifest.xml` file that is in the [`<Android_agent>/client/src/main` folder](https://github.com/wso2/cdmf-agent-android/tree/release-2.0.0/client/client/src/main), and add a new receiver. The name of the receiver should be the same as the listener class you created in [step 1](about:blank#ReportingCriticalEventsviaAlerts-step1).  
            Example:

            **Receiver**

            <receiver android:name=".events.listeners.ApplicationStateListener">
                <intent-filter>
                    <action android:name="android.intent.action.PACKAGE_ADDED" />
                    <action android:name="android.intent.action.PACKAGE_REMOVED" />
                    <action android:name="android.intent.action.PACKAGE_REPLACED" />
                    <action android:name="android.intent.action.PACKAGE_DATA_CLEARED" />
                    <data android:scheme="package"/>
                </intent-filter>
            </receiver>

            Now you have set up the listener.

        7.  Call the `startListening` method from the `register` method, to register the listener. The `register` method is in the `EventRegistry` class.

            

            

            For more information on the default configuration, see the [`EventRegistry.java` file](https://github.com/wso2/cdmf-agent-android/blob/release-2.0.0/client/client/src/main/java/org/wso2/emm/agent/events/EventRegistry.java).

            

            

            Example:

            public void register() {
                // First, check if event listening is enabled. If yes, check each event that is enabled and
                // start event listening.
                if (Constants.EventListners.EVENT_LISTENING_ENABLED) {
                    if (Constants.EventListners.APPLICATION_STATE_LISTENER) {
                        // If the listener is implementing broadcast listener, calling start listener
                        // should start listening for events.
                        ApplicationStateListener applicationState = new ApplicationStateListener();
                        applicationState.startListening();
                    }
                    if (Constants.EventListners.RUNTIME_STATE_LISTENER) {
                        // If the event is running on a scheduled polling, it is only necessary to schedule
                        // the alarm manager. If the same DEFAULT_START_TIME and DEFAULT_INTERVAL
                        // can be used for any new event, there is no need to create a new
                        // scheduled alarm here.
                        EventRegistry.startDefaultAlarm(Constants.EventListners.DEFAULT_LISTENER_CODE,
                                                        Constants.EventListners.DEFAULT_START_TIME,
                                                        Constants.EventListners.DEFAULT_INTERVAL);
                    }
                }
            }

        

        

        

    2.  **Poll an API to check for event changes that are not broadcasted continuously by the Android OS.**
    

    ### Listening to Events Broadcasted by the Android OS

This section provides information on how to listen and capture events that are broadcasted by the Android OS.

**Prerequsite**


Prior knowledge on the Android Broadcast Receivers will help you understand the concept clearly, as Entgra IoT Server uses it to listen to the events. For more information, seeÂ [BroadcastReceiver](http://developer.android.com/intl/zh-cn/reference/android/content/BroadcastReceiver.html).



Follow the steps given below to configure Entgra IoT Server, to listen to the broadcasted events:

1.  Register the receiver in theÂ `startListening()`Â method of theÂ `AlertEventListener` interface, in the class you created in the `org.wso2.emm.agent.events.listeners`Â package.

    Example:Â 

    @Override
    public void startListening() {
      EventRegistry.context.registerReceiver(this, intentFilter);
    }

2.  When an event is received theÂ `onReceive` method that is in theÂ `org.wso2.emm.agent.events.listeners`Â package will be notified. At that point you need to extract the data from the intent and populate the bean with the data.

    

    

    The Bean classes are located in theÂ `org.wso2.emm.agent.events.beans` package.

    

    

3.  Convert the data populated by the bean to a JSON string using theÂ `CommonUtils.toJSON(beanInstance)`.  
    Example:

    ApplicationStatus applicationState = new ApplicationStatus();
    applicationState.setState(status);
    applicationState.setPackageName(packageName);
    String appState = CommonUtils.toJSON(applicationState);

4.  Publish the data to Entgra IoT Server by calling the `publishEvent` method. Â For this you need to configure the `publish` method so that it calls the `HttpDataPublisher`.  
    Example:

    @Override
    public void publishEvent(String payload, String type) {
     EventPayload eventPayload = new EventPayload();
     eventPayload.setPayload(payload);
     eventPayload.setType(type);
     HttpDataPublisher httpDataPublisher = new HttpDataPublisher();
     httpDataPublisher.publish(eventPayload);
    }

5.  Optionally, if you wish to publish data to a different server other than Entgra IoT Server or use a different protocol, you can do so by writing a new data publisher.  
    Implement the `DataPublisher` interface that is in the packageÂ `org.wso2.emm.agent.events.publisher`, to write the new data publisher.

    

    

    

    /**
     * Listening to application state changes such as an app getting installed, uninstalled,
     * upgraded and data cleared.
     */
    public class ApplicationStateListener extends BroadcastReceiver implements AlertEventListener {
        private static final String TAG = ApplicationStateListener.class.getName();

        @Override
        public void startListening() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_REPLACED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_DATA_CLEARED);
            intentFilter.addDataScheme("package");
            EventRegistry.context.registerReceiver(this, intentFilter);
        }

        @Override
        public void stopListening() {
            if (EventRegistry.context != null) {
                EventRegistry.context.unregisterReceiver(this);
            }
        }

            @Override
            public void publishEvent(String payload, String type) {
                EventPayload eventPayload = new EventPayload();
                eventPayload.setPayload(payload);
                eventPayload.setType(type);
                HttpDataPublisher httpDataPublisher = new HttpDataPublisher();
                httpDataPublisher.publish(eventPayload);
            }

        @Override
        public void onReceive(Context context, final Intent intent) {
            String status = null;
            ApplicationStatus applicationState;
            switch (intent.getAction()) {
                case Intent.ACTION_PACKAGE_ADDED:
                    status = "added";
                    break;
                case Intent.ACTION_PACKAGE_REMOVED:
                    status = "removed";
                    break;
                case Intent.ACTION_PACKAGE_REPLACED:
                    status = "upgraded";
                    break;
                case Intent.ACTION_PACKAGE_DATA_CLEARED:
                    status = "dataCleared";
                    break;
                default:
                    Log.i(TAG, "Invalid intent received");
            }
            if (status != null) {
                String packageName = intent.getData().getEncodedSchemeSpecificPart();
                applicationState = new ApplicationStatus();
                applicationState.setState(status);
                applicationState.setPackageName(packageName);
                try {
                    String appState = CommonUtils.toJSON(applicationState);
                    publishEvent(appState, Constants.EventListners.APPLICATION_STATE);
                    if (Constants.DEBUG_MODE_ENABLED) {
                        Log.d(TAG, appState);
                    }
                } catch (AndroidAgentException e) {
                    Log.e(TAG, "Could not convert to JSON");
                }
            }
        }
    }

    

    

6.  Open the `AndroidManifest.xml` file that is in the [`<Android_agent>/client/src/main`Â folder](https://github.com/wso2/cdmf-agent-android/tree/release-2.0.0/client/client/src/main), and add a new receiver. The name of the receiver should be the same as the listener class you created in [step 1](about:blank#ListeningtoEventsBroadcastedbytheAndroidOS-step1).  
    Example:

    **Receiver**

    <receiver android:name=".events.listeners.ApplicationStateListener">
        <intent-filter>
            <action android:name="android.intent.action.PACKAGE_ADDED" />
            <action android:name="android.intent.action.PACKAGE_REMOVED" />
            <action android:name="android.intent.action.PACKAGE_REPLACED" />
            <action android:name="android.intent.action.PACKAGE_DATA_CLEARED" />
            <data android:scheme="package"/>
        </intent-filter>
    </receiver>

    Now you have set up the listener.

7.  Call theÂ `startListening`Â method from the `register` method, to register the listener. The `register` method is in the `EventRegistry` class.

    

    

    For more information on the default configuration, seeÂ the [`EventRegistry.java`Â file](https://github.com/wso2/cdmf-agent-android/blob/release-2.0.0/client/client/src/main/java/org/wso2/emm/agent/events/EventRegistry.java).

    

    

    Example:

    public void register() {
        // First, check if event listening is enabled. If yes, check each event that is enabled and
        // start event listening.
        if (Constants.EventListners.EVENT_LISTENING_ENABLED) {
            if (Constants.EventListners.APPLICATION_STATE_LISTENER) {
                // If the listener is implementing broadcast listener, calling start listener
                // should start listening for events.
                ApplicationStateListener applicationState = new ApplicationStateListener();
                applicationState.startListening();
            }
            if (Constants.EventListners.RUNTIME_STATE_LISTENER) {
                // If the event is running on a scheduled polling, it is only necessary to schedule
                // the alarm manager. If the same DEFAULT_START_TIME and DEFAULT_INTERVAL
                // can be used for any new event, there is no need to create a new
                // scheduled alarm here.
                EventRegistry.startDefaultAlarm(Constants.EventListners.DEFAULT_LISTENER_CODE,
                                                Constants.EventListners.DEFAULT_START_TIME,
                                                Constants.EventListners.DEFAULT_INTERVAL);
            }
        }
    }
        

        

    ### Listening to Events Not Broadcasted by the Android OS    

        This section describes how Entgra IoT Server is configured to check the status of the events that are not broadcasted by the OS. This is done by continuously polling the relevant APIs. 

        Follow the steps given below to check the status of the events via the Android API when the alarm is triggered.

        1.  Configure the `startListening()` method of the `AlertEventListener` interface, for the API to poll continuously, and check if any events have triggered the alarm.

            

            

            The `startListening()` method needs to be configured in the new class you created. This class is in the `org.wso2.emm.agent.events.listeners` package.

            

            

            @Override
            public void startListening() {
             RuntimeInfo runtimeInfo = new RuntimeInfo(EventRegistry.context, new String[] {
              "top",
              "-n",
              "1",
              "-m",
              "1",
              "-s",
              "cpu"
             });
             Application application = runtimeInfo.getHighestCPU();
             if (application.getCpu() > CPU_THRESHOLD) {
              try {
               String appState = CommonUtils.toJSON(application);
               publishEvent(appState, Constants.EventListners.RUNTIME_STATE);
               if (Constants.DEBUG_MODE_ENABLED) {
                Log.d(TAG, appState);
               }
              } catch (AndroidAgentException e) {
               Log.i(TAG, "Could not convert to JSON");
              }
             }
            }

        2.  When an event is received, the `onReceive` method that is in the `org.wso2.emm.agent.events.listeners` package is notified. At that point you need to extract the data from the intent and populate the bean with the data.

            

            

            The Bean classes are located in the `org.wso2.emm.agent.events.beans` package.

            

            

        3.  Convert the data populated by the bean to a JSON string using the `CommonUtils.toJSON(beanInstance)`.  
            Example:

            ApplicationStatus applicationState = new ApplicationStatus();
            applicationState.setState(status);
            applicationState.setPackageName(packageName);
            String appState = CommonUtils.toJSON(applicationState);

        4.  Publish the data to Entgra IoT Server by calling the `publishEvent` method. For this you need to configure the `publish` method so that it calls the `HttpDataPublisher`.  
            Example:

            @Override
            public void publishEvent(String payload, String type) {
             EventPayload eventPayload = new EventPayload();
             eventPayload.setPayload(payload);
             eventPayload.setType(type);
             HttpDataPublisher httpDataPublisher = new HttpDataPublisher();
             httpDataPublisher.publish(eventPayload);
            }

        5.  If you wish to periodically poll the APIs, you need to schedule the AlarmManager. This can be done via two options, depending on your requirement.

            

            

            For more information on the AlarmManager concept, see [AlarmManager](http://developer.android.com/intl/zh-cn/reference/android/app/AlarmManager.html).

            

            

            *   **Option 1: Setting up the default alarm** Use this option, if you wish to use the existing `DEFAULT_START_TIME` and `DEFAULT_INTERVAL` values. 

                1.  In this method, you don't have to do any changes to the default `EventRegistry.startAlarm` method that is in the `org.wso2.emm.agent.events` package.

                    

                    

                    *   `DEFAULT_START_TIME`: The time the alarm should first go off, in milliseconds. The default value is 1000.
                    *   `DEFAULT_INTERVAL`: The interval between subsequent repeats of the alarm, in milliseconds. The default value is 30000.
                    *   For more information on the default configuration, see [`EventRegistry.java` file](https://github.com/wso2/product-emm/blob/master/modules/mobile-agents/android/client/client/src/main/java/org/wso2/emm/agent/events/EventRegistry.java).

                    

                    

                2.  Register the new listener by adding an `or` condition to the existing `if` condition.

                Example:

                if (Constants.EventListners.RUNTIME_STATE_LISTENER || Constants.EventListners.NEWLY_ADDED_LISTENER) {
                  // If the event is running on a scheduled polling, it is only necessary to schedule
                  // the alarm manager. If the same DEFAULT_START_TIME and DEFAULT_INTERVAL
                  // can be used for any new event, there is no need to create a new
                  // scheduled alarm here.
                  EventRegistry.startAlarm(Constants.EventListners.DEFAULT_LISTENER_CODE,
                    Constants.EventListners.DEFAULT_START_TIME,
                    Constants.EventListners.DEFAULT_INTERVAL);
                }


                Use this method, if you wish to change the `DEFAULT_START_TIME` and `DEFAULT_INTERVAL` values. 

                

                

                You will be using the same `EventAlarmReceiver` class, which is in the `org.wso2.emm.agent.events` package, in both option 1 and option 2.

                

                

                1.  Configure the `DEFAULT_LISTENER_CODE`, `DEFAULT_START_TIME`, and `DEFAULT_INTERVAL` values.

                    EventRegistry.startAlarm(Constants.EventListners.DEFAULT_LISTENER_CODE,
                      Constants.EventListners.DEFAULT_START_TIME,
                      Constants.EventListners.DEFAULT_INTERVAL);

                    Example:

                    EventRegistry.startDAlarm(Constants.EventListners.NEWLY_ADDED_LISTENER_CODE,
                                                        Constants.EventListners.3000,
                                                        Constants.EventListners.20000);

                    

                    *   `DEFAULT_LISTENER_CODE`: The code used by the `EventAlarmReceiver` to identify the event listeners. Make sure to change this field to a preferred value.

                    *   `DEFAULT_START_TIME`: Define the time the alarm should first go off, in milliseconds.
                    *   `DEFAULT_INTERVAL`: Define the interval between subsequent repeats of the alarm, in milliseconds.
                    *   For more information on the default configuration, see [`EventRegistry.java` file](https://github.com/wso2/cdmf-agent-android/blob/release-2.0.0/client/client/src/main/java/org/wso2/emm/agent/events/EventRegistry.java).

                    

                    

                2.  Configure the `EventAlarmReceiver` class that is in the `org.wso2.emm.agent.events` package.
                    1.  Configure the `Constants.EventListners.DEFAULT_LISTENER_CODE`  property by updating it with the new code you defined in [step a](about:blank#ReportingCriticalEventsviaAlerts-stepa).

                        `if (requestCode == Constants.EventListners.DEFAULT_LISTENER_CODE)`

                        Example:

                        `if (requestCode == Constants.EventListners.NEWLY_ADDED_LISTENER_CODE)`

                    2.  Write a logic inside the `startListening()` to periodically poll the API.

                        if (requestCode == Constants.EventListners.DEFAULT_LISTENER_CODE) {
                            RuntimeStateListener runtimeStateListener = new RuntimeStateListener();
                          runtimeStateListener.startListening();
                        }

    ## Integrating with the Entgra IoT Analytics Profile


This section explains how to configure Entgra IoT Server if you have changed the default ports or how to configure Entgra IoT Server in a production environment, to publish events received from the Android agent. 





You don't need to follow the steps given below if you are not running in a production environment or if you are using the default settings of Entgra IoT Server.





Follow the steps given below:

    Skip this step if you are running Entgra IoT Server in a developer environment.  

    *   `ReceiverServerURL`: The URL must be in the `tcp://<HOSTNAME>:<PORT>` format. The default HTTP port for the analytics profile in Entgra IoT Server is 7613.

        

        

        If you want to change the TCP URL, see [Setting up Multi Receiver and Load Balancing Data Agent](https://docs.wso2.com/display/DAS300/Setting+up+Multi+Receiver+and+Load+Balancing+Data+Agent) in the WSO2 Data Analytics Server (DAS) documentation.

        

        

    *   `AdminUsername`: Provide the administrator username. The default username is `admin`.
    *   `AdminPassword`: Provide the password of the administrator. The default password is `admin`.

    Example: Single receiver

    <AnalyticsConfiguration>
       <Enabled>true</Enabled>
       <ReceiverServerUrl>tcp://localhost:7613</ReceiverServerUrl>
       <AdminUsername>admin</AdminUsername>
       <AdminPassword>admin</AdminPassword>
    </AnalyticsConfiguration>


    

    

    The default HTTPS port of the analytics profile in Entgra IoT Server is 9445.


## Setting Up Single-Purpose Devices


In this document, you will set up and use Corporate-Owned, Single-Use (COSU) devices also known as a kiosk devices with Entgra IoT Server. COSU type enrollment is most suitable for COPE type of device enrolment usecases.



Before you begin



*   You will need a kiosk device. For testing purposes, you can use a factory reset device.  
*   Your device needs to support Near Filed Communication (NFC).
*   In Entgra IoT Server this feature is only supported for devices that are Android 6.0 and above.
*   Download and install Android Studio. For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).





### Use case

Alive & Healthy.Inc is a pharmaceutical company that distributes its products to many hospitals around the area. There had been many complaints about their delivery staff dropping off wrong products at the hospital. Therefore, the management decided to provide kiosk devices to the delivery staff to make it easy for them to track where the packages need to be delivered. Take a look at how this helped Alive & Healthy.Inc:

*   Let's say the delivery employee is Alex. Alex Near Field Communication (NFC) bumps the kiosk device using his device to get the details of the package he needs to deliver.
*   Once Alex's details are recognized by the system, the details of the package that needs to be delivered are sent to the device.  
*   Alex then goes to the collection point and request for the package by showing the package details.
*   Once the package is received, Alex scans the QR code on it to get the details and direction of the drop off point.
*   On reaching the delivery point, Alex hands off the package to the hospital and get the signature on the goods received note.
*   After completing the task and after the data is recorded, the kiosk device erases Alex's details and goes back to the default state.  
    Once back at Alive & Healthy.Inc, Alex returns the device and waits for his call again. 

To make the above scenario work, you need to install the required applications on the device and make sure the devices are managed correctly. Let's take a look how you can use Entgra IoT Server to configure and manage your single-purpose/kiosk devices.

### Step 1: Customizing the Android Agent

The Android agent needs to be customized to enable the kiosk features. Follow the steps given below:





Before you begin, you need to have Android Studio installed. For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).





1.  Replace the `android.jar` file that is in the `<ANDROID>/sdk/platforms/android-<CURRENT_API_LEVEL>` directory with the `android.jar` file found [here](https://github.com/anggrayudi/android-hidden-api/tree/master/android-25).

    

    Why is this needed?

    

    The Entgra Android agent requires access to hidden APIs (APIs that are available at runtime). Therefore, you need to replace the `android.jar` file as mentioned in this step.

    

    

2.  [Download or clone the Android agent source code](https://github.com/wso2/cdmf-agent-android). The folder will be referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout the documentation.
3.  Open the client folder that is in the `<ANDROID_AGENT_SOURCE_CODE>/client` directory via Android Studio.

4.  Entgra IoT Server supports API levels 16 to 25\. Therefore, install the Android API levels 16 to 25 on Android Studio.
    1.  Open Android Studio.
    2.  Click **SDK Manager**. **![image](2.png)**
    3.  Click the **SDK Platform **tab and select the 16 and 25 API levels.  
        Example:  
        ![image](352821327.png)
    4.  Click **Apply**.
5.  Open the `build.gradle` file, and configure the parameters given below under the `**debug**` build variant.

    

    

    To make sure you are building the correct build variant via Android Studio, click **Build > Select Build Variants** and check what's been built.

    

    

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <td>
            <p><code>DEFAULT_OWNERSHIP</code></p>
          </td>
          <td>Define COSU as the value.</td>
        </tr>
        <tr>
          <td>
            <p><code>DEFAULT_HOST</code></p>
          </td>
          <td>
            
              <p>Define <code>&lt;NETWORK_IP&gt;:8280</code> as the value for HTTP communication.</p>
              
                
                  <p>All devices must be connected to the same network.</p>
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

    Example: 

    // Set DEFAULT_OWNERSHIP to null if no overiding is needed. Other possible values are,
    // BYOD or COPE. If you are using the mutual SSL authentication
    // This value must be set to a value other than null.
    buildConfigField "String", "DEFAULT_OWNERSHIP", "\"COSU\""
    //DEFAULT_HOST - Hardcode the server host to avoid having the user type it during
    //enrollment. If the user must type the hostname/IP during enrollment, leave it as null.
    buildConfigField "String", "DEFAULT_HOST", "\"http://192.168.12.110:8280\""

    

    Let's take a look at other configurations you can do to improve the Android agent by configuring the `build.gradle` file as listed below:

    *   Disable the `DISPLAY_WIPE_DEVICE_BUTTON` and `COSU_SECRET_EXIT` properties in a production environment. These are enabled by default so that you can use the kiosk device in a testing environment by factory resetting the device or by clicking the Entgra logo six times to exit the COSU mode. Therefore, we strongly recommend disabling these settings in the production environment.

        buildConfigField "boolean", "DISPLAY_WIPE_DEVICE_BUTTON", "false"
        buildConfigField "boolean", "COSU_SECRET_EXIT", "false"

    *   Optionally, enable the `HIDE_LOGIN_UI` property to hide the login page.

        `buildConfigField "boolean", "HIDE_LOGIN_UI", "true"`

    

6.  Build the project to create a new APK with the changes. The new `client-debug.apk` file is created in the `<` `ANDROID_AGENT_SOURCE_CODE>/client/client/build/outputs/apk` directory.

7.  Run the following command to get the checkSum value of the agent, which was created in [locally-built-APK](about:blank#SettingUpSingle-PurposeDevices-locally-built-APK).

    `cat <THE FILE PATH OF THE APK FILE YOU CREATED IN STEP 1.6.> | openssl dgst -binary -sha1 | openssl base64 | tr '+/' '-_' | tr -d '='`

    Example:

    `cat <ANDROID_AGENT_SOURCE_CODE>/client/client/build/outputs/apk/client-debug.apk | openssl dgst -binary -sha1 | openssl base64 | tr '+/' '-_' | tr -d '='`

8.  Upload the APK file to a public location, such as GitHub. You will need this step later on when customizing the NFC provisioning application.

    

    

    To get the downloadable APK file path from GitHub, click on the uploaded file, right click on **view raw**, and copy the link. Use this link when customizing the NFC provisioning application.

    

    

### Step 2: Provisioning the Kiosk Device

Entgra offers various provisioning mechanisms available for your single purpose devices. Provisioning means the process of setting up the device for management by your company. You send all relevant configurations via an encrypted line to the device to setup components such as Wifi, Time zone, Locale and etc. This document consists of the following provisioning methods supported by Entgra,

1.  NFC Provisioning.
2.  QR Provisioning.
3.  ADB Provisioning.

#### NFC Provisioning

##### i. Customizing the Entgra NFC Provisioning application

You need to configure the NFC provisioning application to provision the Entgra Android agent. Let's take a look at how it's done.

1.  [Download or clone the](https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android) [`devicemgt-nfc-provisioning-android`](https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android) from GitHub.
2.  Import the project into Android Studio.

    

    Before you begin

    

    Make sure to have the following:

    *   Android SDK 25
    *   Android Build Tools v25.0.2
    *   Android Support Repository

    

    

3.  Open the `build.gradle` file, and configure the `DEFAULT_HOST` parameter that is under the `**debug **`build variant.

    *   If you are using HTTP for testing purposes, configure the parameter as shown below.

        `buildConfigField "String", "DEFAULT_HOST", "\"http://172.20.10.2:8280\""`

    *   If you are using HTTPS in a production environment, configure the parameter as shown below.

        `buildConfigField "String", "DEFAULT_HOST", "\"https://172.20.10.2:8243\""`

        

        

        To successfully configure the application for HTTPS, you need to follow the same steps given under [Configuring Android for HTTPS Communication](/doc/en/lb2/Configuring-Android-for-HTTPS-Communication.html).

        

        

4.  Define the values that need to be entered in the NFC application by configuring the `devicemgt-nfc-provisioning-android/resources/app/res/values/configurations.xml` file. These values are used in [step3.3](about:blank#SettingUpSingle-PurposeDevices-step3.3). 

    

    

    If you want to change the values once the application is installed, you need to update the settings in the applications as shown in [s](about:blank#SettingUpSingle-PurposeDevices-s).

    

    

    <resources>
        <string name="package_name">io.entgra.iot.agent</string>
        <string name="package_download_location">http://192.168.8.151:8000/client-debug.apk</string>
        <string name="package_checksum">3ZqkN92u0I7jbQoDuIrOaOZ7IPE</string>
        <string name="wifi_ssid">Dialog 4G</string>
        <string name="wifi_security_type" >WPA</string>
        <string name="wifi_password">123456</string>
        <string name="time_zone">Asia/Colombo</string>
        <string name="locale">en_US</string>
        <string name="encryption">false</string>
        <string name="kiosk_app_download_location">http://192.168.8.151:8000/Calculator.apk</string>
    </resources>

    Let's get to know the properties that are used.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>
            <p><code>package_name</code></p>
          </th>
          <td>The package name of the Entgra Android agent. Don't change the default value defined here.</td>
        </tr>
        <tr>
          <th>
            <p><code>package_download_location</code></p>
          </th>
          <td>
            <p>The path of the APK file you hosted in <a href="#SettingUpSingle-PurposeDevices-step1.8">step1.8</a>.</p>
          </td>
        </tr>
        <tr>
          <th>
            <p><code>package_checksum</code></p>
          </th>
          <td>
            <p>The checksum value of the agent you got in <a href="#SettingUpSingle-PurposeDevices-step1.7">step1.7</a>.</p>
          </td>
        </tr>
        <tr>
          <th>
            <p><code>wifi_ssid</code></p>
          </th>
          <td>The SSID of the Wi-Fi network that should be used during NFC device owner provisioning for downloading the mobile device management application.&nbsp;</td>
        </tr>
        <tr>
          <th>
            <p><code>wifi_security_type</code></p>
          </th>
          <td>The Wi-Fi security type of your network. The most common types of wireless security are&nbsp;Wired Equivalent Privacy&nbsp;(WEP), Wi-Fi Protected Access (WPA) and NONE if there is no password.</td>
        </tr>
        <tr>
          <th><code>wifi_password</code></th>
          <td>The password of the Wi-Fi network.</td>
        </tr>
        <tr>
          <th>
            <p><code>time_zone</code></p>
          </th>
          <td>The time zone that your network belongs to. <a href="https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android/blob/master/app/src/main/res/values/arrays.xml#L628-L1221" class="external-link" rel="nofollow">Android only recognizes specific time zones</a>. Therefore, make sure to add a time zone that is already specified.<br>Example: Asia/Colombo</td>
        </tr>
        <tr>
          <th>
            <p><code>locale</code></p>
          </th>
          <td>The language used. <a href="https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android/blob/master/app/src/main/res/values/arrays.xml#L1926-L2623" class="external-link" rel="nofollow">Android only recognizes specific languages</a>. Therefore, make sure to add a language that is already specified.<br>Example: en_US</td>
        </tr>
        <tr>
          <th>
            <p><code>encryption</code></p>
          </th>
          <td>If the default value is set to false the kiosk device will not be encrypted when NFC bumped. Therefore, define the default value as <strong>false</strong>.</td>
        </tr>
        <tr>
          <th>
            <p><code>kiosk_app_download_location</code></p>
          </th>
          <td>
            
              <p>This property is not required for the standalone pack and is only required for a cloud deployment. Define the APK location of the app you want to download after provisioning your KIOSK device.<br>If you want to enable this setting, open the app <code>build.gradle</code> file and configure the <code>PUSH_KIOSK_APP</code> property that is under the <strong><code>debug</code> </strong>variant as follows:</p>
              
                
                  
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

5.  Optionally, configure the settings to access the NFC application via Firebase Remote Configurations. Generally, it is the device management administrator who defines the default values for the above step. If you want an individual other than the device management administrator to NFC bump the devices, you need to follow the steps given here.

    

    

    

    1.  To enable the firebase remote configuration for the NFC application, configure the `USE_REMOTE_CONFIG` prop app in the `build.gradle` file.

        ` buildConfigField "boolean", "USE_REMOTE_CONFIG", "true"`

    2.  Go to the [Firebase API Console](https://console.firebase.google.com/).
    3.  Click **CREATE NEW PROJECT**.  
        ![image](352823389.png)
    4.  Provide a preferred name for the project (example: NFC-Application), select the** country/region**, and click **CREATE PROJECT**.  
        ![image](352823433.png)

        The new project will be created in few seconds, and you will be navigated to the overview page of the project.

    5.  Click **Add Firebase to your Android App** on the overview page of the project.  
        ![image](352823400.png)

    6.  Provide the package name of the WSO2 Android agent, which is `org.wso2.iot.nfcprovisoning` and click **ADD APP**.  
        ![image](352823427.png)  
        The generated  **`google-services.json`**file downloads. If the file didn't download check step g.

    7.  Click **CONTINUE > FINISH** to finish the process and create the application.   
        You will then be navigated to the overview of the created application.
    8.  Optionally, if the `google-services.json` file did not download, follow the steps given below to download it.
        1.  Click on your application and click **settings**.  
            ![image](352823439.png)
        2.  Click the download **google-services.json** button.  
            ![image](352823445.png)
    9.  Replace the `devicemgt-nfc-provisioning-android/app/google-services.json` file with the `google-services.json` file you downloaded.
    10.  Add the fields that need to be shown in the NFC application as shown in [step3.4](about:blank#SettingUpSingle-PurposeDevices-step3.4).

        1.  Click on your firebase application and click **Remote Config** on the left navigation.  
            ![images](352823457.png)
        2.  Add the following parameters and their default values:

            <table style="width: 71.5131%;">
              <colgroup>
                <col style="width: 31.3609%;">
                <col style="width: 68.6391%;">
              </colgroup>
              <tbody>
                <tr>
                  <th>Parameter</th>
                  <th>Description</th>
                </tr>
                <tr>
                  <td><code>prov_package_name</code></td>
                  <td>The package name of the WSO2 Android agent. Make sure to use <strong>org.wso2.iot.agent</strong> as the default value.</td>
                </tr>
                <tr>
                  <td><code>prov_package_locale</code></td>
                  <td>The language used. <a href="https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android/blob/master/app/src/main/res/values/arrays.xml#L1926-L2623" class="external-link" rel="nofollow">Android only recognizes specific languages</a>. Therefore, make sure to add a language that is already specified.<br>Example: en_US</td>
                </tr>
                <tr>
                  <td><code>prov_package_time_zone</code></td>
                  <td>The time zone that your network belongs to. <a href="https://github.com/wso2-extensions/devicemgt-nfc-provisioning-android/blob/master/app/src/main/res/values/arrays.xml#L628-L1221" class="external-link" rel="nofollow">Android only recognizes specific time zones</a>. Therefore, make sure to add a time zone that is already specified.<br>Example: Asia/Colombo</td>
                </tr>
                <tr>
                  <td><code>prov_package_wifi_ssid</code></td>
                  <td>
                    <p>The SSID of the Wi-Fi network that should be used during NFC device owner provisioning for downloading the mobile device management application.&nbsp;</p>
                    <p>If you are unsure of the Wi-Fi network that is being used, do not define a default value. You can enter the Wi-Fi SSID from the application configuration screen.</p>
                  </td>
                </tr>
                <tr>
                  <td><code>prov_package_wifi_type</code></td>
                  <td>The Wi-Fi security type of your network. The most common types of wireless security are&nbsp; Wired Equivalent Privacy &nbsp;( WEP ), Wi-Fi Protected Access (WPA) and NONE if there is no password.<br><br>If you are unsure of the Wi-Fi network that is being used, do not define a default value. You can enter the Wi-Fi type from the application configuration screen.</td>
                </tr>
                <tr>
                  <td><code>prov_package_wifi_password</code></td>
                  <td>The password of your Wi-Fi network.<br><br>If you are unsure of the Wi-Fi network's password, do not define a default value. You can enter the WiFi SSID from the application configuration screen.</td>
                </tr>
                <tr>
                  <td><code>prov_package_skip_encryption</code></td>
                  <td>If the default value is set to true the kiosk device will not be encrypted when NFC bumped. Therefore, define the default value as <strong>true</strong>.</td>
                </tr>
                <tr>
                  <td><code>prov_package_package_location</code></td>
                  <td>The path of the APK file you hosted in <a href="#SettingUpSingle-PurposeDevices-step1.7">step1.7</a>.</td>
                </tr>
                <tr>
                  <td><code>prov_package_package_checksum</code></td>
                  <td>The checksum value of the agent you got in <a href="#SettingUpSingle-PurposeDevices-step2.3">step2.3</a>.</td>
                </tr>
              </tbody>
            </table>

            Example:  
            ![image](352823411.png)

    

    

6.  Build the APK file.  
    Once the build completes, the `.apk` file is created in the `<android-NfcProvisioning>/Application/build/outputs/apk` directory.

##### ii. Installing the NFC application on your device



Before you begin



*   Start the Entgra IoT Server core and analytics scripts respectively.

    cd <IOTS_HOME>/bin  
    ------Linux/Mac OS/Solaris ----------
    ./iot-server.sh
    ./analytics.sh

    -----Windows-----------
    iot-server.bat
    analytics.bat

*   Make sure to enable the Android Beam setting on your device.  
    To enable the Android Beam, tap on **Settings > NFC and Payment** and enable it.
*   Factory reset your kiosk device or use a kiosk device.

    

    

    Once the device is reset do not tap anything, only follow the steps given below.

    

    





1.  Install the NFC application on your device.

    

    

    You need to make the NFC Provisioning APK file publicly accessible, so users can download it onto their device.

    Don't have a place to host the NFC application? You can use Entgra IoT Server for this. [Enroll your device with Entgra IoT Server]({{< param doclink >}}quick-start-guide/mobile-device-app-management/android-device.html), publish the NFC provisioning application to the [app publisher](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813506/Creating+an+Android+Application), and [install them on your device via the app store](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813484/Installing+an+Application+on+a+Device).


    

    

2.  Tap **Install** on your device to finish installing the application.
3.  Sign in to the NFC application using your username, password, and organization. If you are using the application locally, enter **carbon.super** for the organization.  
    Example:  
    ![image](352823406.png)

4.  The default values you defined in [Step2](about:blank#SettingUpSingle-PurposeDevices-Step2) are pushed to the installed NFC application. If you want to change the values, click configurations and update the values.

    Example:   
    ![image](352823422.png)

Now you can start using the application to NFC bump the kiosk device and register it with Entgra IoT Server under your user account.

##### iii. Installing the Android agent on the kiosk device

1.  NFC bump your device and the kiosk device.

    

    Not sure what to do?

    

    Bring the devices close to each other and tap them. 

    

    

2.  Your kiosk device will connect to the Wi-Fi connection based on the settings you have defined in the NFC provisioning application.

    

    

    If the Wi-Fi SSID and password are not defined properly, the device will fail to connect to the network and you will not be able to proceed.

    

    

3.  The Entgra Device Management agent downloads and installs on the kiosk device.

    

    How does this happen automatically?

    

    In step 2.4 you configured the NFC application to download the customized Device Management agent. When you NFC bump the devices, the kiosk device will start downloading the agent application.

    

    

Now you won't be able to use the default apps on the kiosk device. You can only use the applications installed via the Entgra Android agent. Now let's install applications on the kiosk device.

#### QR Provisioning

Entgra EMM has the capability to provision single purpose devices via QR code as Android started supporting QR for Kiosk Provisioning from [Android 7 onwards](https://developer.android.com/work/versions/android-7.0). Entgra EMM is capable of generating a QR, based on the values provided by the user from the Server. The QR shall be generated with the following configuration details set by the Entgra EMM server.

*   Name of the package 
*   Location of the package
*   Wifi_SSID
*   WiFi Password
*   Encryption
*   Package checksum - The checksum is encrypted with SHA256







1.  Open the command prompt.
2.  Go to the file path of the apk file.
3.  Run the following command to get the check sum value of the agent.

**Creating package checksum**

`cat <THE FILE PATH OF THE APK FILE.apk> | openssl dgst -binary -sha256 | openssl base64 | tr '+/' '-_' | tr -d '='`





##### i. Configuring values to the QR Code

1.  Click on the menu button of the Entgra IoT Server.
2.  Select ‘Configuration Management’ option out of the available options.

![image](352823379.png)

3.  Pick 'Platform Configurations' in Configuration Management.

    

4.  Select ‘Android Configurations’ from the available sets of Configurations.





The QR is generated based on the configurations below. Note: The data embedded in the QR is encrypted with SHA 256. 

PROVISIONING_DEVICE_ADMIN_COMPONENT_NAME
PROVISIONING_DEVICE_ADMIN_PACKAGE_CHECKSUM
PROVISIONING_DEVICE_ADMIN_PACKAGE_DOWNLOAD_LOCATION
PROVISIONING_WIFI_SSID
PROVISIONING_WIFI_SECURITY_TYPE
PROVISIONING_WIFI_PASSWORD
PROVISIONING_SKIP_ENCRYPTION



![image](352823374.png)

##### ii. Installing Android Agent on the Kiosk Device

Android 7 upwards allow you to scan QR for Kiosk Provisioning. 

Follow the steps below to convert your device to a Kiosk.

1.  Boot up the new factory reset device. 
2.  Tap the device screen 6 times instead of going with the usual device resetting flow. 
3.  Connect to your Wi-fi network by entering relevant credentials. (This step can be overridden for Android 9 as it comes bundled with a QR library)
4.  Download a QR Scanning application. (This step can be overridden for Android 9 as it comes bundled with a QR library and will allow to set credentials during QR scanning)

    

    

    **Android 7** There is no inbuilt QR scanning app users are required to download a QR application.

    **Android 8** also requires users to download a QR application.

    **Android 9** comes with a QR library hence you would not need to download a separate QR Scanning application. It will automatically set the Wi-fi credentials during QR scanning. Thereby, your device will connect to the configured Wi-fi network based on your QR code.

    

    

    5\. Scan the QR code from your device. 

    

    

    

    Steps to view and scan the QR code in Entgra EMM.

    1.  Traverse to Device Management.
    2.  Select Enroll New Device.
    3.  Select Android.
    4.  Scroll down to bottom of the page to view the QR code related to Kiosk provisioning.
    5.  Scan the QR code from the QR Scanning application which is inbuilt or which you downloaded on Step 4.

    

    

    ![image](352823369.png)

      6\. Then, the Entgra Device Management agent downloads and installs on the kiosk device.

            ![image](352823364.png)

Hereafter you won't be able to use the default apps on the provisioned device. You can only use the applications installed via the Entgra EMM to the Android agent.

#### ADB Provisioning

1.  Reset your mobile device.
2.  Configure the basic settings in the usual manner.

    

    

    Make sure that you do not add any Google Account as soon as you start using the device.

    

    

3.  Connect your mobile device to your PC via a USB cable.
4.  Enable USB debugging on your mobile device.

    

    

    

    1.  Navigate to Settings to view the **Build Number** of the device.
    2.  Tap the **Build Number** 7 times, if successful a message stating ‘You’re now a developer’ shall be displayed
    3.  Navigate back and select ‘**Developer Options**’ 
    4.  Select the option ‘**Enable USB debugging**’
    5.  Navigate back Select ‘**Security**’ and select ‘ **Install from unknown sources**’

    

    

5.  Avail the Device owner provisioning content to a folder on your computer.
6.  Open the command prompt and execute the following command.

    **ADB shell command**

    `adb shell dpm set-device-owner io.entgra.iot.agent/.services.AgentDeviceAdminReceiver`

    

    Warning

    

    The above command is not supported by SDK API levels below 21 because it does not support Device Policy Manager (dpm) that is used in the command.

    

    

7.  Then, a success message is displayed accordingly to the user.

Hereafter you won't be able to use the default apps on the kiosk device. You can only use the applications installed via the Entgra EMM to the Android agent.

### Step 3: Installing the required applications on the kiosk device

Once the Android agent is installed on the kiosk device, the mobile device management administrators can install the required applications on the device and manage the device via the Entgra IoT Server. 





You are not able to install web clips on the kiosk device as it would require the user to access the web browser, which restricted on a kiosk device.





Let's take a look at how you can install an application on the kiosk device. 



Before you begin



For this use case, you need to create another application and publish it from the app publisher to the store.

To make it easy for you to understand, let's create the Android app catalog application that we used in our quick start guide and install it on the kiosk device.









1.  Sign in to the App Publisher using the following URL: `https://<IoT_HOST>:9443/publisher`
2.  Click **Add** **New Mobile Application** that is under the** Mobile Application** drop down.
3.  Select Android for **Platform** on which the application (app) will be based on.
4.  Select the **Enterprise Store Type**.
5.  Upload the `nfc-app.apk` file that is in the `<IoT_HOME>/samples/mobile-qsg/apps/android` directory.
6.  Click **Next**, and you will get the following screen:
7.  Enter the following details of the app.

    *   **Name** - The full name of the app.
    *   **Display Name **- The name of the app that is displayed to the user.
    *   **Description** -  A summarized description of the app.
    *   **Recent Changes** - Optional. A summarized description of what is new in this app when compared to its previous version.
    *   **Version - **Version of the public application. If you have previously entered a different version of this app, make sure to enter a unique version number.
    *   **Category** - Select the category that this app needs to be listed under.
    *   **Visibility** - Enter the user roles that this app will be visible to.
    *   **Tags** - You can group mobile applications under a specific category using tags.  Define the category and press enter.Click on the respective **+** buttons to upload the required images.
        *   **Banner** - Image that will appear as the app banner.
        *   **Screenshots** - Screenshots of the app so that the user can get an understanding of what the app offers. A maximum of four screenshots are allowed.
        *   **Icon file** - The image that will be used as the application icon in the Store and when the application is installed on a device.

        

        

        The recommended image extension is `.png` while the recommended dimensions are as follows:

        *   **Icons**: 124px x 124px
        *   **Screenshots**: 288px x 512px (landscape) and 512px x 288px (portrait)
        *   **Banners**: 705px x 344px

        

        

8.  Click **Create**. The created app is shown in the created list of apps.
9.  Click on the application you created in the `https://<IoT_HOST>:9443/publisher`.
10.  Click **Submit for Review** **> Approve > Publish**.

11.  The application is now published and is available in your App Store.











1.  Navigate to the App Store using the following URL: `**https://<IoT_HOST>:9443/store**` 

2.  Sign in using your username and password.

3.  Click on the mobile application you want to install, and click **Install**.

    

    Want to install the application on many COSU devices?

    

    In this step, you click **Install**, because you are only installing the application on one device.  
    Let's say you have many devices and want to install the application in one go on all of those devices. Click **Ent. Install** and select the users or user roles of the device owner for that the application needs to be installed on the respective devices.

    

    

    Example:  
    ![]({{site.baseurl}}/assets/images/352823451.png)

4.  Select **Instant install**, and click **Yes**.  
5.  Click on your kiosk device in the pop-up menu to install and subscribe to the application.  
    A success message will be shown when you have successfully subscribed to the application.
6.  On your kiosk device, tap **Install** to finish installing the application.  
    You can install many applications on your kiosk device by default. [one-app](about:blank#SettingUpSingle-PurposeDevices-one-app), you will only be able to install one application on the kiosk device.The users will only be able to use the applications you install.  
    Example:  
    ![image](352823463.png)

### What's next?

*   Once you are done using the device, device administrators can factory reset the device. 

    

    

    

    

    1.  On the device management console home page, click **View** under Devices.
    2.  Click on the kiosk device, and click on the **Wipe Data** operation that is under device operations.  
        If you don't have a PIN on your kiosk device, you can leave the field empty and carry out the operation.

    

    

    

*   Manage the system updates COSU devices by publishing a policy on them. For more information

### Adding a System Update Policy for COSU Devices

A COSU device is also known as a single purpose device. You are only able to work with an assigned application or applications on this device and all other device functions will be locked. As you are unable to manually manage the system updates on these devices let's take a look at how you can set a policy to manage the system updates on a COSU device.

Follow the steps given below:

1.  Navigate to the device management console: `https://<IoT_HOST>:9443/devicemgt`
2.  Click **Add** under Policies.  
    ![image](352823616.png)
3.  Click on the Android mobile platform.  
    ![image](352823647.png)
4.  Select the System Update Policy (COSU) and enable it.  
    The selected policy is disabled by default, therefore, click on the on-off switch to enable it.
5.  Define how you want to update the system when system updates are sent to the device, and click **CONTINUE**.  
    Select one of the following options. 

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Automatic</th>
          <td>Install the system update automatically as soon as one is available.</td>
        </tr>
        <tr>
          <th>Postpone</th>
          <td>Incoming system updates (except for security updates) will be blocked for 30 days, after which the policy will no longer be effective and the system will revert back to its normal behavior as if no policy were set.</td>
        </tr>
        <tr>
          <th>Window</th>
          <td>
            
          </td>
        </tr>
      </tbody>
    </table>

6.  Assign the configured profile of policies to the preferred groups.  

    1.  Select the device ownership type by clicking on the preferred option from the **set device ownership type** drop-down list:

        <table>
          <tbody>
            <tr>
              <th>Device<br>ownership<br>type&nbsp;</th>
              <th>Description</th>
            </tr>
            <tr>
              <td>BYOD</td>
              <td>Bring Your Own Device.</td>
            </tr>
            <tr>
              <td>COPE</td>
              <td>Corporate-Owned, Personally Enabled.</td>
            </tr>
            <tr>
              <td>Any</td>
              <td>The configured profile of policies will be assigned to both the BYOD and COPE device ownership types.</td>
            </tr>
          </tbody>
        </table>

    2.  Assign the policy to user roles or users:

    *   Assigning user role/s.

        1.  Select the **set user role/s** option.

        2.  Select the preferred role from the item list.

            

            

            The items that are listed here, are the roles that were added when managing roles through the Device Management console.

            For more information, see [managing roles]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#adding-a-role-and-permissions).


            

            

    *   Assigning user/s.
        1.  Select the **set user/s** option.
        2.  Enter the characters of the username/s you wish to add, and the system will prompt the users having the names with the characters you entered. You can select the users from the item list.

    4.  Click **SAVE**.Example:  
    ![image](352823610.png)
7.  Define a name for the configured profile of policies and a description.

    

    

    **Set a Name to your Policy** is a mandatory field.

    

    

    ![image](352823604.png)

8.  Click **SAVE **to save the configured profile or click **SAVE & PUBLISH **to save and publish the configured profile as an active policy to the database.

    

    

    If you **SAVE** the configured profile it will be in the inactive state. Therefore, the policy will not be taken into account when the IoT Server filters policies, to enforce a suitable policy on a device that registers with WSO2 IoTS.

    If you **SAVE & PUBLISH** the configured profile of policies it will be in the active state. The active policies will be enforced on new devices that enroll with WSO2 IoTS based on the [Policy enforcement criteria](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352814160/Key+Concepts#KeyConcepts-Policyenforcementcriteria). If you want to push this policy to the existing devices and want this policy to be applied to the devices, click **Apply changes**. Then the newly added policy will be a candidate in the policy selection pool based on the policy Enforcement criteria.

    

    





If the device users do not comply with the policy enforced on a device, Entgra IoT Server forcefully enforces the policies back on the device.


## Remote Control for Android Devices


The remote control feature allows administrators to troubleshoot devices that are enrolled with Entgra IoT Server using the device management console. You can create a remote session, send adb shell commands, view the device logs, and remotely view and interact with the screen of an Android device.

Let's take a look at how you can start using it.



Before you begin!



*   Make sure to enroll an Android device. For more information, see [Android Device]({{< param doclink >}}quick-start-guide/mobile-device-app-management/android-device.html).

*   Make sure that your Android device supports the Android Lollipop (API level 21) version or above to use the [screen sharing](about:blank#RemoteControlforAndroidDevices-Viewthedevicescreen) feature.
*   The enrolled device needs to be connected to the network at all times.





### Create a remote session

Follow the steps given below to create a remote session between the device and Entgra IoT Server:

1.  Start Entgra IoT Server's core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile. 

2.  Access the device management console: `https://<IOTS_HTTPS_HOST>:9443/devicemgt` For example: `https://localhost:9443/devicemgt`
3.  Sign in as an administrator. By default, the username is `admin` and the password is `admin`.
4.  Under **DEVICES**, click **View**.  
    ![image](352823693.png)  
    A page appears that lists all the devices that are enrolled with Entgra IoT Server because you have administrator privileges. If you do not have administrator privileges, you only the see the devices that you enrolled.
5.  Click the device you want to troubleshoot.
6.  Click the **Remote Session** tab.  
    ![image](352823681.png)
7.  Click **Connect to Device** to start a remote session with the device.   
    After the server connects to the device, you see the following screen:  
    ![image](352823687.png)
8.  You can now troubleshoot the device as described in the next sections. To stop the remote sharing session, click **Close Session.**

### Send adb shell commands

[Android Debug Bridge (adb)](http://adbshell.com/) is a command line tool that lets you communicate with an emulator or connected Android device. Follow the steps given below to troubleshoot the device using adb shell commands:

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Shell**.
3.  Write the shell command (see [adb shell commands](http://adbshell.com/commands/adb-shell-pwd) for the available commands) and press **Enter**.  
    For example, if you want to get the CPU and memory usage of the device, use the `top` adb command.  
    Sample output:  
    ![image](352823705.png)

### View device logs

[Logcat](https://developer.android.com/studio/command-line/logcat.html) is a tool that displays messages from the device log in real time and keeps a history so you can view the old messages. Follow the steps given below to view the device logs.

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Logcat**.

You can now see the log for the device. 

![image](352823711.png)

### View the device screen via the remote session

To troubleshoot a device, it can be helpful to view the device's screen so you can monitor how the device owner is using it and then take actions yourself, such as opening applications and configuring settings. To view the screen, take the following steps: 

1.  If you haven't already [set up a remote session](about:blank#RemoteControlforAndroidDevices-Createaremotesession), set it up now.
2.  Click **Screen Share > Start**.  
    **![image](352823717.png)**
3.  A message is sent to the device asking the device owner to share the screen with Entgra IoT Server. After the device owner accepts this message, you can view the device's screen and start interacting with it using the mouse.  
    ![image](352823699.png)
4.  Click **Stop** to stop viewing the screen of the device.

### View and interact with the device via the remote session

Viewing the screen of the device alone does not help you to solve the issue. You need to be able to carry out actions on the shared screen to successfully troubleshoot the device. Follow the steps given below to try it out:

1.  Enable the Android System Service Applications. 1.  Build the system service application.
        1.  [Download the source code](https://github.com/wso2/cdmf-agent-android.git).
        2.  The system service app can not be built via the usual Android developer Software Development Kit (SDK), as it requires access to [349634571](about:blank#). Therefore, you need to replace the existing `android.jar` file that is under the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs. You can get the new `jar` file using one of the following options:   

            *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

            *   Use a pre-built jar file from a third party developer. You can find it [here](https://github.com/anggrayudi/android-hidden-api).

                

                

                Make sure to use the jar file that matches the `compileSdkVersion` of the Entgra Android agent. The current `compileSdkVersion` is 25.

                

                

        3.  Open the system service application source code via Android Studio and clean build it as a usual Android application.
    2.  Sign the application via the device firmware signing key. If you don’t have access to the firmware signing key, you have to get the system application signed via your device vendor. 

        

        

        For more information of singing the system service, see [Signing Your Applications](http://developer.android.com/intl/zh-cn/tools/publishing/app-signing.html).

        

        

    3.  Install the system service application by following any of the methods given below:

        *   If you have your own firmware, the system service application is available out of the box with your firmware distribution.

            1.  Copy the signed system service APK file to the `/system/priv-apps` directory of the device. 

            2.  When the device boots or restarts for the first time, it automatically installs the application as a system application.

        *   Install the system service application externally via an Android Debug Bridge (adb) command.

            

            

            For more information on how this takes place on Entgra IoTS, see [Configuring the service application]({{< param doclink >}}Working-with-Android-Devices/Working-with-Android-Devices.html).


            

            

    4.  Enable the system service invocations through the Entgra  Android Agent application. 

        

        

        

        

        

        1.  Clone the `cdmf-agent-android` GIT repository. This is referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout this document.

            `https://github.com/wso2/cdmf-agent-android.git -b <ENTER_THE_VERSION>`

            Check the [Entgra IoT Server and Agent Compatibility]({{< param doclink >}}entgra-iot-server-agent-compatibility.html) and find out what branch of this repo you need to clone.


        2.  Open the client folder that is in the `<ANDROID_AGENT_SOURCE_CODE>` via Android Studio.

        

        

        

        

        

        

        Make sure to sign the Android agent using the same device firmware signing key that was used to sign the System Service Application, else you run into security exceptions.

        

        

    1.  Navigate to the `Constants.java` class, which is in the `org.wso2.iot.agent.utils` package and configure the `SYSTEM_APP_ENABLED` field as follows:

        ` public static final boolean SYSTEM_APP_ENABLED = true;`

    2.  Rebuild the Android agent application.

    6.  Install the Android agent you just built to your mobile device.  
        You need to copy the APK to you device and install it. For more information on installing the Android agent, see [Registering an Android device]({{< param doclink >}}quick-start-guide/mobile-device-app-management/android-device.html#AndroidDevice-install). Follow the steps from step 6 onwards. 

2.  Restart or start Entgra IoT Server's core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.
3.  Access the device management console: `https://<IOTS_HTTPS_HOST>:9443/devicemgt`  
    For example: [`https://localhost:9443/devicemgt`](https://localhost:9443/devicemgt)
4.  Sign in as an administrator. By default, the username is `admin` and the password is `admin`.
5.  Under **DEVICES**, click **View**.  
    ![image](352823693.png)You are navigated to a page that lists out all the devices that are 
    enrolled with Entgra IoT Server because you have administrator privileges. If you do not have administrator privileges, you only the see the devices that you enrolled.
6.  Click on the device you want to start a remote session.
7.  Click the **Remote Session** tab.
8.  Click **Connect to Device** to start a remote session with the device. 
9.  Click **Screen Share > Start**.   
    ![image](352823699.png)
10.  Click on the applications you want to open or the configurations you want to enable using the mouse.
11.  Click **Stop** to stop viewing the screen of the device.
    
## Transferring Files


Entgra IoT Server enables you to upload and download files from Android devices. You need to set up a shared location to manage the files that are sent to the device and the files that are sent from the device to Entgra IoT Server. The server sends an operation to the device to send the requested file to the defined shared location. Similarly, if a file needs to be sent to the device, the server sends an operation to the device to get the file from the shared location.

Let's take a look at how it works:



Before you begin



*   Make sure to have an Android device enrolled. For more information, see [Android]({{< param doclink >}}tutorials/android/android.html).

*   Set up a shared location to manage the files that need to be sent to the device and the files that are downloaded from the device. The shared location can have the HTTP, FTP, and SFTP transfer protocols.





### Uploading files to the device

Follow the steps below to upload a file to your Android device:

1.  Access the device management console at `https://<IOTS_HTTPS_HOST>:9443/devicemgt` and sign in as an administrator. The default username is `admin` and the default password is `admin`.
2.  Click **View** under the **DEVICES** tile.  
    ![image]({352823753.png)
3.  Click the device thumbnail and navigate to the device details page.  
    ![image](352823759.png)
4.  Click **File Transfer**.   
    ![image](352823747.png)
5.  Select the **To device** option.   
    ![image](352823781.png)
6.  Enter the details to upload a file to the device:
    1.  Select the preferred transfer protocol, such as **HTTP**, **FTP**, or **SFTP**.
    2.  Enter the shared location where the file is stored in one of the following formats:
        *   For **HTTP**: `<HTTP_URL>`
        *   For **FTP**: `FTP://<USER_NAME>@<HOST_NAME>/<FOLDER_PATH>/<FILE_NAME>`
        *   For **SFTP**: `SFTP://<USER_NAME>@<HOTS_NAME>/<FOLDER_PATH>/<FILE_NAME>`
    3.  Optionally, enter the location on the device to save the file. It is recommended to provide the location. 
        *   The file path needs to be in the following format: `/<DIRECTORY>/<DIRECTORY>`. For example,  `/sdcard/Donwload/`.
        *   If the location is not defined, the file is saved to the download directory of the device. On some devices, the files are saved in the file explorer's download directory. 
    4.  Optionally select **Authentication required **if your shared location is secured using credentials, and enter the username and password for a secure transfer.

        

        

        If you are using the FTP or SFTP protocols to transfer files and if the username is part of the file URL the Username field is automatically updated using the data from the File URL field.

        Example:

        ![image](352823787.png)

        It is optional to enter the password. Certain FTP accounts do not require passwords for authentication. In such scenarios, you can leave the password field empty.

        

7.  Click **Send to Device**.  
    You can see the **FILE_UPLOAD_TO_THE_DEVICE** entry immediately changing to the **PENDING** state in the Operations Log. After the file is successfully uploaded to the device, the **FILE_UPLOAD_TO_THE_DEVICE** entry changes to the **COMPLETED** state.

    

    

    If the file size is large, it takes a few minutes for the file to download to the device based on your network condition.

    

    

    Example: Successfully completing the operation:  
    ![image](352823805.png)

### Downloading files from the device

Follow the steps below to download a file from your Android device:

1.  Click **File Transfer**.   
    ![image](352823747.png)
2.  Select the **From device** option.   
    ![image](352823799.png)
3.  Enter the details to download a file from the device:
    1.  Select the preferred transfer protocol, such as **HTTP**, **FTP**, or **SFTP**.
    2.  Enter the shared location path where the file needs to be saved to:
        *   For **HTTP**: `<HTTP_URL>`
        *   For **FTP**: `FTP://<USER_NAME>@<HOST_NAME>/<FOLDER_PATH>`
        *   For **SFTP**: `SFTP://<USER_NAME>@<HOTS_NAME>/<FOLDER_PATH>`
    3.  Enter the exact location of the file in the device that needs to be sent to the shared location. If the file path is wrong, the operation is terminated.  
        The file path needs to be in the following format: `/<DIRECTORY>/<DIRECTORY>/<FILE_NAME>`. For example,  `/sdcard/Donwload/fileName.pdf`.
    4.  Optionally, select **Authentication required **if your shared location is secured using credentials, and enter the username and password for a secure transfer.

        

        

        If you are using the FTP or SFTP protocols to transfer files  and if  the username is part of the file URL the Username field is automatically updated using the data from the File URL field.

        Example:

        ![image](352823770.png)

        It is optional to enter the password. Certain FTP accounts do not require passwords for authentication. In such scenarios, you can leave the password field empty.

        

4.  Click **Send to Device**.
5.  A notification is sent to the device asking the device to share the file with Entgra IoT Server. The device owner needs to **Allow** this request. Once allowed, the file is sent to the shared location.  
    ![image](352823811.png)  
    You can see the **FILE_DOWNLOAD_TO_THE_DEVICE** entry immediately changing to the** PENDING** statein the Operations Log. Once the file is successfully sent to the defined shared location the **FILE_DOWNLOAD_TO_THE_DEVICE **entry changes to the **COMPLETED** state.

    

    

    If the file size is large, it takes a few minutes for the file to download to the shared location based on your network condition.

    

    

    ![image](352823793.png)  
    Initially, the file is not there in the shared location you defined. Once the operation is completed you see the file you sent from the device downloaded to the directory.  
    ![image](352823828.jpg)

### Failing to transfer files

If you enter the wrong username and password for the shared location credentials, enter an incorrect file path, or if you don't allow the device to share the file when the notification is sent to the device, the file transfer operation is terminated. This is indicated in the Operations Log as an ERROR.

Example: The Operations Log when the device owner cancels the file share request.  
![image](352823817.png)
    

        
## Customizing the Android Agent Download Location for Tenants


Do you want the users who are under the tenant `foo1.com` to download and install an Android agent that is different from the users under the tenant `foo2.com`? This document guides you on how to change the Android agent download URL among tenants. 

Follow the steps given below:

1.  Make a copy of the `cdmf.unit.device.type.android.type-view` directory that is in the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/units` directory in the same directory.
2.  Rename the newly copied file as follows: `<TENANT_DOMAIN>.cdmf.unit.device.type.android.type-view` .  
    1.  Open the `type-view.js` file and replace all the content in it with the following configurations:  
        Update the `enrollmentURL` with the URL to download the agent.

        function onRequest(context) {
          var viewModel = {};
          var devicemgtProps = require("/app/modules/conf-reader/main.js")["conf"];
          var isCloud = devicemgtProps["isCloud"];
          viewModel["isVirtual"] = request.getParameter("type") == 'virtual';
          viewModel["isCloud"] = isCloud;
          viewModel["hostName"] = devicemgtProps["httpsURL"];
          viewModel["enrollmentURL"] = "http://abc.com";
          return viewModel;
        }

    2.  Open the `type-view.json` file and replace all the content in it with the following configurations:

        {
          "version": "1.0.0",
          "extends": "cdmf.unit.device.type.android.type-view"
        }
        

        Why am I extending this unit?

        

        For this tutorial only the Android agent download URL changes. All the other content of the Android agent is the same. Therefore, all you need to do is extend the existing `cdmf.unit.device.type.android.type-view` unit.

        

        

### What's next?

*   [Sign in to Entgra IoT Server's Device Management console]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console). The username needs to be in the following format `<USERNAME>@TENANT_DOMAIN`.

*   Now, try [enrolling an Android device]({{< param doclink >}}tutorials/android/android.html) and you see that the agent download URL has changed.

        

        
