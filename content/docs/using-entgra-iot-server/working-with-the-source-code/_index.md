---
bookCollapseSection: true
weight: 10
---
# Working with the Source Code

The source code of all Entgra products as well as the scripts that are used for building Entgra products are maintained in GitHub repositories. If you are a developer, you can easily clone the source code from these Git repositories, and if required, you can do modifications and build a customized product on your own. For more information, see the following: 

*   [Entgra GitHub Repositories]({{< param doclink >}}using-entgra-iot-server/working-with-the-source-code/entgra-github-repository/)

*   [Using Maven to Build Entgra products]({{< param doclink >}}using-entgra-iot-server/working-with-the-source-code/using-maven-to-build-entrga-products/)

*   [Contributing to the Code Base](https://wso2.github.io/index.html)
