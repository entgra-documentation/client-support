# Using Maven to Build Entgra products

# Prerequisites to build the product

*   Install Mavan and Oracle JDK 1.8 or above.
*   If you get OutOfMemeory errors, you will need to set the environment variable, MAVEN_OPTS="-Xmx2048M -XX:MaxPermSize=1024m"
*   Your machine should have internet connectivity
*   Due a [restriction in Windows OS](https://support.microsoft.com/en-us/help/830473/command-prompt-cmd-exe-command-line-string-limitation), product cannot be built on top of Windows.

# Building the product

In order to build a product, you must build product components and them the product repository. Clone the following repositories with the given command to a location in your building machine,

Carbon device management core - git clone [https://gitlab.com/entgra/carbon-device-mgt](https://www.google.com/url?q=https://gitlab.com/entgra/carbon-device-mgt&sa=D&source=hangouts&ust=1543055877426000&usg=AFQjCNE4hTDu_rzOTohZ4-XMxn21_NX-Kw)

Android & Windows plugin - git clone [https://gitlab.com/entgra/carbon-device-mgt-plugins](https://www.google.com/url?q=https://gitlab.com/entgra/carbon-device-mgt-plugins&sa=D&source=hangouts&ust=1543055906461000&usg=AFQjCNFVYa-bbfuU95SVDFqQo2_8jkH0lg)

Product IoTs - git clone [https://gitlab.com/entgra/product-iots](https://www.google.com/url?q=https://gitlab.com/entgra/product-iots&sa=D&source=hangouts&ust=1543055925899000&usg=AFQjCNE58NPw4Sd6kIWt1YipFnps4c2R4w)

Build all these repositories in the above order with the command mvn clean install. If you wish, you can skip the unit tests that takes a long time to build by using following command instead, mvn clean install -Dmaven.test.skip=true

Once you build the product IoTs, you can find the product distribution zip inside <PRODUCT_IOT_REPOSITORY_HOME>/modules/distribution/target
