# Entgra GitHub Repositories

**Component level repositories**

Carbon device management core - [https://gitlab.com/entgra/carbon-device-mgt](https://www.google.com/url?q=https://gitlab.com/entgra/carbon-device-mgt&sa=D&source=hangouts&ust=1543055877426000&usg=AFQjCNE4hTDu_rzOTohZ4-XMxn21_NX-Kw)

Android & Windows plugin - [https://gitlab.com/entgra/carbon-device-mgt-plugins](https://www.google.com/url?q=https://gitlab.com/entgra/carbon-device-mgt-plugins&sa=D&source=hangouts&ust=1543055906461000&usg=AFQjCNFVYa-bbfuU95SVDFqQo2_8jkH0lg)

**Product level repository**

Product IoTs - [https://gitlab.com/entgra/product-iots](https://www.google.com/url?q=https://gitlab.com/entgra/product-iots&sa=D&source=hangouts&ust=1543055925899000&usg=AFQjCNE58NPw4Sd6kIWt1YipFnps4c2R4w)

Agent repositories

iOS agent - [https://gitlab.com/entgra/ios-agent](https://gitlab.com/inosh/ios-agent)

Android Agent - [https://gitlab.com/entgra/android-agent](https://gitlab.com/entgra/android-agent)
