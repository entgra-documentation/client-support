# Device Enrollment Program

Device Enrollment Program (DEP) is a program provided by Apple to allow device management solutions to have control over corporate-owned devices. Let's take a look at what you need to do, to get started and understand why you need DEP.

## Getting started guide on Apple DEP for EMM administrators

The diagram given below guides you on what you need to do get started with Apple DEP and on how to configure Entgra IoT Server's EMM solution for DEP.



Before you begin!



*   You need to configure Entgra IoT Server with the iOS features. For more information, see [iOS
 Configurations]({{< param generaldoclink  >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/).
*   Enroll an iOS device and check if the server is successfully configured with the iOS features
. Try out the [iOS quick start guide]({{< param generaldoclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/).







## Benefits of using DEP

*   Provides zero touch device enrolments for IT administrators.

*   Provides better control over iOS devices and provides the features described in the features section.

*   Restrict the user from removing EMM management from the device.

## Features available with DEP

The following features are available with DEP devices also known as supervised devices.

*   Block users from removing the Mobile Device Management (MDM) profiles from the device.
*   Restricts users from:

    <table>
      <colgroup>
        <col style="width: 290.0px;">
        <col style="width: 1026.0px;">
      </colgroup>
      <tbody>
        <tr>
          <th>
            <p><code>allowPodcasts</code></p>
          </th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables podcasts. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 8.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowDefinitionLookup</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables definition lookup. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 8.1.3 and later and in macOS 10.11.2 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowPredictiveKeyboard</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables predictive keyboards. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 8.1.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAutoCorrection</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables keyboard auto-correction. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 8.1.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowSpellCheck</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables keyboard spell-check. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 8.1.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>forceWatchWristDetection</code></th>
          <td>
            <p>If set to&nbsp;<code>true</code>, a paired Apple Watch will be forced to use Wrist Detection. Defaults to&nbsp;<code>false</code>.<br>Availability:&nbsp;Available in iOS 8.2 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowMusicService</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, Music service is disabled and Music app reverts to classic mode. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.3 and later and macOS 10.12 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowNews</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables News. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowUIAppInstallation</code></th>
          <td>
            <p>When&nbsp;<code>false</code>, the App Store is disabled and its icon is removed from the Home screen. However, users may continue to use Host apps (iTunes, Configurator) to install or update their apps. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowKeyboardShortcuts</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, keyboard shortcuts cannot be used. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowPairedWatch</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables pairing with an Apple Watch. Any currently paired Apple Watch is unpaired and erased. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowPasscodeModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, prevents the device passcode from being added, changed, or removed. Defaults to&nbsp;<code>true</code>. This restriction is ignored by shared iPads.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowDeviceNameModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, prevents device name from being changed. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowWallpaperModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, prevents wallpaper from being changed. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAutomaticAppDownloads</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, prevents automatic downloading of apps purchased on other devices. Does not affect updates to existing apps. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowRadioService</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, Apple Music Radio is disabled. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowNotificationsModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, notification settings cannot be modified. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowRemoteScreenObservation</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, remote screen observation by the Classroom app is disabled. Defaults to&nbsp;<code>true</code>.<br>This key should be nested beneath&nbsp;<code>allowScreenShot</code>&nbsp;as a sub-restriction. If&nbsp;<code>allowScreenShot</code>&nbsp;is set to&nbsp;<code>false</code>, it also prevents the Classroom app from observing remote screens.<br>Availability:&nbsp;Available in iOS 9.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowDiagnosticSubmissionModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, the diagnostic submission and app analytics settings in the Diagnostics &amp; Usage pane in Settings cannot be modified. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 9.3.2 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowBluetoothModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, prevents modification of Bluetooth settings. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 10.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowDictation</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disallows dictation input. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available only in iOS 10.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>forceWiFiWhitelisting</code></th>
          <td>
            <p>If set to&nbsp;<code>true</code>, the device can join Wi-Fi networks only if they were set up through a configuration profile. Defaults to&nbsp;<code>false</code>.<br>Availability:&nbsp;Available only in iOS 10.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAirPrint</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disallow AirPrint. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 11.0 and later and macOS 10.13 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAirPrintCredentialsStorage</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disallows keychain storage of username and password for Airprint. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available only in iOS 11.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>forceAirPrintTrustedTLSRequirement</code></th>
          <td>
            <p>If set to&nbsp;<code>true</code>, trusted certificates are requiredm for TLS printing communication. Defaults to&nbsp;<code>false</code>.<br>Availability:&nbsp;Available in iOS 11.0 and later and macOS 10.13 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAirPrintiBeaconDiscovery</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables iBeacon discovery of AirPrint printers. This prevents spurious AirPrint Bluetooth beacons from phishing for network traffic. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 11.0 and later and macOS 10.13 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowSystemAppRemoval</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disables the removal of system apps from the device. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available only in iOS 11.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowVPNCreation</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, disallow the creation of VPN configurations. Defaults to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available only in iOS 11.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAccountModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, account modification is disabled.<br>Availability:&nbsp;Available only in iOS 7.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAirDrop</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, AirDrop is disabled.<br>Availability:&nbsp;Available only in iOS 7.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAppCellularDataModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, changes to cellular data usage for apps are disabled.<br>Availability:&nbsp;Available only in iOS 7.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowAppInstallation</code></th>
          <td>When <code>false</code> , the App Store is disabled and its icon is removed from the Home screen. Users are unable to install or update their applications. This key is deprecated on unsupervised devices.</td>
        </tr>
        <tr>
          <th><code>allowAssistantUserGeneratedContent</code></th>
          <td>
            <p>When&nbsp;<code>false</code>, prevents Siri from querying user-generated content from the web.<br>Availability:&nbsp;Available in iOS 7 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowBookstore</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, the iBooks Store will be disabled. This will default to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 6.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowBookstoreErotica</code></th>
          <td>
            <p>Supervised only prior to iOS 6.1\. If set to&nbsp;<code>false</code>, the user will not be able to download media from the iBooks Store that has been tagged as erotica. This will default to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS and in tvOS 11.3 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowChat</code></th>
          <td>
            <p>When&nbsp;<code>false</code>, disables the use of the Messages app with supervised devices.<br>Availability:&nbsp;Available in iOS 6.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowFindMyFriendsModification</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, changes to Find My Friends are disabled.<br>Availability:&nbsp;Available only in iOS 7.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowGameCenter</code></th>
          <td>
            <p>When&nbsp;<code>false</code>, Game Center is disabled and its icon is removed from the Home screen. Default is&nbsp;<code>true</code>.<br>Availability:&nbsp;Available only in iOS 6.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowHostPairing</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, host pairing is disabled with the exception of the supervision host. If no supervision host certificate has been configured, all pairing is disabled. Host pairing lets the administrator control which devices an iOS 7 device can pair with.<br>Availability:&nbsp;Available only in iOS 7.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowUIConfigurationProfileInstallation</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, the user is prohibited from installing configuration profiles and certificates interactively. This will default to&nbsp;<code>true</code>.<br>Availability:&nbsp;Available in iOS 6.0 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>forceAssistantProfanityFilter</code></th>
          <td>When&nbsp; <code>true</code>, forces the use of the profanity filter assistant.</td>
        </tr>
        <tr>
          <th><code>allowEraseContentAndSettings</code></th>
          <td>If set to&nbsp; <code>false</code>, disables the “Erase All Content And Settings” option in the Reset UI.</td>
        </tr>
        <tr>
          <th><code>allowSpotlightInternetResults</code></th>
          <td>
            <p>If set to&nbsp;<code>false</code>, Spotlight will not return Internet search results.<br>Availability:&nbsp;Available in iOS and in macOS 10.11 and later.</p>
          </td>
        </tr>
        <tr>
          <th><code>allowEnablingRestrictions</code></th>
          <td>If set to&nbsp;<code>false</code>, disables the "Enable Restrictions" option in the Restrictions UI in Settings.</td>
        </tr>
      </tbody>
    </table>

#  Adding Devices to the DEP Portal


The Apple Device Enrollment Program (DEP) allows iOS devices purchased through the Apple DEP program and devices purchased outside the DEP program to be enrolled as supervised (DEP enabled) devices to a mobile device management system. You need to add the Corporate Owned, Personally Enabled (COPE) devices to the DEP portal.



Before you begin!



Make sure that you have:

1.  [Enrolled with DEP]({{< param generaldoclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/ios-DEP-enrollment/).
2.  [Linked Entgra IoT Server's EMM solution with the DEP portal](/doc/en/lb2/Adding-the-Entgra-EMM-Solution-to-the-DEP-Portal.html).





The steps to add a device to the DEP program varies based on how you obtained the COPE iOS device. Follow the steps given below:

[Adding devices purchased via DEP]({{< param doclink>}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#adding-devices-purchased-via-dep)

[Adding normal iOS devices using the Apple configurator]({{< param doclink>}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#adding-normal-ios-devices-using-the-apple-configurator)

## Adding devices purchased via DEP

iOS devices that are purchased directly from Apple as DEP or COPE devices, can be added to your DEP account automatically. Follow the steps given below:





If you selected the **Automatically Assign New Devices** option when [creating the virtual machine for the MDM server](/doc/en/lb2/Adding-the-Entgra-EMM-Solution-to-the-DEP-Portal.html#AddingtheEntgraEMMSolutiontotheDEPPortal-MDM-Server) in the Apple DEP account, your devices are automatically added to the server you created. Therefore, you do not need to follow the steps given below.





1.  Sign in to the [Apple DEP account](https://deploy.apple.com/).
2.  Under **Manage Devices**, click **Choose Devices By.**  There are three options in this section:
    1.  **Serial Number**: Provide the serial number of the devices.
    2.  **Order Number**: Provide the purchase order number of the devices.
    3.  **Upload CSV File**: Upload a CSV file that contains a list of serial numbers of those 
    devices needed to be enrolled. ![image](3.png)
3.  Assign the device to the Entgra IoT Server's EMM solution by clicking **Choose Action**.
    1.  Select the **Assign to Server** option from the first drop-down box.   
        ![image](352824463.png)
    2.  Select the virtual server you created for Entgra IoT Server's EMM solution from the second drop-down box.  
        ![image](352824451.png)
4.  Click **Ok** to complete adding the devices to the virtual machine.

## Adding normal iOS devices using the Apple configurator

The devices purchased outside DEP can be added manually to the DEP account. You need to plug in each device to a Mac and factory reset the device using THE Apple Configurator.   
Follow the steps given below:



Before you begin!



You need to have the following to try out this option:

*   Devices that have an OS that is iOS 11 or higher.
*   Apple Configurator 2.5 or higher.
*   DEP account.





Follow the steps given below:

1.  Setting up the WiFi profile:
    1.  Open the Apple Configurator 2.
    2.  Click **File** > **New Profile** > **Wi-Fi**, and click configure.
    3.  Configure the Wi-Fi profile and save it.
2.  Adding the device to DEP:
    1.  Plug in your iOS device to the Mac, and open the Apple Configurator 2.5.
    2.  Click **Blueprints** > **Edit Blueprints** > **New** to create a blueprint.
    3.  Enter a name for the blue print and click **Done**.
    4.  Click** Prepare** to prepare the device to be added to DEP.   
        ![image](352824457.png)
    5.  Enter the details:  

        1.  For **Prepare with,** select **Manual Configurations.**
        2.  Select only the Add to Device Enrollment Program option.![image](2.png)
    6.  Click **Next**.
    7.  Select **New Server** and click** Next.**
    8.  Enter a name, keep the default URL, and click **Next**.
    9.  You are directed to a screen that give the following error message: Unable to verify the server's enrollment URL.   
        Do not worry about it, click **Next**.  
        ![image](352824433.png)
    10.  Click **Next** again, enter the DEP account's username and password, and click **Sign In**.  
        ![image](352824439.png)

Since you selected the **Automatically Assign New Devices** option when [creating the virtual machine for the MDM server](/doc/en/lb2/Adding-the-Entgra-EMM-Solution-to-the-DEP-Portal.html#AddingtheEntgraEMMSolutiontotheDEPPortal-MDM-Server) in the Apple DEP account, your devices are automatically added to the server you created. 


# Adding the Entgra EMM Solution to the DEP Portal

You need to your MDM solution, which is Entgra IoT Server's EMM solution, to the Device Enrollment Program (DEP) portal. This allows you to successfully manage the Corporate Owned, Personally Enabled (COPE) iOS devices via Entgra IoT Server's EMM device management console.



Before you begin!



1.  [Make sure to be enrolled in the Apple DEP program](/doc/en/lb2/Enrolling-in-the-Apple-Device-Enrollment-Program.html).
2.  [Download and install OpenSSL](https://www.openssl.org/source/). Linux OS's have OpenSSL installed by default.

3.  Generating the public key in the `.pem` format:

    1.  Create a new directory to generate the public key.

    2.  Create a file named `openssl.cnf` in the directory you just created.

    3.  Copy the code given below to the `openssl.cnf` and save it.

        
        [ v3_req ]# Extensions to add to a certificate request
        basicConstraints=CA:TRUE
        keyUsage = digitalSignature, keyEncipherment

        [ v3_ca ]
        # Extensions for a typical CA
        # PKIX recommendation.
        subjectKeyIdentifier=hash
        authorityKeyIdentifier=keyid:always,issuer
        # This is what PKIX recommends but some broken software chokes on critical
        # extensions.
        basicConstraints = critical,CA:true
        # So we do this instead.
        #basicConstraints = CA:true
        # Key usage: this is typical for a CA certificate. However since it will
        # prevent it being used as an test self-signed certificate it is best
        # left out by default.
        keyUsage = digitalSignature, keyCertSign, cRLSign
        

    4.  Navigate into the directory and run the commands given below in the given order.

        
        openssl genrsa -out dep_private.key 4096 

        openssl req -new -key dep_private.key -out dep.csr

        openssl x509 -req -days 365 -in dep.csr -signkey dep_private.key -out dep.crt -extensions v3_ca -extfile ./openssl.cnf

        openssl x509 -in dep.crt -out dep.pem
        

        Now, you see the `dep.pem` file created in the directory you created.





1.  Navigate to the [Apple Deployment Programs](https://idmsa.apple.com/IDMSWebAuth/login.html?disable2SV=true&rv=2&appIdKey=09273cfd6b56a8ce5af52a0153d1d796d364e03a36c6e87ef21e92c77a83ef3f&path=/qforms/web/index/avs&language=US-EN&country=US).

    

    Note!

    

    Do not close this browser session until you are done configuring the DEP portal. If you do close the browser session, you need to enter the verification code again and start configuring the DEP portal from where you stopped.

    

    

2.  Sign in with your organization's Apple credentials.

3.  Click **Get Started** to automate the Mobile Device Management (MDM) enrollment.  
    ![image](352824500.png)

4.  Confirm your identity by entering the verification code that was sent to the device you entered when creating an account for DEP, and click **Continue**.   
    ![image](352824494.png)  
    The DEP portal screen appears.
5.  In the left-navigation pane, select **Manage Servers.**  
    **![image](352824518.png)**
6.  Click **Add MDM Server** to create a virtual machine that groups devices in the DEP portal. This allows Entgra IoT Server to manage the EMM devices.

7.  Enter the **MDM Server name** for your organization. For example, you can enter Entgra EMM Server.  
    ![image](352824506.png)

    

    

    If you selected **Automatically Assign New Devices**, each new device that is added to your DEP account is automatically added to the MDM Server you are creating. It is recommended to select this, if your organization is having only one MDM Server, as it will be easy for you to manage the devices.

    

    

8.  Click **Next**.

9.  Click **Choose File** and upload the[ public key that you generated as a `.pem` file before starting this tutorial.](/doc/en/lb2/Adding-the-Entgra-EMM-Solution-to-the-DEP-Portal.html)  
    ![image](352824524.png)
10.  Click **Next**.
11.  Click Your Server Token and click  **Done** .. An encrypted Apple server token file in the .p7m file format downloads. Make sure to save it in a convenient location.  
    ![image](352824512.png)





The DEP server token expires in an year (365 days). Therefore, you need to renew it when it expires. For more information on how to renew the expired token, see [Renewing the DEP Server Token](/doc/en/lb2/Renewing-the-DEP-Server-Token.html).



# Creating and Assigning Profiles to iOS Devices

You can configure the device startup settings of the Corporate Owned, Personally Enabled (COPE) iOS devices also known as Device Enrollment Program (DEP) devices, to skip configurations or include additional configurations. This is done by creating profiles in Entgra IoT Server and assigning it to the devices.



Before you begin!



*   [Configure the iOS features in Entgra IoT Server's EMM solution](/doc/en/lb2/iOS-Configurations.html).
*   [Link Entgra IoT Server with the DEP portal](/doc/en/lb2/Linking-the-Entgra-EMM-Solution-with-Apple-DEP.html).





Follow the steps given below:

1.  If you have not already started Entgra IoT Server, start Entgra IoT Server's core profile.

    
    cd <IOTS_HOME>/bin
    ./iot-server.sh
    

2.  Sign in to the device management console using a username and password that has administrator privileges. The default administrator password is `admin` and the default administrator password is `admin`.

3.  Click ![image](3.png) > **CONFIGURATION MANAGEMENT > DEP 
CONFIGURATIONS > Add Profile**.  
    The profile form is displayed.  
    ![image](352824556.png)
4.  Configure the profile settings:

    <table style="width: 100.0%;">
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Enter profile name</th>
          <td>Provide a name for your profile.</td>
        </tr>
        <tr>
          <th>Enter profile URL</th>
          <td>Enter the URL to access the Entgra IoT Server's EMM server.<br>The URL needs to be in the following format: <code>https://&lt;IOTS_HOST&gt;:8243/api/ios/v1.0/profile-dep/</code>. If you port offset Entgra IoT Server's core profile, make sure to offset the port defined here too.</td>
        </tr>
        <tr>
          <th>Is supervised</th>
          <td>If selected, the device is set to the Supervised mode. The supervised devices are also referred to as DEP enabled device. The EMM administrators are able to carry out operations on the device as they are owned by the organization.</td>
        </tr>
        <tr>
          <th>Is MDM removable</th>
          <td>If selected, the device user is unable to unregister the device from the Entgra IoT Server's EMM solution.</td>
        </tr>
        <tr>
          <th>Is mandatory</th>
          <td>If selected, the device users need to complete enrolling their devices with Entgra IoT Server during the setup, and cannot skip the step.</td>
        </tr>
        <tr>
          <th>Department</th>
          <td>Enter the department the device belongs to. This value is displayed when the device is starting up.</td>
        </tr>
        <tr>
          <th>Support phone number</th>
          <td>Enter the support number. This is provided during the setup if device users require help or run into issues.</td>
        </tr>
        <tr>
          <th>Support email address</th>
          <td>Enter the support email address. This is provided during the setup if device users require help or run into issues.</td>
        </tr>
        <tr>
          <th>Anchor certs</th>
          <td>
            
              <ul>
                <li>
                  <p><strong>Production environment</strong>: You don't need to enter any value here because Entgra IoT Server has a valid SSL certificate in a production environment.</p>
                </li>
                <li>
                  <p><strong>Testing/Development environment</strong>: If the testing environment does not have a valid SSL certificate, follow the steps given to get the values you need to enter:</p>
                  
                    
                      <p><a href="https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=28278870#CreatingandAssigningProfilestoiOSDevices-tip" data-linked-resource-id="146178168" data-linked-resource-version="1" data-linked-resource-type="page">You need to ensure that you have configured Entgra IoT Server with the iOS features before running the commands given below as mentioned in the before you begin section</a>.</p>
                    
                  
                  <ol>
                    <li>Navigate to the <code>&lt;IOTS_HOME&gt;/ios-configurator/output</code> directory via the terminal.<br>This directory is available only if you configured Entgra IoT Server with the iOS features.</li>
                    <li>
                      <p><span style="color: rgb(51,51,51);">Use the bellow command to get the value needed for anchor certs</span></p>
                      
                        
                          
                        
                      
                    </li>
                  </ol>
                </li>
              </ul>
            
          </td>
        </tr>
        <tr>
          <th>Skip setup items</th>
          <td>
            <ul>
              <li>
                <p>If <strong>Skip setup items</strong> is selected, the device automatically skips through all the pages that appear at the time of setting up an iOS device.</p>
              </li>
              <li>If only specific items are selected, the device skips the selected pages at the time of setting up the iOS device.<br>For example, if you select passcode and Siri, you don't have to enter a passcode or set up Siri at the time of setting up the iOS device for the first time.</li>
            </ul>
          </td>
        </tr>
      </tbody>
    </table>

5.  Click **Add** to add the configured profile.
6.  Navigate to the Device list page.
7.  Click **Sync** to get the list of devices that are enrolled as DEP devices from the DEP portal.
8.  Assign a profile to a device. The settings in the profile are used when starting up the device for the first time.

# Enrolling in the Apple Device Enrollment Program


The first step is to enroll your organization with Apple Device Enrollment Program (DEP) and link Entgra IoT Server's EMM solution to your DEP portal. Follow the steps given below to enroll with DEP.

1.  Follow the four steps under ** Enroll in Apple Deployment Programs ** in the [Apple guide]({{< param generaldoclink>}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/) to Create
 an account by providing
 basic information about your business including a D‑U‑N‑S number. The enrollment process is free of charge.
2.  Add an administrator to the Apple DEP portal as instructed under  **Getting Started with the Device Enrollment Program**- **step 1** in the [Apple guide]({{< param generaldoclink>}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/).

    

    The following information is required:

    *   First and last name of the individual enrolling on behalf of the business.

        

        

        **NOTE**: This must be a legal, human name. First and last names such as “IT Coordinator” or “iPad Deployment” will not be accepted.

        

        

    *   A work email address that isn’t associated with an iTunes or iCloud account, and that hasn’t been used as an Apple ID for any other Apple service or website.

        

        

        **CAUTION**: Don’t use this new Apple ID with an iTunes or iCloud account, or any other Apple services or website other than the Volume Purchase Program. Doing so causes the Apple ID to stop working with all Apple Deployment Programs.

        

        

    *   Work phone number.

    *   Title/Position

# Linking the Entgra EMM Solution with Apple DEP

fter creating a container or virtual machine for Entgra IoT Server's Enterprise Mobility Management (EMM) solution in the Apple Device Enrollment Program (DEP), you need to link to the Apple DEP account from Entgra IoT Server. 



Before you begin!



1.  [Add the Entgra EMM solution to the DEP portal](/doc/en/lb2/Adding-the-Entgra-EMM-Solution-to-the-DEP-Portal.html).
2.  [Configure Entgra IoT Server with the iOS configurations]({{< param generaldoclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/).
3.  If you have not already installed OpenSSL, [download and install OpenSSL](https://www.openssl.org/source/) .  
    Linux OS's have OpenSSL installed by default.





Follow the steps given below to link Entgra IoT Server's EMM solution with the Apple DEP:

1.  Navigate to the folder where you saved the [Apple server token you downloaded when Adding the Entgra EMM Solution to the DEP Portal]({{< param doclink >}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#adding-normal-ios-devices-using-the-apple-configurator) via the terminal.

2.  Decrypt the server token using the command given below:

    `openssl smime -decrypt -in "<THE-.PM7-TOKEN-SERVER-FILE-NAME>.pm7" -inkey "dep_private.key" > token.json`

    You see the `token.json` file created in the same directory.

3.  Start Entgra IoT Server's core profile.

    
    cd <IOTS_HOME>/bin
    ./iot-server.sh
    

4.  Sign in by entering the EMM administrators username and password. The default username is `admin` and the default password is `admin`.
5.  Click the ![image](5.png) icon **> CONFIGURATION MANAGEMENT > 
PLATFORM CONFIGURATIONS > iOS Configurations**.
6.  Scroll down until you come to DEP only configurations.  
    ![image](352824585.png)
7.  Update the DEP related details:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Agent App ID</th>
          <td>
            <p>You are able to enroll and iOS device with Entgra IoT Server's EMM solution, with or without the agent.</p>
            <ul>
              <li>
                <p><strong>If you are not using the agent</strong>, you can leave this section blank.</p>
              </li>
              <li>
                <p><strong>If you are using the agent</strong>, follow the steps given below:</p>
                
                  <p class="title">Before you begin!</p><span class="aui-icon aui-icon-small aui-iconfont-approve confluence-information-macro-icon"></span>
                  
                    <ul>
                      <li><a class="external-link" href="https://developer.apple.com/xcode/download/" rel="nofollow">Download Xcode</a>&nbsp;and install it.</li>
                    </ul>
                  
                
                <ol>
                  <li>Configure Entgra IoT Server to install iOS mobile applications:<ol>
                      <li style="list-style-type: none;background-image: none;">
                        <ol>
                          <li>Open&nbsp;the&nbsp;<code>&lt;IOTS_HOME&gt;/conf/app-manager.xml</code>&nbsp;file.</li>
                          <li>
                            <p>Add&nbsp;<code>%https%</code>&nbsp;as&nbsp;the value for&nbsp;the&nbsp;<code>AppDownloadURLHost</code>&nbsp;property.</p>
                            
                              
                                
                              
                            
                          </li>
                        </ol>
                      </li>
                    </ol>
                  </li>
                  <li>Download the iOS agent source code.<br>For more information on the agent version related to the IoT Server version you are using, see <a href="{{< param doclink >}}entgra-iot-server-agent-compatibility.html" data-linked-resource-id="352814129" data-linked-resource-version="1" data-linked-resource-type="page">Entgra IoT Server and Agent Compatibility</a>.</li>

                  <li><a class="external-link" href="https://help.apple.com/xcode/mac/8.0/#/devdc0193470" rel="nofollow">Build</a>&nbsp;and export the project as an iOS application using Xcode. This will generate an&nbsp;<code>ipa</code>&nbsp;file.</li>
                  <li>Sign in to Entgra IoT Server's App Publisher console: <code>https://&lt;IOTS_SERVER_HOST&gt;:9443/publisher</code>.<br>The default username is <code>admin</code> and the default password is <code>admin</code>, and the default <code>IOTS_SERVER_HOST</code> is <code>localhost</code>.</li>
                  <li><a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813333/Creating+an+iOS+Application" data-linked-resource-id="352813333" data-linked-resource-version="1" data-linked-resource-type="page">Create a new application and upload the <code>ipa</code> file you just generated</a>.</li>
                  <li>Once the application is created, click on the application.</li>
                  <li>Note down the App ID from the URL.<br>Example: <code>https://172.20.10.12:9443/publisher/asset/mobileapp/<strong>667026af-2ed4-426f-95c3-246a5707db66</strong></code></li>
                  <li>Enter the App ID as the value for <strong>Agent App ID</strong>.</li>
                </ol>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <th>Consumer Key</th>
          <td>Open the&nbsp;<code>token.json</code>&nbsp;file you just generated and enter the value given for the <code>consumer_key</code> here.</td>
        </tr>
        <tr>
          <th>Consumer Secret</th>
          <td>Enter the value given for the <code>consumer_secret</code> in the <code>token.json</code> file.</td>
        </tr>
        <tr>
          <th>Access Token</th>
          <td>Enter the value given for <code>access_token</code> in the <code>token.json</code> file.</td>
        </tr>
        <tr>
          <th>Access Secret</th>
          <td>Enter the value given for <code>access_secre</code>t in the <code>token.json</code> file, here.</td>
        </tr>
        <tr>
          <th>Access Token Expiry</th>
          <td>Enter the value given for <code>access_token_expiry</code> in the <code>token.json</code> file, here.</td>
        </tr>
      </tbody>
    </table>

8.  Click **Save**.

# Renewing the DEP Server Token

The Device Enrollment Program (DEP) server token is only valid for a year (365 days). Therefore, you need to renew it and update the Entgra IoT Server's iOS platform configurations with the new token. 

## Generating the public key

The public key you generated when [Adding the Entgra EMM Solution to the DEP Portal]({{< param doclink >}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#adding-normal-ios-devices-using-the-apple-configurator) is configured to expire in a year. Therefore, you need to generate a new public key in the `.pem` format.

1.  Create a new directory to generate the public key.

2.  Create a file named `openssl.cnf` in the directory you just created.

3.  Copy the code given below to the `openssl.cnf` and save it.

    
    [ v3_req ]# Extensions to add to a certificate request
    basicConstraints=CA:TRUE
    keyUsage = digitalSignature, keyEncipherment

    [ v3_ca ]
    # Extensions for a typical CA
    # PKIX recommendation.
    subjectKeyIdentifier=hash
    authorityKeyIdentifier=keyid:always,issuer
    # This is what PKIX recommends but some broken software chokes on critical
    # extensions.
    basicConstraints = critical,CA:true
    # So we do this instead.
    #basicConstraints = CA:true
    # Key usage: this is typical for a CA certificate. However since it will
    # prevent it being used as an test self-signed certificate it is best
    # left out by default.
    keyUsage = digitalSignature, keyCertSign, cRLSign
    

4.  Navigate into the directory and run the commands given below in the given order.

    
    openssl genrsa -out dep_private.key 4096 

    openssl req -new -key dep_private.key -out dep.csr

    openssl x509 -req -days 365 -in dep.csr -signkey dep_private.key -out dep.crt -extensions v3_ca -extfile ./openssl.cnf

    openssl x509 -in dep.crt -out dep.pem
    

    Now, you see the `dep.pem` file created in the directory you created.

## Generating the new DEP server token

Follow the steps given below to generate the new DEP server token from the DEP portal:

1.  Navigate to the [Apple Deployment Programs](https://idmsa.apple.com/IDMSWebAuth/login.html?disable2SV=true&rv=2&appIdKey=09273cfd6b56a8ce5af52a0153d1d796d364e03a36c6e87ef21e92c77a83ef3f&path=/qforms/web/index/avs&language=US-EN&country=US).

    

    Note!

    

    Do not close this browser session until you are done configuring the DEP portal. If you do close the browser session, you need to enter the verification code again and start configuring the DEP portal from where you stopped.

    

    

2.  Sign in with your organization's Apple credentials.

3.  Click **Get Started** to automate the Mobile Device Management (MDM) enrollment.  
    ![image](352824640.png)

4.  Confirm your identity by entering the verification code that was sent to the device you entered when creating an account for DEP, and click **Continue**.   
    ![image](352824612.png)  
    The DEP portal screen appears.
5.  In the left-navigation pane, select **Manage Servers.** 
6.  Click on the server you created for Entgra IoT Server's EMM solution.

7.  Click **Replace Key** and upload the `.pem` file you just generated.  
    ![image](352824629.png)

8.  Click **Generate New Token** to download the new DEP server token.   
    An encrypted Apple server token file in the `.` `p7m` file format downloads. Make sure to save it in a convenient location.  
    ![image](352824618.png)
9.  Click **OK**.

## Updating Entgra IoT Server with the token

Follow the steps given below to update the token details in the Entgra IoT Server:

1.  1.  Navigate to the folder where you saved the [Apple server token you downloaded when Adding the Entgra EMM Solution to the DEP Portal]({{< param doclink >}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#adding-normal-ios-devices-using-the-apple-configurator) via the terminal.

    2.  Decrypt the server token using the command given below:

        `openssl smime -decrypt -in "<THE-.PM7-TOKEN-SERVER-FILE-NAME>.pm7" -inkey "dep_private.key" > token.json`

        You see the `token.json` file created in the same directory.

    3.  Start Entgra IoT Server's core profile.

        
        cd <IOTS_HOME>/bin
        ./iot-server.sh
        

    4.  Sign in by entering the EMM administrators username and password. The default username is `admin` and the default password is `admin`.
    5.  Click the ![image](5.png) icon **> CONFIGURATION 
    MANAGEMENT > PLATFORM CONFIGURATIONS > iOS Configurations**.
    6.  Scroll down until you come to DEP only configurations.  
        ![image](352824585.png)
    7.  Update the DEP related details:

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>Agent App ID</th>
              <td>
                <p>You are able to enroll and iOS device with Entgra IoT Server's EMM solution, with or without the agent.</p>
                <ul>
                  <li>
                    <p><strong>If you are not using the agent</strong>, you can leave this section blank.</p>
                  </li>
                  <li>
                    <p><strong>If you are using the agent</strong>, follow the steps given below:</p>
                    
                      <p class="title">Before you begin!</p><span class="aui-icon aui-icon-small aui-iconfont-approve confluence-information-macro-icon"></span>
                      
                        <ul>
                          <li><a class="external-link" href="https://developer.apple.com/xcode/download/" rel="nofollow">Download Xcode</a>&nbsp;and install it.</li>
                        </ul>
                      
                    
                    <ol>
                      <li>Configure Entgra IoT Server to install iOS mobile applications:<ol>
                          <li style="list-style-type: none;background-image: none;">
                            <ol>
                              <li>Open&nbsp;the&nbsp;<code>&lt;IOTS_HOME&gt;/conf/app-manager.xml</code>&nbsp;file.</li>
                              <li>
                                <p>Add&nbsp;<code>%https%</code>&nbsp;as&nbsp;the value for&nbsp;the&nbsp;<code>AppDownloadURLHost</code>&nbsp;property.</p>
                                
                                  
                                    
                                  
                                
                              </li>
                            </ol>
                          </li>
                        </ol>
                      </li>
                      <li>Download the iOS agent source code.<br>For more information on the agent version related to the IoT Server version you are using, see <a href="{{< param doclink >}}entgra-iot-server-agent-compatibility.html" data-linked-resource-id="352814129" data-linked-resource-version="1" data-linked-resource-type="page">Entgra IoT Server and Agent Compatibility</a>.</li>

                      <li><a class="external-link" href="https://help.apple.com/xcode/mac/8.0/#/devdc0193470" rel="nofollow">Build</a>&nbsp;and export the project as an iOS application using Xcode. This will generate an&nbsp;<code>ipa</code>&nbsp;file.</li>
                      <li>Sign in to Entgra IoT Server's App Publisher console: <code>https://&lt;IOTS_SERVER_HOST&gt;:9443/publisher</code>.<br>The default username is <code>admin</code> and the default password is <code>admin</code>, and the default <code>IOTS_SERVER_HOST</code> is <code>localhost</code>.</li>
                      <li><a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813333/Creating+an+iOS+Application" data-linked-resource-id="352813333" data-linked-resource-version="1" data-linked-resource-type="page">Create a new application and upload the <code>ipa</code> file you just generated</a>.</li>
                      <li>Once the application is created, click on the application.</li>
                      <li>Note down the App ID from the URL.<br>Example: <code>https://172.20.10.12:9443/publisher/asset/mobileapp/<strong>667026af-2ed4-426f-95c3-246a5707db66</strong></code></li>
                      <li>Enter the App ID as the value for <strong>Agent App ID</strong>.</li>
                    </ol>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <th>Consumer Key</th>
              <td>Open the&nbsp;<code>token.json</code>&nbsp;file you just generated and enter the value given for the <code>consumer_key</code> here.</td>
            </tr>
            <tr>
              <th>Consumer Secret</th>
              <td>Enter the value given for the <code>consumer_secret</code> in the <code>token.json</code> file.</td>
            </tr>
            <tr>
              <th>Access Token</th>
              <td>Enter the value given for <code>access_token</code> in the <code>token.json</code> file.</td>
            </tr>
            <tr>
              <th>Access Secret</th>
              <td>Enter the value given for <code>access_secre</code>t in the <code>token.json</code> file, here.</td>
            </tr>
            <tr>
              <th>Access Token Expiry</th>
              <td>Enter the value given for <code>access_token_expiry</code> in the <code>token.json</code> file, here.</td>
            </tr>
          </tbody>
        </table>

    8.  Click **Save**.


