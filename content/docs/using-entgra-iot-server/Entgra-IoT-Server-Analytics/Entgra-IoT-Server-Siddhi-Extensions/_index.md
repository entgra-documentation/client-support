# Entgra IoT Server Siddhi Extensions


Entgra IoT Server's analytics feature allows you to publish operations to devices via the external or internal events that come through analytics. Once published, an operation entry is created in the Entgra IoT Server's core profile. These operations are delivered to the device via [Firebase Cloud Messaging (FCM)]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#configuring-android-with-firebase-cloud-messaging) or [local polling]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#android-notification-methods) for android devices and via [Apple Push Notification Service (APNS)]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-notification-method/) for iOS.


To expose basic functions in the Entgra IoT Server's device management core to the Siddhi runtime, you need the following Entgra IoT Server Siddhi extensions. You can use them according to your scenario:

## Confirming enrolled devices

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension returns true if a device matches the given device ID of a device that is already enrolled with Entgra IoT Server and device type, such as Android, iOS, and Windows. Else, it returns false.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>device:isEnrolled(deviceId, deviceType)</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>deviceId</code> and <code>deviceType</code> values defined. Else, you need to define values for them.</p>
          
            
              from inputStream[device:isEnrolled(deviceId, deviceType)]
select deviceId insert into outputStream;
            
          
        
      </td>
    </tr>
  </tbody>
</table>

## Confirming if the device is in a device group

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension returns true if the given device that has the specified device ID and device type, such as Android, iOS, and Windows, is in the specified group.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>device:isInGroup(groupId, deviceId, deviceType)</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>groupId</code>, <code>deviceId</code>, and <code>deviceType</code> values defined. Else, you need to define values for them.</p>
          
            
              from inputStream[device:isInGroup(groupId, deviceId, deviceType)] 
select deviceId insert into outputStream;
            
          
        
      </td>
    </tr>
  </tbody>
</table>

## Confirming if a user has a specific device type enrolled

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension checks if a user having the specified username has already enrolled a specific device type, such as Android, iOS, and Windows.</p>
        <p>Optionally, you are able to specify the status of the device too, such as active, inactive, and removed.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>device:hasDevicesOfUser(user, deviceType [, status])</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>user</code> and <code>deviceId</code> values defined. Else, you need to define values for them.</p>
          
            
            <script class="ap-iframe-body-script">
              (function() {
                var data = {
                  "addon_key": "ch.bitvoodoo.confluence.plugins.navitabs",
                  "uniqueKey": "ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup8350938129514489583",
                  "key": "localtabgroup",
                  "moduleType": "dynamicContentMacros",
                  "moduleLocation": "content",
                  "cp": "/wiki",
                  "general": "",
                  "w": "100px",
                  "h": "100px",
                  "url": "https://navitabs.connect.bitvoodoo.ch/static/macros/localtab-group/render?paramVerticalTabs=&pageVersion=1&pageId=352822814&macroId=d8fbb9080e20886193d0b0917d337321&xdm_e=https%3A%2F%2Fentgra.atlassian.net&xdm_c=channel-ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup8350938129514489583&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=ch.bitvoodoo.confluence.plugins.navitabs&lic=active&cv=1.527.0&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJxc2giOiJhMWQ3MDg5NGEyYjJkODZlMjAwMDlmYjY2Nzg1ZjEwMGU2NjMzYTUxMjcxMTlhOGRhM2NlMmEyM2YxNWQ2NzQzIiwiaXNzIjoiMzQ3YjE5YzYtOTRjYi0zMWVhLTg0YjEtM2JhYjNiNDFhMDI5IiwiY29udGV4dCI6e30sImV4cCI6MTU3MTgxNDQ0MiwiaWF0IjoxNTcxODE0MjYyfQ.w0b0IJIssE-Tf-6zxmgTCWJGgtIQwHQ1gfRNG6pSDlQ",
                  "contextJwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJpc3MiOiIzNDdiMTljNi05NGNiLTMxZWEtODRiMS0zYmFiM2I0MWEwMjkiLCJjb250ZXh0Ijp7ImxpY2Vuc2UiOnsiYWN0aXZlIjp0cnVlfSwiY29uZmx1ZW5jZSI6eyJtYWNybyI6eyJvdXRwdXRUeXBlIjoiaHRtbF9leHBvcnQiLCJoYXNoIjoiZDhmYmI5MDgwZTIwODg2MTkzZDBiMDkxN2QzMzczMjEiLCJpZCI6ImQ4ZmJiOTA4MGUyMDg4NjE5M2QwYjA5MTdkMzM3MzIxIn0sImNvbnRlbnQiOnsidHlwZSI6InBhZ2UiLCJ2ZXJzaW9uIjoiMSIsImlkIjoiMzUyODIyODE0In0sInNwYWNlIjp7ImtleSI6IklvVFMzNzAiLCJpZCI6IjM1MjY0OTIzNiJ9fX0sImV4cCI6MTU3MTgxNTE2MiwiaWF0IjoxNTcxODE0MjYyfQ.12o_xzKFSJotoqg6HYUekgzsoDEWOlQ-75f951oZ3bs",
                  "structuredContext": "{\"license\":{\"active\":true},\"confluence\":{\"macro\":{\"outputType\":\"html_export\",\"hash\":\"d8fbb9080e20886193d0b0917d337321\",\"id\":\"d8fbb9080e20886193d0b0917d337321\"},\"content\":{\"type\":\"page\",\"version\":\"1\",\"id\":\"352822814\"},\"space\":{\"key\":\"IoTS370\",\"id\":\"352649236\"}}}",
                  "contentClassifier": "content",
                  "productCtx": "{\"page.id\":\"352822814\",\"macro.hash\":\"d8fbb9080e20886193d0b0917d337321\",\"space.key\":\"IoTS370\",\"user.id\":\"5d9323e6e610e60dd0600646\",\"page.type\":\"page\",\"content.version\":\"1\",\"page.title\":\"Entgra IoT Server Siddhi Extensions\",\"macro.body\":\"\u003cdiv class=\\\"ap-container\\\" id=\\\"ap-ch.bitvoodoo.confluence.plugins.navitabs__localtab6028181051633393574\\\"\u003e\\n\\n \u003cdiv class=\\\"ap-conte\",\": = | RAW | = :\":null,\"space.id\":\"352649236\",\"macro.truncated\":\"true\",\"content.type\":\"page\",\"output.type\":\"html_export\",\"page.version\":\"1\",\"user.key\":\"8a7f808a6d80e7d5016d86c4cba6047b\",\"content.id\":\"352822814\",\"macro.id\":\"d8fbb9080e20886193d0b0917d337321\"}",
                  "timeZone": "Asia/Colombo",
                  "origin": "https://navitabs.connect.bitvoodoo.ch",
                  "hostOrigin": "https://entgra.atlassian.net",
                  "apiMigrations": {
                    "gdpr": true
                  }
                };
                if (window.AP && window.AP.subCreate) {
                  window._AP.appendConnectAddon(data);
                } else {
                  require(['ac/create'], function(create) {
                    create.appendConnectAddon(data);
                  });
                }
              }());
            </script>
          
        
      </td>
    </tr>
  </tbody>
</table>

## Getting the list of devices enrolled by a user

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension returns a list of devices in the JSON format if it belongs to the specified device type and is owned by the specified user.</p>
        <p>Optionally, you are able to specify the status of the device too, such as active, inactive, and removed.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>device:getDevicesOfUser(user, deviceType [, status])</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>user</code>, <code>status</code>, and <code>deviceType</code> values defined. Else, you need to define values for them.</p>
          
            
            <script class="ap-iframe-body-script">
              (function() {
                var data = {
                  "addon_key": "ch.bitvoodoo.confluence.plugins.navitabs",
                  "uniqueKey": "ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup1827451242524969674",
                  "key": "localtabgroup",
                  "moduleType": "dynamicContentMacros",
                  "moduleLocation": "content",
                  "cp": "/wiki",
                  "general": "",
                  "w": "100px",
                  "h": "100px",
                  "url": "https://navitabs.connect.bitvoodoo.ch/static/macros/localtab-group/render?paramVerticalTabs=&pageVersion=1&pageId=352822814&macroId=e2d7de035249ceff011aba21da855ffc&xdm_e=https%3A%2F%2Fentgra.atlassian.net&xdm_c=channel-ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup1827451242524969674&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=ch.bitvoodoo.confluence.plugins.navitabs&lic=active&cv=1.527.0&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJxc2giOiI2NWY2ZTAwZDc3MjVhYmMxNTkxZGM5MzE2MzY3Y2RlNDI1YmJlMzFiMTdkMTNkYmJlZTIwMzAzY2I3NDllZTJmIiwiaXNzIjoiMzQ3YjE5YzYtOTRjYi0zMWVhLTg0YjEtM2JhYjNiNDFhMDI5IiwiY29udGV4dCI6e30sImV4cCI6MTU3MTgxNDQ0MiwiaWF0IjoxNTcxODE0MjYyfQ.bF5HbeRHltHuZ5tlWiREo44bXeuA5EPnRDRrwSbOo_w",
                  "contextJwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJpc3MiOiIzNDdiMTljNi05NGNiLTMxZWEtODRiMS0zYmFiM2I0MWEwMjkiLCJjb250ZXh0Ijp7ImxpY2Vuc2UiOnsiYWN0aXZlIjp0cnVlfSwiY29uZmx1ZW5jZSI6eyJtYWNybyI6eyJvdXRwdXRUeXBlIjoiaHRtbF9leHBvcnQiLCJoYXNoIjoiZTJkN2RlMDM1MjQ5Y2VmZjAxMWFiYTIxZGE4NTVmZmMiLCJpZCI6ImUyZDdkZTAzNTI0OWNlZmYwMTFhYmEyMWRhODU1ZmZjIn0sImNvbnRlbnQiOnsidHlwZSI6InBhZ2UiLCJ2ZXJzaW9uIjoiMSIsImlkIjoiMzUyODIyODE0In0sInNwYWNlIjp7ImtleSI6IklvVFMzNzAiLCJpZCI6IjM1MjY0OTIzNiJ9fX0sImV4cCI6MTU3MTgxNTE2MiwiaWF0IjoxNTcxODE0MjYyfQ.yjdzYT3_avKKORUHpBGhn59kHCNUPtqoeLyv8EITal4",
                  "structuredContext": "{\"license\":{\"active\":true},\"confluence\":{\"macro\":{\"outputType\":\"html_export\",\"hash\":\"e2d7de035249ceff011aba21da855ffc\",\"id\":\"e2d7de035249ceff011aba21da855ffc\"},\"content\":{\"type\":\"page\",\"version\":\"1\",\"id\":\"352822814\"},\"space\":{\"key\":\"IoTS370\",\"id\":\"352649236\"}}}",
                  "contentClassifier": "content",
                  "productCtx": "{\"page.id\":\"352822814\",\"macro.hash\":\"e2d7de035249ceff011aba21da855ffc\",\"space.key\":\"IoTS370\",\"user.id\":\"5d9323e6e610e60dd0600646\",\"page.type\":\"page\",\"content.version\":\"1\",\"page.title\":\"Entgra IoT Server Siddhi Extensions\",\"macro.body\":\"\u003cdiv class=\\\"ap-container\\\" id=\\\"ap-ch.bitvoodoo.confluence.plugins.navitabs__localtab3857271215745760034\\\"\u003e\\n\\n \u003cdiv class=\\\"ap-conte\",\": = | RAW | = :\":null,\"space.id\":\"352649236\",\"macro.truncated\":\"true\",\"content.type\":\"page\",\"output.type\":\"html_export\",\"page.version\":\"1\",\"user.key\":\"8a7f808a6d80e7d5016d86c4cba6047b\",\"content.id\":\"352822814\",\"macro.id\":\"e2d7de035249ceff011aba21da855ffc\"}",
                  "timeZone": "Asia/Colombo",
                  "origin": "https://navitabs.connect.bitvoodoo.ch",
                  "hostOrigin": "https://entgra.atlassian.net",
                  "apiMigrations": {
                    "gdpr": true
                  }
                };
                if (window.AP && window.AP.subCreate) {
                  window._AP.appendConnectAddon(data);
                } else {
                  require(['ac/create'], function(create) {
                    create.appendConnectAddon(data);
                  });
                }
              }());
            </script>
          
        
      </td>
    </tr>
  </tbody>
</table>

## Checking for devices that belong to a specified device status

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension checks if there are any enrolled devices that belong to the specified device status.</p>Optionally, you are able to specify the device type too, such as Android, iOS, and Windows.
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td>
        <p><code>device:hasDevicesOfStatus(status [, deviceType])</code></p>
      </td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>status</code> and <code>deviceId</code> values defined. Else, you need to define values for them.</p>
          
            
            <script class="ap-iframe-body-script">
              (function() {
                var data = {
                  "addon_key": "ch.bitvoodoo.confluence.plugins.navitabs",
                  "uniqueKey": "ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup6750615065533199490",
                  "key": "localtabgroup",
                  "moduleType": "dynamicContentMacros",
                  "moduleLocation": "content",
                  "cp": "/wiki",
                  "general": "",
                  "w": "100px",
                  "h": "100px",
                  "url": "https://navitabs.connect.bitvoodoo.ch/static/macros/localtab-group/render?paramVerticalTabs=&pageVersion=1&pageId=352822814&macroId=efecd6150e83dc129994cd128f373dbe&xdm_e=https%3A%2F%2Fentgra.atlassian.net&xdm_c=channel-ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup6750615065533199490&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=ch.bitvoodoo.confluence.plugins.navitabs&lic=active&cv=1.527.0&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJxc2giOiJlMGVjYTEzMDk4OGRiOTk4MTA3NzdhMjIwMTRlYmIxOWY1OWY5OTZmY2JkZGJiYmJjMWI1NTlkZWQwMjRkYTQwIiwiaXNzIjoiMzQ3YjE5YzYtOTRjYi0zMWVhLTg0YjEtM2JhYjNiNDFhMDI5IiwiY29udGV4dCI6e30sImV4cCI6MTU3MTgxNDQ0MiwiaWF0IjoxNTcxODE0MjYyfQ.dC6hIkFPV-GxhFpuaD9u3WQQdnGt-wyDf_YzeAd2A7U",
                  "contextJwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJpc3MiOiIzNDdiMTljNi05NGNiLTMxZWEtODRiMS0zYmFiM2I0MWEwMjkiLCJjb250ZXh0Ijp7ImxpY2Vuc2UiOnsiYWN0aXZlIjp0cnVlfSwiY29uZmx1ZW5jZSI6eyJtYWNybyI6eyJvdXRwdXRUeXBlIjoiaHRtbF9leHBvcnQiLCJoYXNoIjoiZWZlY2Q2MTUwZTgzZGMxMjk5OTRjZDEyOGYzNzNkYmUiLCJpZCI6ImVmZWNkNjE1MGU4M2RjMTI5OTk0Y2QxMjhmMzczZGJlIn0sImNvbnRlbnQiOnsidHlwZSI6InBhZ2UiLCJ2ZXJzaW9uIjoiMSIsImlkIjoiMzUyODIyODE0In0sInNwYWNlIjp7ImtleSI6IklvVFMzNzAiLCJpZCI6IjM1MjY0OTIzNiJ9fX0sImV4cCI6MTU3MTgxNTE2MiwiaWF0IjoxNTcxODE0MjYyfQ.0Vf8HQgue74000dd5UQ9Z-2jKY-q_0eK8FUzA0awtNE",
                  "structuredContext": "{\"license\":{\"active\":true},\"confluence\":{\"macro\":{\"outputType\":\"html_export\",\"hash\":\"efecd6150e83dc129994cd128f373dbe\",\"id\":\"efecd6150e83dc129994cd128f373dbe\"},\"content\":{\"type\":\"page\",\"version\":\"1\",\"id\":\"352822814\"},\"space\":{\"key\":\"IoTS370\",\"id\":\"352649236\"}}}",
                  "contentClassifier": "content",
                  "productCtx": "{\"page.id\":\"352822814\",\"macro.hash\":\"efecd6150e83dc129994cd128f373dbe\",\"space.key\":\"IoTS370\",\"user.id\":\"5d9323e6e610e60dd0600646\",\"page.type\":\"page\",\"content.version\":\"1\",\"page.title\":\"Entgra IoT Server Siddhi Extensions\",\"macro.body\":\"\u003cdiv class=\\\"ap-container\\\" id=\\\"ap-ch.bitvoodoo.confluence.plugins.navitabs__localtab9074903177827817555\\\"\u003e\\n\\n \u003cdiv class=\\\"ap-conte\",\": = | RAW | = :\":null,\"space.id\":\"352649236\",\"macro.truncated\":\"true\",\"content.type\":\"page\",\"output.type\":\"html_export\",\"page.version\":\"1\",\"user.key\":\"8a7f808a6d80e7d5016d86c4cba6047b\",\"content.id\":\"352822814\",\"macro.id\":\"efecd6150e83dc129994cd128f373dbe\"}",
                  "timeZone": "Asia/Colombo",
                  "origin": "https://navitabs.connect.bitvoodoo.ch",
                  "hostOrigin": "https://entgra.atlassian.net",
                  "apiMigrations": {
                    "gdpr": true
                  }
                };
                if (window.AP && window.AP.subCreate) {
                  window._AP.appendConnectAddon(data);
                } else {
                  require(['ac/create'], function(create) {
                    create.appendConnectAddon(data);
                  });
                }
              }());
            </script>
          
        
      </td>
    </tr>
  </tbody>
</table>

## Getting the list of devices based on the device status

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>This extension returns a list of devices in the JSON format if it belongs to specified device status, such as active, inactive, and removed.</p>
        <p>Optionally, you are able to specify the device type too, such as Android, iOS, and Windows.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>device:getDevicesOfStatus(status [, deviceType])</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>status</code> and <code>deviceId</code> values defined. Else, you need to define values for them.</p>
          
            
            <script class="ap-iframe-body-script">
              (function() {
                var data = {
                  "addon_key": "ch.bitvoodoo.confluence.plugins.navitabs",
                  "uniqueKey": "ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup1975717670838310678",
                  "key": "localtabgroup",
                  "moduleType": "dynamicContentMacros",
                  "moduleLocation": "content",
                  "cp": "/wiki",
                  "general": "",
                  "w": "100px",
                  "h": "100px",
                  "url": "https://navitabs.connect.bitvoodoo.ch/static/macros/localtab-group/render?paramVerticalTabs=&pageVersion=1&pageId=352822814&macroId=cca730a92fbba26fc554a8cf01d246c2&xdm_e=https%3A%2F%2Fentgra.atlassian.net&xdm_c=channel-ch.bitvoodoo.confluence.plugins.navitabs__localtabgroup1975717670838310678&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=ch.bitvoodoo.confluence.plugins.navitabs&lic=active&cv=1.527.0&jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJxc2giOiJhNDk1ZDZiNmYyMmJhZjc5MjliOTM2NDM5M2NmOTJjNzE1MmRjYzkxYTJhMzA0ZTNjOTA4NTY3ZWI5Yjg3N2IzIiwiaXNzIjoiMzQ3YjE5YzYtOTRjYi0zMWVhLTg0YjEtM2JhYjNiNDFhMDI5IiwiY29udGV4dCI6e30sImV4cCI6MTU3MTgxNDQ0MiwiaWF0IjoxNTcxODE0MjYyfQ.rKWC0vD8PYJZLVrivJ0j0_waj7hN-b5qRNNkD5MxZSc",
                  "contextJwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ZDkzMjNlNmU2MTBlNjBkZDA2MDA2NDYiLCJpc3MiOiIzNDdiMTljNi05NGNiLTMxZWEtODRiMS0zYmFiM2I0MWEwMjkiLCJjb250ZXh0Ijp7ImxpY2Vuc2UiOnsiYWN0aXZlIjp0cnVlfSwiY29uZmx1ZW5jZSI6eyJtYWNybyI6eyJvdXRwdXRUeXBlIjoiaHRtbF9leHBvcnQiLCJoYXNoIjoiY2NhNzMwYTkyZmJiYTI2ZmM1NTRhOGNmMDFkMjQ2YzIiLCJpZCI6ImNjYTczMGE5MmZiYmEyNmZjNTU0YThjZjAxZDI0NmMyIn0sImNvbnRlbnQiOnsidHlwZSI6InBhZ2UiLCJ2ZXJzaW9uIjoiMSIsImlkIjoiMzUyODIyODE0In0sInNwYWNlIjp7ImtleSI6IklvVFMzNzAiLCJpZCI6IjM1MjY0OTIzNiJ9fX0sImV4cCI6MTU3MTgxNTE2MiwiaWF0IjoxNTcxODE0MjYyfQ.A-ZyheI3AgMzFXtrC-OIWeydy-QoBqwYs6qzvNAupGQ",
                  "structuredContext": "{\"license\":{\"active\":true},\"confluence\":{\"macro\":{\"outputType\":\"html_export\",\"hash\":\"cca730a92fbba26fc554a8cf01d246c2\",\"id\":\"cca730a92fbba26fc554a8cf01d246c2\"},\"content\":{\"type\":\"page\",\"version\":\"1\",\"id\":\"352822814\"},\"space\":{\"key\":\"IoTS370\",\"id\":\"352649236\"}}}",
                  "contentClassifier": "content",
                  "productCtx": "{\"page.id\":\"352822814\",\"macro.hash\":\"cca730a92fbba26fc554a8cf01d246c2\",\"space.key\":\"IoTS370\",\"user.id\":\"5d9323e6e610e60dd0600646\",\"page.type\":\"page\",\"content.version\":\"1\",\"page.title\":\"Entgra IoT Server Siddhi Extensions\",\"macro.body\":\"\u003cdiv class=\\\"ap-container\\\" id=\\\"ap-ch.bitvoodoo.confluence.plugins.navitabs__localtab7260901546166170097\\\"\u003e\\n\\n \u003cdiv class=\\\"ap-conte\",\": = | RAW | = :\":null,\"space.id\":\"352649236\",\"macro.truncated\":\"true\",\"content.type\":\"page\",\"output.type\":\"html_export\",\"page.version\":\"1\",\"user.key\":\"8a7f808a6d80e7d5016d86c4cba6047b\",\"content.id\":\"352822814\",\"macro.id\":\"cca730a92fbba26fc554a8cf01d246c2\"}",
                  "timeZone": "Asia/Colombo",
                  "origin": "https://navitabs.connect.bitvoodoo.ch",
                  "hostOrigin": "https://entgra.atlassian.net",
                  "apiMigrations": {
                    "gdpr": true
                  }
                };
                if (window.AP && window.AP.subCreate) {
                  window._AP.appendConnectAddon(data);
                } else {
                  require(['ac/create'], function(create) {
                    create.appendConnectAddon(data);
                  });
                }
              }());
            </script>
          
        
      </td>
    </tr>
  </tbody>
</table>

## Getting the property from the JSON string

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>Returns the value of the property from the given JSON string.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>json:getProperty(payload, 'property name')</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          
            
              from inputStream
select id, json:getProperty(payload, 'property_name') as property_name insert into outputStream;
            
          
          <p>Example:</p>
          
            
              from inputStream
select id, json:getProperty(payload, 'latitude') as latitude insert into outputStream;
            
          
        
      </td>
    </tr>
  </tbody>
</table>

## Getting the JSON array

<table>
  <tbody>
    <tr>
      <th>Description</th>
      <td>
        <p>Returns JSON array for objects in the argument, such as <code>STRING</code>, <code>INT</code>, <code>DOUBLE</code>, or <code>FLOAT</code>.</p>
      </td>
    </tr>
    <tr>
      <th>Extension</th>
      <td>
        <p><code>json:getArray(args ..)</code></p>
      </td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          <p>It is assumed that the input stream has the <code>arg1</code> and <code>arg2</code> values defined. Else, you need to define values for them.</p>
          
            
              from inputStream 
select json:getArray(arg1, arg2) as array insert into outputStream;
            
          
        
      </td>
    </tr>
  </tbody>
</table>

## Checking if the specified coordinates are within the geo location

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Description</th>
      <td>This extension checks if the specified longitude and latitude are within the geo fence specified by the <code>geoJson</code>.</td>
    </tr>
    <tr>
      <th>Extension</th>
      <td><code>geo:within(longitude,latitude,geoJson)</code></td>
    </tr>
    <tr>
      <th>Usage</th>
      <td>
        
          
            
              from inputStream [geo:within(longitude,latitude,"{geoJSOn}")]
select deviceId insert into outputStream;
            
          
          <p>Example</p>
          
            
              from inputStream [geo:within(longitude,latitude,"{'type':'Polygon','coordinates'
:[[[79.85213577747345, 6.909673257977737],[79.85266149044037,6.909673257977737],
[79.85266149044037,6.91003538888127],[79.85213577747345,6.91003538888127],
[79.85213577747345,6.909673257977737]]]}")]
select deviceId insert into outputStream;
            
          
        
      </td>
    </tr>
  </tbody>
</table>

## What's next?

*   For more information on the WSO2 Siddhi extensions, see the [Sidhhi extensions guide](https://wso2.github.io/siddhi/extensions/).
*   Try out the tutorials listed below and see how the extensions described above are put into use.
    *   [Applying Policies on Devices Based on Geofencing]({{< param doclink >}}Monitoring-Devices-Using-Location-Based-Services/)


    *   [Analyzing the Data Gathered from Devices]({{< param doclink >}}docs/tutorials/monitoring-devices/analyzing-data-gathered-from-devices/)


    *   [Sending Operations to Devices Based on Time]({{< param doclink >}}docs/tutorials/monitoring-devices/sending-operations-to-devices-based-on-time/)

