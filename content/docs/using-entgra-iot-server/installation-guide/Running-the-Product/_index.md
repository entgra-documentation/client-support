# Installing the product

# Running the Product

To run Entgra products, you start the product server at the command line. The following sections in this page explain everything you need to know about running the product:

## Starting the Server

Before you begin


If you wish to try out the Raspberry Pi and Arduino sample plugins, in the Entgra IoT Server console, navigate to the Entgra IoTS directory and run the `device-plugins-deployer.xml` file.  
Example: 


cd <IOTS_HOME>/samples
mvn clean install -f device-plugins-deployer.xml


**NOTE**: Android, Windows and Android Sense are included in the server by default. You will need to [configure Entgra IoT Server to try out iOS]({{< param doclink >}}tutorials/ios/).


Follow the instructions below to start your Entgra product based on the Operating System you use:

* * *

### Running IoT Server

Once you start the server, you can access the Entgra IoT Server consoles by opening a Web browser and typing in the respective URLs. 

The Entgra IoTS Console URLs are as follows:

    https://<IOTS_HTTP_HOST>:9763/portal
*   **Access via secure HTTP**:   
    Entgra IoT Server Device Management Console:` https://<IOTS_HTTPS_HOST>:9443/devicemgt`  
    Entgra IoT Server Management Console: `https://<IOTS_HTTPS_HOST>:9443/carbon` Entgra IoT Server Device Monitoring Dashboard: `https://<IOTS_HTTPS_HOST>:9443/portal`

When accessing the Management Console from the same server where it's installed, you can type `localhost` instead of the IP address:  `https://localhost:9443/carbon`


The Management Console URL can be changed by uncommenting and modifying the value of the `MgtHostName` in the `<PRODUCT_HOME>/conf/carbon.xml` file.

Example:

` <MgtHostName>localhost</MgtHostName>`


For more information, see [Accessing the Device Management Console]
({{< param doclink>}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-device-management-console), [Accessing the Entgra IoTS Management Console]({{< param doclink>}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console), and [Accessing the WSO2 Device Monitoring Dashboard](/doc/en/lb2/Accessing-the-Entgra-IoT-Server-Consoles.html#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceMonitoringConsole).

You can use the above URLs to access the Entgra IoT Server consoles on this computer from any other computer connected to the Internet or LAN.

When these pages appear, the web browser will typically display an "insecure connection" message, which requires your confirmation before you can continue.

The IoT Server consoles are based on the HTTPS protocol, which is a combination of HTTP and SSL protocols. This protocol is generally used to encrypt the traffic from the client to server for security reasons. The certificate it works with is used only for encryption and does not prove the server identity, so when you try to access these consoles, a warning of untrusted connection is usually displayed. To continue working with this certificate, some steps should be taken to "accept" the certificate before access to the site is permitted. If you are using the Mozilla Firefox browser, this usually occurs only on the first access to the server, after which the certificate is stored in the browser database and marked as trusted. However, with other browsers, the insecure connection warning might be displayed every time you access the server.

This scenario is suitable for testing purposes, or for running the program on the company's internal networks. If you want to make these consoles available to external users, your organization should obtain a certificate signed by a well-known certificate authority, which verifies that the server actually has the name it is accessed by and that this server belongs to the given organization.

If you leave the IoT Server consoles unattended, the session will time out. The default timeout value is 15 minutes, but you can change this in the `<IOTS_HOME>/conf/tomcat/carbon/WEB-INF/web.xml` file as follows:

<session-config>
   <session-timeout>15</session-timeout>
</session-config>


Restricting Access to the Management Console and Web Applications:

You can restrict access to the management console of your product by binding the management console with selected IP addresses. Note that you can either restrict access to the management console only, or you can restrict access to all web applications in your server as explained below.

*   To control access only to the management console, add the IP addresses to the `<PRODUCT_HOME>/conf/tomcat/carbon/META-INF/context.xml` file as follows:

    `<Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="<IP-address-01>|<IP-address-02>|<IP-address-03>"/>`

    The `RemoteAddrValve` Tomcat valve defined in this file will only apply to the Carbon management console, and thereby all outside requests to the management console will be blocked. 

*   To control access to all web applications deployed on your server, add the IP addresses to the `<PRODUCT_HOME>/conf/tomcat/context.xml` file as follows:

    `<Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="<IP-address-01>|<IP-address-02>|<IP-address-03>"/>`

    The `RemoteAddrValve` Tomcat valve defined in this file will apply to each web application hosted on the Carbon server. Therefore, all outside requests to any web application will be blocked.

*   You can also restrict access to particular servlets in a web application by adding a Remote Address Filter to the `web.xml` file (stored in the `<PRODUCT_HOME>/repository/conf/tomcat/` directory), and by mapping that filter to the servlet URL. In the Remote Address Filter that you add, you can specify the IP addresses that should be allowed to access the servlet.

    The following example from a web.xml file illustrates how access to the management page (`/carbon/admin/login.jsp`) is granted only to one IP address:

    <filter>
        <filter-name>Remote Address Filter</filter-name>
        <filter-class>org.apache.catalina.filters.RemoteAddrFilter</filter-class>
            <init-param>
                <param-name>allow</param-name>
                <param-value>127.0.01</param-value>
            </init-param>
    </filter>

    <filter-mapping>
        <filter-name>Remote Address Filter</filter-name>
        <url-pattern>/carbon/admin/login.jsp</url-pattern>
    </filter-mapping>

**Note:** Any configurations (including valves) defined in the `<PRODUCT_HOME>/conf/tomcat/catalina-server.xml` file applies to all web applications and is globally available across server, regardless of host or cluster. See the official Tomcat documentation for more information about using [remote host filters](http://tomcat.apache.org/tomcat-7.0-doc/config/valve.html#Remote_Host_Filter) .


* * *

### Stopping the Server

To stop the server, press **Ctrl+C** in the command window.

---
## Accessing the Entgra IoT Server Consoles


### Accessing the Entgra IoT Server Management Console

Follow the instructions below to sign in to the Entgra IoT Server management console:

1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).

2.  Access the Entgra IoT Server Management Console using one of the following URLs:

    *   Accessing the console via HTTP:``http://<HTTP_HOST>:9763/carbon/`` Example:` http://localhost:9763/carbon/`
    *   Accessing the console via HTTPS:   
        `https://<HTTPS_HOST>:9443/carbon/` Example:` https://localhost:9443/carbon/`![image]
        (352786975.png)
3.  Enter the tenant username and password. The default username and password are both **admin**. 


    *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

    *   This field is case-sensitive.


4.  If you wish your browser to save your login details, select the **Remember Password** check box.
5.  Click **Sign-in**.   
    Upon successfully signing up in the management console home page will appear.


To sign out of the Entgra IoT Server management console click **Sign-out**.

### Accessing the Device Management Console

Follow the instructions below to sign in to the Entgra IoT Server device management console:

1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).

2.  Access the device management console.

        `http://<IOTS_HTTP_HOST>:9763/devicemgt/`

        For example:` http://localhost:9763/devicemgt/`
    *   For access via secured HTTP:   
        `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
3.  Enter the username and password, and click **LOGIN**.


    *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).

    *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.



    By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
    ![image](352786981.png)

4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
    ![image](352787008.png)

    The respective device management console will change, based on the permissions assigned to the user.

    For example, the device management console for an administrator is as follows:  
    ![image](352786987.png)

### Accessing the Data Analytics Server Console


Only users having the administrator permissions will be allowed to access the WSO2 Data Analytics Server (DAS) console. For more information, see [adding a role and permissions]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#adding-a-role-and-permissions).



1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/) and access the WSO2 DAS Console. `https://<IOTS_HOST>:9445/portal`

2.  Enter the username. The super tenant administrator has to use **admin** as his/her username. 

3.  Enter the password. The super tenant administrator has to use **admin** as the password. 

4.  Click **Sign In**.   
    Upon successfully signing in the management console home page will appear.


To sign out of the Entgra IoT Server management console click **Sign-out**.


### Accessing the API store

1.  If you have not started server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/) and access the API store that is pre-packed with Entgra IoT Server.`  https://<IOTS_HOST>:9443/api-store`

2.  Enter the username. The super tenant administrator has to use **admin** as his/her username. 

3.  Enter the password. The super tenant administrator has to use **admin** as the password. 

4.  Click **LOG IN**. 


To sing out of the device monitoring console, click the user icon, and click **LOG OUT. **


### Accessing the App Publisher


What does the app publisher do?

Entgra IoT Server provides mobile application management capabilities out of the box. You can create your applications and publish to the Entgra IoT Server app store using the WSO2 app publisher.


    https://<IOTS_HOST>:9443/publisher

2.  Enter the username. The super tenant administrator has to use **admin** as his/her username. 

3.  Enter the password. The super tenant administrator has to use **admin** as the password. 

4.  Click **LOG IN**.   
    By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
    ![image](352786981.png)
5.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
    ![image](352787008.png)

### Accessing the App Store


What does the app store do?


Entgra IoT Server provides mobile application management capabilities out of the box. The Entgra IoT Server app store functions as a normal app store. You are able to select and install applications onto devices via the store.


    https://<IOTS_HOST>:9443/store

2.  Enter the username. The super tenant administrator has to use **admin** as his/her username. 

3.  Enter the password. The super tenant administrator has to use **admin** as the password. 

4.  Click **LOG IN**.   
    By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.   
    ![image](352786981.png)
5.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
    ![image](352787008.png)

---
## Registering with Entgra IoT Server


Follow the instructions below to register with Entgra IoT Server:

1.  [Start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).

2.  Access the device management console.

        `http://<HTTP_HOST>:<HTTP_PORT>/devicemgt/`
        

        For example: [`http://localhost:9763/devicemgt/`](http://localhost:9763/admin/carbon/)
    *   For access via secured HTTP:   
        `https://<HTTPS_HOST>:<HTTPS_PORT>/devicemgt/` For example: [`https://localhost:9443/devicemgt/`](https://localhost:9443/admin/carbon/)
3.  Click **Create an Account**.  
    ![image](352787066.png)
4.  Fill out the registration form. All the fields are mandatory fields.

    To be compliant with the General Data Protection Regulations (GDPR), you need to agree to the Entgra IoT Server 3.3.0 privacy policy.
s
    ![image](352787060.png)

5.  Click **Register**.
