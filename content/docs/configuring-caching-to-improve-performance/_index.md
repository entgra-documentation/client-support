---
bookCollapseSection: true
weight: 14
---

# Configuring Caching to Improve Performance

Repeatedly getting the same information from the server is time-consuming and expensive. Therefore, you can improve the performance of Entgra IoT Server by configuring the following caching scenarios:

## Device caching configurations

Performance improves when device cache is enabled as the server doesn't have to make several request calls to get the device information such as device ID, device name, and more. Once the device cache expires, the server gets the information again when the next request call goes out and stores it until it expires again.





To improve performance, you can enable the device cache for all the worker nodes in a clustered setup.





Open the `<IOTS_HOME>/conf/cdm-config.xml` file and configure the following:  
The default configuration is as follows:

<DeviceCacheConfiguration>
	<Enable>true</Enable>
	<ExpiryTime>600</ExpiryTime>
</DeviceCacheConfiguration>

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><strong><code>Enable</code></strong></td>
      <td>
        <p>By default, the device caching is enabled. If you don't want to have it enabled you can define <code>false</code> as the value but this will affect the performance of Entgra IoT Server after enrolling many devices.</p>
      </td>
    </tr>
    <tr>
      <td><strong><code>ExpireTime</code></strong></td>
      <td>
        <p>The time the cache needs expire. Define the time in seconds.</p>
        <p>By default, a certificates cache is set to ten minutes, which is 600 seconds. <span style="color: rgb(36,41,46);">You can define a preferred expiry time period.</span></p>
      </td>
    </tr>
  </tbody>
</table>

## Certificate caching configurations

Entgra IoT Server has a caching layer before the database for mutual SSL certificates. Previously, each time a device sent a request, Entgra IoT Server authenticated and validated the mutual SSL certificate against the database. With the new approach, the server doesn't have to go to the database to gather the certificate details each time a request is made and this improves the performance.

Let's look at how you can configure it:

Open the `<IOTS_HOME>/conf/cdm-config.xml` file and configure the following: The default configuration is as follows:

<CertificateCacheConfiguration>
	<Enable>true</Enable>
	<ExpiryTime>86400</ExpiryTime>
</CertificateCacheConfiguration>

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><strong><code>Enable</code></strong></td>
      <td>
        <p>By default, the certificate caching is set true.</p>
        <p><span>If you don't want to have it enabled you can define <code>false</code> as the value but this will affect the performance of Entgra IoT Server when having many certificates.</span></p>
      </td>
    </tr>
    <tr>
      <td><strong><code>ExpireTime</code></strong></td>
      <td>
        <p>The time the certificate cache should expire. Define the time in seconds.</p>
        <p>By default, a certificates cache is set to one day, which is 86400 seconds. Since the certificate expiry time is longer y<span style="color: rgb(36,41,46);">ou can set a higher value, such as one month or more if you have few devices enrolled with Entgra IoT Server. If your server has many enrolled devices, make sure to set the value for at least 2 to 3 days and not more than that as it will lead to out of memory issues with caching.</span></p>
      </td>
    </tr>
  </tbody>
</table>

## Policy caching configurations





This section is work in progress!





Entgra IoT Server has a caching layer for the device [policies]({{< param generaldoclink >}}guide-to-work-with-the-product/device-management-guide). <Info to come in>

Let's look at how you can configure it:

Open the `<IOTS_HOME>/conf/cdm-config.xml`file and configure the following: The default configuration is as follows:

<PolicyConfiguration>
	<CacheEnable>true</CacheEnable>
</PolicyConfiguration>

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><strong><code>CacheEnable</code></strong></td>
      <td>
        <p>By default, the policy caching is set true.</p>
        <p>If you don't want to have it enabled you can define <code>false</code> as the value but this will affect the performance of Entgra IoT Server when you have many device policies.</p>
      </td>
    </tr>
  </tbody>
</table>
