---
bookCollapseSection: true
weight: 2
---
# iOS Device


The following sections guide you through configuring iOS in Entgra IoT Server and enrolling an iOS device. **Entgra IoT Server is bundled with the WSO2 Enterprise Mobility Management (WSO2 EMM) capabilities**.

**Device Requirements**  
Lowest version - iOS 8

Highest version - Always supported upto the latest OS version available.

This tutorial follows the sample scenario of the Mob X organization of which Chris (the system administrator) is configuring iOS with Entgra IoT Server for the organization and Alex (the device owner) is registering an iOS device. 

Before you begin

1.  Entgra IoT Server supports devices on iOS 7, 8, 9, and 10.
2.  Install [Oracle Java SE Development Kit (JDK)](http://java.sun.com/javase/downloads/index.jsp) version 1.8.* and set the `JAVA_HOME` environment variable. For more information on setting up `JAVA_HOME` on your OS, see [Installing the Product]({{< param doclink >}}using-entgra-iot-server/installation-guide/installing-the-product/).

3.  [Download Entgra IoT Server](https://entgra.io/) or [download WSO2 EMM](https://wso2.com/solutions/enterprise-mobility/enterprise-mobility-management/#download) and unzip it.

The downloaded Entgra IoT Server file is large. Therefore, when unzipping it the OS might extract it halfway through and stop. To avoid this, we recommend that you unzip the file via the terminal.  
Example:

`unzip wso2iot-3.2.0.zip`

4.  Start Entgra IoT Server core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

    
    cd <IOTS_HOME>/bin
    sh iot-server.sh
    

    The default port assigned for the core is 9443.

5.  In your browser, access the IoT Server Console by navigating to **`https://<IoT_HOST>:9443/devicemgt`** and log in using **admin** as the username and password.

6.  Fill in the following [iOS form](http://wso2.com/products/iot-server/csr/) via the WSO2 site. WSO2 will send you an email with the following information.

    *   The P2 repository with the iOS features, End User License Agreement (EULA) and the README.txt. 

    *   The iOS agent source code. You can use this if you want to customize the iOS agent application in Entgra IoT Server.

7.  Get your certificate signed by Apple. Register your organization with the  [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/) . Thereafter, follow the steps mentioned in  [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html) . 

## Configuring iOS support in Entgra IoT Server

For the sake of simplicity in this tutorial, we are not setting up the Entgra IoT Server iOS agent on your device. Therefore, we will not walk through the iOS client configurations. Note that the 'device ringing', 'sending messages', and 'getting the device location' operations require the Entgra IoT Server iOS agent, so those operations will not be available in this scenario.  

For more information on configuring Entgra IoT Server for iOS, including iOS client configurations, see the [iOS configurations guide]({{< param doclink >}}using-entgra-iot-server/working-with-ios/).


If you are facing issues while configuring Entgra IoT Server for iOS in your testing environment, [contact us](https://entgra.io/contact) and we will help you out.


### Configuring Entgra IoT Server to install iOS applications

The download URL for Entgra IoT Server is configured for HTTP by default. Although you can install Android mobile applications using this default configuration, to install iOS applications, you need to configure it for HTTPS as it's required for the iOS MDM protocol behavior.

Follow the steps given below to configure Entgra IoT Server to install iOS mobile applications:

1.  Open the `<IoT_HOME>/conf/app-manager.xml` file.
2.  Add `%https%` as the value for the `AppDownloadURLHost` property.

    `<Config name="AppDownloadURLHost">%https%</Config>`

    **Tip**: To test Entgra IoT Server App management features on Android devices, please use one of the following options:

    *   Change the value of the `AppDownloadURLHost` property back to HTTP 
    *   Continue using HTTPS to install applications on Android devices by [Generating a BKS File for Android]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices//#generating-a-bks-file-for-android).


    Chris has now done the configurations needed to allow users to register and monitor their devices with Entgra IoT Server. Chris then proceeds to configure iOS support in IoT Server.

### Configuring the IP or hostname

This script automatically configures the IP and creates the required SSL certificates for the IP or hostname. This method is recommended because manually configuring the IP address includes many steps and if you miss out on a step you will run into errors.

1.  Navigate to the `<IOTS_HOME>/scripts` directory.
2.  Run the `change-ip` script.  
    Example: Running the script on a Mac or Linux OS.

    **Tip:** The script will find and replace the IP address given in argument1 (`localhost`) with the IP address given as argument2 (`10.10.10.14`), in the necessary configuration files. 

    `./change-ip.sh`

1.  Change the current IP address of the IoT Server core, broker, and analytics profile.

2.  Enter the values for IoT Server core SSL certificate.

    Enter the requested information when prompted.

     <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <thead>
        <tr>
          <th>Field</th>
          <th>Usage/Purpose</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Country</td>
          <td>The name of your country. Enter the two digit code for your country.</td>
        </tr>
        <tr>
          <td>State</td>
          <td>The state your organization is at.</td>
        </tr>
        <tr>
          <td>Location</td>
          <td>The city your organization is located at.</td>
        </tr>
        <tr>
          <td>
            <p>Organization</p>
          </td>
          <td>
            <p>The name of your organization. For this scenario, we entered wso2.</p>
          </td>
        </tr>
        <tr>
          <td>Organization Unit</td>
          <td>Defined the Team ID as the organization unit.</td>
        </tr>
        <tr>
          <td>
            <p>Email</p>
          </td>
          <td>
            <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a href="mailto:chris@wso2.com" class="external-link" rel="nofollow">chris@wso2.com</a>&nbsp;as the email.</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Commonname</p>
          </td>
          <td>
            <p>Fully qualified domain name of your server.</p>
          </td>
        </tr>
      </tbody>
    </table>

    If you want to configure the steps manually, see [Configuring the IP or hostname manually]({{< param doclink >}}set-up-the-server/configuring-ip-or-hostname) and if you want to change the default ports, see [Changing the Default Ports]({{< param doclink >}}using-entgra-iot-server/product-administration/working-with-databases/#changing-the-default-ports).



    #### Installing the iOS features

After obtaining a signed CSR, the P2 repository, and the other files via email, Chris can install the specific features in the P2 repository that are required to enable iOS by following the instructions given below. 

1.  Download and extract the ios-feature-deployer that you received via email. 

2.  Copy the ios-feature-deployer folder to the `<IOTS_HOME>` directory.  
3.  Navigate inside the ios-feature-deployer folder on the terminal and execute the following command to install the iOS features to WSO2 IoT.  Agree to the End User License Agreement (EULA) to complete the iOS configurations.

    
    cd ios-feature-deployer 
    mvn clean install -f ios-feature-deployer.xml
    


    Tip by Chris

    **Tip:** To verify successful installation of the iOS features, do the following:

    Open the `<IOTS-HOME>/wso2/components/default/configuration/org.eclipse.equinox.simpleconfigurator/`bundles.info`` file and verify if the following properties have changed from `false` to **`true`**.

    
    org.wso2.carbon.device.mgt.ios.api.utils,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.api.utils_3.0.5.SNAPSHOT.jar,4,true

    org.wso2.carbon.device.mgt.ios.apns,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.apns_3.0.5.SNAPSHOT.jar,4,true

    org.wso2.carbon.device.mgt.ios.core,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.core_3.0.5.SNAPSHOT.jar,4,true

    org.wso2.carbon.device.mgt.ios.payload,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.payload_3.0.5.SNAPSHOT.jar,4,true

    org.wso2.carbon.device.mgt.ios.plugin,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.plugin_3.0.5.SNAPSHOT.jar,4,true
    

    Once the `ios-feature-deployer` script has finished running, you can see the `<IOTS_HOME>/ios-configurator` directory that was created.

4.  Navigate inside the `<IOTS_HOME>/ios-configurator` directory and run the `ios.sh` script. 


    cd ios-configurator
    sh ios.sh

    5.  Enter the requested information when prompted. Be sure to provide the same information when configuring the [iOS platform configurations]({{< param generaldoclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices).

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Field</th>
          <th>Usage/Purpose</th>
        </tr>
        <tr>
          <td>Country</td>
          <td>The name of your country. Enter the two digit code for your country.</td>
        </tr>
        <tr>
          <td>State</td>
          <td>The state your organization is at.</td>
        </tr>
        <tr>
          <td>Location</td>
          <td>The city your organization is located at.</td>
        </tr>
        <tr>
          <td>
            <p>Organization</p>
          </td>
          <td>
            <p>The name of your organization. For this scenario, we entered MobX.</p>
          </td>
        </tr>
        <tr>
          <td>Organization Unit</td>
          <td>Defined the Team ID as the organization unit.</td>
        </tr>
        <tr>
          <td>
            <p>Email</p>
          </td>
          <td>
            <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a class="external-link" rel="nofollow" href="mailto:chris@mobx.com">chris@mobx.com</a>&nbsp;as the email.</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Commonname</p>
          </td>
          <td>
            <p>Fully qualified domain name of your server.</p>
          </td>
        </tr>
      </tbody>
    </table>

    <br/>
    <img src = "352815627.png">
    <br/>

Make sure the `<EnabledUpdateApi>` parameter in the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file is set to `false` if you need to restart the server many times after configuring the IP.  This configuration is enabled when you run the `ios.sh` script.

By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.

### Generating an MDM APNS certificate

Except for a few operations (ring, message and get location) that are performed by the Entgra IoT Server agent, all the other device operations, and policies are applied on an iOS device via it's operating system. Therefore, for the Entgra IoT Server to communicate with the operating system of the device, you need to generate the MDM APNS certificate.

The MDM APNS certificate will be referred to as the MDM certificate in the IoT Server Console.

1.  Go to ** [https://appleid.apple.com/account#!&page=create](https://appleid.apple.com/account#!&page=create) **and get an Apple ID, if you do not have one already. 
2.  Go to the Apple Push Certificate Portal at** [https://identity.apple.com/pushcert/](https://identity.apple.com/pushcert/) **and log in with your customer account details. You do not need an enterprise account for this. Your Apple ID is sufficient.
3.  Click **Create Certificate** and agree to the terms and conditions.
4.  Upload the encoded `.plist` file you got after signing your certificate.

Didn't get your certificate signed by Apple? Register your organization with the [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/) and follow the steps mentioned in [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html). 

5.  Download the generated MDM signing certificate, which is a certificate for third-party servers provided by Apple, and rename it to `MDM_Certificate`.

6.  Get the `USERID` (TOPIC ID) from the MDM signing certificate (`MDM_Certificate.pem`), as it will be used later in the configuration.   
    You can decode the MDM signing certificate to obtain the `USERID` by executing the following command:

    `openssl x509 -in MDM_Certificate.pem -text -noout`

    The TOPIC ID is assigned to the UID property.  
    Example: `UID=com.apple.mgmt.External.0dbe5637-8f16-4d9f-bac4-e18cac2f7d4e`

7.  Remove the password/pass phrase from your private key file (e.g.,`customerPrivateKey.pem`).

`openssl rsa -in customerPrivateKey.pem -out customerKey.pem `

8.  Merge the customer key file that was derived in the latter step with the MDM signing certificate to generate the MDM Apple Push Notification Service (APNS) Certificate. In this example, Chris merges the `customerKey.pem` file with the `MDM_Certificate.pem` file to generate the `MDM_APNSCert.pem` file.


    **Tip**: Before you merge the `customerKey.pem` file and the `MDM_Certificate.pem` file, make sure both files are in the same directory.

    `cat MDM_Certificate.pem customerKey.pem > MDM_APNSCert.pem`

9.  Open the APNS Certificate (`MDM_APNSCert.pem`) and add a line break between the content of the two files. For example, if your content looks as "`-----END CERTIFICATE----------BEGIN RSA PRIVATE KEY-----`", add a line break after 5 dashes so that the content looks as follows:


-----END CERTIFICATE-----
-----BEGIN RSA PRIVATE KEY-----


10.  Convert the `MDM_APNSCert.pem` file to `MDM_APNSCert.pfx` file. You need a password for this, which you will need again when configuring the iOS platform configurations.

    `openssl pkcs12 -export -out MDM_APNSCert.pfx -inkey customerPrivateKey.pem -in MDM_APNSCert.pem`

### Configuring the iOS platform

Multiple tenants can use Entgra IoT Server while maintaining tenant-based isolation. The iOS platform configurations page enables tenants to customize the iOS settings based on their own requirements.

Chris proceeds to configure the iOS platform configurations by adding the generated MDM APNS certificate.

1.  Restart Entgra IoT Server's core profile. You can stop the profiles using `CTRL+C`respectively. 

    
    cd <IOTS_HOME>/bin
    sh iot-server.sh
    

2.  Log in to the device management console at** `https://<IoT_HOST>:9443/devicemgt` **using **admin** as the username and password if you haven't previously logged in.

3.  Click **Menu > CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS > iOS Configurations** and fill in the form.

    Use the generated MDM-APNS certificate for both the MDM certificate and the APNS certificate.

In this tutorial, you are not installing the Entgra IoT Server's iOS agent on the device. Therefore, you don't need to generate the APNS certificate. The APNS certificate is used to trigger operations, such as ring, notification, and iOS. If you want to trigger these operations, you need to [generate the APNS certificate]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-configurations/#generating-an-apns-certificate) and upload it.


*   `MDM Certificate Password`: Give the same password you gave when converting the `MDM_APNS` certificate from the `pem` to the `pfx` format. 
    *   `MDM Certificate Topic ID`: Give the topic ID of the certificate. If you are unsure of the topic ID, refer [step 6 under generating the MDM APNS certificate]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-configurations/#generating-an-apns-certificate).

 <br/>
    <img src = "352815798.png">
<br/>

**Tip**: To learn more about each platform setting, hover your mouse pointer over the help tip.

## Creating users and a sample policy

Follow the steps given below to create two users, Alex and Chris, and a configured passcode policy. This will make it easy for you to try out the IoT Server quick start guide.

Tip by Chris

Have you run the sample script previously when trying out the [Android quick start guide]({{< param doclink >}}quick-start-guide/android-device/)? If yes, delete the two users alex and chris, the role iotMobileUser and the sample policies that were created for Android, iOS, and Windows. 

1.  Navigate to the `mobile-qsg` directory located in the `<IOTS_HOME>/samples` directory on the terminal window. 
2.  Run the `mobile-qsg.sh` script to configure the iOS server settings in Entgra IoT Server. 

    
    cd <IoT_PRODUCT_HOME>/samples/mobile-qsg
    sh mobile-qsg.sh
    

When you add a new user to Entgra IoT Server, the user receives an email with the username and password that needs to be used to log in to Entgra IoT Server. In this quick start guide, we have not configured Entgra IoT Server to send emails. Therefore you will get an error in the terminal where the core profile is running. If you don't want to see this error, you need to configure the email settings. For more information, see [Configuring the Email Settings]({{< param doclink >}}configuring-email-settings).



[2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:chris@mobx.com URL.
[2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:alex@example.com URL.
[2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'chris@mobx.com'
org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:chris@mobx.com URL.
    at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    at java.lang.Thread.run(Thread.java:748)
[2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'alex@example.com'
org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:alex@example.com URL.
    at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    at java.lang.Thread.run(Thread.java:748)


Check out the Entgra IoT Server dashboard by signing in to the Entgra IoT Server console using **chris** as the username and **chrisadmin** as the password: **`https://<IoT_HOST>:9443/devicemgt`**  
You will then see the new **iotMobileUser** role, three new policies, and the two new users that were added using this script

## Updating the passcode policy

As a security measure, the MobX management has asked Chris to update the passcode policy for all iOS devices so that a device user can only enter the wrong password five times. If a user fails to enter the correct password in the fifth attempt, the device will not be accessible for 15 minutes. This can be regulated in Entgra IoT Server by updating the passcode policy that was already in place. Chris follows the steps given below to update the policy:

1.  Sign out of the IoT Server console and log in using the following credentials:  
    Enter** chris **as the username and **chrisadmin** as the password.

2.  Click **LOG IN**.
3.  In the IoT Server console click **View** under POLICIES.  
    <br/>
    <img src = "352815460.png">
    <br/>
4.  Click the edit logo that is on the passcode policy for iOS devices**.**

5.  Update the passcode profile that is already in place by defining the maximum fail attempts as 5, and click **CONTINUE**.

A profile in the context of IoT Server refers to a collection of policies.
If you want to know more about each policy setting, hover your mouse pointer over the help tip.

<br/>
    <img src = "352815840.png">
<br/>

6.  The passcode policy that was created for this scenario has the following groups assigned. You can optionally update the groups.   

    1.  Chris selects the **set user role/s** option and then selects the **iotMobileuser** role from the item list.

    2.  Click** CONTINUE**.  
        Optionally, you can update the name and the description of the policy.

7.  It is mandatory that the policy is assigned to Alex's mobile device, so Chris clicks **SAVE & PUBLISH **to make the policy active immediately.

If you **SAVE** the configured profile, it will be in the inactive state and will not be applied to any devices.

If you **SAVE & PUBLISH** the configured profile of policies, it will be in the active state. The active policies will be enforced on new devices that enroll with IoT Server based on the policy enforcement criteria. If you want to push this policy to the existing devices and want this policy to be applied to the devices, click **APPLY CHANGES TO DEVICES**.

Tip by Chris

You can ensure that the policy is applied on the device by going to the enrolled device page, clicking on your device and clicking the **Policy Compliance** tab on the Device Management console.

Another way to ensure that the policy is enforced on the device is by checking the settings in the device. For more information, see [Verifying Policies Applied on an iOS Device]({{< param doclink >}}using-entgra-iot-server/working-with-ios/verifying-policies-applied-on-an-ios-device/).


## Publishing applications

Chris needs to publish the WSO2Con application. Let's take a look at how you can publish an application using the Entgra IoT Server application publisher..

1.  Navigate to the App Publisher using the following URL:** `https://<IoT_HOST>:9443/publisher`**
2.  Enter the username as **chris** and password as **chrisadmin**. 

3.  Click **Add ****New Mobile Application** that is under the** Mobile Application** drop down.
4.  Select **iOS** for the **Platform**. 
5.  Select **Enterprise** as the **Store Type**
6.  [Download the IPA file from here and select the IPA file](attachments/352815391/352815981.ipa) as shown below. The IPA file size should not exceed 200MB.  
    <br/>
    <img src = "352815986.png">
    <br/>
7.  Click **Next**,and you get the following screen.
    <br/>
    <img src = "352815975.png">
    <br/>

    Enter the following details of the app.

<table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Attribute Name</th>
          <th>Value</th>
        </tr>
        <tr>
          <th>Name</th>
          <td>iOS-App</td>
        </tr>
        <tr>
          <th><strong>Display Name</strong></th>
          <td><span>iOS-App</span></td>
        </tr>
        <tr>
          <th><strong>Description</strong></th>
          <td>This is an iOS application.</td>
        </tr>
        <tr>
          <th><strong>Recent Changes</strong></th>
          <td>Leave blank.</td>
        </tr>
        <tr>
          <th>Version</th>
          <td>Keep the default value.</td>
        </tr>
        <tr>
          <th>Category</th>
          <td>Keep the default value.</td>
        </tr>
        <tr>
          <th>Restrict Visibility</th>
          <td><span>Leave blank.</span></td>
        </tr>
        <tr>
          <th>Banner</th>
          <td>Upload the <code>&lt;IOTS_HOME&gt;/samples/mobile-qsg/apps/ios/images/banner.jpg</code> file.</td>
        </tr>
        <tr>
          <th>Screenshots</th>
          <td><span>Upload the </span><code>&lt;IOTS_HOME&gt;/samples/mobile-qsg/apps/ios/images/screen1.jpg</code><span> file.</span></td>
        </tr>
        <tr>
          <th>Icon file</th>
          <td><span>Upload the </span><code>&lt;IOTS_HOME&gt;/samples/mobile-qsg/apps/ios/images/icon.jpg</code><span> file.</span></td>
        </tr>
      </tbody>
    </table>

8.  Click **Create**. The created app will appear in the created list of apps.  
    Now toy are directed the page that has all the created applciations.
9.  Click **Submit for Review** for the **iOS-App** application.
10.  Click **Approve > Publish**.  
    The iOS-App application is now available in the app store for device owners like Alex to install on their devices.

To learn more about the mobile application life cycle, see [Mobile Application Lifecycle Management]({{< param doclink >}}using-entgra-iot-server/Managing-Mobile-Applications/#mobile-application-lifecycle-management).


## Registering the iOS device

Entgra IoT Server supports devices on iOS 7, 8, 9, and 10.

Tip by Chris


Before you begin to enroll the device, make sure that the Entgra IoT Server server and the device are on the same network.

Before starting to enroll the device without the iOS device agent, make sure to **disable** the **device location** on your device. Else, you run into errors.

Follow the instructions below to register an iOS device:

1.  Sign in to the Entgra IoT Server device management console using **alex** as the username and **alexuser** as the password.

2.  Click **Enroll New Device**.
3.  Click **iOS **to enroll your device with WSO2 IoTS.
4.  Enroll the device.
    1.  Click **Enroll Device**.
    2.  Scan the QR code and the Safari browser will display the IoT Server iOS enrollment screen.  
        After scanning the QR code you will be directed to a web page. When this page appears, the web browser will typically display an "insecure connection" message, which requires your confirmation before you can continue. 

5.  Tap **Install IoT Server Certificate**. The iOS devices need the root certificate to be downloaded to trust the server certificate.   
    <br/>
    <img src = "352815621.png">
    <br/>
6.  The Install Profile screen appears. Tap **Install**.   
    <br/>
    <img src = "352815750.png">
    <br/>

 Tip by Chris

If you are using iOS 10 with a self-signed certificate, do the following to mark it as a trusted certificate. This is required in iOS 10 because there is a change in how self-signed CA certificates are trusted by iOS devices from iOS 10 onwards.

1.  Open the **Certificate Trust Settings** found in **Settings > General > About**.
2.  Enable full trust for the certificate related to Entgra IoT Server.

7.  Tap the **Skip Agent Installer** link at the bottom of the screen.  
    In this tutorial, you are registering the iOS device with Entgra IoT Server without installing the WSO2 iOS agent. Therefore, you will not be able to ring the device, get the device location or send notifications to the device using Entgra IoT Server. If you want to try out the said operations, click Install IoT Server agent.  
    <br/>
    <img src = "352815897.png">
    <br/>
8.  On the IoT Server Registration screen, enter your details:  

    *   **Domain** - You don't need to enter the domain details for this scenario.
    *   **Username** - Enter **alex** as the username.
    *   **Password** - Enter **alexuser** as the password. 

By default Entgra IoT Server only supports the "bring your own device" (BYOD) registration process for the iOS platform.

9.  Tap****Log In.**** 

    Troubleshooting tip

    If you get an error stating that the server certificate is invalid, it may be because your iOS version is version 10 or a later version. Ensure that you have enabled full trust for the certificate as mentioned on [this tip in step 6](about:blank#iOSDevice-tip). To resolve, do the following:

    1.  Open the **Certificate Trust Settings** found in **Settings>General>About**.
    2.  Enable full trust for the certificate related to Entgra IoT Server.

    3.  Open **Settings>General**, scroll to the bottom and open **Profiles**.  
        If you do not see a **Profiles** section, you do not have a configuration profile installed.
    4.  In the **Profiles** section, select the IoT profile and tap **Delete Profile**.
    5.  Restart the device enrollment process from [step 4](about:blank#iOSDevice-step4).

10.  After reading the End User License Agreement (EULA), tap **I accept the terms**. 
    <br/>
    <img src = "352815777.png">
    <br/>

11.  Tap **Install** when prompted to install the carbon.super Profile Service.   
    <br/>
    <img src = "352815866.png">
    <br/>

    Troubleshooting tip

    If you get an error stating that the server certificate is invalid, it may be because your iOS version is version 10 or a later version. Ensure that you have enabled full trust for the certificate as mentioned on [this tip in step 6](about:blank#iOSDevice-tip). To resolve, do the following:

    1.  Open the **Certificate Trust Settings** found in **Settings>General>About**.
    2.  Enable full trust for the certificate related to Entgra IoT Server.

    3.  Open **Settings>General**, scroll to the bottom and open **Profiles**.  
        If you do not see a **Profiles** section, you do not have a configuration profile installed.
    4.  In the **Profiles** section, select the IoT profile and tap **Delete Profile**.
    5.  Restart the device enrollment process from [step 4](about:blank#iOSDevice-step4).

12.  A warning message appears to indicate that by installing the profile the IoT Server will remotely manage the iOS device. Tap **Install**.
    <br/>
    <img src = "352815866.png">
    <br/>

13.  Tap **Trust** to confirm that you are aware of the device being remotely managed by installing the profile.
    <br/>
    <img src = "352815658.png">
    <br/>

14.  After the profile is installed, click **Done**.
    <br/>
    <img src = "352815664.png">
    <br/>

15.  Upon the successful registration, the **DEVICE ADDED** confirmation appears.

Since you are not installing the Entgra IoT Server iOS agent in this guide, you will get a Cannot Open Page warning message after the enrollment is complete.

### Trying out iOS device operations

Alex can now navigate to the Device management page, view information specific to her device, and carry out operations on the device. She follows the steps given below:

1.  Access the IoT Server Management Console using **alex** as username and **alexuser** as the password.
2.  Click the <img src = "352815819.png"> icon.
3.  Click **DEVICE MANAGEMENT**.
4.  Click on the registered device.  
    <br/>
    <img src = "352815855.png">
    <br/>
5.  Alex tries out the various operations on her device via the Entgra IoT Server Management Console.

 **NOTE:**

Because we didn't install the Entgra IoT Server iOS agent while registering the device, the ring device, send notifications, and get device location operations do not function. For more information on installing the agent in the registration process, you can follow the steps in the [Enrolling With the iOS Agent]({{< param doclink >}}tutorials/ios/#enrolling-with-ios-agent) tutorial.

In this tutorial, you are not installing the agent. Therefore, the device location operation fails as it needs the agent to push the location details to Entgra IoT Server. To avoid error logs while testing this tutorial, comment out the following configuration that is in the `<IOTS_HOME>/Repository/deployment/server/devicetypes/ios.xml` file.


<!--Operation>
    <Name>DEVICE_LOCATION</Name>
    <RecurrentTimes>1</RecurrentTimes>
</Operation-->


<br/>
    <img src = "352815642.png">
<br/>

The details of each registered devices will be shown on separate pages within the **Device** page.

<table>
    <colgroup>
    <col>
    <col>
    </colgroup>
    <tbody>
    <tr>
        <th><br></th>
        <th>Description</th>
    </tr>
    <tr>
        <td>Device Details</td>
        <td>
        <p>The following device information will be retrieved automatically when you register with WSO2 IoTS.</p>
        <div class="table-wrap">
            <table>
            <tbody>
                <tr>
                <td>Device</td>
                <td>This shows the name the user has given his/her device (e.g., Kim's iPhone).</td>
                </tr>
                <tr>
                <td>Model</td>
                <td>The type of device will be stated (e.g., iPhone, iPad)</td>
                </tr>
                <tr>
                <td>IMEI</td>
                <td>The International Mobile Station Equipment Identity (IMEI) number of all GSM devices. This is not applicable to devices suchasiPadswhichwork without a SIM.</td>
                </tr>
                <tr>
                <td>Status</td>
                <td>Indicates if the device is active, inactive or removed from WSO2 IoTS.</td>
                </tr>
                <tr>
                <td>Ownership</td>
                <td>Indicates if the device is your own device/Bring Your Own Device (BYOD) or if it's a Corporate-Owned, Personally Enabled (COPE) device.<br>For the device to be a COPE device,&nbsp;<a rel="nofollow" href="{{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application" class="external-link">Entgra IoT Server must be configured to use the system server application</a>.</td>
                </tr>
            </tbody>
            </table>
        </div>
        </td>
    </tr>
    <tr>
        <td>Operation Log</td>
        <td>A list of all the operations that have been carried out by you and its current status.</td>
    </tr>
    <tr>
        <td>Location</td>
        <td>Provides the location of your device.</td>
    </tr>
    <tr>
        <td>Applications</td>
        <td>A list of all the applications that have been installed on your device and the memory consumption by each application will be listed under this section.</td>
    </tr>
    <tr>
        <td>Policy Compliance</td>
        <td>If your device does not comply with certain criteria in the enforced policy, the aspects in which your device is none compliant will be highlighted under this section.</td>
    </tr>
    </tbody>
</table>

Click on the operation you wish to carry out and provide the required details (if requested) to apply the selected operation on your device.

<table>
    <colgroup>
    <col>
    <col>
    </colgroup>
    <tbody>
    <tr>
        <th>Operation</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-DeviceLock">Device Lock</h5>
        </td>
        <td>Ability to lock a device via Entgra IoT Server.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-Location">Location</h5>
        </td>
        <td>Ability to receive the location of the device.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-Ring">Ring</h5>
        </td>
        <td>Ability to ring the device via Entgra IoT Server.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-ClearPassword">Clear Password</h5>
        </td>
        <td>Ability to remove a device lock via Entgra IoT Server.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-EnterpriseWipe">Enterprise Wipe</h5>
        </td>
        <td>Ability to unregister a device from Entgra IoT Server.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-SendNotification">Send Notification</h5>
        </td>
        <td>Ability to send a notification to the device via Entgra IoT Server. The Entgra IoT Server admin can use<br>this device operation to send notifications to the Entgra IoT Server users.</td>
    </tr>
    <tr>
        <td>
        <h5 id="iOSDevice-WipeData">Wipe Data</h5>
        </td>
        <td>Ability to carry out a factory reset operation on your own device via the IoT Server. The user will have&nbsp;<br>to provide the PIN, which he/she entered when registering to IoT Server, to be able to wipe his/her device.</td>
    </tr>
    </tbody>
</table>

Tip by Chris

To know if a policy is applied on your device, see [Verifying Policies Applied on an iOS Device]({{< param doclink >}}using-entgra-iot-server/working-with-ios/verifying-policies-applied-on-an-ios-device/).


## Installing an application on the iOS device

MobX wants Chris to ensure that the employees can only download mobile applications made available via the MobX app store. For this, Chris creates and publishes the WSO2Con application to the MobX app store.

Let's take a look at how Alex installs this application on the device.

1.  Navigate to the App Store using the following URL:  `**https://<IoT_HOST>:9443/store**`
2.  Sign in using **alex** and **alexuser** as the username and password.
3.  Click the iOS-App mobile application, and click **Install**.   
    <br/>
    <img src = "352815953.png">
    <br/>
4.  Leave the text box empty and click **Yes**. This will install the application instantly on the device. 
5.  A notification pops up on the device asking permission to install the application. Click **Allow**.
6.  A success message will be shown when you have successfully subscribed to the application.
7.  Tap install on your device to finish installing the application.

Note from Chris

Remember to change the `AppDownloadURLHost` property value that is in the `<IOTS_HOME>/conf/app-manager.xml` file back to HTTP if you are trying out the quick start guide for Android or testing Entgra IoT Server with Android devices.   
If you wish to continue using HTTPS to install applications on Android devices, [generate a BKS file for Android]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#generating-a-bks-file-for-android).

## Unregistering and uninstalling the iOS agent

Want to unregister your device from Entgra IoT Server and uninstall the iOS agent that's on your device? Follow the steps given below:

1.  Navigate to **Settings** > **General** > **Profiles **or** Profiles & Device Management.**
2.  Tap the WSO2 Inc profiles, such as  **wso2 Mobile Device Management**.
3.  Tap  Delete App.

Now you have successfully unregistered your device from Entgra IoT Server, removed the profiles that were added when the iOS agent was installed, and uninstalled the WSO2 Device management application.
