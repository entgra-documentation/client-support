---
bookCollapseSection: true
weight: 1
---
# Android Device

---

Let's take a look at the tasks that Chris (the IoT team administrator) and Alex (the device owner) have to do, from [downloading Entgra IoT Server](https://entgra.io/) to registering the Android device. **Entgra IoT Server is bundled with the WSO2 Enterprise Mobility Management (WSO2 EMM) capabilities**.


Before you begin


1.  Entgra IoT Server supports devices on Android version 4.2.x to 7.0 (Android Jelly Beans to Nougat).
2.  Install [Oracle Java SE Development Kit (JDK)](http://java.sun.com/javase/downloads/index.jsp) version 1.8.* and set the `JAVA_HOME` environment variable. For more information on setting up `JAVA_HOME` on your OS, see [Installing the Product]({{< param doclink >}}using-entgra-iot-server/installation-guide/installing-the-product/#installing-the-product).
3.  [Download Entgra IoT Server](https://entgra.io/) and unzip the file.

4.  Start Entgra IoT Server's core profile, which corresponds to device management, device plugins, transports, APIs, and authentication and authorization components of Entgra IoT Server.

    
    cd <IOTS_HOME>/bin
    ./iot-server.sh --> for Linux/MAC/Solaris
    iot-server.bat --> for Windows
    

5.  Access the WSO2 device management console by navigating to **`https://<IoT_HOST>:9443/devicemgt`** from a web browser. Use **admin** for both username and the password. You will see the Admin role and the Admin user.

Have you run the sample script previously when trying out the [iOS quick start guide]({{< param doclink >}}quick-start-guide/ios-device/)? If so, delete the two users Alex and Chris, the role iotMobileUser, and the sample policies that were created for Android, iOS, and Windows.

**Let's get started!**

## Creating users and a sample policy

Follow the steps given below to create the two users Alex and Chris, and a configured passcode policy. This is done so that it will be easy for you to try out the quick start guide.

1.  Navigate to the `<IOTS_HOME>/samples/mobile-qsg` directory and start the `mobile-qsg` script.

    
    cd <IOTS_HOME>/samples/mobile-qsg
    ./mobile-qsg.sh --> for Linux/MAC/Solaris 
    java -jar mobile-qsg.jar --> for Windows
    

When you add a new user to Entgra IoT Server, the user receives an email with the username and password that needs to be used to log in to Entgra IoT Server. In this quick start guide, we have not configured Entgra IoT Server to send emails. Therefore you will get an error in the terminal where the core profile is running. If you don't want to see this error, you need to configure the email settings. For more information, see [Configuring the Email Settings]({{< param doclink >}}configuring-email-settings).



 
    [2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:chris@mobx.com URL.
    [2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:alex@example.com URL.
    [2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'chris@mobx.com'
    org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:chris@mobx.com URL.
    	at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    	at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    	at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    	at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    	at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    	at java.lang.Thread.run(Thread.java:748)
    [2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'alex@example.com'
    org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:alex@example.com URL.
    	at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    	at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    	at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    	at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    	at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    	at java.lang.Thread.run(Thread.java:748)


Check out the Entgra IoT Server dashboard by signing in to the Entgra IoT Server console using **chris** as the username and **chrisadmin** as the password: **`https://<IoT_HOST>:9443/devicemgt`**.    
You will then see the new iotMobileUser role, 3 new policies (only 2 policies will be created if you have not configured Entgra IoT Server for iOS), and 2 new users that were added using these scripts.

## Registering the Android device

Entgra IoT Server supports devices on Android version 4.2.x to 7.0 (Android Jelly Beans to Nougat).

Android restricts third-party apps and less secure apps from being installed on the device. Therefore, you need to configure your device to disable this restriction as the Entgra IoT Server device management agent application acts as a third-party application.

Follow the steps given below:

1.  Navigate to **Setting > Security**.
2.  Enable the **Unknown sources** option.


The role Chris assigned to Alex, has permission to enroll a device. Therefore, Alex follows the steps given below to enroll the Android device:

1.  Sign in to the Entgra IoT Server device management console using **alex** as the username and **alexuser** as the password.

2.  Click **Enroll New Device**.
3.  Click **Android **to enroll your device with Entgra IoT Server.
4.  Scan the QR code to download the Android agent onto your Android device.


Make sure that your Android device and the IoT Server are on the same network, else you will not be able to download the Android agent.


After scanning the QR code you will be directed to a web page. When this page appears, the web browser will typically display an "insecure connection" message, which requires your confirmation before you can continue.


The WSO2 IoTS consoles are based on the HTTPS protocol, which is a combination of HTTP and SSL protocols. This protocol is generally used to encrypt the traffic from the client to server for security reasons. The certificate it works with is used for encryption only, and does not prove the server identity, so when you try to access these consoles, a warning of untrusted connection is usually displayed. To continue working with this certificate, some steps should be taken to "accept" the certificate before access to the site is permitted. If you are using the Mozilla Firefox browser, this usually occurs only on the first access to the server, after which the certificate is stored in the browser database and marked as trusted. However, with other browsers, the insecure connection warning might be displayed every time you access the server.

This scenario is suitable for testing purposes, or for running the program on the company's internal networks. If you want to make these consoles available to external users, your organization should obtain a certificate signed by a well-known certificate authority, which verifies that the server actually has the name it is accessed by and that this server belongs to the given organization.

5.  Alex taps **Download IoT Server Agent** on this screen.  

    Open the downloaded file.

6.  Tap **INSTALL** to start installing the Android agent.
    <br/>
    <img src = "352815173.png">
    <br/>
    
7.  Tap **OPEN**, once the WSO2 Agent is successfully installed.
8.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

<br/>
<img src = "352814584.png">
<br/>

9.  Tap **SKIP AND GO TO ENROLLMENT**, which will direct you to install the device with Entgra IoT Server in the default manner.

    Tip by Chris


    In Entgra IoT Server, data containerization is implemented using the Managed Profile feature. For more information on how to **set up the Work-Profile**, see [Setting Up the Work Profile]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#setting-up-the-work-profile). You need to tap on the given options only if your device is on the Lollipop OS version or above. Else, move to the next step.


10.  Enter the server IP and the port as your server address in the text box provided and tap **START REGISTRATION**.

    **Example**: Register the device via HTTP:** 10.100.7.35:8280**

<br/>
<img src = "352815132.png">
<br/>

11.  Enter your details and tap **SIGN IN**. A confirmation message will appear..  

    *   **Organization** - In this example, Alex leaves the **Organization** empty, as it is only required if the server is hosted with multi-tenant support. 
    *   **Username** - Enter **alex** as the username.
    *   **Password** - Enter **alexuser** as the password.

<br/>
<img src = "352815155.png">
<br/>

The **Sign In** credentials can be the credentials of the admin user or if not, a user needs to be created under Entgra IoT Server **User Management** first, prior to entering user details in the device, when registering the device with the WSO2 IoT Server. 

1.  Read the policy agreement, and tap **Agree** to accept the agreement. 
2.  Tap **ACTIVATE** to enable the Entgra IoT Server device administrator on your device. A confirmation message appears after enabling the device admin.  
<br/>
<img src = "352814577.png">
<br/>

3.  Tap **ALLOW** to allow the Entgra Android agent to make and manage phone calls, and to access photos, media, files, and the device location.

4.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the Do Not Disturb setting enabled will affect the ring, and mute operations. This setting is only shown for Android Nougat and above.

    1.  Tap **OK.**
<br/>
<img src = "352815062.png">
<br/>  
        
    2.  Enable WSO2 Device Management to access the Do Not Disturb setting.
<br/>
<img src = "352815068.png">
<br/>    
    3.  Click **ALLOW**.
<br/>
<img src = "352815086.png">
<br/>        
5.  Alex sets a PIN code with a minimum of four digits and taps **SET PIN CODE**. The PIN code is used to secure your personal data. Therefore, Entgra IoT Server will not be able to carry out critical operations on your personal data without using this PIN.   
    Example: If Chris needs to enterprise-wipe Alex's device or remove data from the device, Chris cannot directly wipe it without the PIN code. 


    You will be prompted to provide a PIN code only if your device is a BYOD device.
<br/>
<img src = "352815149.png">
<br/>

6.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server. 
<br/>
<img src = "352815167.png">
<br/> 

## Updating the passcode policy

As a security measure, the MobX management has requested Chris to update the passcode policy for all Android devices so that a device user can only enter the wrong password four times. If a user fails to enter the correct password in the fourth attempt, the device will not be accessible for 15 minutes. This can be regulated in Entgra IoT Server by updating the passcode policy that was already in place. Chris follows the steps given below to update the policy:

1.  Access the Entgra IoT Server console by navigating to** `https://<IoT_HOST>:9443/devicemgt`**.

    Tip by Chris

    

    1.  By default, `<IOTS_PORT>` has been set to 9443 for HTTPS and 9763 for HTTP. Since we access the IoT console over HTTPS, use 9443 in the URL.
    2.  When the pages appear, the web browser will typically display an "insecure connection" message, which requires your confirmation before you can continue.


        The Entgra IoT Server consoles are based on the HTTPS protocol, which is a combination of HTTP and SSL protocols. This protocol is generally used to encrypt the traffic from the client to server for security reasons. The certificate it works with is used for encryption only and does not prove the server identity, so when you try to access these consoles, a warning of untrusted connection is usually displayed. To continue working with this certificate, you must "accept" the certificate before access to the site is permitted. If you are using the Mozilla Firefox browser, this usually occurs only on the first access to the server, after which the certificate is stored in the browser database and marked as trusted. However, with other browsers, the insecure connection warning might be displayed every time you access the server.

        This scenario is suitable for testing purposes, or for running the program on the company's internal networks. If you want to make the consoles available to external users, your organization should obtain a certificate signed by a well-known certificate authority, which verifies that the server actually has the name it is accessed by and that this server belongs to the given organization.

1.  Sign out from Alex. Enter **chris** as the username and **chrisadmin **as the password.

2.  Click **LOG IN**. 

    The device management dashboard appears, giving you easy access to the devices, users, and policies in your organization. 

2.  Click **View** under POLICIES.
3.  Click the <img src = "352815051.png">

icon that is on the passcode policy for Android devices**.**
4.  Update the passcode profile that is already in place by defining the maximum fail attempts as 4, and click **CONTINUE**.

5.  Update the user group details as below.
    1.  Chris selects the **set user role/s** option and then selects the **iotMobileUser** role from the item list.

    2.  Click** CONTINUE**.
6.  Optionally, you can update the name and the description of the policy.

7.  Click **SAVE & PUBLISH **to make the policy active immediately when the device enrolls with Entgra IoT Server.

8.  Click **SAVE** to save the configured profile or click **SAVE & PUBLISH **to save and publish the configured profile as an active policy to the database. It is mandatory that the policy is assigned to Alex's mobile device, so Chris clicks **SAVE & PUBLISH **to make the policy active immediately when the device enrolls with Entgra IoT Server.

 *   If you **SAVE** the configured profile, it will be in the inactive state and will not be applied to any devices.
    *   If you **SAVE & PUBLISH** the configured profile of policies, it will be in the active state. The active policies will be enforced on new devices that enroll with Entgra IoT Server based on the policy enforcement criteria.
    *   If you want to push this policy to the existing devices and want this policy to be applied to the devices, click **APPLY CHANGES TO DEVICES**.


Once the policy is pushed to the device, Alex gets a notification to update the device's passcode/password so that it meets the requirements configured in the policy. 

## Publishing Applications

Chris needs to publish the WSO2Con-Android application that was created when running the `mobile-qsg` script.

1.  Navigate to the App Publisher using the following URL:https://<IoT_HOST>:9443/publisher. Use **chris** as the username and **chrisadmin **as the password.

2.  Click **Submit for Review** for the WSO2Con-Android application.  
<br/>
<img src = "352815046.png">
<br/> 

3.  Click **Approve** and then click** Publish**. The App Catalog application is now published.
<br/>
<img src = "352815041.png">
<br/>

The WSO2Con-Android application is now available in the app store for device owners like Alex to install on their device.

To know more about the mobile application life cycle, see [Mobile Application Lifecycle Management]({{< param doclink >}}using-entgra-iot-server/Managing-Mobile-Applications/#mobile-application-lifecycle-management).

## Trying out Android device operations

Alex can now navigate to the Device Management page, view information specific to the device, and carry out operations on the device as explained below:

1.  Access the Entgra IoT Server's device management console using **alex** as the username and **alexuser** as the password that was sent via email.
2.  Click **View** under **DEVICES**.  
3.  Click on the registered device. 
    Alex tries out the various device operations via the Entgra IoT Server console.
<br/>
<img src = "352815161.png">
<br/>

    1.  Alex misplaces the mobile device, so Alex clicks **Ring** to ring the device and find out if the device is lying around the workspace.

    2.  Alex then remembers that the phone was unlocked, so Alex uses the **Device Lock** operation to lock the device so that the content in the device will be safe.

    3.  Using the **Location** operations Alex finds out that device is in Chris's office. Alex calls Chris and asks Chris to call back from the device.

    4.  To make sure Chris can call using the device, Alex removes the device lock (password) using the** Clear Password**operation.

    For more information on the available features, [try out the Android operations]({{< param generaldoclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-operations).

## Installing an application on the Android device

MobX wants Chris to ensure that the employees can only download mobile applications made available via the MobX app store. For this, Chris creates and publishes the WSO2Con-Android to the MobX app store.

Before installing the application on the device, make sure that the `AppDownloadURLHost` property in the`<IOTS_HOME>/``conf/app-manager.xml` file is configured to `http`. Else, an error occurs when installing the application in this tutorial. 

`<Config name="AppDownloadURLHost">%http%</Config>`

If you want to install applications using HTTPS, you need to [Generating a BKS File for Android]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices//#generating-a-bks-file-for-android).

Let's take a look at how Alex installs this application on the device.

1.  Navigate to the App Store using the following URL: `**https://<IOTS_HOST>:9443/store**` 

2.  Sign in using **alex** and **alexuser** as the username and password.

3.  Click the WSO2Con-Android mobile application, and click **Install**. 
<br/>
<img src = "352815074.png">
<br/>

    You can install the application on many devices in one go too, which is know and enterprise install. By default, only the administrator can enterprise install applications. For more information on enabling the enterprise subscription for other user roles, see [Enabling Enterprise Subscriptions for Mobile Apps]({{< param doclink >}}using-entgra-iot-server/Managing-Mobile-Applications/#enabling-enterprise-subscriptions-for-mobile-apps).


4.  Select **Instant install**, and click **Yes**.  
5.  Click on your device in the pop-up menu to install and subscribe to the application.  
    A success message will be shown when you have successfully subscribed to the application.
6.  Tap **Install** on your device to finish installing the application.  
    Now you can use start using the application.



