---
bookCollapseSection: true
weight: 4
---

# Configuring email settings

Configure the email settings to send out registration confirmation emails to new users and invite existing users to register their device with Entgra IoT Server.





In Entgra IoT Server, user registration confirmation emails are disabled by default, and the admin needs to provide the required configuration details to enable it.





1.  Create an email account to send out emails to users that register with Entgra IoT Server (e.g., [no-reply@foo.com](mailto:no-reply@foo.com)).

2.  Open the `<IoTS_HOME>/conf/axis2/axis2.xml` file, uncomment the `mailto` `transportSender` section, and configure the Entgra IoT Server email account.

    
    <transportSender name="mailto" class="org.apache.axis2.transport.mail.MailTransportSender">
       <parameter name="mail.smtp.host">smtp.gmail.com</parameter>
       <parameter name="mail.smtp.port">587</parameter>
       <parameter name="mail.smtp.starttls.enable">true</parameter>
       <parameter name="mail.smtp.auth">true</parameter>
       <parameter name="mail.smtp.user">synapse.demo.0</parameter>
       <parameter name="mail.smtp.password">mailpassword</parameter>
       <parameter name="mail.smtp.from">synapse.demo.0@gmail.com</parameter>
    </transportSender>
    

    

    

    For `mail.smtp.user`, `mail.smtp.password`, and `mail.smtp.from,` use the username, password, and email address (respectively) from the email account you set up.

    

    

    Example:

    
    <transportSender name="mailto" class="org.apache.axis2.transport.mail.MailTransportSender">
       <parameter name="mail.smtp.host">smtp.gmail.com</parameter>
       <parameter name="mail.smtp.port">587</parameter>
       <parameter name="mail.smtp.starttls.enable">true</parameter>
       <parameter name="mail.smtp.auth">true</parameter>
       <parameter name="mail.smtp.user">no-reply</parameter>
       <parameter name="mail.smtp.password">$foo1234</parameter>
       <parameter name="mail.smtp.from">no-reply@foo.com</parameter>
    </transportSender>


    

    

    After updating the Axis2 configurations, restart the core profile.

    

    

3.  Optionally, configure the email sender thread pool.  
    Navigate to the `email-sender-config.xml` file, which is in the `<IOTS_HOME>/conf/etc` directory, and configure the following fields under `<EmailSenderConfig>`.

    *   `MinThreads`: Defines the minimum number of threads that need to be available in the underlying thread pool when the email sender functionality is initialized.

    *   `MaxThreads `: Defines the maximum number of threads that should serve email sending at any given time.

    *   `KeepAliveDuration `: Defines the duration that a connection should be kept alive. If the thread pool has initialized more connections than what was defined in `MinThreads`, and they have been idle for more than the `KeepAliveDuration`, those idle connections will be terminated

    *   `ThreadQueueCapacity `: Defines the maximum concurrent email sending tasks that can be queued up.

    Example:

    
    <EmailSenderConfig>
       <MinThreads>8</MinThreads>
       <MaxThreads>100</MaxThreads>
       <KeepAliveDuration>20</KeepAliveDuration>
       <ThreadQueueCapacity>1000</ThreadQueueCapacity>
    </EmailSenderConfig>
    

4.  Optionally, customize the email templates that are in the `<IoTS_HOME>/repository/resources/email-templates` directory. 

    

    

    The email templating functionality of Entgra IoT Server is implemented on top of Apache Velocity, which is a free and open-source template engine.

    

    

    1.  Open the email template that you wish to edit based on the requirement, such as the `user-enrollment.vm` or `user-registration.vm` file.
    2.  Edit the `<Subject>` and `<Body>` to suit your requirement.

    

    

    *   If you need to access `HTTP` or `HTTPS` base URLs of the server within your custom template configs, use the `$base-url-http` and `$base-url-https` variables, respectively.
    *   You can update the look and feel of the email template via the Entgra IoT Server management console too. For more information, see [Customizing Email Templates for Tenants]({{< param generaldoclink >}}guide-to-work-with-the-product/manage-tenants/#customizing-email-templates-for-tenants).

    

    
