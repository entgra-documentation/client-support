---
bookCollapseSection: true
weight: 6
---

# Manage Notifications

## Scheduling the Push Notification Task
   
   Push notifications are used to send messages to devices. If you have configured Entgra IoT Server to push notifications to the devices via [Firebase Cloud Messaging (FCM)]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#configuring-android-with-firebase-cloud-messaging) or [Apple Push Notification (APNS)]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-notification-method/), the notification is sent to the devices, the devices request for the pending operations, and the server returns the list of pending operations, which the devices then execute.
   
   
   When many devices communicate with the server at the same time, the server crashes. To avoid this situation Entgra IoT Server implemented a task to manage the number of devices communicating with the server at a given time. The task sends the notifications to the devices in batches. This way at a given time only a defined number of devices will communicate with the server.  
   
   Let's take a look at how you can configure the push notification scheduling task:
   
   1.  Open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `SchedulerTaskEnabled` that is under `PushNotificationConfiguration` is enabled. This configuration is enabled by default.
   
       
   
       
   
       In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. 
   
       
   
       
   
   
       <PushNotificationConfiguration>
          <SchedulerBatchSize>1000</SchedulerBatchSize>
          <SchedulerBatchDelayMills>60000</SchedulerBatchDelayMills>
          <SchedulerTaskInitialDelay>60000</SchedulerTaskInitialDelay>
          <SchedulerTaskEnabled>true</SchedulerTaskEnabled>
          <PushNotificationProviders>
             <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm.FCMBasedPushNotificationProvider</Provider>
             <!--<Provider>org.wso2.carbon.device.mgt.mobile.impl.ios.apns.APNSBasedPushNotificationProvider</Provider>-->
             <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.mqtt.MQTTBasedPushNotificationProvider</Provider>
             <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.xmpp.XMPPBasedPushNotificationProvider</Provider>
          </PushNotificationProviders>
       </PushNotificationConfiguration>
   
       <table>
         <colgroup>
           <col>
           <col>
         </colgroup>
         <tbody>
           <tr>
             <th><code>SchedulerBatchSize</code></th>
             <td><span style="color: rgb(34,34,34);">Define how many devices or the device batch size a notification needs to be sent at a given time.<br>By default, a notification is sent to 200 devices at a given time.&nbsp;If you have less than 200 devices registered with Entgra IoT Server, the notification will be sent to all those devices.</span></td>
           </tr>
           <tr>
             <th><code>SchedulerBatchDelayMills</code></th>
             <td><span style="color: rgb(34,34,34);">Define after how many milliseconds the notifications needs to be sent to the next batch&nbsp;of devices.<br></span>By default, the next batch of devices will get the notification after 60,000 milliseconds, which is in a minute.</td>
           </tr>
           <tr>
             <th><code>SchedulerTaskInitialDelay</code></th>
             <td><span style="color: rgb(34,34,34);">The server will not send notifications to the devices soon as it starts. Therefore, define when the push notification schedule task needs to start. The value needs to be defined in milliseconds.<br></span>By default, the task starts 60,000 milliseconds or one minute after the server starts.</td>
           </tr>
           <tr>
             <th><code>SchedulerTaskEnabled</code></th>
             <td>Enable the push notification scheduler task. If it is is not enabled, Entgra IoT Server will send the push notifications to all the registered devices in one go. This might result in the server crashing as explained above.</td>
           </tr>
           <tr>
             <th><code>PushNotificationProviders</code></th>
             <td><span style="color: rgb(34,34,34);">The default push notifications that are supported and implemented in Entgra IoT Server.</span></td>
           </tr>
         </tbody>
       </table>
   
   2.  Configure the device type to run the push notification scheduler task.  
       Navigate to the `<IOTS_HOME>/repository/deployment/server/devicetype` directory, open the `<DEVICE_TYPE>.xml` file, and configure the field given below:  
       The default configuration in the `android.xml` file is shown below:
   
       `<PushNotificationProvider type="FCM" isScheduled="false">--> <!--</PushNotificationProvider>`
   
       
   
       
   
       The task for the FCM push notification method is disabled for the Android device type by default. Therefore, all the Android devices registered with Entgra IoT Server will receive the push notifications at once and the devices will communicate with the server at the same time. If you want to change this default behavior, enable the scheduler task for the device type.
   
       `<PushNotificationProvider type="FCM" isScheduled="true">--> <!--</PushNotificationProvider>`
   
        **NOTE**: In Entgra IoT Server, the Android devices are configured to send notifications via the [local polling notification method]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices#android-notification-methods) by default.
   
   
       
   
       

## Checking Notifications


There will be situations where you want certain operations to be carried out on a device but that device will not be able to support the operation. The failure to carry out such operations will be notified to the administrator and the device owner. There will also be instances where the user will need to be notified of additional information such as the new lock reset pin.

The example given below will help you understand how the administrator and the device owner also know as privileged users will be notified of an operation failure:

**Example**: You wish to carry out the device lock operation on your Windows device that is registered with Entgra IoTS but the respective device does not have a device lock enabled (it does not have a pin or a security pattern enabled). Therefore when you carry out the device lock operation, the operation will fail.

*   When the operation fails you will be notified via a notification.   
    ![image](352821264.png)
*   To check the notification, click the notification icon.

    

    

    The unread notifications are shown under **Unread** and the notifications that have already been read will be shown under **All Notifications**.

    

    

    ![image](352821269.png)

*   When the device lock operation fails the admin or device owner is advised to reset the lock on the device. To carry out the lock-rest operation click the view icon of the respective notification. 

    

    

    If you have more than one device enrolled in Entgra IoTS you can navigate to the respective device on, which the operation failed, by clicking the view icon.

    

    

    ![image](352821284.png)

*   Once navigated to the respective device page, as mentioned in the notification carry out the lock-reset operation.
*   EMM will notify the administrator and the device owner of the new device pin. Use the given pin to unlock the device.

    

    

    Click the view icon, if you wish to know on, which device the lock-reset operation was carried out.

    

    

    ![image](352821274.png)
    
