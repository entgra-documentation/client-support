---
bookCollapseSection: true
weight: 8
---

# Customizing Entgra IoT Server



## Customizing Android APK File 


You will want to make changes to the Android Application Package (APK) that is in Entgra IoT Server to suit your business or Organization requirements. For example, you will want to white label the Entgra Android agent. In such situations, you need to do the required changes and create a new APK file. 

### Prerequisites

1.  Download and install Android Studio.

    For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).

2.  Replace the `android.jar` file that is in the `<ANDROID_STUDIO>/<SDK location>/platforms/android-<CURRENT_API_LEVEL>`  
    directory with the `android.jar` file. You can get the new `jar` file using **one of the following** options:

    *   Use a pre-built jar file from a third party developer. You can find it [here](https://github.com/anggrayudi/android-hidden-api).

        Make sure to use the jar file that matches the `compileSdkVersion` of the Entgra Android agent. Currently, the `compileSdkVersion` is 25.

    *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

    Why is this needed?

    The Entgra Android agent requires access to hidden APIs (APIs that are available at runtime). Therefore, you need to replace the `anrdoid.jar` file as mentioned in this step.

    After the above steps, make sure to close Android studio fully and restart adb with following 2 commands

    1.  adb kill-server
    2.  adb start-server
    
3.  Entgra IoT Server supports API levels 17 to 25\. Therefore, install the Android API levels 17 to 25 on Android Studio.
    1.  Open Android Studio.
    2.  Click **SDK Manager**. **![image](2.png)**
    3.  Click the **SDK Platform **tab and select the 17 and 25 API levels.  
        Example:  
        ![image](352821327.png)
        
4.  Click **Apply**.

### Creating a new APK

1.  Clone the cdmf-agent-android GIT repository. This will be referred to as `<ANDROID_AGENT_SOURCE_CODE>`.

    `https://gitlab.com/entgra/android-agent.git -b <ENTER_THE_VERSION>`

    Check the [Entgra IoT Server and Agent Compatibility]({{< param doclink >}}temp/entgra-iot-server-agent-compatibility/) and find out what branch of this repo you need to clone and add the value for `<ENTER_THE_VERSION>`.

    Example: Entgra IoT Server 3.4.0 is compatible with the Entgra Android agent 3.4.0\. Therefore, the release tag will be release-3.4.0. 

2.  Open the client folder that is in the `<ANDROID_AGENT_SOURCE_CODE>` via Android Studio.
3.  Do the necessary changes you wish to make.  
    Example:   
    [White Labeling the Entgra Android Agent]({{< param doclink>}}using-entgra-iot-server/product-administration/customizing-entgra-iot-server/#white-labeling-the-entgra-android-agent) or changing the Android agent `SERVER_PROTOCOL` to `https` and `SERVER_PORT` to 9`443` in the `constant.java` file that is in the `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main/java/org/wso2/iot/agent/utils` directory.

4.  Build the project to create a new APK file that has all the changes you made.

5.  Rename the created `.apk` file to  `android-agent.apk`.
6.  Copy the renamed file and replace it instead of the existing `android-agent.apk` file that is in the `<`IoT_HOME>/core/repository/deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.type-view/public/assets`` directory.


## Customizing UI and Documentation


This section explains how you can customize the Entgra IoT Server documentation, and the UI by adding preferred logos, themes, and images.


### White Labeling

White labeling enables users to change all the logos, which are used in all the  Entgra IoT Server interfaces so that it appears as though the product was designed specifically for the respective organization.

#### White labeling the device management console


Let's take a look at how you can white label the device management console. 

##### White labeling the header

Follow the steps given below:

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/units` directory.
2.  Create a new directory that follows the following naming convention: `<NAME>.unit.ui.header.logo`  
    Example: `white-labeling.unit.ui.header.logo`
3.  Create a file named `logo.hbs` and configure it as follows:

    {% raw %}
    {{!
      Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
      WSO2 Inc. licenses this file to you under the Apache License,
      Version 2.0 (the "License"); you may not use this file except
      in compliance with the License.
      You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
      Unless required by applicable law or agreed to in writing,
      software distributed under the License is distributed on an
      "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
      KIND, either express or implied. See the License for the
      specific language governing permissions and limitations
     under the License.
    }}
    {{#zone "productName"}}<ENTER THE NAME OF YOUR PRODUCT>{{/zone}}
    {{#zone "productNameResponsive"}}<ENTER THE NAME OF YOUR PRODUCT - THIS WILL BE USED WHEN THE CONSOLE BECOMES SMALLER IN SIZE>{{/zone}}
    {% endraw %}

4.  Create a file named `logo.json` and copy the configuration given below:

    js
    {
      "version": "1.0.0",
      "extends": "cdmf.unit.ui.header.logo"
    }
    
5.  Change the logo:  

    1.  Create a directory named `public` and in it create a directory named `img`.
    2.  Add your logo to the `img` directory. Make sure that the size of the logo is `288px X 112px` and that it's of the SVG format.

        Make sure:

        *   The mage size of the logo is `288px X 112px`.
        *   The image format is of the SVG format.
        *   The name of the image is `logo-inverse.svg`.

6.  Restart the Entgra IoT Server and sign into the device management console using admin/admin credentials: `https://<IoT_HOST>:9443/devicemgt.`
7.  You can now see the updated logo on the top left corner of the screen. 

<table>
   <colgroup>
      <col>
      <col>
   </colgroup>
   <tbody>
      <tr>
         <th>Before white labeling the header</th>
         <th>After white labeling the header</th>
      </tr>
      <tr>
         <td>
         </td>
         <td>
         </td>
      </tr>
   </tbody>
</table>

##### White labeling the footer

Follow the steps given below:

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/units` directory.
2.  Create a new directory that follows the following naming convention: `<NAME>.unit.footer`  
    Example: `white-labeling.unit.footer`
3.  Create a file named `footer.hbs` and configure it as follows:

    {% raw %}
    
    {{!
    Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
    WSO2 Inc. licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file except
    in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.
    }}
    {{#zone "footer"}}
    <p>
       <span class="hidden-xs">{ENTER THE NAME OF YOUR PRODUCT AND VERSION - THIS IS THE DEFAULT TEXT THAT WILL BE SHOWN}
       <span class="visible-xs-inline">{ENTER PRODUCT ABBREVIATION AND VERSION - THIS WILL BE USED WHEN THE CONSOLE BECOMES SMALLER IN SIZE } | &copy; 
       <script>document.write(new Date().getFullYear());</script>,
       <a href="{ENTER YOUR WEBSITE URL}" target="_blank"><img src="{{@unit.publicUri}}/img/logo.png"> Inc</a>. All Rights Reserved.
    </p>
    {{/zone}}
    {% endraw %}
    
4.  Create a file named `footer.json` and copy the configuration given below:

    js
    {
      "version": "1.0.0",
      "extends": "cdmf.unit.footer"
    }
    
5.  Changing the logo:
    1.  Create a directory named `public` and in it create a directory named `img`.
    2.  Add your logo to the `img` directory. Make sure that the size of the logo is `21px` `X` `21px`.

    This logo is added to the footer by the following configuration in the `footer.hbs` file.

    `<img src="{% raw %}{{@unit.publicUri}} {% endraw %}/img/logo.png">`

    For example, your final directory will look as follows:  
    ![image](352821405.png)

    

6.  Restart the Entgra IoT Server and sign into the device management console using admin/admin credentials: `https://<IoT_HOST>:9443/devicemgt.`
7.  You can now see the updated footer on the bottom left corner of the screen. 

<table>
   <colgroup>
      <col>
      <col>
   </colgroup>
   <tbody>
      <tr>
         <th>Before white labeling the footer</th>
         <th>After white labeling the footer</th>
      </tr>
      <tr>
         <td>
         </td>
         <td>
         </td>
      </tr>
   </tbody>
</table>


#### White labeling the publisher


##### Changing the Publisher logo

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/publisher/themes/mobileapp/img` directory.

2.  Change the `appm_logo.png` (`173px X 40px`) file in order to change the header.  
    ![image](352821436.png)

##### Changing Entgra related text 

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/publisher/themes/mobileapp/partials` directory.
2.  Configure the `footer.hbs` file accordingly.  
    Default configurations:

<p>
   WSO2 App Manager | &copy;
   <script>document.write(new Date().getFullYear());</script>
   <a href="http://wso2.com/" target="_blank"><i class="icon fw fw-wso2"></i> Inc</a>.
</p>
       
#### White Labeling Store

##### Changing the Store logo

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/store/themes/store/img` directory.
2.  Change the `app-store-logo.png` (`154px X 40px`) file in order to change the header logo.  
    ![image](352821461.png)

##### Changing WSO2 related text

1.  Navigate to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/store/themes/store/partials` directory.

2.  Change the `footer.hbs` file accordingly.  
    Default configuration:

    {% raw %}
    {{!-- footer --}}
    {% endraw %}
    <footer class="footer">
       <p>
          WSO2 App Manager | &copy;
          <script>document.write(new Date().getFullYear());</script>
          <a href="http://wso2.com/" target="_blank"><i class="icon fw fw-wso2"></i> Inc</a>.
       </p>
    </footer>
    


#### White Labeling the Entgra Android Agent



##### Prerequisites 

1.  Download and install Android Studio.

    For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).

2.  Replace the `android.jar` file that is in the `<ANDROID_STUDIO>/<SDK location>/platforms/android-<CURRENT_API_LEVEL>`  
    directory with the `android.jar` file. You can get the new `jar` file using **one of the following** options:

    *   Use a pre-built jar file from a third party developer. You can find it [here](https://github.com/anggrayudi/android-hidden-api).

        Make sure to use the jar file that matches the `compileSdkVersion` of the Entgra Android agent. Currently, the `compileSdkVersion` is 25.

    *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

    Why is this needed?

    The Entgra Android agent requires access to hidden APIs (APIs that are available at runtime). Therefore, you need to replace the `anrdoid.jar` file as mentioned in this step.

    After the above steps, make sure to close Android studio fully and restart adb with following 2 commands

    1.  adb kill-server
    2.  adb start-server
3.  Entgra IoT Server supports API levels 17 to 25\. Therefore, install the Android API levels 17 to 25 on Android Studio.
    1.  Open Android Studio.
    2.  Click **SDK Manager**. **![image](2.png)**
    3.  Click the **SDK Platform **tab and select the 17 and 25 API levels.  
        Example:  
        ![image](352821327.png)
    4.  Click **Apply**.

##### Changing Android Agent related logos and icons

1.  [Download the Android agent source code](https://github.com/wso2/cdmf-agent-android/releases/tag/v2.0.0). The folder will be referred to as `<ANDROID_AGENT_SOURCE_CODE>` throughout the documentation.

2.  Navigate to the `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main/res`  directory.

    1.  Navigate to the `drawable-xxhdpi/` folder and change the following:
        *   Change the `ic_launcher.png` (`144px X 144px`) file in order to change the icon. For more information, go to the [Android icon guide](http://developer.android.com/design/style/iconography.html).   
            ![image](352821489.png)
    1.  *   Change the following file logos in order to change the header logos:  

<table>
   <tbody>
      <tr>
         <td><strong><code>ic_logo.png</code> &nbsp;( <code>240px&nbsp;X 72px</code> )</strong></td>
         <td>
         </td>
      </tr>
      <tr>
         <td><strong><code>ic_logo_darker.png</code> &nbsp;( <code>240px&nbsp;X 72px)</code></strong></td>
         <td>
         </td>
      </tr>
      <tr>
         <td><strong><code>repeat_bg.png</code> &nbsp;&nbsp;( <code>277px&nbsp;X 125px</code> )</strong></td>
         <td>
         </td>
      </tr>
   </tbody>
</table>

    2.  Navigate to the `drawable-xhdpi` folder.
        *   Change the `ic_launcher.png` (`96px X 96px`) file to change the icon.
        *   Change the `ic_logo.png` (`160px X 48px`), `ic_logo_darker.png`  (`160px X 48px`) and `repeat_bg.png` (`222px X 100px`) file in order to change the logos.  
    3.  Navigate to the `drawable-hdpi` folder.  
        *   Change the `ic_launcher.png` (`72px X 72px`) file in order to change the icon.
        *   Change the `ic_logo.png` (`120px X 36px`), `ic_logo_darker.png`  (`120px X 36px`) and `repeat_bg.png` (`122px X 55px`) files in order to change the logos.
    4.  Navigate to the `drawable-mdpi` folder.  
        *   Change the `ic_launcher.png` (`48px X 48px`) file to change the icon.
        *   Change the `ic_logo.png` (`80px X 24px`) and `ic_logo_darker.png`  (`80px X 24px`) files in order to change the logos.

3.  Change the permanent device lock screen logo.
    1.  Navigate to the  `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main/res/drawable-hdpi` directory.
    2.  Replace  the `ic_logo_dark.png` file to change the icon.
    ![image](352821494.png)
4.  Open the client folder that is in the  `<ANDROID_AGENT_SOURCE_CODE>/client`  directory via Android Studio.
5.  The Android agent can't be built via the usual Android developer SDK, as it requires access to [developer restricted APIs](about:blank#). Therefore, you need to replace the existing android.jar file that is in the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with [the explicitly built android.jar file that has access to the restricted APIs.](https://github.com/anggrayudi/android-hidden-api/tree/master/android-23) 
6.  Build the project to create a new APK with the changes.

7.  Rename the created`.apk` file to `android-agent.apk`.
8.  Copy the renamed file and replace it instead of the existing `android-agent.apk`. file that is in the ``<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.type-view/public/assets``  directory.


#### White Labeling the Entgra iOS Agent


This section will guide you on how to white label the iOS agent.



Before you begin:



*   [Download Xcode](https://developer.apple.com/xcode/download/) and install it.
*   [Download the iOS Agent source code](https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.1).





##### Changing iOS Agent related logos and icons

Follow the steps given below to change the logos and icons:

1.  Open the [iOS Agent source code](https://github.com/wso2/cdmf-agent-ios/releases/tag/v2.0.1) by importing it to Xcode.

2.  Click **Supporting Files > Images.xcassets**.

    **![image](352821570.png)**

3.  Change the icons that are in **AppIcon.**  
    ![image](352821576.png)

4.  [Build](https://help.apple.com/xcode/mac/8.0/#/devdc0193470) and export the project as an iOS application. This will generate an `ipa` file. Rename this file as `ios-agent.ipa` and move it to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/ios-web-agent/app/pages/mdm.page.enrollments.ios.download-agent/public/asset` directory.

##### Changing Entgra related text

Follow the steps given below to change the text:

1.  Open the Agent source code by importing it to Xcode.

2.  Click **Supporting Files > Images.xcassets**.  
    ![image](352821570.png)

3.  Change the text of the images that are in the **Launchimage**.  
    ![image](352821564.png)

4.  Click on the image you want to edit and edit the text by clicking on the respective text area.

5.  [Build](https://help.apple.com/xcode/mac/8.0/#/devdc0193470) and export the project as an iOS application. This will generate a `.ipa` file. Rename this file as `ios-agent.ipa` and move it to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/ios-web-agent/app/pages/mdm.page.enrollments.ios.download-agent/public/asset` directory.

    The ios-web-agent directory is there only if you [configured Entgra IoT Server for iOS]({{< param generaldoclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/apple-server-configurations).


##### Changing the header of the Entgra iOS Agent

Follow the steps given below to change the header of the Entgra iOS agent:

1.  Open the Agent source code by importing it to Xcode.
2.  Click **Supporting Files**.
3.  Remove the current header.png file and add your new header image. 

    *   Make sure to name your new header image as header.png.
    *   The size needs to be 759 x 370 pixels.

    Example:  
    ![image](352821592.png)

4.  [Build](https://help.apple.com/xcode/mac/8.0/#/devdc0193470) and export the project as an iOS application. This will generate a `.ipa` file. Rename this file as `ios-agent.ipa` and move it to the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/ios-web-agent/app/pages/mdm.page.enrollments.ios.download-agent/public/asset` directory.

    The ios-web-agent directory is there only if you [configured Entgra IoT Server for iOS]({{< param generaldoclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/apple-server-configurations).


#### White Labeling Entgra Documentation


You need to export the Entgra IoT Server documentation as HTML or XML files in order to be able to edit the documentation for white labelling purposes. Follow the steps given below:

1.  At the bottom of the left navigation, click **Space Tools > Content Tools**, and Click the ****Export** **tab.  
    ![image](352821644.png)
2.  Based on your preference, select either **HTML** or **XML** export options and click **Next.**  
    ![image](352821627.png)
3.  Select the type of export.
    *   **Normal Export** - This will export all the pages.
    *   **Custom Export** - If you select this option, you will need to select as to which pages you wish to export. By default, all the pages will be selected. Therefore, unselect the pages that you do not wish to export.  
        ![image](352821638.png)
4.  Click **Export**.


#### Customizing the Management Console


Some of the Entgra products consist of a web user interface named the Management Console. It allows administrators to configure, monitor, tune, and maintain Entgra products. The components that formulate the design and style of the Management Console are defined in resource (JAR) files. You can customize the Management Console by modifying these resource files. You need to create a fragment bundle for the original resource file that specifies a required bundle for it. Then, you can pack the modified resource files in the required bundle. The files in the required bundle will get precedence and will override the files in the original bundle.

You can use this same technique to customize any aspect of the Management Console. The advantage of this technique is that you will not lose your customizations when you apply official patches to the product by replacing the original bundles.

For example, when you access the Management Console using the following URL, by default, it has the Entgra product logo as shown below:  `https://10.100.5.72:9443/carbon/`

![image](352821664.png)

Follow the steps below to change this logo.

1.  Find the `<IOTS_HOME>/wso2/components/plugins/org.wso2.carbon.ui_4.4.16.jar` file, and extract it.

    Find the bundle that contains the resource files you need to modify. In this case, the logo and the related CSS files are contained in the `org.wso2.carbon.ui_4.4.16.jar` file.

2.  Create a new Maven project using your IDE (e.g. `org.wso2.carbon.ui_4.4.16_patch` ). 

    This creates a project for a new bundle to pack the files you modify. Since the symbolic name of the original bundle is ‘`org.wso2.carbon.ui`’, this patch bundle should have the symbolic name as part of its name (e.g. ‘`org.wso2.carbon.ui_4.4.16_patch`’). Also, you need to use this name with the symbolic name as part of it, as the required bundle in the fragment bundle, which you will create later.

3.  Add the following content to the `pom.xml` file of the  `org.wso2.carbon.ui_4.4.16_patch` project.     

    <project xmlns="http://maven.apache.org/POM/4.0.0"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <modelVersion>4.0.0</modelVersion>
       <groupId>org.wso2.carbon</groupId>
       <artifactId>org.wso2.carbon.ui_4.4.16_patch</artifactId>
       <version>1.0.0</version>
       <packaging>bundle</packaging>
       <build>
          <plugins>
             <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <version>3.0.1</version>
                <extensions>true</extensions>
                <configuration>
                   <instructions>
                      <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                      <Bundle-Name>${project.artifactId}</Bundle-Name>
                      <Export-Package>web.admin.*</Export-Package>
                   </instructions>
                </configuration>
             </plugin>
          </plugins>
       </build>
    </project>
    

4.  Create the `web/`, `admin/`, css/, `images/` and `layout/` directory locations as they were in the original bundle as shown in the file structure below.   
    ![image](352821669.png)

5.  Create a new CSS file (e.g. `customizations.css`) with the following content.

    This file includes the logo customization styles.

    #header div#header-div div.left-logo {
    	background-image: url( ../images/new-logo.png );
        background-repeat: no-repeat;
        background-position: left top;
        background-size: contain;
    	height: 40px;
    	width: 300px;
    	margin-top: 23px;
    	margin-left: 20px;
    	float: left;
    }
    

6.  Add the `customizations.css` file to the `<org.wso2.carbon.ui_4.4.16_patch>/web/admin/css/` directory.  

    This file includes the logo customization styles. 

7.  Copy the content of the `<org.wso2.carbon.ui_4.4.16.jar>/` `web/admin/layout/template.jsp `file to the `<org.wso2.carbon.ui_4.4.16_patch>/web/admin/layout/template.jsp` file.

8.  Add the following line to the `<org.wso2.carbon.ui_4.4.16_patch>` `web/admin/layout/template.jsp` file: 

    `<link href="../admin/css/customizations.css" rel="stylesheet" type="text/css" media="all"/>`

9.  Add the below image as the new logo (e.g.   `new-logo.png`) to the `<org.wso2.carbon.ui_4.4.16_patch>/` `web/admin/images/` directory.

    ![image](352821674.png)

10.  Create another new Maven project using your IDE (e.g. `o` `rg.wso2.carbon.ui_4.4.16_fragment`).

    This creates a project for the fragment bundle. Since the symbolic name of the original bundle is ‘`org.wso2.carbon.ui`’, the fragment host value of this bundle should be the same (e.g. o `rg.wso2.carbon.ui_4.4.16_fragment`). This fragment bundle will not contain anything (expect the `pom.xml` file) when it is built.  

11.  Add the following content to the `pom.xml` file of the `org.wso2.carbon.ui_4.4.16_fragment` project.   

    This `pom.xml` file of the fragment bundle define properties including the required bundle value (i.e. ‘`org.wso2.carbon.ui_4.4.16_patch`’).  

<project xmlns="http://maven.apache.org/POM/4.0.0"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>
   <groupId>org.wso2.carbon</groupId>
   <artifactId>org.wso2.carbon.ui_4.4.16_fragment</artifactId>
   <version>1.0.0</version>
   <packaging>bundle</packaging>
   <build>
      <plugins>
         <plugin>
            <groupId>org.apache.felix</groupId>
            <artifactId>maven-bundle-plugin</artifactId>
            <version>3.0.1</version>
            <extensions>true</extensions>
            <configuration>
               <instructions>
                  <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                  <Bundle-Name>${project.artifactId}</Bundle-Name>
                  <Require-Bundle>org.wso2.carbon.ui_4.4.16_patch</Require-Bundle>
                  <Fragment-Host>org.wso2.carbon.ui</Fragment-Host>
               </instructions>
            </configuration>
         </plugin>
      </plugins>
   </build>
</project>

12.  Build the following two projects by executing the following command: `mvn clean install`   

    *   `org.wso2.carbon.ui_4.4.16_fragment`

    *   `org.wso2.carbon.ui_4.4.16_patch`
13.  Once the project is built, copy the two JAR files (from the `<PROJECT_HOME>/target/` directory) to the `<IOTS_HOME>/dropins/` directory.  

    *   `org.wso2.carbon.ui_4.4.16_fragment-1.0.0.jar`

    *   `org.wso2.carbon.ui_4.4.16_patch-1.0.0.jar`
14.  Restart the WSO2 Carbon server.
15.  Access the Management Console of WSO2 Carbon using the following URL: `https://<IOTS_HOST>:9443/carbon` (The default IoT Server host is localhost.)




## Customizing the Android Agent to Analyze Device Logs





You will want to analyze the data gathered from the devices that are registered with Entgra IoT Server. Wondering how you can do this in Entgra IoT Server?

Entgra IoT Server uses Splunk to capture, index and correlate the real-time data into a searchable repository. Using the data in this repository you are able to generate graphs, reports, alerts, dashboards, and more. 

Le's take a look at how you can configure Splunk with the Entgra IoT Server Android Agent.

1.  [Download the Android agent source code](https://github.com/wso2/cdmf-agent-android/releases/tag/v2.0.0). This will be referred to as the `<ANDROID_AGENT_SOURCE_CODE>` throughout this document.

2.  Open the `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main/java/org/wso2/emm/agent/utils/Constant.java` file.

3.  Define `SPLUNK_PUBLISHER` as the value for the string variable `LOG_PUBLISHER_IN_USE`.

    `public static final String LOG_PUBLISHER_IN_USE = SPLUNK_PUBLISHER;`

4.  Configure the fields shown below:

    public final class SplunkConfigs {
      public static final String API_KEY = "<SPLUNK_API_KEY>";
      public static final String TYPE_HTTP = "HTTP";
      public static final String TYPE_MINT = "MINT";
      public static final String DATA_COLLECTOR_TYPE = TYPE_MINT;
      public static final String HEC_TOKEN = "<SPLUNK_HEC_TOKEN>";
      public static final String HEC_MINT_ENDPOINT_URL = "<SPLUNK_HEC_MINT_ENDPOINT_URL>";
    }
   
    <table>
      <tbody>
        <tr>
          <th>Field</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
          </td>
          <td>Provide the Splunk API Key.</td>
        </tr>
        <tr>
          <td>
          </td>
          <td>Provide the HTTP Event Collector token.</td>
        </tr>
        <tr>
          <td>
          </td>
          <td>Provide the HTTP Event Collector&nbsp;endpoint URL.</td>
        </tr>
      </tbody>
    </table>

5.  Apply the logcat operation on your device to push data to Splunk through Entgra IoT Server.

    For more information on applying an operation on your device, see the [Android device operations]({{< param doclink >}}tutorials/android/). 
