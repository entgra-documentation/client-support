# Setting up Work Profile

A work profile creates a containerized environment in a [BYOD device]({{< param generaldoclink >}}key-concepts/devices/#device-ownership) to run corporate data and applications. This enables device admins to take control of the corporate data and applications running on the device without preventing the device owner from using the primary profile functionality.



Before you begin



*   Make sure to have an Android device that supports the Lollipop version or upwards.

    

    

    Entgra IoT Server implements data containerization using the [Android Managed Profile](https://developer.android.com/work/managed-profiles.html#overview) feature, which is only available on Android Lollipop OS version and above.

    

    

*   Do you have work profile that is already running on your device? If yes, on your device, go to **Settings > Accounts** and remove it. Else, you run into errors.

*   Start the Entgra IoT Server core profile.

    `cd <IoT_HOME>/bin sh iot-server.sh`

*   Download the Android agent.

    

    

    

    Follow the steps given below to get the Android Agent. 

    

    





Follow the instructions given below to set up the Android work profile:

1.  Tap **INSTALL** to start installing the Android agent.

    ![image](352816582.png)

2.  Tap **OPEN**, once the Entgra Android Agent is successfully installed.  

3.  Tap **SETUP WORK-PROFILE** to proceed with registering the Android device via the Work-Profile.  
    ![image](352816680.png)

4.  Tap **SET UP**.

    ![image](352816708.png)

    

    

    If your device was not encrypted previously, you will be prompted to encrypt the device.

    

    

5.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

    

    

    If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

    

    

    ![image](352816766.png)

6.  Enter the server address based on your environment, in the text box provided and tap **START REGISTRATION**. A confirmation message appears.

    *   Developer Environment - Enter the server IP as your server address.  
        Example: `10.10.10.123:8280`
    *   Deployment Environment - Enter the domain as your server address.

        

        

        The Android Agent app's default port is 8280\. If you are using any other port, the server address should state the new port in the following format: `www.abc.com``:<PORT>`, e.g., if the port is 8289 the server IP is as follows: `www.abc.com:8289`.

        

        

    ![image](352816614.png)

7.  Enter your details and tap **SIGN IN**.  

    *   **Organization -** Enter the organization name only if the server is hosted with multi-tenant support or enter the default `carbon.super`, which is the default organization name on a non-multi-tenant environment.
    *   **Username** - Enter the Entgra IoTS username.
    *   **Password** - Enter the Entgra IoTS password.
    ![]({{site.baseurl}}/assets/images/352816593.png)  
    Read the policy agreement, and tap **AGREE** to accept the agreement.  
8.  Tap **ALLOW** to allow the Entgra Android Agent to access photos, media, and files, make and manage phone calls, and access the device location respectively.

    ![image](352816714.png)

9.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the do Not Disturb setting enabled will affect the ring, and mute operations. This settings is only shown for Android Nougat and above.

    1.  Tap **OK.**  
        ![image](352816760.png)

    2.  Enable Entgra Device Management for the Do Not Disturb setting.  
        ![image](352816772.png)
    3.  Click **ALLOW**.  
        ![image](352816744.png)
10.  Set a PIN code of your choice with a minimum of 4 digits. A confirmation message appears.

    

    

    You will be prompted to provide a PIN code only if your device is a [BYOD]({{< param doclink >}}key-concepts/#device-ownership) device. The PIN code will be used to secure your personal data. Thereby, Entgra IoT Server will not be able to carry out critical operations on your personal data without using this PIN. 

    Example: A device management admin cannot wipe your device or remove data from the device without the PIN code. You have to provide the PIN code to get your device wiped or you can log into the device management console and wipe your device by entering the PIN code. 

    

    

    ![image](352816696.png)

11.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server.  
    ![image](352816702.png)

Once the registration process is complete, navigate to the launcher of your device. Notice the duplication of application icons. The applications with red icons are the ones used by Entgra IoT Server.

![image](352816732.png)





To deactivate Android Work Profile:

1.  Navigate to **Settings > Accounts** on your device.
2.  Click **Remove work profile**.
3.  Tap **DELETE** and proceed with the deactivation.  
    ![image](352816738.png)  
    Once the deactivation is complete, navigate to the launcher of the device. Notice the disappearance of the applications with red icons.  
    ![image](352816720.png)


