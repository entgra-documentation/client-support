---
bookCollapseSection: true

---
# Android

**In this tutorial**, you are enrolling your Android device also known as Bring Your Own Device (BYOD) with Entgra IoT Server. Entgra IoTS supports devices on Android version 4.2.x to 7.0 (Android Jelly Beans to Nougat). If you want to enroll a Corporate-Owned, Personally Enabled (COPE) device, you need to [configure Entgra IoT Server is the system service application](about:blank#) and install the configured Android agent on the device.

The following subsections explain how you can work with the Android device type:

Before you begin


1.  Android restricts third-party apps and less secure apps from being installed on the device. Therefore, you need to configure your device to disable this restriction as the Entgra IoT Server device management agent application acts as a third-party application.

    

    

    

    

    Follow the steps given below:

    1.  Navigate to **Setting > Security**.
    2.  Enable the **Unknown sources** option.

    

    

    

2.  Start Entgra IoT Server by starting the two profiles in the following order:

    1.  Start the core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

        
        cd <IOTS_HOME>/bin
        ./iot-server.sh
        

        The default port assigned for the core is 9443.

    2.  Start the analytics profile, which corresponds to the WSO2 Data Analytics Server profile.

        `./analytics.sh`

        The default port assigned for analytics is 9445.





## Enroll an Android device

Follow the instructions given below to enroll an Android device:

**Using the QR code**



1.  Access the WSO2 device management console by navigating to** `https://<IoT_HOST>:9443/devicemgt`** and sign in.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


    2.  Access the device management console.

        *   For access via HTTP:js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`
            

            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#registering-with-entgra-iot-server).


        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![image](352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![image](352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![image](352819647.png)

    

    

    

2.  Click **Enroll New Device**.  
    ![image](352816972.png)
3.  Click **Android** to enroll your device with Entgra IoTS.  
    ![image](352816277.png)
4.  Enroll the device.
    1.  Click **Enroll Device**.
    2.  Scan the QR code to download the Android agent onto your Android device.

        

        

        You need to make sure that your Android device and the IoT Server are on the same network, else you will not be able to download the Android agent.

        

        

        After scanning the QR code you will be directed to a web page. When this page appears, the web browser will typically display an "insecure connection" message, which requires your confirmation before you can continue.

        

        

        

        

        

        The Entgra IoTS consoles are based on the HTTPS protocol, which is a combination of HTTP and SSL protocols. This protocol is generally used to encrypt the traffic from the client to server for security reasons. The certificate it works with is usedforencryptiononly,anddoesnot prove the server identity, so when you try to access these consoles, a warning of untrusted connection is usually displayed. To continue working with this certificate, some steps should be taken to "accept" the certificate before access to the site is permitted. If you are using the Mozilla Firefox browser, this usually occurs only on the first access to the server, after which the certificate is stored in the browser database and marked as trusted. However, with other browsers, the insecure connection warning might be displayed every time you access the server.

        This scenario is suitable for testing purposes, or for running the program on the company's internal networks. If you want to make these consoles available to external users, your organization should obtain a certificate signed by a well-known certificate authority, which verifies that the server actually has the name it is accessed by and that this server belongs to the given organization.

        

        

        

        

5.  Click **Download IoT Server Agent**.

6.  Tap **INSTALL** to start installing the Android agent.

    ![image](352816433.png)

7.  Tap **OPEN**, once the WSO2 Android Agent is successfully installed.
8.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

    

    

    If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

    

    

    ![image](352816341.png)

9.  Tap **Skip and go to Enrollment** to proceed with the default Android enrollment process.  
    If you want to set up the work profile and enable data containerization, click **[Setup Work-Profile]({{< param doclink >}}tutorials/android/setting-up-work-profile/)**. You will see the below screen only if your device is on the Lollipop OS version or above. Else, move to the next step.
    ![image](352816406.png)

10.  Enter the server address based on your environment, in the text box provided. A confirmation message will appear.

    *   Developer Environment - Enter the server IP as your server address.  
        Example: 10.10.10.123:8280
    *   Deployment Environment - Enter the domain as your server address.

        

        

        The Android Agent app's default port is 8280\. If you are using any other port, the server address should state the new port in the following format: `www.abc.com` `:<PORT>`  
        For example, if the port is 8289 the server IP is as follows: `www.abc.com:8289`

        

        

        ![image](352816439.png)
11.  Enter your details and tap **Register**. A confirmation message will appear.  

    *   **Organization** - Enter the organization name only if the server is hosted with multi-tenant support or enter the default carbon.super, which is the default organization name on a non-multi-tenant environment.

    *   **Username** - Enter your Entgra IoT Server username.

    *   **Password** - Enter your Entgra IoT Server password. 

    ![image](352816445.png)

12.  Read the tenant policy agreement, and tap **Agree** to accept the agreement.  
13.  Tap **ACTIVATE** to enable the WSO2 agent administrator on your device. A confirmation message will appear after enabling the device admin.  
    ![image](352816329.png)

14.  Tap **ALLOW** to allow the WSO2 Android agent to make and manage phone calls, access photos, media and files, and access the device location respectively.

    

    

    You will get this message only if your Android OS is Marshmallow (6.0) or above.

    

    

    Example:

    ![image](352816271.png)

15.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the Do Not Disturb setting enabled will affect the ring, and mute operations. This settings is only shown for Android Nougat and above.

    1.  Tap **OK.**  
        ![image](352816359.png)

    2.  Enable WSO2 Device Management to access the Do Not Disturb setting.  
        ![image](352816335.png)
    3.  Click **Allow**.  
        ![image](352816353.png)
16.  Set a PIN code of your choice with a minimum of 4 digits. The PIN code will be used to secure your personal data. Thereby, the IoT server will not be able to carry out critical operations on your personal data without using this PIN.   
    Example: If the device management admin needs to wipe your device or remove data from the device, he/she can not directly wipe it without the PIN code. You have to provide the PIN code to get your device wiped or you can log into the device management console and wipe your device by entering the PIN code. A confirmation message will appear.

    

    

    You will be prompted to provide a PIN code only if your device is a BYOD device.

    

    

    ![image](352816427.png)

17.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server.  
    ![image](352816313.png)

    

    

    

    Follow the instructions below to uninstall the Android agent app:

    1.  Open the Android agent application and click **Unregister**.

    2.  Finally, go and uninstall the Android agent by tapping long on the device and then dragging it to **UNINSTALL**.

    

    



**Inviting via email**



**Prerequsite**



Configure the email settings to send out an invitation for users to register their device with Entgra IoTS.











In Entgra IoTS, user registration confirmation emails are disabled by default, and the admin needs to provide the required configuration details to enable it.





1.  Create an email account to send out emails to users that register with IoT Server (e.g., [no-reply@foo.com](mailto:no-reply@foo.com)).

2.  Open the `<IOTS_HOME>/repository/conf/axis2/axis2.xml` file, uncomment the `mailto` transportSender section, and configure the IoTS email account.

    
    <transportSender name="mailto" class="org.apache.axis2.transport.mail.MailTransportSender">
       <parameter name="mail.smtp.host">smtp.gmail.com</parameter>
       <parameter name="mail.smtp.port">587</parameter>
       <parameter name="mail.smtp.starttls.enable">true</parameter>
       <parameter name="mail.smtp.auth">true</parameter>
       <parameter name="mail.smtp.user">synapse.demo.0</parameter>
       <parameter name="mail.smtp.password">mailpassword</parameter>
       <parameter name="mail.smtp.from">synapse.demo.0@gmail.com</parameter>
    </transportSender>
    

    

    

    For `mail.smtp.from`, `mail.smtp.user`, and `mail.smtp.password`, use the email address, username, and password (respectively) from the mail account you set up.

    

    

    Example:

    
    <transportSender name="mailto" class="org.apache.axis2.transport.mail.MailTransportSender">
       <parameter name="mail.smtp.host">smtp.gmail.com</parameter>
       <parameter name="mail.smtp.port">587</parameter>
       <parameter name="mail.smtp.starttls.enable">true</parameter>
       <parameter name="mail.smtp.auth">true</parameter>
       <parameter name="mail.smtp.user">foo</parameter>
       <parameter name="mail.smtp.password">$foo1234</parameter>
       <parameter name="mail.smtp.from">no-reply@foo.com</parameter>
    </transportSender>
    

3.  Optionally, configure the email sender thread pool.  
    Navigate to the `email-sender-config.xml` file, which is in the `<IOTS_HOME>/repository/conf/etc` directory, and configure the following fields under `<EmailSenderConfig>`.

    *   `MinThreads`: Defines the minimum number ofthreadsthatneeds to be available in the underlying thread pool when the email sender functionality is initialized.

    *   `MaxThreads`: Defines the maximum number of threads that should serve email sending at any given time.

    *   `KeepAliveDuration`: Defines the duration a connection should be kept alive. If the thread pool has initialized more connections than what was defined in `MinThreads`, and they have been idle for more than the `KeepAliveDuration`, those idle connections will be terminated

    *   `ThreadQueueCapacity`: Defines the maximum concurrent email sending tasks that can be queued up.

    Example:

    
    <EmailSenderConfig>
       <MinThreads>8</MinThreads>
       <MaxThreads>100</MaxThreads>
       <KeepAliveDuration>20</KeepAliveDuration>
       <ThreadQueueCapacity>1000</ThreadQueueCapacity>
    </EmailSenderConfig>
    

4.  Optionally, customize the email templates that are in the `<IOTS_HOME>/repository/resources/email-templates` directory. 

    

    

    The email template functionality of Entgra IoT Server is implemented on top of Apache Velocity, which is a free and open-source template engine.

    

    

    1.  Open the email template that you wish to edit based on the requirement, such as the `user-invitation.vm` or `user-registration.vm` file.
    2.  Edit the `<Subject>` and `<Body>`tosuite your requirement.
    3.  Restart Entgra IoT Server.

    

    

    If you need to access `HTTP` or `HTTPS` base URLs of the server within your custom template configs, use the `$base-url-http` and `$base-url-https` variables, respectively

    

    







1.  Access the WSO2 device management console by navigating to** `https://<IoT_HOST>:9443/devicemgt`** and sign in.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


    2.  Access the device management console.

        *   For access via HTTP:js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`


            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#registering-with-entgra-iot-server).


        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![image](352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![image](352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![image](352819647.png)

    

    

    

2.  Click **Invite by Email**.
3.  Enter the email address of the users who need to enroll their device with Entgra IoTS, and click **Send Invite(s)**.  
    If you entered your email address, you will receive the registration email.
4.  Click on the link in the email to download the Android agent.
5.  Click **Download IoT Server Agent**.

6.  Tap **INSTALL** to start installing the Android agent.  
    ![image](352816433.png)

7.  Tap **OPEN**, once the WSO2 Android Agent is successfully installed.
8.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

    

    

    If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

    

    

    ![image](352816341.png)

10.  Tap **Skip and go to Enrollment** to proceed with the default Android enrollment process.  
    If you want to set up the work profile and enable data containerization, click **[Setup Work-Profile]({{< param doclink >}}tutorials/android/setting-up-work-profile/)**. You will see the below screen only if your device is on the Lollipop OS version or above. Else, move to the next step.
    ![image](352816406.png)

11.  Enter the server address based on your environment, in the text box provided. A confirmation message will appear.

    *   Developer Environment - Enter the server IP as your server address.  
        Example: 10.10.10.123:8280 
    *   Deployment Environment - Enter the domain as your server address.

        

        

        The Android Agent app's default port is 8280\. If you are using any other port, the server address should state the new port in the following format: `www.abc.com` `:<PORT>`  
        For example, if the port is 8289 the server IP is as follows: `www.abc.com:8289`

        

        

    ![image](352816439.png)

12.  Enter your details and tap **SIGN IN**. A confirmation message will appear.  

    *   **Organization** - Enter the organization name only if the server is hosted with multi-tenant support or enter the default carbon.super, which is the default organization name on a non-multi-tenant environment.

    *   **Username** - Enter your Entgra IoTS username.

    *   **Password** - Enter your Entgra IoTS password. 

        ![image](352816323.png)
13.  Read the tenant policy agreement, and tap **Agree** to accept the agreement.  
14.  Tap **ALLOW** to allow the WSO2 Android agent to make and manage phone calls, access photos, media,andfiles, and access the device location respectively.

    

    

    You will get this message only if your Android OS is Marshmallow (6.0) or above.

    

    

    Example:

    ![image](352816271.png)

15.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the Do Not Disturb setting enabled will affect the ring, and mute operations. This settings is only shown for Android Nougat and above.

    1.  Tap **OK.**  
        ![image](352816359.png)

    2.  Enable WSO2 Device Management to access the Do Not Disturb setting.  
        ![image](352816335.png)
    3.  Click **Allow**.  
        ![image](352816353.png)
16.  Set a PIN code of your choice with a minimum of 4 digits and tap **SET PIN CODE**. The PIN code will be used to secure your personal data. Thereby, the IoT server will not be able to carry out critical operations on your personal data without using this PIN.   
    Example: If the device management admin needs to wipe your device or remove data from the device, he/she can not directly wipe it without the PIN code. You have to provide the PIN code to get your device wiped or you can log into the device management console and wipe your device by entering the PIN code. A confirmation message will appear.

    

    

    You will be prompted to provide a PIN code only if your device is a BYOD device.

    

    

    ![image](352816427.png)

17.  Tap **ACTIVATE** to enable the WSO2 agent administrator on your device. A confirmation message will appear after enabling the device admin.  
    ![image](352816329.png)
18.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server.  
    ![image](352816313.png)

    

    

    

    Follow the instructions below to uninstall the Android agent app:

    1.  Open the Android agent application and click **Unregister**.

    2.  Finally, go and uninstall the Android agent by tapping long on the device and then dragging it to **UNINSTALL**.

    

    



**Downloading the APK**



1.  Access the WSO2 device management console by navigating to** `https://<IoT_HOST>:9443/devicemgt`** and sign in.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


    2.  Access the device management console.

        *   For access via HTTP:js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`

            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#registering-with-entgra-iot-server).


        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![image](352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![image](352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![image](352819647.png)

    

    

    

2.  Click **Download APK**.
3.  Copy the downloaded file to your mobile device.
4.  Tap **INSTALL** to start installing the Android agent.

    ![image](352816433.png)

5.  Tap **OPEN**, once the WSO2 Android Agent is successfully installed.
6.  You need to agree by clicking **CONTINUE** to share the device details listed in the following screen with Entgra IoT Server when enrolling your Android device.

    

    

    If you click **EXIT**, you are not able to register your device with Entgra IoT Server.

    

    

    ![image](352816341.png)

8.  Tap **Skip and go to Enrollment** to proceed with the default Android enrollment process.  
    If you want to set up the work profile and enable data containerization, click **[Setup Work-Profile]({{< param doclink >}}tutorials/android/setting-up-work-profile/)**. You will see the below screen only if your device is on the Lollipop OS version or above. Else, move to the next step.
    ![image](352816406.png)

9.  Enter the server address based on your environment, in the text box provided. A confirmation message will appear.

    *   Developer Environment - Enter the server IP as your server address.  
        Example: 10.10.10.123:8280 
    *   Deployment Environment - Enter the domain as your server address.

        

        

        The Android Agent app's default port is 8280\. If you are using any other port, the server address should state the new port in the following format: `www.abc.com` `:<PORT>`  
        For example, if the port is 8289 the server IP is as follows: `www.abc.com:8289`

        

        

    ![image](352816439.png)

10.  Enter your details and tap **Register**. A confirmation message will appear.  

    *   ****Organization****

    *   **Username** - Enter your Entgra IoTS username.

    *   **Password** - Enter your Entgra IoTS password. 

        ![image](352816323.png)
11.  Read the tenant policy agreement, and tap **Agree** to accept the agreement.  
12.  Tap **ALLOW** to allow the WSO2 Android agent to make and manage phone calls, access photos, media,andfiles, and access the device location respectively.

    

    

    You will get this message only if your Android OS is Marshmallow (6.0) or above.

    

    

    Example:

    ![image](352816271.png)

13.  Allow Entgra IoT Server to disable the do not disturb setting when it is enabled. This is required because having the Do Not Disturb setting enabled will affect the ring, and mute operations. This settings is only shown for Android Nougat and above.

    1.  Tap **OK.**  
        ![image](352816359.png)

    2.  Enable WSO2 Device Management to access the Do Not Disturb setting.  
        ![image](352816335.png)
    3.  Click **Allow**.  
        ![image](352816353.png)
14.  Set a PIN code of your choice with a minimum of 4 digits and type **SET PIN CODE**. The PIN code will be used to secure your personal data. Thereby, the IoT server will not be able to carry out critical operations on your personal data without using this PIN.   
    Example: If the device management admin needs to wipe your device or remove data from the device, he/she can not directly wipe it without the PIN code. You have to provide the PIN code to get your device wiped or you can log into the device management console and wipe your device by entering the PIN code. A confirmation message will appear.

    

    

    You will be prompted to provide a PIN code only if your device is a BYOD device.

    

    

    ![image](352816427.png)

15.  Tap **ACTIVATE** to enable the WSO2 agent administrator on your device. A confirmation message will appear after enabling the device admin.  
    ![image](352816329.png)
16.  You have now successfully registered your Android device. Tap **Device Information** to get device specific information, and tap **Unregister** if you wish to unregister your device from Entgra IoT Server.  
    ![image](352816313.png)

    

    

    

    Follow the instructions below to uninstall the Android agent app:

    1.  Open the Android agent application and click **Unregister**.

    2.  Finally, go and uninstall the Android agent by tapping long on the device and then dragging it to **UNINSTALL**.

    

    



###   
Try it out

You can view device details and carry out operations on your device, after successfully enrolling it with Entgra IoTS. Let's take a look at how you can do it:

1.  Navigate to the Device Management page to view all the created devices.

    

    

    

    

    

    1.  [Sign in to the Device Management console]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-consoles).

    2.  Click the ![image](2.png)icon.
    3.  Click **Device Management**.  
        ![image](352816293.png)

    

    

    

    

2.  Click the view icon on the Android device you enrolled.  
    Example:  
    ![image](352816395.png)

3.  You will be directed to the device details page where you can view the device information and try out operations on a device.  
    Example:  
    ![image](352816347.png)

    

    

    

    

    The details of each registered devices will be shown on separate pages within the **Device** page.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th><br></th>
          <th>Description</th>
        </tr>
        <tr>
          <td>Device Details</td>
          <td>
            
              <p>The following device information will be retrieved automatically when you register with IoTS.</p>
              
                <table>
                  <tbody>
                    <tr>
                      <td>Device</td>
                      <td>This shows the name the user has given his/her device (e.g., Kim's iPhone).</td>
                    </tr>
                    <tr>
                      <td>Ownership</td>
                      <td>Indicates if the device is your own device/Bring Your Own Device (BYOD) or if it's a Corporate-Owned, Personally Enabled (COPE) device.<br>For the device to be a COPE device,&nbsp;<a href="{{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application" class="external-link" rel="nofollow">Entgra IoT Server must be configured to use the system server application</a>.</td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td>Indicates if the device is active, inactive or removed from Entgra IoTS.</td>
                    </tr>
                  </tbody>
                </table>
              
            
          </td>
        </tr>
        <tr>
          <td>Policy Compliance</td>
          <td>If your device does not comply with certain criteria in the enforced policy, the aspects in which your device is none compliant will be highlighted under this section.</td>
        </tr>
        <tr>
          <td>Device Location</td>
          <td>Provide the location of your device.</td>
        </tr>
        <tr>
          <td>Installed Application</td>
          <td>A list of all the applications that have been installed on your device and the memory consumption by each application will be listed under this section.</td>
        </tr>
        <tr>
          <td>Operation Log</td>
          <td>A list of all the operations that have been carried out by you and its current status.<br>The operation logs shows the following statuses:<br>
            <ul>
              <li><code>IN-PROGRESS</code>&nbsp;- The operation is processing on the IoT server side and has not yet been delivered to the device.</li>
              <li><code>PENDING</code>&nbsp;- The operation is delivered to the device but the response from the device is pending.</li>
              <li><code>COMPLETED</code>&nbsp;- The operation is delivered to the device and the server has received a response back from the device.</li>
              <li><code>ERROR</code>&nbsp;- An error has occurred while carrying out the operation.</li>
            </ul>
          </td>
        </tr>
      </tbody>
    </table>

    

    

    

    

    

    

    

    The default operations that are available for Android devices are accessible for BYOD devices. The COPE devices can only carry out selected operations. If you want to enable the COPE devices to carry out more operations or if you want to limit BYOD devices from carrying out selected operations, you can do so via policies.

    Click on the operation you wish to carry out and provide the required details (if requested) to apply the selected operation on your device.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Operation</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
            <h5 id="Android-DeviceLock">Device Lock</h5>
          </td>
          <td>Ability to lock a device via the IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-DeviceUnlock">Device Unlock</h5>
          </td>
          <td>Ability to unlock the device via the IoT Server</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-Location">Location</h5>
          </td>
          <td>Ability to receive the location of the device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-MuteDevice">Mute Device</h5>
          </td>
          <td>Ability to&nbsp;enable the silent profile on a device&nbsp;via the IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-EnterpriseWipe">Enterprise Wipe</h5>
          </td>
          <td>Ability to unregister a device from IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-ClearPassword">Clear Password</h5>
          </td>
          <td>
            
              <p>Ability to remove a device lock via the IoT Server.</p>
              
                <p class="title">For Android 7.0 (API 24) and higher devices</p><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
                
                  <p>Clear password for device admins now apply to profile owners. Device admins can no longer clear passwords that are already set.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-ChangeLock-Code">Change Lock-Code</h5>
          </td>
          <td>
            
              <p>Ability to change the provided passcode or lock-code.</p>
              
                <p class="title">For Android 7.0 (API 24) and higher devices</p><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
                
                  <p>Change lock code for device admins now apply to profile owners. Device admins can no longer change lock codes that are already set. Device admins can still set a lock code, but only when the device has no password, PIN, or pattern.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-Ring">Ring</h5>
          </td>
          <td>Ability to ring the device via the IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-Message">Message</h5>
          </td>
          <td>Ability to send a message to the device via the IoT Server. The IoTS admin can use<br>this device operation to send group messages or even private messages to the IoTS users.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-WipeData">Wipe Data</h5>
          </td>
          <td>Ability to carry out a factory reset operation on your own device via the IoT Server. The user will have<br>to provide the PIN, which he/she entered when registering to IoTS, to be able to wipe his/her device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-Reboot">Reboot</h5>
          </td>
          <td>Ability to reboot or restart your Android device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-UpgradeFirmware">Upgrade Firmware</h5>
          </td>
          <td>
            
              <p>Ability to upgrade the firmware to a newer version, over-the-air (OTA)</p>
              
                
                  <p>For more information, see <a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352823254/Upgrading+Firmware+of+an+Android+Device" data-linked-resource-id="352823254" data-linked-resource-version="1" data-linked-resource-type="page">Upgrading Firmware of an Android Device</a>.</p>
                
            
          </td>
        </tr>
        <tr>
          <td>
            <h5 id="Android-Logcat">Logcat</h5>
          </td>
          <td>Ability to push logcat of the device to the analytics server. Logcat displays messages in real time and keeps a history so you can view the old messages.</td>
        </tr>
      </tbody>
    </table>

    #### System service application operations

    Entgra IoT Server provides a separate service application that can be signed by a firmware signing key and installed on the devices as a system application alongside the Android agent application. This enables you to have better control over the devices registered with Entgra IoT Server. to install the system service application on the devices, you need to integrate it with Entgra IoT Server. For more information, see [Integrating the Android System Service Application]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/#integrating-the-android-system-service-application).


    You can perform the following operations if you have the system service application installed on your Android device.

    *   Schedule firmware upgrades on the device.
    *   Reboot or restart your device.
    *   Install and update applications in silent mode that is without the user's confirmation via the system service application.
    *   Hard lock an Android device, where the Administrator permanently locks the device.
    *   Unlock a device that was hard locked.

    

    

    



