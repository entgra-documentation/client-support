---
bookCollapseSection: true
weight: 2
---
# Creating Fire Alarm device type


**In this tutorial**, you are going to create a new fire alarm device type, enroll a fire alarm using the device management console and start using the device. A sample Python agent is used to make the use case interesting.



Before you begin



*   Install Python.

*   Install pip.
    *   Enter `pip -V` in your terminal to check the current version of pip in your machine.

    *   If it is already installed you will get a message similar to what is shown below:  
        `pip 9.0.1 from /usr/lib/python2.7/dist-packages (python 2.7)`  
        Else you will get the following message:  
        `The program 'pip' is currently not installed. You can install it by typing:`  
        `sudo apt install python-pip`

    *   If pip is not installed use the following command to install it.

        `sudo apt install python-pip`

        

        

        If you are running on windows, remove the `sudo` prefix from the above command. Instead, you need to run the command in the command prompt as an administrator.

        

        

*   Install Paho-MQTT. Paho-MQTT It is the MQTT client library for Python.  
    You need to have pip installed before installing Paho-MQTT.

    `sudo pip install paho-mqtt`

    

    

    If you are running on windows, remove the `sudo` prefix from the above command. Instead, you need to run the command in the command prompt as an administrator.

    

    

*   You need to start the Entgra IoT Serve's broker, core, and analytics profiles.

    
    cd <IOTS_HOME>/bin  
    ------Linux/Mac OS/Solaris ----------
    ./broker.sh
    ./iot-server.sh
    ./analytics.sh

    -----Windows-----------
    broker.bat
    iot-server.bat
    analytics.bat
    





Let's get started!

## Creating the fire alarm device type

1.  Sign in to the Entgra Device Management Console.

    

    

    If you are an administrator, sign into the device management console using `admin` as the username and `admin` as the password. If you want to register with Entgra IoT Server, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#registering-with-entgra-iot-server).


    

    

2.  Click ![image](2.png) > **DEVICE TYPES > CREATE DEVICE TYPE**.

    

    

    Entgra IoT Server has the Android, Windows, Raspberry Pi, Arduino, Android Sense and iOS sample device types by default. If you have not run the `<IOTS_HOME>/samples/device-plugins-deployer.xml` file, you will only have the Android, Android Sense, and Windows device type by default.

    

    

3.  Enter the following details to add a new device type and click **Add Device Type**.

    

    

    Make sure to enter the values given below. Else, you are not able to complete the tutorial successfully.

    

    

    <table style="width: 37.4429%;">
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Name</th>
          <td>firealarm</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>This is a device used for testing.</td>
        </tr>
        <tr>
          <th>Push Notification Transport</th>
          <td>MQTT</td>
        </tr>
        <tr>
          <th>Feature name</th>
          <td>msg</td>
        </tr>
        <tr>
          <th>Feature code</th>
          <td>msg</td>
        </tr>
        <tr>
          <th>Feature description</th>
          <td>Message from the server to the device.</td>
        </tr>
      </tbody>
    </table>

    

    

    For more information about each field, see [Creating a New Device Type via the Device Management Console]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#creating-a-new-device-type-via-the-device-management-console).

    

    

    ![image](352819214.png)

4.  Create the event stream to gather the sensor readings or data from the device, and click **Add**.  
    Enter the following values:

    

    

    Make sure to enter the values given below. Else, you are not able to complete the tutorial successfully.

    

    

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Transport</th>
          <td>MQTT</td>
        </tr>
        <tr>
          <th>Event name</th>
          <td>temperature</td>
        </tr>
        <tr>
          <th>Event data type</th>
          <td>INT</td>
        </tr>
      </tbody>
    </table>

    

    

    For more information about each field, see [Creating a New Device Type via the Device Management Console]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#creating-a-new-device-type-via-the-device-management-console).

    

    

    ![image](352819202.png)

## Enrolling a fire alarm

Follow the steps given below to enroll a fire alarm for the device type you just created:

1.  On the Enroll Device page, click on the fire alarm device type.  
    ![image](352819208.png)
2.  Click **+ Create Device**.
3.  Enter the details to create the device and click **Create Device**.

    

    

    Make sure to enter the values given below. Else, you are not able to complete the tutorial successfully.

    

    

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Name</th>
          <td>Alarm1</td>
        </tr>
        <tr>
          <th>Device Identifier</th>
          <td>123456</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>Enrolling a device for testing.</td>
        </tr>
      </tbody>
    </table>

    

    

    For more information about each field, see [Creating a New Device Type via the Device Management Console]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#creating-a-new-device-type-via-the-device-management-console).

    

    

    ![image](352819232.png)

    The `123456.json` file downloads automatically.

4.  Download the [`SampleApp.zip`](https://github.com/wso2/samples-iots/releases) file to a preferred location. This directory is referred to as `<SAMPLE_APP>` through out this document. 

5.  Rename the downloaded `123456.json` file to `config.json` and copy it to the `<SAMPLE_APP>/Configuration` directory.

6.  Navigate to the `<SAMPLE_APP>` directory via the terminal and run the python script.

    
    cd <SAMPLE_APP>
    python SimpleClient.py
   

    The sample starts.

    

    

    If the sample successfully connects to Entgra IoT Server, you will see the `Connected with result code 0` message in the terminal and the app starts pushing data to Entgra IoT Server.  
    ![image](352819244.png)

    

    

## Viewing the data gathered from the fire alarm

Click ![image](352819238.png)  **> DEVICE MANAGEMENT**, and click on the fire alarm device you just 
enrolled to view the real time and historical data that was pushed from the fire alarm to Entgra IoT Server.

### Real time data

You see the real-time data pushed to Entgra IoT Server as shown below:

The current temperature is 46. 

![image](352819226.png)

### Historical data

Click **View Device Analytics** to view the historical temperature readings of the fire alarm.

![image](352819220.png)

## Sending a Message to the fire alarm

Follow the steps given below to send a message to the fire alarm. You configured the fire alarm device type to have the messaging feature when [creating the new fire alarm device type](https://entgra.atlassian.net/wiki/pages/resumedraft.action?draftId=2827880-message).

1.  Click the msg operation.  
    ![image](352819256.png)
2.  Enter the message you want to send, and click **Send to device**.  
    ![image](352819262.png)  
    Go to the terminal where you were running the sample. The following message is shown if the message was successfully delivered to the fire alarm.  
    ![image](352819250.png)





*   The access token generated to access the fire alarm expires in an hour. Therefore, you are not able to use the `SimpleClient.py` script after an hour.
*   To overcome this, you can use the `SampleClient2.py` script. Similarly, if you want to make changes to the current script and try many things with Entgra IoT Server.  
    ![image](352819268.png)


