---
bookCollapseSection: true
weight: 3
---
# iOS

You can enroll an iOS device with Entgra IoT Server with or without the iOS agent. Note that the device ring, send messages, and the get device location operations require the Entgra IoT Server iOS agent, else they don't work. 

## Enrolling DEP devices


Before you begin!



Make sure that the EMM administrator has set up Entgra IoT Server's Enterprise Mobility Manager (EMM) solution to enroll Device Enrollment Program (DEP) devices. For more information, see [Device Enrollment Program]({{< param doclink >}}using-entgra-iot-server/working-with-ios/device-enrollment-program/#device-enrollment-program).





*   All you need to do is start up the iOS device that was given to you by your organization and your device will be enrolled with Entgra EMM as a DEP device.
*   If your organization has configured the DEP profile to prompt for your username and password, you need to enter the username and password that is used within the organization.

## Enrolling with iOS agent


The following sections guide you through configuring iOS in Entgra IoT Server and enrolling an iOS device. This tutorial follows the sample scenario of the Mob X organization of which Chris (the system administrator) is configuring iOS with Entgra IoT Server for the organization and Alex (the device owner) is registering an iOS device. 



Before you begin



1.  Entgra IoT Server supports devices on iOS 7, 8, 9, and 10.
2.  Install [Oracle Java SE Development Kit (JDK)](http://java.sun.com/javase/downloads/index.jsp) version 1.8.* and set the `JAVA_HOME` environment variable. For more information on setting up `JAVA_HOME` on your OS, see [Installing the Product]({{< param doclink >}}using-entgra-iot-server/installation-guide/installing-the-product/).

3.  [Download Entgra IoT Server](https://entgra.io/) and unzip it.

    

    

    The downloaded Entgra IoT Server file is large. Therefore, when unzipping it the OS might extract it halfway through and stop. To avoid this, we recommend that you unzip the file via the terminal.  
    Example:

    `unzip wso2iot-3.2.0.zip`

    

    

4.  Start Entgra IoT Server core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

    
    cd <IOTS_HOME>/bin
    sh iot-server.sh
    

    The default port assigned for the core is 9443.

5.  In your browser, access the IoT Server Console by navigating to **`https://<IoT_HOST>:9443/devicemgt`** and log in using **admin** as the username and password.

6.  Fill in the following [iOS form](http://wso2.com/products/iot-server/csr/) via the WSO2 site. WSO2 will send you an email with the following information.

    *   The P2 repository with the iOS features, End User License Agreement (EULA) and the README.txt. 

    *   The iOS agent source code. You can use this if you want to customize the iOS agent application in Entgra IoT Server.

7.  Get your certificate signed by Apple. Register your organization with the  [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/) . Thereafter, follow the steps mentioned in  [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html) . 





### Configuring iOS support in Entgra IoT Server





For the sake of simplicity in this tutorial, we are not setting up the Entgra IoT Server iOS agent on your device. Therefore, we will not walk through the iOS client configurations. Note that the 'device ringing', 'sending messages', and 'getting the device location' operations require the Entgra IoT Server iOS agent, so those operations will not be available in this scenario.  

For more information on configuring Entgra IoT Server for iOS, including iOS client configurations, see the [iOS configurations guide]({{< param doclink >}}using-entgra-iot-server/working-with-ios/).






*   ### Configuring Entgra IoT Server to install iOS applications

    

    

    

    

    The download URL for Entgra IoT Server is configured for HTTP by default. Although you can install Android mobile applications using this default configuration, to install iOS applications, you need to configure it for HTTPS as it's required for the iOS MDM protocol behavior.

    Follow the steps given below to configure Entgra IoT Server to install iOS mobile applications:

    1.  Open the `<IoT_HOME>/conf/app-manager.xml` file.
    2.  Add `%https%` as the value for the `AppDownloadURLHost` property.

        `<Config name="AppDownloadURLHost">%https%</Config>`

        

        

        To test Entgra IoT Server App management features on Android devices, please use one of the following options:

        *   Change the value of the `AppDownloadURLHost` property back to HTTP 
        *   Continue using HTTPS to install applications on Android devices by [Generating a BKS 
        File for Android]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices//#generating-a-bks-file-for-android)
        

        

    Chris has now done the configurations needed to allow users to register and monitor their devices with Entgra IoT Server. Chris then proceeds to configure iOS support in IoT Server.

    

    

    

*   ### Configuring the IP or hostname

    

    

    

    

    This script automatically configures the IP and creates the required SSL certificates for the IP or hostname. This method is recommended because manually configuring the IP address includes many steps and if you miss out on a step you will run into errors.

    1.  Navigate to the `<IOTS_HOME>/scripts` directory.
    2.  Run the `change-ip` script.  
        Example: Running the script on a Mac or Linux OS.

        **Tip:** The script will find and replace the IP address given in argument1 (`localhost`) with the IP address given as argument2 (`10.10.10.14`), in the necessary configuration files. 

        `./change-ip.sh`

    1.  Change the current IP address of the IoT Server core, broker, and analytics profile.

    2.  Enter the values for IoT Server core SSL certificate.

        

        

        

        Enter the requested information when prompted.

        <table>
          <thead>
            <tr>
              <th>Field</th>
              <th>Usage/Purpose</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Country</td>
              <td>The name of your country. Enter the two digit code for your country.</td>
            </tr>
            <tr>
              <td>State</td>
              <td>The state your organization is at.</td>
            </tr>
            <tr>
              <td>Location</td>
              <td>The city your organization is located at.</td>
            </tr>
            <tr>
              <td>
                <p>Organization</p>
              </td>
              <td>
                <p>The name of your organization. For this scenario, we entered wso2.</p>
              </td>
            </tr>
            <tr>
              <td>Organization Unit</td>
              <td>Defined the Team ID as the organization unit.</td>
            </tr>
            <tr>
              <td>
                <p>Email</p>
              </td>
              <td>
                <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a href="mailto:chris@wso2.com" class="external-link" rel="nofollow">chris@wso2.com</a>&nbsp;as the email.</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Commonname</p>
              </td>
              <td>
                <p>Fully qualified domain name of your server.</p>
              </td>
            </tr>
          </tbody>
        </table>

        ![image](352818400.png)

        

        

    

    

    If you want to configure the steps manually, see [Configuring the IP or hostname manually]({{< param doclink >}}using-entgra-iot-server/product-administration/configuring-ip-or-hostname/) and if you want to change the default ports, see [Changing the Default Ports]({{< param doclink >}}using-entgra-iot-server/product-administration/changing-the-default-ports/#changing-the-default-ports).


    

    

    

    

    

*   ### Installing the iOS features

    

    

    

    

    

    

    If you are facing issues while configuring Entgra IoT Server for iOS in your testing environment, [contact us](https://entgra.io/contact) and we will help you out.

    

    

    After obtaining a signed CSR, the P2 repository, and the other files via email, Chris can install the specific features in the P2 repository that are required to enable iOS by following the instructions given below. 

    1.  Download and extract the ios-feature-deployer that you received via email. 

    2.  Copy the ios-feature-deployer folder to the `<IOTS_HOME>` directory.  
    3.  Navigate inside the ios-feature-deployer folder on the terminal and execute the following command to install the iOS features to WSO2 IoT.  Agree to the End User License Agreement (EULA) to complete the iOS configurations.

        
        cd ios-feature-deployer 
        mvn clean install -f ios-feature-deployer.xml
        

        

        Tip by Chris

        

        **Tip:** To verify successful installation of the iOS features, do the following:

        Open the [`bundles.info`](http://bundles.info/) file found in the `<IOTS-HOME>/wso2/components/default/configuration/org.eclipse.equinox.simpleconfigurator` directory and verify if the following properties have changed from `false` to **`true`** .

        
        org.wso2.carbon.device.mgt.ios.api.utils,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.api.utils_3.0.5.SNAPSHOT.jar,4,true

        org.wso2.carbon.device.mgt.ios.apns,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.apns_3.0.5.SNAPSHOT.jar,4,true

        org.wso2.carbon.device.mgt.ios.core,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.core_3.0.5.SNAPSHOT.jar,4,true

        org.wso2.carbon.device.mgt.ios.payload,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.payload_3.0.5.SNAPSHOT.jar,4,true

        org.wso2.carbon.device.mgt.ios.plugin,3.0.5.SNAPSHOT,../plugins/org.wso2.carbon.device.mgt.ios.plugin_3.0.5.SNAPSHOT.jar,4,true
        

        

        

        Once the `ios-feature-deployer` script has finished running, you can see the `<IOTS_HOME>/ios-configurator` directory that was created.

    4.  Navigate inside the `<IOTS_HOME>/ios-configurator` directory and run the `ios.sh` script. 

        
        cd ios-configurator
        sh ios.sh
        

    5.  Enter the requested information when prompted. Be sure to provide the same information when configuring the [iOS platform configurations]({{< param doclink >}}using-entgra-iot-server/working-with-ios/).

        <table>
          <colgroup></colgroup>
          <tbody>
            <tr>
              <th>Field</th>
              <th>Usage/Purpose</th>
            </tr>
            <tr>
              <td>Country</td>
              <td>The name of your country. Enter the two digit code for your country.</td>
            </tr>
            <tr>
              <td>State</td>
              <td>The state your organization is at.</td>
            </tr>
            <tr>
              <td>Location</td>
              <td>The city your organization is located at.</td>
            </tr>
            <tr>
              <td>
                <p>Organization</p>
              </td>
              <td>
                <p>The name of your organization. For this scenario, we entered MobX.</p>
              </td>
            </tr>
            <tr>
              <td>Organization Unit</td>
              <td>Defined the Team ID as the organization unit.</td>
            </tr>
            <tr>
              <td>
                <p>Email</p>
              </td>
              <td>
                <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;<a class="external-link" href="mailto:chris@mobx.com" rel="nofollow">chris@mobx.com</a>&nbsp;as the email.</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Commonname</p>
              </td>
              <td>
                <p>Fully qualified domain name of your server.</p>
              </td>
            </tr>
          </tbody>
        </table>

        ![image](352818540.png)  
        This part of the script creates a key pair, generates a signature, and signs the key using the signature.

    6.  Uncomment the `APNSBasedPushNotificationProvider` that is under the `PushNotificationProviders` configuration in the `<IOTS_HOME>/conf/cdm-config.xml` file.

        
        <PushNotificationProviders>
            <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.mqtt.MQTTBasedPushNotificationProvider</Provider>
            <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.xmpp.XMPPBasedPushNotificationProvider</Provider>
            <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.gcm.GCMBasedPushNotificationProvider</Provider>
            <Provider>org.wso2.carbon.device.mgt.mobile.impl.ios.apns.APNSBasedPushNotificationProvider</Provider>
        </PushNotificationProviders>
        

    

    

    Make sure the `<EnabledUpdateApi>` parameter in the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file is set to `false` if you need to restart the server many times after configuring the IP.  This configuration is enabled when you run the `ios.sh` script.

    By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.

    

    

    

    

    

*   ### Generating an MDM APNS certificate

    

    

    

    

    Except for a few operations (ring, message and get location) that are performed by the Entgra IoT Server agent, all the other device operations, and policies are applied on an iOS device via it's operating system. Therefore, for the Entgra IoT Server server to communicate with the operating system of the device, you need to generate the MDM APNS certificate.

    The MDM APNS certificate will be referred to as the MDM certificate in the IoT Server Console.

    1.  Go to ** [https://appleid.apple.com/account#!&page=create](https://appleid.apple.com/account#!&page=create) **and get an Apple ID, if you do not have one already. 
    2.  Go to the Apple Push Certificate Portal at** [https://identity.apple.com/pushcert/](https://identity.apple.com/pushcert/) **and log in with your customer account details. You do not need an enterprise account for this. Your Apple ID is sufficient.
    3.  Click **Create Certificate** and agree to the terms and conditions.
    4.  Upload the encoded `.plist` file got after signing your certificate.

        

        

        Didn't get your certificate signed by Apple? Register your organization with the [Apple Developer Enterprise Program](https://developer.apple.com/programs/enterprise/) and follow the steps mentioned in [MDM Vendor CSR Signing Overview](https://developer.apple.com/library/content/documentation/Miscellaneous/Reference/MobileDeviceManagementProtocolRef/7-MDMVendorCSRSigningOverview/MDMVendorCSRSigningOverview.html). 

        

        

    5.  Download the generated MDM signing certificate, which is a certificate for third-party servers provided by Apple, and rename it to `MDM_Certificate`.

    6.  Get the `USERID` (TOPIC ID) from the MDM signing certificate (`MDM_Certificate.pem`), as it will be used later in the configuration.   
        You can decode the MDM signing certificate to obtain the `USERID` by executing the following command:

        `openssl x509 -in MDM_Certificate.pem -text -noout`

        

        

        The TOPIC ID is assigned to the UID property.  
        Example: `UID=com.apple.mgmt.External.0dbe5637-8f16-4d9f-bac4-e18cac2f7d4e`

        

        

    7.  Remove the password/pass phrase from your private key file (e.g.,`customerPrivateKey.pem`).

        `openssl rsa -in customerPrivateKey.pem -out customerKey.pem `

    8.  Merge the customer key file that was derived in the latter step with the MDM signing certificate to generate the MDM Apple Push Notification Service (APNS) Certificate. In this example, Chris merges the `customerKey.pem` file with the `MDM_Certificate.pem` file to generate the `MDM_APNSCert.pem` file.

        

        

        **Tip**: Before you merge the `customerKey.pem` file and the `MDM_Certificate.pem` file, make sure both files are in the same directory.

        

        

        `cat MDM_Certificate.pem customerKey.pem > MDM_APNSCert.pem`

    9.  Open the APNS Certificate (`MDM_APNSCert.pem`) and add a line break between the content of the two files. For example, if your content looks as "`-----END CERTIFICATE----------BEGIN RSA PRIVATE KEY-----`", add a line break after 5 dashes so that the content looks as follows:

        
        -----END CERTIFICATE-----
        -----BEGIN RSA PRIVATE KEY-----
        

    10.  Convert the `MDM_APNSCert.pem` fileto `MDM_APNSCert.pfx` file. You need a password for this, which you will need again when configuring the iOS platform configurations.  

        `openssl pkcs12 -export -out MDM_APNSCert.pfx -inkey customerPrivateKey.pem -in MDM_APNSCert.pem`

    

    

    

*   ### Generating the APNS certificate

    

    

    

    

    This section guides you on how to generate an APNS certificate.

    

    Why is this step required?

    

    You can register an iOS device with Entgra IoT Server, with or without the Entgra IoT Server's iOS agent. This certificate is required to carry out operations on the device that need to be triggered via the iOS agent, such as ringing the device, getting the device location, and sending notifications or messages to the device. Therefore, if you are not installing the iOS agent on your devices, you don't need this certificate.

    Further, in iOS, the server passes messages to the client via the [Apple Push Notification Service (APNS)]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-notification-method/). When doing so in order to establish a secure connection between Entgra IoT Server and the APNS server, a client SSL certificate needs to be generated and downloaded from Apple Inc. This APNS certificate is used to send an awake message to the iOS agent application.


    

    

    

    Prerequisites

    

    *   You have to be enrolled in the  [Apple Developer Program](https://developer.apple.com/programs/) as an individual or organization before starting the iOS server configurations.
    *   A valid distribution certificates that you obtained from Apple.

    

    

    Follow the steps given below:

    1.  Clone the [`emm-agent-ios`](https://github.com/wso2/emm-agent-ios) repository to a preferred location.

        `git clone https://github.com/wso2/emm-agent-ios`

    2.  Open the `emm-agent-ios` from X-Code and follow the subsequent steps:
        1.  Change the `org.wso2.carbon.emm.ios.agent` Bundle Identifier so that it matches your organization details.  
            Example:`org.<ORGANIZATION_NAME>.emm.ios.agent`
        2.  Select the development team, provisioning profile and sign certificate from Xcode.

            

            

            If you are unsure of how to select the development team, or add the provisioning profile or sign the certificate via Xcode, see the blog post on [How to export “in-house” developed iOS app as an enterprise application](https://medium.com/@mharindu/how-to-export-in-house-developed-ios-app-as-an-enterprise-application-dc087bdd64c3#.ptkvra6o3).

            

            

    3.  [Log in to the Apple Developer ](https://idmsa.apple.com/IDMSWebAuth/login?appIdKey=891bd3417a7776362562d2197f89480a8547b108fd934911bcbea0110d07f757&path=%2Faccount%2F&rv=1)program and follow the subsequent steps:

        

        

        Before you follow the steps, confirm that your machine is connected to the Internet and that Xcode has a valid developer account.

        

        

        1.  Navigate to **Certificates, IDs & Profiles** that is under **Identifiers**.
        2.  Click **App IDs** and see if the Bundle ID that you defined under Xcode is listed here.  
            ![image](352824045.png)
    4.  Click the Bundle ID, and click **Edit**.  
        ![image](352824039.png)
    5.  Creating an APNs SSL certificate:
        1.  Select Push Notifications to enable the setting.  
            ![image](352824016.png)  
            Once push notification is enabled, you are able to generate the development and production certificates.
        2.  To try out the create certificate use case, let's create a development SSL certificate.  
            Please note that the development SSL certificate is created only as an example. You can create a production SSL certificate if you have registered with the Apple Developer Program as an Organization.  

            Click **Create Certificate** that is under Development SSL Certificate.  
            ![image](352824010.png)
    6.  Creating a CSR file using the keychain access tool in the Mac OS:
        1.  Launch the keychain access application.
        2.  On the menu bar click **KeyChain Access > Certificate Assistant > Request a Certificate from Certificate Authority**.  
            ![image](352824028.png)
        3.  Define the email address, common name, select **Saved to disk**, and click Continue.  
            Example:  
            ![image](352824022.png)
    7.  Go back to the Apple Developer Portal, upload the generated certificate, and click **Continue**.  
        ![image](352824004.png)
    8.  Exporting the certificate to the `pfx` format.
        1.  Click **Download** to download the file.  
            ![image](352823998.png)
        2.  Double-click the downloaded file to open it with the Keychain access tool.
        3.  Right-click the certificate and select export.  

        4.  Define the location where you wish to save the file and set a password for the exported file when prompted.
        5.  Rename the `p12` extensionof the file to `pfx`.

   

*   ### Configuring the iOS platform

    

    

    

    

    Multiple tenants can use Entgra IoT Server while maintaining tenant-based isolation. The iOS platform configurations page enables tenants to customize the iOS settings based on their own requirements.

    Chris proceeds to configure the iOS platform configurations by adding the generated MDM APNS certificate.

    1.  Log in to the device management console at** `https://<IoT_HOST>:9443/devicemgt` **using **admin** as the username and password if you haven't previously logged in.

    2.  Click ![image](2.png) **> CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS > iOS 
    Configurations** and fill in the form.

        

        

        Use the generated MDM-APNS certificate for both the MDM certificate and the generated APNS certificate for the APNS certificate.

        

        

        *   `MDM Certificate Password`: Give the same password you gave when converting the `MDM_APNS` certificate from the `pem` tothe `pfx` format. 
        *   `MDM Certificate Topic ID`: Give the topic ID of the certificate. If you are unsure of the topic ID, refer [step 6 under generating the MDM APNS certificate](about:blank#EnrollingWiththeiOSAgent-MDM_APNS_TopicID).
        *   `APNS Certificate Password`: Give the same [password](about:blank#EnrollingWiththeiOSAgent-APNS_Password) you gave when converting the `APNS` certificate to the `pfx` format. 

        ![image](352818389.png)

        

        

        **Tip**: To learn more about each platform setting, hover your mouse pointer over the help tip.

        

        

    

    

    





Make sure to configure the `<EnabledUpdateApi>` property in the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file back to  `false`  if you need to restart the server again after the configuring the IP.   

By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.





## Creating users and a sample policy

Follow the steps given below to create two users, Alex and Chris, and a configured passcode policy. This will make it easy for you to try out the IoT Server quick start guide.



Tip by Chris



Have you run the sample script previously when trying out the [Android quick start guide]({{< param doclink >}}quick-start-guide/android-device/)? If yes, delete the two users alex and chris, the role iotMobileUser and the sample policies that were created for Android, iOS, and Windows. 






1.  Navigate to the `mobile-qsg` directory located in the `<IOTS_HOME>/samples` directory on the terminal window. 
2.  Run the `mobile-qsg.sh` script to configure the iOS server settings in Entgra IoT Server. 

    
    cd <IoT_PRODUCT_HOME>/samples/mobile-qsg
    sh mobile-qsg.sh
    

    

    

    When you add a new user to Entgra IoT Server, the user receives an email with the username and password that needs to be used to log in to Entgra IoT Server. In this quick start guide, we have not configured Entgra IoT Server to send emails. Therefore you will get an error in the terminal where the core profile is running. If you don't want to see this error, you need to configure the email settings. For more information, see [Configuring the Email Settings]({{< param doclink >}}using-entgra-iot-server/product-administration/configure-email-settings/).


    

    

    

    
    [2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:chris@mobx.com URL.
    [2017-07-27 02:47:14,527] [IoT-Core] ERROR - {org.apache.axis2.description.ClientUtils} The system cannot infer the transport information from the mailto:alex@example.com URL.
    [2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'chris@mobx.com'
    org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:chris@mobx.com URL.
    	at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    	at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    	at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    	at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    	at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    	at java.lang.Thread.run(Thread.java:748)
    [2017-07-27 02:47:14,531] [IoT-Core] ERROR - {org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl} Error occurred while delivering the message, subject: 'You have successfully been registered in WSO2 IoT', to: 'alex@example.com'
    org.apache.axis2.AxisFault: The system cannot infer the transport information from the mailto:alex@example.com URL.
    	at org.apache.axis2.description.ClientUtils.inferOutTransport(ClientUtils.java:81)
    	at org.apache.axis2.client.OperationClient.prepareMessageContext(OperationClient.java:288)
    	at org.apache.axis2.description.OutOnlyAxisOperationClient.executeImpl(OutOnlyAxisOperation.java:249)
    	at org.apache.axis2.client.OperationClient.execute(OperationClient.java:149)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:511)
    	at org.apache.axis2.client.ServiceClient.fireAndForget(ServiceClient.java:488)
    	at org.wso2.carbon.email.sender.core.service.EmailSenderServiceImpl$EmailSender.run(EmailSenderServiceImpl.java:117)
    	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
    	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
    	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
    	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
    	at java.lang.Thread.run(Thread.java:748)
    

    

    

    

    





Check out the Entgra IoT Server dashboard by signing in to the Entgra IoT Server console using **chris** as the username and **chrisadmin** as the password: **`https://<IoT_HOST>:9443/devicemgt`**  
You will then see the new **iotMobileUser** role, three new policies, and the two new users that were added using this script





## Enroll an iOS device

Follow the instructions given below to enroll an iOS device:

1.  Sign in to the Entgra IoT Server Device Management Console.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


    2.  Access the device management console.

        *   For access via HTTP:js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`


            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![image](352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![image](352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![image](352819647.png)

    

    

    

2.  Click **Enroll New Device** if you a device user. If you are administrator click **Add** under DEVICES.  
    ![image](352816972.png)
3.  Click **iOS** to enroll your device with Entgra IoT Server.  
    The IoT Server registration screen appears in the Safari browser. iOS devices need the root certificate to be downloaded to trust the server certificate. If the certificate is a self-signed SSL certificate, tap **Install IoT Server Certificate,** else go to [step 5](about:blank#EnrollingWiththeiOSAgent-Step).

    ![image](352818452.png)

4.  Tap **Install** as required to download the root certificate. The IoT Server registration screen appears.  
    ![image](352818481.png)

    

    Tip

    

    If you are using iOS 10 with a self-signed certificate, do the following to mark it as a trusted certificate. This is required in iOS 10 because there is a change in how self-signed CA certificates are trusted by iOS devices from iOS 10 onwards.

    1.  Open the **Certificate Trust Settings** found in **Settings>General>About**.
    2.  Enable full trust for the certificate related to Entgra IoT Server.

    

    

5.  Tap **Install IoT Server Agent.  
    ![image](352818446.png)**

    

    

    If you are using iOS 10 or above, you will get the **Cannot connect to <IP or host name>** error message.  
    Example:  
    ![](352818487.png)To overcome this, follow the steps given below: This is required in iOS 10 because there is a change in how self-signed CA certificates are trusted by iOS devices from iOS 10 onwards.

    1.  Open the **Settings > General > About > **Certificate Trust Settings**** .
    2.  Enable full trust for the certificate related to Entgra IoT Server.

    3.  Repeat step 5 again and Install the Entgra IoT Server agent.

    

    

6.  After the IoT Server agent is installed, click on the Entgra IoT Server app on your device to start the registration.

    

    Getting an Untrusted Enterprise Developer message?

    

    When you open the WSO2 IoTS app for the first time, you may see the **Untrusted Enterprise Developer** message.

    

    

    

    Follow the steps given below to establish the trust for the IoTS iOS application:

    1.  Click **Cancel** to dismiss the Untrusted Enterprise Developer message.  
        ![image](352818524.png)
    2.  Tap **Settings** > **General** > **Profiles** or **Profiles & Device Management**.  
        Example:  
        ![image](352818550.png)
    3.  Tap the WSO2 Inc profile that is under **Enterprise App**.
    4.  Tap **Trust "WSO2 Inc"**.

        

        

        An Internet connection is required to verify the app developer's certificate when establishing trust. If you are behind a firewall, make sure it's configured to allow connections to `https://ppq.apple.com`. If you are not connected to the Internet when you trust an app, the device displays **Not Verified** instead. In order to use the app, you need to connect to the Internet, and tap the **Verify App** button.

        

        

        ![image](352818475.png)

    5.  Tap **Trust** to confirm that you trust the developer.  
        ![image](352818344.png)The developer WSO2 Inc. remains trusted until you use **Delete App** 
        to remove all apps from the developer.  
        ![image](352818469.png)

    

    

    

    

    ![image](352818378.png)

7.  Tap **Allow** to let the WSO2 Agent access your device location.

8.  Enter your server address, based on your environment, in the text box provided and tap Enroll Device. 

    *   Developer Environment - Enter your entire server address as your server address.  
        Example: `https://10.10.10.123`
    *   Deployment Environment - Enter your domain as your server address.![image](352818383.png)
9.  Enter your details:  

    *   **Domain** - Enter the domain only if the server is hosted with multi-tenant support.
    *   **Username** - Enter the username that you provided when [registering with WSO2 IoTS]({{< param doclink >}}installation-guide/Running-the-Product.html).

    *   **Password** - Enter the password that you provided when [registering with WSO2 IoTS]({{< param doclink >}}installation-guide/Running-the-Product.html).


    

    

    By default, WSO2 IoTS only supports the BYOD registration process for the iOS platform.

    

    

    ![image](352818508.png)

10.  Tap ****Login. ****
11.  Read the End User License Agreement (EULA) and tap **I accept the terms**.  
    ![image](352818308.png)

12.  Tap **Install** where ever required to install the WSO2 Profile Service.  
    ![image](352818366.png)

    A warning message appears to indicate that by installing the profile, WSO2 IoTS has access to remotely manage the iOS device. 

13.  Tap **Install**.

    ![image](352818372.png)

14.  Tap **Trust** to confirm that you are aware of the device being remotely managed by installing the profile.

    ![image](352818394.png)

15.  After the profile is installed, click **Done**.  
    Upon the successful registration, the following confirmation appears.

    ![image](352818355.png)

    Thereafter, your browser automatically closes and after 1 or 2 seconds, the native iOS Agent is loaded.

### Try it out

You can view device details and carry out operations on your device, after successfully enrolling it with WSO2 IoTS. Let's take a look at how you can do it:

1.  Navigate to the WSO2 IoTS Device Management page to view all the created devices.

    

    

    

    

    

    1.  [Sign in to the Device Management console]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-consoles).

    2.  Click the menu icon. ![image](2.png)
    3.  Click **Device Management**.  
        ![image](352818268.png)

    

    

    

    

2.  Click the view icon on the iOS device you enrolled.

    This directs you to the device details page where you can view the device information and try out operations on a device.

    

    

    

    

    The details of each registered devices will be shown on separate pages within the **Device** page.

    <table>
      <tbody>
        <tr>
          <th><br></th>
          <th>Description</th>
        </tr>
        <tr>
          <td>Device Details</td>
          <td>
            <p>The following device information will be retrieved automatically when you register with WSO2 IoTS.</p>
            
              <table>
                <tbody>
                  <tr>
                    <td>Device</td>
                    <td>This shows the name the user has given his/her device (e.g., Kim's iPhone).</td>
                  </tr>
                  <tr>
                    <td>Model</td>
                    <td>The type of device will be stated (e.g., iPhone, iPad)</td>
                  </tr>
                  <tr>
                    <td>IMEI</td>
                    <td>The International Mobile Station Equipment Identity (IMEI) number of all GSM devices. This is not applicable to devices such as iPads which work without a SIM.</td>
                  </tr>
                  <tr>
                    <td>Status</td>
                    <td>Indicates if the device is active, inactive or removed from WSO2 IoTS.</td>
                  </tr>
                </tbody>
              </table>
            
          </td>
        </tr>
        <tr>
          <td>Policy Compliance</td>
          <td>If your device does not comply to certain criteria in the enforced policy, the aspects in which your device is none compliant will be highlighted under this section.</td>
        </tr>
        <tr>
          <td>Device Location</td>
          <td>Provide the location of your device.</td>
        </tr>
        <tr>
          <td>Installed Application</td>
          <td>A list of all the applications that have been installed on your device and the memory consumption by each application will be listed under this section.</td>
        </tr>
        <tr>
          <td>Operation Log</td>
          <td>A list of all the operations that have been carried out by you and its current status</td>
        </tr>
      </tbody>
    </table>

    

    

    

    

    

    

    

    Click on the operation you wish to carry out and provide the required details (if requested) to apply the selected operation on your device.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Operation</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-DeviceLock">Device Lock</h5>
          </td>
          <td>Ability to lock a device via WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-Location">Location</h5>
          </td>
          <td>Ability to receive the location of the device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-MuteDevice">Mute Device</h5>
          </td>
          <td>Ability to&nbsp;enable the silent profile on a device&nbsp;via WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-EnterpriseWipe">Enterprise Wipe</h5>
          </td>
          <td>Ability to unregister a device from WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-ClearPassword">Clear Password</h5>
          </td>
          <td>Ability to remove a device lock via WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-ChangeLock-Code">Change Lock-Code</h5>
          </td>
          <td>Ability to change the provided passcode or lock-code via WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-Ring">Ring</h5>
          </td>
          <td>Ability to ring the device via WSO2 IoTS.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-Message">Message</h5>
          </td>
          <td>Ability to send a message to the device via WSO2 IoTS. The WSO2 IoTS admin can use<br>this device operation to send group messages or even private messages to the WSO2 IoTS users.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-APNConfigurations">APN Configurations</h5>
          </td>
          <td>Ability to set the APN configurations on a user's device. In iOS, the Entgra IoT Server does not detect whether a device (i.e., iPad) has only Wifi, or whether the device has 3G and Wifi. If there is only&nbsp;<br>Wifi, the APN configurations can not be pushed, and a policy violation occurs.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-Googlecalendar">Google calendar</h5>
          </td>
          <td>Ability to set the google calendar configurations on the WSO2 IoTS user's device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="EnrollingWiththeiOSAgent-LDAP">LDAP</h5>
          </td>
          <td>Ability to set the LDAP account configurations on the WSO2 IoTS user's device.</td>
        </tr>
      </tbody>
    </table>

    

    

    

## Unregistering and uninstalling the iOS agent

Want to unregister your device from Entgra IoT Server and uninstall the iOS agent that's on your device? Follow the steps given below:

1.  Open the Entgra iOS agent application and tap unregister.
2.  Next, go back, tap and hold down the icon of the iOS agent app. After a few seconds, all the apps in your iPhone will start wiggling and a tiny "X" icon will appear beside them. Note that the "X" icon will not appear beside the apps that came with the device.
3.  Press the "X" icon that appears beside the iOS agent app. A message-box will appear.
4.  Tap **Delete **to delete the iOS agent app.
5.  Navigate to **Settings** > **General** > **Profiles **or** Profiles & Device Management.**
6.  Tap the configuration profile you installed when enrolling the device and tap **Delete Profile**.

Now you have successfully unregistered your device from Entgra IoT Server, removed the profiles that were added when the iOS agent was installed, and uninstalled the WSO2 Device management application.
