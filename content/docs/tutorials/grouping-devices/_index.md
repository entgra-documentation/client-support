# Grouping Devices

**In this tutorial**, you add your enrolled device or devices to a group. Grouping allows you to monitor and view device data of many devices in one go. This way you can view any abnormal behavior of the devices at a glance and take necessary actions to prevent it.

For example, sharing the devices in your lobby with your technician. For this purpose, you can create a group named lobby, add the devices in the lobby to the created group, and share the group with the technician. Now the technician is able to view the devices in the group, analyze the data gathered by the devices and come to a conclusion on the devices that need attention.





By default, Entgra IoT Server has a group created named Bring Your Own Device (BYOD). It groups all the devices that are owned by device users.





Let's get started!

* * *

## Add a device group

Start off by creating a new device group in Entgra IoT Server:

1.  Click **Add** under GROUPS to add a new group.  
    ![image](352819017.png)

2.  Provide the required details and click **Add.**

    *   **Group Name**: Type the group name.

    *   **Description**: Type a short description for the group.

        ![image](352819011.png)

* * *

## Add devices to the group

Follow the steps given below to add the device/s to the group that you created:

1.  Click **View** under GROUPS.  
    ![image](352818963.png)
2.  Click on the group you want to add the devices.
3.  Click **Assign from My Devices**.  
    ![image](352819005.png)
4.  Click **Select** to select the devices you need to add to a group and click **Add To Group**.
5.  Select the group from the drop-down list and click **Add device to group**.  
    ![image](352818999.png)





The groups that appear in the drop down list are the groups you create using [add a device group]({{< param doclink >}}tutorials/grouping-devices/#add-a-device-group).









To confirm that the devices were added to the group, go to the group management page, click on the device group, and check if the devices were added.





* * *

## Sharing Groups with User Roles

You need to share the device group you created with other users so that they can access the device type too. 

Follows the steps given below:

1.  Sign in to the Entgra IoTS device [management console]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/).


2.  Click **View** under GROUPS.  
    ![image](352818981.png)
3.  Click the share icon on the group you want to share.  
    ![image](352818993.png)
4.  Share the groups with user roles:  

    *   Share the group with existing user roles by entering the role name or names and clicking **Share**.
    *   Need to create a new role and share the group? Click **New Role** and [create the new role]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#adding-a-role-and-permissions). Next, navigate to the group management page, click the share icon, enter the name of the role you just created, and click **Share**.

    *   Want to share the group with more roles but want them to be merged as a new role? Enter the names of the roles you want to share the group, click **New Role from Selection**, and enter the name of the new role you are creating. Click **Yes** if you want to [add new users to the role]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management) you just created or click **No** to finish sharing the group.

        

        

        The permissions of the roles you selected will be merged to create the permissions for the new role.

        

        

        ![image](352818975.png)
5.  If you need to add more user/s to the selected roles, select **Yes** in the confirmation message that appears and you are directed to the [User Management]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/) screen. Else select **No** and you are done with sharing the group with the selected user roles. 

* * *

## Update group details

Follow the steps given below, to update the group name or description.

1.  Click **View** under GROUPS.  
    ![image](352818963.png)
2.  Click the ![image](2.png) icon on your device group.
3.  Update the details and click **Update**.  
    ![image](352818969.png)

* * *

## Remove a group

Follow the steps given below, to delete a group from Entgra IoT Server.

1.  Click **View** under GROUPS.  
    ![image](352818963.png)
2.  Click the ![image](2.png) icon.
3.  Click **Yes**, to confirm that you want to delete the group.  
    ![image](352818957.png)
