# Windows


Entgra IoT Server supports devices, such as desktop (only for Windows 10), mobile and notebook devices, running on Windows 8.1 and 10. The following subsections explain how you can work with the Windows device type:







*   Windows 10, version 1511 - compatible with Windows mobile & PC
*   Windows 10, version 1607 - compatible with Windows mobile & PC
*   Windows 10, version 1703 - compatible with Windows  PC









Currently, Windows mobile device management only works in the super tenant mode. Therefore, when you log in to Entgra IoT Server you need to get your Entgra IoT Server administrator's assistance.









Before you start, make sure that your system administrator does the following configurations to configure the Windows device type in Entgra IoT Server.

1.  [Windows Server Configurations]({{< param doclink >}}using-entgra-iot-server/working-with-windows-devices/windows-serever-configurations/).
2.  [Windows Platform Configurations]({{< param doclink >}}using-entgra-iot-server/working-with-windows-devices/windows-platform-configurations/).






## Enroll a Windows device

Follow the instructions given below to enroll a Windows device:

## Try it out

You can view device details and carry out operations on your device, after successfully enrolling it with Entgra IoT Server. Let's take a look at how you can do it:

1.  Navigate to the Device Management page to view all the created devices.

    

    

    

    

    

    1.  [Sign in to the Device Management console]({{< param doclink >}}using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-consoles).
    2.  Click the ![image](2.png) icon.
    3.  Click **Device Management**.  

    

    

    

    

2.  Click the view icon on the Windows device that you enrolled.  

    This directs you to the device details page where you can view the device information and try out operations on a device.  
    Example: The device details page when you enroll a Windows 10 device.  
    ![image](352818877.png)

    

    

    

    

    The details of each registered device will be shown on separate pages within the **Device** page.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th><br></th>
          <th>Description</th>
        </tr>
        <tr>
          <td><strong>Device Details</strong></td>
          <td>
            <p>The following device information will be retrieved automatically when you register with IoT Server.</p>
            
              <table>
                <tbody>
                  <tr>
                    <td>Device</td>
                    <td>This shows the name the user has given his/her device (e.g., Kim's iPhone).</td>
                  </tr>
                  <tr>
                    <td>Model</td>
                    <td>The type of device is stated (e.g., iPhone, iPad).</td>
                  </tr>
                  <tr>
                    <td>Status</td>
                    <td>Indicates if the device is active, inactive or removed from Entgra IoT Server.</td>
                  </tr>
                  <tr>
                    <td>Owner</td>
                    <td>The owner of the device is stated (e.g., Kim).</td>
                  </tr>
                  <tr>
                    <td>Ownership</td>
                    <td>The ownership policy is stated (BYOD or COPE).</td>
                  </tr>
                  <tr>
                    <td>Last Update</td>
                    <td>Indicates the last date and time that the device was updated.</td>
                  </tr>
                </tbody>
              </table>
            
          </td>
        </tr>
        <tr>
          <td><strong>Policy Compliance</strong></td>
          <td>If your device does not comply with certain criteria in the enforced policy, the aspects in which your device is none compliant will be highlighted under this section.</td>
        </tr>
        <tr>
          <td><strong>Device Location</strong></td>
          <td>Provides the location of your device.</td>
        </tr>
        <tr>
          <td><strong>Operation Log</strong></td>
          <td>A list of all the operations that have been carried out by you and its current status</td>
        </tr>
      </tbody>
    </table>


    

    Click on the operation you wish to carry out and provide the required details (if requested) to apply the selected operation on your device.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Operation</th>
          <th>Description</th>
        </tr>
        <tr>
          <td><strong>Disenroll</strong></td>
          <td>Ability to unregister a device from Entgra IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-WipeData">Wipe Data</h5>
          </td>
          <td>Ability to carry out a factory reset operation on your own device via Entgra IoT Server. The user will have<br>to provide the PIN, which he/she entered when registering to Entgra IoT Server, to be able to wipe his/her device.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-Ring">Ring</h5>
          </td>
          <td>Ability to ring the device via Entgra IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-DeviceLock">Device Lock</h5>
          </td>
          <td>Ability to lock a device via Entgra IoT Server.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-DeviceLockReset">Device Lock Reset</h5>
          </td>
          <td>Ability to change the provided passcode or lock-code.</td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-Location">Location</h5>
          </td>
          <td>
            
              <p>Ability to retrieve the device location from Entgra IoT Server.</p>
              
                
                  <p>Supported only for Windows 10 devices.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            <h5 id="Windows-Reboot">Reboot</h5>
          </td>
          <td>
            
              <p>Ability to reboot the device via Entgra IoT Server.</p>
              
                
                  <p>Supported only for Windows 10 devices.</p>
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

    

    

    
